<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th class="">id</th>
            <th>Имя</th>
            <th>Сообщение</th>
            <th>Дата</th>
            <th>Отображение</th>
            <th>Категория</th>
            <th class="text-right">
                <a href="{{ route('admin.'.$key.'.create') }}" class="btn btn-primary">@lang('form.create')</a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
			<?php /** @var $item \App\Models\Testimonial */ ?>
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->getAttribute('name') }}</td>
                <td>{{ $item->getAttribute('message') }}</td>
                <td>
                    {{ $item->getAttribute('created_at') }}
                </td>
                <td>
                    {{ translateYesNo((int)$item->getAttribute('active')) }}
                </td>
                <td>
                    @if($item->category)
                        {!! linkEditIfAdmin($item->category, $item->category->getCategoryName()) !!}
                    @endif
                </td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
{{$list->render()}}
