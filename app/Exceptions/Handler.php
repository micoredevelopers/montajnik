<?php

namespace App\Exceptions;

use App\Mail\ExceptionOccurred;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;

class Handler extends ExceptionHandler
{
	/**
	 * A list of the exception types that are not reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		//
	];

	/**
	 * A list of the inputs that are never flashed for validation exceptions.
	 *
	 * @var array
	 */
	protected $dontFlash = [
		'password',
		'password_confirmation',
	];

	/**
	 * @param Exception $exception
	 * @return mixed|void
	 * @throws Exception
	 */
	public function report(Exception $exception)
	{
		parent::report($exception);

		if ($this->shouldReport($exception)) {
			$this->sendEmail($exception); // sends an email
		}
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 * @param Exception $exception
	 * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
	 */
	public function render($request, Exception $exception)
	{
		return parent::render($request, $exception);
	}


	protected function sendEmail(Exception $exception)
	{
		try {
			$e = FlattenException::create($exception);

			$handler = new SymfonyExceptionHandler();

			$html = $handler->getHtml($e);

			if (env('DEBUG_EMAIL') && !isLocalEnv()) {
				$excSha = sha1($html);
				$cacheKey = 'exception.' . $excSha;
				if (!\Cache::has($cacheKey)) {
					\Mail::send(new ExceptionOccurred($html));
					\Cache::set($cacheKey, true);
				}
			}
		} catch (Exception $ex) {

		}
	}
}
