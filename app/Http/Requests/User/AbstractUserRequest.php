<?php


namespace App\Http\Requests\User;


use App\Http\Requests\AbstractRequest;

abstract class AbstractUserRequest extends AbstractRequest
{
	/**
	 * @param array $except
	 * @return array
	 */
	public function getPersonalUserFields(array $except = []): array
	{
		$personal = [
			'name'                  => ['required', 'max:255'],
			'surname'               => ['required', 'max:255'],
			'contact_number'        => ['required', 'max:255'],
			'street_address'        => ['required', 'max:255'],
			'driver_license_number' => ['required', 'max:255'],
			'date_birth'            => ['required', 'max:255'],
		];
		return $this->exceptFields($personal, $except);
	}

	/**
	 * @param array $except
	 * @return array
	 */
	public function getPersonalUserFieldsWithCredentials(array $except = []): array
	{
		$fields = $this->getPersonalUserFields() + $this->getUserCredentials();
		return $this->exceptFields($fields, $except);
	}

	/**
	 * @param array $except
	 * @return array
	 */
	public function getUserCredentials(array $except = []): array
	{
		$personal = [
			'email'    => ['required', 'string', 'email', 'max:255', 'unique:users'],
			'password' => ['required', 'string', 'min:8', 'confirmed'],
		];

		return $this->exceptFields($personal, $except);
	}

	/**
	 * @param array $except
	 * @return array
	 */
	public function getCompanyUserFieldsAdditional(array $except = []): array
	{
		$company = [
			'company_name'              => ['required', 'max:255'],
			'street_address_two'        => ['max:255'],
			'city'                      => ['required', 'max:255'],
			'province'                  => ['required', 'max:255'],
			'postal_code'               => ['required', 'max:255'],
			'renter_phone'              => ['required', 'max:255'],
			'driver_license_issued'     => ['required', 'max:255'],
			'driver_license_expiration' => ['required', 'max:255'],
			'renter_phone_work'         => ['max:255'],
			'renter_phone_other'        => ['max:255'],
			'credit_card'               => ['max:255'],
		];

		return $this->exceptFields($company, $except);
	}

	/**
	 * @param array $except
	 * @return array
	 */
	public function getCompanyUserFields(array $except = []): array
	{
		$fields = $this->getPersonalUserFields() + $this->getCompanyUserFieldsAdditional();
		return $this->exceptFields($fields, $except);
	}

	/**
	 * @param array $except
	 * @return array
	 */
	public function getChangePasswordRules(array $except = []): array
	{
		$fields = [
			'password_new' => 'required|confirmed|min:6|different:password',
		] + $this->getCurrentPasswordRule();

		return $this->exceptFields($fields, $except);
	}

	/**
	 * @return array
	 */
	public function getCurrentPasswordRule():array
	{
		return [
			'password' => 'required|min:6',
		];
	}


}
