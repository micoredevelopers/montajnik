<?php


namespace App\Traits\Models\Scopes\Init;


use App\Scopes\WhereActiveScope;

trait InitPublicScopeActive
{
	public static function initScopesPublic()
	{
		if (!self::hasGlobalScope(new WhereActiveScope())) {
			self::addGlobalScope(new WhereActiveScope());
		}
	}

}
