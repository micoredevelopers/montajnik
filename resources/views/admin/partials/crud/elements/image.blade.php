@isset($edit)
	@php
		$table = $photo_table ?? $edit->getTable() ?? '';
		$name = $name ?? 'image';
        $keyDotted = remakeInputKeyDotted($name);
    	$attributeKey = getLastFromExploded($keyDotted);
		$image = $edit->getAttribute($attributeKey);
		$editable = $editable ?? true;
	@endphp
    <?php /** @var $edit \App\Models\Model */ ?>
	@if(storageFileExists($image))
		<div class="form-group">
			<label class=" control-label">{{ __('form.image.image-current') }}</label>
			<div class="col">
				<div class="image-actions" data-image-actions="">
					<a href="{{ checkImage(imgPathOriginal($image)) }}?{{ storageFilemtime($image) }}" class="fancy">
						<img
							src="{{ checkImage($image) }}?{{ storageFilemtime($image) }}"
							class="img-responsive {{ !$editable ?: 'deleteable croppable' }}" width="150"
							data-image-id="{{ $edit->getPrimaryValue() ?? '' }}"
							data-image-table="{{ $table }}">
					</a>
				</div>
			</div>
		</div>
	@endif
@endisset
