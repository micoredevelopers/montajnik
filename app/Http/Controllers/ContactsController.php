<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactsController extends SiteController
{

	public function __construct()
	{
		parent::__construct();
		$this->addBreadCrumb('Контакты');
	}

	public function index()
	{
		$data = [];
		$this->setTitle(getSetting('global.sitename'));

		$data['content'] = view('public.contacts.index');
		return $this->main($data);
	}
}
