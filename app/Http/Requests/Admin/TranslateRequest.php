<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\AbstractRequest;
use Illuminate\Foundation\Http\FormRequest;

class TranslateRequest extends AbstractRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key' => ['unique:translate,key']
        ];
    }
}
