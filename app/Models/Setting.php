<?php

namespace App\Models;

use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Traits\Models\Relations\BelongsToUser;
use App\Traits\Models\Relations\DeletedByUser;
use App\Traits\Models\SetAttributeValueLimited;
use App\Traits\Singleton;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * App\Models\Setting
 *
 * @property int $id
 * @property string $key
 * @property string|null $display_name
 * @property string|null $value
 * @property string|null $details
 * @property string|null $type
 * @property int $sort
 * @property string|null $group
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereValue($value)
 * @mixin \Eloquent
 * @property int|null $user_id Last edited by user
 * @property int|null $deleted_by Deleted by user
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\User|null $deletedBy
 * @property-read \App\User|null $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting withoutTrashed()
 */
class Setting extends Model
{
    use DeletedByUser;
    use BelongsToUser;
    use SetAttributeValueLimited;
    use Singleton;
    use SoftDeletes;

    const DEFAULT_GROUP = 'global';
    const STAFF_GROUP = '_';

    private static $settings = null;

    private $valueDecoded = false;

    protected $table = 'settings';

    protected $fillable = [
        'type',
        'key',
        'value',
        'display_name',
        'group',
        'sort',
        'details',
    ];

    protected $guarded = [];



	public static function getSettings($key = false)
	{
		if (self::$settings === null) {
			/** @var  $query Builder */
			$query = self::getInstance()->sortOrder();
			$settings = $query->get();
			self::$settings = $settings->keyBy('key');
		}
		if (!$key) {
			return self::$settings;
		}
		if (\Arr::has(self::$settings, $key)) {
			return \Arr::get(self::$settings, $key);
		}
		return null;
	}

    public static function getSetting($key, $field = 'value')
    {
        $setting = self::getSettings($key);
        if ($field) {
            return \Arr::get($setting, $field);
        }

        return $setting;
    }

    /**
     * @param $key
     *
     * @return mixed
     * Является ли переданный ключ, служебной настройкой не доступной для публичного редактирования
     */
    public static function isStaff($key)
    {
        return \Str::startsWith($key, '_');
    }

    public function isFile()
    {
        $types = [
            'file_multiple',
            'file',
            'image',
        ];

        return in_array($this->getTypeAttribute(), $types);
    }

    public function getTypeAttribute()
    {
        return $this->attributes['type'];
    }

    public function isFileValid()
    {
        $isFile = $this->isFile();
        $value = \Arr::get($this, 'value');

        return $isFile AND $value;
    }

    public function isTypeImage()
    {
        return $this->isFile() AND $this->getTypeAttribute() === 'image';
    }

    public function isTypeFile()
    {
        return $this->isFile() AND $this->getTypeAttribute() === 'file';
    }

    public function isTypeFileMultiple()
    {
        return $this->isFile() AND $this->attributes['type'] === 'file_multiple';
    }

    public function isMultiFile()
    {
        $isFile = $this->isFile();
        $type = $this->getTypeAttribute() === 'file_multiple';

        return $isFile AND $type;
    }

    public function getValue($key = false)
    {
        if (!$this->valueDecoded AND isJson($this->attributes['value'])) {
            $this->attributes['value'] = \json_decode($this->attributes['value']);
            $this->valueDecoded = true;
        }

        return $key ? \Arr::get($this->attributes['value'], $key) : $this->attributes['value'];
    }

    public function getKeyForSave()
    {
        $replaced = str_replace('.', '_', $this->getAttribute('key'));
        $prefix = $this->getTable() . '_' . $this->getPrimaryValue() . '_';
        $key = $prefix . $replaced;
        return $key;
    }

    public function getPrepend(): ?string
    {
        $prepend = null;
        switch ($this->getTypeAttribute()) {
            case 'file':
                $prepend = "settingFile('";
                break;
            case 'file_multiple':
                $prepend = "settingFiles('";
                break;
            default:
                $prepend = "getSetting('";
        }
        return $prepend;
    }

    public function getAppend(): ?string
    {
        $append = "')";
        return $append;
    }

    public function setValueAttribute($value)
    {
        return $this->setAttributeWithLimit($value, ValidationMaxLengthHelper::LONGTEXT, 'value');
    }

    public function setDeletedByAttribute($value)
    {
        $this->attributes['deleted_by'] = $value;
    }

}
