<?php

namespace App\Http\Requests\Orders;

use App\Helpers\Order\OrderStepsFields;
use App\Helpers\Order\OrderStepsRules;
use Carbon\Carbon;

class StepOneRequest extends AbstractOrderRequest
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getFillableRequestFields($except = []): array
	{
		return $this->exceptFields(OrderStepsFields::getFieldsStepOne(), $except);
	}

	/**
	 * @return array
	 */
	public function rules()
	{
		$merge = [
			'date_return' => checkDateCarbon($this->get('date_return'))->format('Y-m-d H:i'),
		];
		$this->merge($merge);
		return OrderStepsRules::getRulesStepOne();
	}
}
