import { constants } from "crypto";

$(document).ready(function (e) {
  function topSlick() {
    $(".top-slider").slick({
      // lazyLoad: "ondemand",
      arrows: true,
      prevArrow: $(".top-slider-arrow_prev"),
      nextArrow: $(".top-slider-arrow_next"),
      dots: true,
      appendDots: $(".top-slider-dots__wrap"),
      customPaging: function(slider, i) {
        return `<a>0${i + 1}</a>`;
      }
    });
  }

  function ourWorkSlider() {
    $(".our-works-slider")
        .on("init", function(event, slick) {
          const currentSlide = $(slick.$slides[slick.currentSlide]).index();
          const count = $(slick.slideCount);
          $(".current").text(currentSlide + 1);
          $(".count").text(count[0]);
          const currentEl = $(slick.$slides[currentSlide]);
          const src = $(currentEl)
              .find("img")
              .attr("data-lazy");
          $(".our-works-slider-bg img").attr("src", src);
        })
        .on("beforeChange", function() {
          $(".our-works-slider-bg").addClass("our-works-slider-bg_active");
          $(".our-works__arrows").css({
            pointerEvents: "none"
          });
          setTimeout(function() {
            $(".our-works-slider-bg").removeClass("our-works-slider-bg_active");
            $(".our-works__arrows").css({
              pointerEvents: "auto"
            });
          }, 1000);
        })
        .on("afterChange", function(event, slick, currentSlide, nextSlide) {
          $(".current").text(currentSlide + 1);
          const currentEl = $(slick.$slides[currentSlide]);
          const currentImg = $(currentEl).find("img");
          const currentImgSrc = currentImg[0].src;
          $(".our-works-slider-bg img").attr("src", currentImgSrc);
        });

    $(".our-works-slider").slick({
      arrows: true,
      lazyLoad: "ondemand",
      fade: true,
      prevArrow: $(".our-works__arrows_prev"),
      nextArrow: $(".our-works__arrows_next")
    });
  }

  function reviewSlider() {
    $("#main-page .reviews-slider").slick({
      lazyLoad: "ondemand",
      arrows: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: false,
      prevArrow:
          '<div class="reviews-slider-arrow reviews-slider-arrow_prev"></div>',
      nextArrow:
          '<div class="reviews-slider-arrow reviews-slider-arrow_next"></div>',
      responsive: [
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  }
  function videoReviewSlider() {
    $(".video-reviews-slider").slick({
      slidesToShow: 3,
      arrows: false,
      infinite: false,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 600,
          settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  }
  $(".video-reviews-slider").click(function(e) {
    let that;
    if (e.target.classList.contains("video-reviews-iframe")) {
      that = $(e.target);
    } else if (e.target.classList.contains("play-logo-img")) {
      that = $(e.target)
          .parent()
          .siblings(".video-reviews-iframe");
    } else {
      return false;
    }

    const width = that.width();
    const height = that.height();
    const dataSrc = that.attr("data-src");
    const iframe = `<iframe
                    width=${width}
                    height=${height}
                    src="${dataSrc}"
                    frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                  ></iframe>`;
    that.closest(".iframe-box").html(iframe);
  });

  function scrollTop() {
    const winHeightHalf = $(window).height() / 2;
    $(window).on("scroll", e => {
      let windowScroll = $(e.target).scrollTop();
      let thisScroll = windowScroll + winHeightHalf;
      if (
          thisScroll >= $(".services").offset().top &&
          thisScroll < $(".advantages").offset().top
      ) {
        moveElement({
          element: $(".decor-element_first"),
          coefficient: 0.15,
          scroll: thisScroll
        });
        moveElement({
          element: $(".decor-element_second"),
          coefficient: 0.25,
          scroll: thisScroll
        });
        moveElement({
          element: $(".decor-element_third"),
          coefficient: 0.12,
          scroll: thisScroll
        });
        moveElement({
          element: $(".decor-element_fourth"),
          coefficient: 0.2,
          scroll: thisScroll
        });
      }
    });
  }
  function moveElement({ element, coefficient, scroll }) {
    element.css({ transform: `translateY(${scroll * coefficient}px)` });
  }

  if ($(window).width() > 992) {
    scrollTop();
  }
  topSlick();
  ourWorkSlider();
  reviewSlider();
  videoReviewSlider();

})