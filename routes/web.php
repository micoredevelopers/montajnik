<?php

use App\Http\Controllers\AnswerController;
use App\Http\Controllers\Category\CategoryController;
use App\Http\Controllers\Category\CategoryCustomizeController;
use App\Http\Controllers\ContactsController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\TestimonialController;
use App\Http\Controllers\WorksController;
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'prefix'        => LaravelLocalization::setLocale(),
        'cache.headers' => 'last_modified=Sun, 04 Jul 2021 00:18:02 GMT',
        'middleware'    => ['localizationRedirect', 'seo.to-lower'],
    ], function () {
//        Page::getManualPagesUrlRoutes();

//        Auth::routes();

    /** @see IndexController */
    Route::get('/', 'IndexController@index')->name('home');
    Route::get('/search', 'IndexController@search')->name('search');
    /** @see WorksController */
    Route::get('/works/', 'WorksController@index')->name('works.index');
    Route::get('/works/{work}', 'WorksController@show')->name('works.show');
    /** @see AboutController */
    Route::get('/about-us', 'AboutController@index')->name('about.index');
    /** @see TestimonialController */
    Route::get('/comments/all', 'TestimonialController@index')->name('comments.index');
    Route::post('/comments/add', 'TestimonialController@testimonialAdd')->name('comments.add');
    /** @see NewsController */
    Route::get('/news', 'NewsController@index')->name('news.index');
    Route::get('/news/{news}', 'NewsController@show')->name('news.show');
    /** @see CategoryController */
    CategoryCustomizeController::routes();
    Route::get('/catalog/{category}', 'Category\CategoryController@show')->name('category.show');
    Route::post('/calculations/ceilings', 'Category\CategoryCalculationsController@ceilings')->name('category.calculations.ceilings');
    Route::get('/product/{product}', 'ProductController@show')->name('product.show');
    /** @see AnswerController */
    Route::get('/voprosy-otvety', 'AnswerController@index')->name('answer.index');
    Route::get('/voprosy-otvety/{faq}', 'AnswerController@show')->name('answer.show');
    /** @see ContactsController */
    Route::get('/contacts', 'ContactsController@index')->name('contacts.index');
    /** @see PageController */
    Route::get('pages/{page}', 'PageController@show')->name('page.show');
    Route::get('products/{product}', 'ProductController@show')->name('products.show');
    /** @see \App\Http\Controllers\FeedbackController */
    Route::post('feedback/submit', 'FeedbackController@feedback')->name('feedback.submit');


});

Route::group(
    [
        'prefix'     => LaravelLocalization::setLocale(),
        'middleware' => ['localizationRedirect'],
    ], function () {
    include_once "web_admin.php";
});