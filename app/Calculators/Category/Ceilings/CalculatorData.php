<?php


	namespace App\Calculators\Category\Ceilings;


	class CalculatorData implements CalculatorDataContract
	{
		private $type;
		private $length = 0;
		private $width = 0;
		private $profile;
		private $gapless = 0;
		private $starSky = 0;
		private $angles = 0;
		private $baguette = 0;
		private $light = 0;
		private $pipe = 0;
		private $curvature = 0;

		public function getType()
		{
			return $this->type;
		}

		public function setType($type): CalculatorDataContract
		{
			$this->type = $type;
			return $this;
		}

		public function getLength(): int
		{
			return $this->length;
		}

		public function setLength(int $length): CalculatorDataContract
		{
			$this->length = $length;
			return $this;
		}

		public function getWidth(): int
		{
			return $this->width;
		}

		public function setWidth(int $width): CalculatorDataContract
		{
			$this->width = $width;
			return $this;
		}

		public function getProfile()
		{
			return $this->profile;
		}

		public function setProfile($profile): CalculatorDataContract
		{
			$this->profile = $profile;
			return $this;
		}

		/**
		 * @return int
		 */
		public function getGapless(): int
		{
			return $this->gapless;
		}

		/**
		 * @param int $gapless
		 */
		public function setGapless(int $gapless): CalculatorDataContract
		{
			$this->gapless = $gapless;
			return $this;
		}

		/**
		 * @return int
		 */
		public function getStarSky(): int
		{
			return $this->starSky;
		}

		/**
		 * @param int $starSky
		 */
		public function setStarSky(int $starSky): CalculatorDataContract
		{
			$this->starSky = $starSky;
			return $this;
		}

		/**
		 * @return int
		 */
		public function getAngles(): int
		{
			return $this->angles;
		}

		/**
		 * @param int $angles
		 */
		public function setAngles(int $angles): CalculatorDataContract
		{
			$this->angles = $angles;
			return $this;
		}

		/**
		 * @return int
		 */
		public function getLight(): int
		{
			return $this->light;
		}

		/**
		 * @param int $light
		 */
		public function setLight(int $light): CalculatorDataContract
		{
			$this->light = $light;
			return $this;
		}

		/**
		 * @return int
		 */
		public function getPipe(): int
		{
			return $this->pipe;
		}

		/**
		 * @param int $pipe
		 */
		public function setPipe(int $pipe): CalculatorDataContract
		{
			$this->pipe = $pipe;
			return $this;
		}

		/**
		 * @return int
		 */
		public function getCurvature(): int
		{
			return $this->curvature;
		}

		/**
		 * @param int $curvature
		 */
		public function setCurvature(int $curvature): CalculatorDataContract
		{
			$this->curvature = $curvature;
			return $this;
		}

		/**
		 * @return int
		 */
		public function getBaguette(): int
		{
			return $this->baguette;
		}

		/**
		 * @param int $baguette
		 */
		public function setBaguette(int $baguette): CalculatorDataContract
		{
			$this->baguette = $baguette;
			return $this;
		}

	}