<?php

namespace App\Models;


use App\Traits\EloquentMultipleForeignKeyUpdate;
use App\Traits\Models\BelongsToLanguage;
use App\Traits\Models\SetAttributeValueLimited;

/**
 * App\Models\TranslateLang
 *
 * @property int $translate_id
 * @property string|null $value
 * @property int $language_id
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Translate $translate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TranslateLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TranslateLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TranslateLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TranslateLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TranslateLang whereTranslateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TranslateLang whereValue($value)
 * @mixin \Eloquent
 */
class TranslateLang extends Model
{
	use BelongsToLanguage;
	use EloquentMultipleForeignKeyUpdate;
	use SetAttributeValueLimited;

	protected $table = 'translate_lang';

	public $timestamps = false;

	public $incrementing = false;

	protected $guarded = [];

	protected $primaryKey = ['translate_id', 'language_id'];

	public function translate()
	{
		return $this->belongsTo('App\Models\Translate');
	}

	public function setValueAttribute($value)
	{
		return $this->setAttributeWithLimit($value, 60000, 'value');
	}
}
