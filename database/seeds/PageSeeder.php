<?php

use App\Repositories\PageRepository;

class PageSeeder extends AbstractLanguageableSeeder
{
    private $existsPages;

    public function __construct(PageRepository $pageRepository)
    {
        parent::__construct();
        $this->existsPages = $pageRepository->getForAdmin();
    }

    private function pageExistsByUrl(string $url)
    {

    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function run()
    {
        $pages = [
            ['title' => 'Rent a car', 'url' => '/rent-a-car', 'pages' => [
                ['title' => 'About us', 'url' => '/rent-a-car/about-us'],
                ['title' => 'Personal rentals', 'url' => '/rent-a-car/personal-rentals'],
                ['title' => 'Business rentals', 'url' => '/rent-a-car/business-rentals'],
                ['title' => 'Buy a car', 'url' => '/rent-a-car/buy-a-car'],
            ],
            ],
            ['title' => 'Rates & Reservation', 'url' => '/rates-and-reservation', 'pages' => [
                ['title' => 'Choose by Car Type', 'url' => '/rates-and-reservation/vehicles'],
                ['title' => 'Modify a Reservation', 'url' => '/rates-and-reservation/modify'],
            ],
            ],
            ['title' => 'Discounts & Deals', 'url' => '/discounts', 'pages' => [
                ['title' => 'Buy a car', 'url' => '/discounts/last-minute-rate'],
                ['title' => 'Weekly Discount', 'url' => '/discounts/main/weekly-rate'],
                ['title' => 'Monthly Discount', 'url' => '/discounts/main/monthly-rate'],
            ],
            ],
            ['title' => 'Contact Us', 'url' => '/contact-us'],

            ['title' => 'Locations', 'url' => '/locations'],
            ['title' => 'Customer service', 'url' => '/customer-service', 'pages' => [
                ['title' => 'Inquiry/Feedback', 'url' => '/customer-service/feedback'],
                ['title' => 'Accident forms', 'url' => '/customer-service/accident-form'],
                ['title' => 'Clients testimonials', 'url' => '/testimonials'],
                ['title' => 'Rental Policies', 'url' => '/pages/rental-policies'],
                ['title' => 'FAQ', 'url' => '/customer-service/faq'],
            ],
            ],
            ['title' => 'Membership', 'url' => '/membership/membership-benefits',
             'pages' => [
                 ['title' => 'Join the Club', 'url' => '/membership/join-the-club'],
                 ['title' => 'Reward Program', 'url' => '/pages/reward-points-program'],
                 ['title' => 'Corporate Accounts', 'url' => '/membership/corporate'],
                 ['title' => 'Register', 'url' => '/register'],
             ],
            ],
            ['title' => 'Car hire Downtown', 'url' => '/location/car-hire-toronto-downtown'],
            ['title' => 'Mississauga', 'url' => '/location/car-rental-mississauga'],
            ['title' => 'Monthly Car Rental', 'url' => '/location/short-long-term-car-rental-toronto'],
            ['title' => 'Rental Terms', 'url' => '/location/short-long-term-car-rental-toronto-rental-terms'],
            ['title' => 'Minivans', 'url' => '/location/van-rental-toronto'],
            ['title' => 'Downtown', 'url' => '/location/car-rental-downtown-toronto'],
            ['title' => 'Richmond Hill', 'url' => '/location/car-rental-richmond-hill'],
            ['title' => 'Markham', 'url' => '/location/car-rental-richmond-hill-markham'],
            ['title' => 'Pearson airport', 'url' => '/location/pearson-airport-car-rental'],
            ['title' => 'Airport', 'url' => '/location/car-rental-toronto-airport'],

        ];
        $this->loop($pages);
    }

    /**
     * @param array $pages
     * @param \App\Models\Page|null $parentPage
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function loop(array $pages, \App\Models\Page $parentPage = null)
    {
        foreach ($pages as $page) {
            $pageModel = $this->createPage($page, $parentPage);
            if (Arr::has($page, 'pages')) {
                $this->loop(Arr::get($page, 'pages'), $pageModel);
            }
        }
    }

    /**
     * @param array $page
     * @param \App\Models\Page|null $parentPage
     * @return \App\Models\Page|mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function createPage(array $page, \App\Models\Page $parentPage = null)
    {
        /** @var  $pageModel \App\Models\Page */
        $page = array_merge(['manual' => 1], $page);
        $pageModel = app()->make(\App\Models\Page::class);
        $pageModel->fillExisting($page)->setUrlAttribute(Arr::get($page, 'url'));
        if ($parentPage) {
            $pageModel->setAttribute('parent_id', $parentPage->getPrimaryValue());
        }
        $pageModel->save();
        foreach ($this->languages as $language) {
            $pageLang = app()->make(\App\Models\PageLang::class);
            $pageLang->associateWithLanguage($language);
            $pageLang->page()->associate($pageModel);
            $pageLang->fillExisting($page);
            $pageLang->save();
            $pageModel->setRelation('lang', $pageLang);
        }
        return $pageModel;
    }
}
