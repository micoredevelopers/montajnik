<?php

use App\Events\Admin\MenusChanged;
use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    private $languages;

    public function __construct()
    {
        $this->languages = \App\Models\Language::all();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //todo edit seeder for montajnik
        $roles = [
            'main_menu' => [
                ['name' => 'Контакты', 'url' => '/contacts'],
                ['name' => 'Вопросы и ответы', 'url' => '/voprosy-otvety'],
                ['name' => 'Новости', 'url' => '/news'],
                ['name' => 'Отзывы', 'url' => '/comments/all'],
                ['name' => 'О нас', 'url' => '/about-us'],
                ['name' => 'Наши работы', 'url' => '/works'],
            ],
        ];
        $default = ['active' => 1];
        foreach ($roles as $role => $menus) {
            if (!$menus) {
                continue;
            }
            $group = \App\Models\MenuGroup::getByRoleName($role);
            if (!$group) {
                continue;
            }
            foreach ($menus as $item) {
                $item = array_merge($default, $item);
                $menu = $this->saveMenu($item, $group);
                if (Arr::has($item, 'menus')) {
                    foreach (Arr::get($item, 'menus') as $itemChild) {
                        $itemChild = array_merge($default, $itemChild);
                        $itemChild['parent_id'] = $menu->id;
                        $this->saveMenu($itemChild, $group);
                    }
                }
            }
        }

        event(new MenusChanged());
    }

    private function saveMenu(array $item, \App\Models\MenuGroup $menuGroup): \App\Models\Menu
    {
        ($menu = new \App\Models\Menu())->fillExisting($item)
            ->menuGroup()->associate($menuGroup)->save();
        foreach ($this->languages as $language) {
            ($menuLang = new \App\Models\MenuLang())->fillExisting($item);
            $menuLang->menu()->associate($menu);
            $menuLang->associateWithLanguage($language)
                ->save();
        }
        return $menu;
    }
}
