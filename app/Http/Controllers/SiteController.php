<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\Menu;
use App\Models\Meta;
use App\Models\Page;
use App\Repositories\CategoryRepository;
use App\Repositories\MenuRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Response;

abstract class SiteController extends BaseController {

    protected $lang = 1;

    protected $categories;
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var \Illuminate\Support\Collection
     */
    private $languagesList;

    public function __construct() {
        parent::__construct();

        $this->initGlobalScopesModelsPublic();
        $this->addBreadCrumb(getSetting('global.home-bread-name'), route('home'));
        $this->languages = new Language();
        $this->languagesList = $this->getLanguages();
        $this->lang = getLang();
        $this->categories = $this->getCategories();
        $this->shareViews();
        $this->checkForCanonical();
    }


    public function main($data = []) {
        $this->checkMetaData();

        $data['breadcrumbs'] = $data['breadcrumbs'] ?? $this->getBreadCrumbs();
        $data['categories'] = $this->categories;
        $data['menus'] = $this->getMenus()['main_menu'];
        $data['languages'] = $this->languagesList;

        $this->setTitle(' - ' . getSetting('global.sitename'));

        return view('layout.site-app', $data);
    }

    public function checkMetaData(): void {
        $metadata = Meta::getMetaData();
        if ($metadata) {
            if ($metadata->title) {
                $this->setTitle($metadata->title, true);
            }
            if ($metadata->description) {
                $this->setDescription($metadata->description, true);
            }
            if ($metadata->keywords) {
                $this->setKeywords($metadata->keywords, true);
            }
            return;
        }
        if ($meta_default = Meta::whereUrl('*')->GetLang($this->lang)->first()) {
            if (!$this->getTitle()) {
                $this->setTitle($meta_default->getAttribute('title'));
            }
            if (!$this->getDescription()) {
                $this->setDescription($meta_default->getAttribute('description'));
            }
            if (!$this->getKeywords()) {
                $this->setKeywords($meta_default->getAttribute('keywords'));
            }
        }
    }

    protected function getLanguages() {
        $languages = $this->languages->active(1)->get();
        $languages = $languages->map(static function ($item) {
            $item->current = ($item->key === getCurrentLocale());
            return $item;
        });
        return $languages;
    }

    private function shareViews(): void {
        View::share('categories', $this->categories);
    }

    private function getCategories() {
        try {
            /** @var  $categoryRepository CategoryRepository */
            $categoryRepository = app()->make(CategoryRepository::class);
            $categories = $categoryRepository->getCategories();
        } catch (\Exception $exception) {
            $categories = collect([]);
        }

        return $categories;
    }

    private function getMenus() {
        try {
            /** @var  $menuRepository MenuRepository */
            $menuRepository = app()->make(MenuRepository::class);
            $menus = $menuRepository->getMenus();
        } catch (\Exception $exception) {
            $menus = null;
            if (isLocalEnv()) {
                d($exception);
            }
        }

        return $menus;
    }


    private function initGlobalScopesModelsPublic() {
        Page::initScopesPublic();
        Menu::initScopesPublic();
    }

    protected function checkForCanonical(): void {
        if (Input::has('page')) {
            $this->setCanonical(url()->current());
        }
    }

    protected function setNextPrevLinkForPagination(LengthAwarePaginator $paginator): void {
        $nextUrl = $paginator->nextPageUrl();
        $prevUrl = $paginator->previousPageUrl();

        if ($paginator->currentPage() === 2) {
            $this->setPrev(url()->current());
        } elseif ($prevUrl !== null) {
            $this->setPrev($prevUrl);
        }
        if ($paginator->hasMorePages()) {
            $this->setNext($nextUrl);
        }
    }
}











