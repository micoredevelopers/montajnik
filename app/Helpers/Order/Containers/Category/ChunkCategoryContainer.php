<?php

namespace App\Helpers\Order\Containers\Category;

use App\Helpers\View\ConcreteColumnWidthCssFramework;
use App\Models\Category\Category;
use Illuminate\Support\Collection;

class ChunkCategoryContainer {
    /**
     * @var Category
     */
    private $category;

    /**
     * @var Collection
     */
    private $chunks;
    /**
     * @var ConcreteColumnWidthCssFramework
     */
    private $columnWidthCssFramework;


    /**
     * ChunkCategoryContainer constructor.
     * @param Category                        $category
     * @param Collection                      $chunks
     * @param ConcreteColumnWidthCssFramework $columnWidthCssFramework
     */
    public function __construct(
        Category $category,
        Collection $chunks,
        ConcreteColumnWidthCssFramework $columnWidthCssFramework) {
        $this->category = $category;
        $this->setChunks($chunks);
        $this->columnWidthCssFramework = $columnWidthCssFramework;
    }

    public function getCategory(): Category {
        return $this->category;
    }

    private function setChunks(Collection $collection): self {
        $this->chunks = $collection;
        return $this;
    }

    public function getChunks(): Collection {
        return $this->chunks;
    }

    public function getColByChunk(array $chunk): string {
        return $this->columnWidthCssFramework->getColNumByCount(count($chunk));
    }
}