<?php

namespace App\Console\Commands;

use App\Models\Model;
use Illuminate\Console\Command;

class DeleteAllTablesFromDatabase extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'db:drop:tables';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Drop all tables in database for local development only';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	/**  */
	public function handle()
	{
		if (!isLocalEnv()) {
			$this->error('PRODUCTION DROP DEPRECATED');
			return;
		}

		if($this->confirm('Drop all tables from database?')){
			(new  Model())->getConnection()->getSchemaBuilder()->dropAllTables();
			$this->info('Database tables dropped');
		}

	}
}
