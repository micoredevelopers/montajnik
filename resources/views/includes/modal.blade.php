<!-- call Modal -->
<div
        class="modal fade"
        id="phoneModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="phoneModalTitle"
        aria-hidden="true"
>
    <div class="modal-dialog modal-dialog-centered align-items-end" role="document">
        <div class="modal-content">
            <div class="modal-content-box">
                <div class="modal-header d-flex justify-content-center align-items-center p-3">
                    <p class="text_opacity03 text-center mb-0">Выбор номера</p>
                </div>
                <div class="modal-body border-bottom text-center">
                    <a href="tel:+{{ formatTel(getSetting('contacts.phone-vodafone')) }}"
                       class="phones-box__item flex-row align-items-center justify-content-center">
                        <img class="mb-0 mr-2" src="{{ asset('img/mp/Vodafone_Ukraine.png') }}" alt=""/>
                        <span class="phone">{{ getSetting('contacts.phone-vodafone') }}</span>
                    </a>
                </div>
                <div class="modal-body border-bottom">
                    <a href="tel:+{{ formatTel(getSetting('contacts.phone-kyivstar')) }}"
                       class="phones-box__item flex-row align-items-center justify-content-center">
                        <img class="mb-0 mr-2" src="{{ asset('img/mp/kievstar.png') }}" alt=""/>
                        <span class="phone">{{ getSetting('contacts.phone-kyivstar') }}</span>
                    </a>
                </div>
                <div class="modal-body border-bottom">
                    <a
                            href="tel:+{{ formatTel(getSetting('contacts.phone-lifecell')) }}"
                            class="phones-box__item flex-row align-items-center justify-content-center"
                    >
                        <img class="mb-0 mr-2" src="{{ asset('img/mp/Lifecell_logo.png') }}" alt=""/>
                        <span class="phone">{{ getSetting('contacts.phone-lifecell') }}</span>
                    </a>
                </div>
                <div class="modal-body border-bottom">
                    <a
                            href="tel:+{{ formatTel(getSetting('contacts.phone-intretelecom')) }}"
                            class="phones-box__item flex-row align-items-center justify-content-center"
                    >
                        <img class="mb-0 mr-2" src="{{ asset('img/mp/phone.png') }}" alt=""/>
                        <span class="phone">{{ getSetting('contacts.phone-intretelecom') }}</span>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                    <span class="mx-auto" data-dismiss="modal">
                    Отмена
                    </span>
            </div>
        </div>
    </div>
</div>

<!-- modal feedback -->
<div
        class="modal fade"
        id="feedBackModalCenter"
        tabindex="-1"
        role="dialog"
        aria-labelledby="feedBackModalCenter"
        aria-hidden="true"
>
    <div class="modal-dialog modal-dialog-centered maxWth600" role="document">
        <div class="modal-content">
            <div class="modal-body position-relative text-center">
                <h5 class="feedBack-modal-title ">
                    Обратная связь
                </h5>
                <form action="{{ route('feedback.submit') }}" class="callback-form">
                    <p class="text-lg-16 text_opacity03 mb-lg-30">
                        Готовы к сотрудничеству? Свяжитесь с нами удобным способом –
                        через форму обратной связи или по номеру телефона.
                    </p>
                    <input class="feedBack-modal-input" placeholder="{{ getTranslate('forms.name') }}" name="name" required="required"
                           value="{{ old('class', getTestField('class')) }}" autocomplete="off" type="text"/>
                    <input class="feedBack-modal-input" placeholder="{{ getTranslate('forms.phone') }}" name="phone" required="required"
                           value="{{ old('class', getTestField('class')) }}" autocomplete="off" type="text"/>
                    <textarea class="feedBack-modal-input" placeholder="{{ getTranslate('forms.message') }}" name="message" id="feedback-message" rows="5"></textarea>
                    <button type="submit" class="btn-custom mx-auto">{{ getTranslate('forms.send') }}</button>
                </form>
                <button
                        type="button"
                        class="close custom-cross"
                        data-dismiss="modal"
                        aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
</div>

{{-- feedBackModal with photo --}}
<div
        class="modal fade"
        id="feedBackModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="feedBackModalCenter"
        aria-hidden="true"
>
    <div class="modal-dialog modal-dialog-centered maxWth600" role="document">
        <div class="modal-content">
            <div class="modal-body position-relative text-center">
                <h5 class="feedBack-modal-title ">
                    Оставить отзыв
                </h5>
                <form action="{{ route('comments.add') }}" class="feedBack-modal-form">
                    @isset($category)
                        <input type="hidden" name="category_id" value="{{ $category->id ?? null }}">
                    @endisset
                    <input class="feedBack-modal-input" minlength="2"
                           name="name" required="required"
                           value="{{ old('name', getTestField('name')) }}"
                           type="text" autocomplete="off"
                           placeholder="{{ getTranslate('forms.name') }}"
                    />
                    <input class="feedBack-modal-input" minlength="2"
                           name="phone" required="required"
                           value="{{ old('phone', getTestField('phone')) }}"
                           type="text" autocomplete="off"
                           placeholder="{{ getTranslate('forms.phone') }}"
                    />
                    <input class="feedBack-modal-input" minlength="2"
                           name="email" required="required"
                           value="{{ old('email', getTestField('email')) }}"
                           type="text" autocomplete="off"
                           placeholder="{{ getTranslate('forms.email') }}"
                    />
                    <input class="feedBack-modal-input " name="image[]" type="file" accept="image/jpeg,png,jpg"/>
                    <textarea class="feedBack-modal-input" placeholder="{{ getTranslate('forms.message') }}" id="review-message"
                              minlength="10" name="message" required="required" rows="5" autocomplete="off">{!! old('message', getTestField('message')) !!}</textarea>
                    <button type="submit" class="btn-custom mx-auto">{{ getTranslate('forms.send') }}</button>
                </form>
                <button type="button" class="close custom-cross" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
</div>

<div id="success-modal" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content text-center px-5 py-4">
            Комментарий отправлен на модерацию, и будет добавлен в ближайшее время
        </div>
    </div>
</div>