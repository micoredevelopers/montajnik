<?php

namespace App\Models;


/**
 * App\Models\TestimonialType
 *
 * @property int $id
 * @property string $type
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Testimonial[] $testimonials
 * @property-read int|null $testimonials_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TestimonialType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TestimonialType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TestimonialType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TestimonialType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TestimonialType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TestimonialType whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class TestimonialType extends Model
{
    protected $table = 'testimonial_types';
    protected $guarded = ['id'];

    public $timestamps = false;

    const TYPE_VIDEO_ID = '1';
    const TYPE_IMAGE_ID = '2';

    public function testimonials()
    {
        return $this->hasMany(Testimonial::class, 'type_id', 'id');
    }
}
