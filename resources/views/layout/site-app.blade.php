<!DOCTYPE html>
<html lang="{{ getCurrentLocale() }}">

<head>
  <meta charset="utf-8"/>
  {!! SEOMeta::generate() !!}
  @php if(!isset($_SERVER['HTTP_USER_AGENT']) || !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse')): @endphp
  @if (!Agent::isRobot())
    {!! getSetting('seo.google-analytics-head') !!}
  @endif
  @php endif @endphp
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  {{--    Preload--}}
  <link rel="preload" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" as="style">
  <link rel="preload" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" as="style">
  <link rel="preload" href="{{ asset('images/default.svg') }}" as="image">
  {{-- <link rel="preload" href="{{ asset('img/slider-img.png') }}"> --}}
  {{--    End preload--}}

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  @php if(!isset($_SERVER['HTTP_USER_AGENT']) || !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse')): @endphp
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
  @php endif; @endphp
  <link rel="stylesheet" href="{{ mix('css/style.css') }}">

  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
{!! $styles ?? '' !!}

@yield('css')

@stack('css')

<!-- meta header -->
@php if(!isset($_SERVER['HTTP_USER_AGENT']) || !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse')): @endphp
{!! getSetting('seo.head-global-codes') !!}
@php endif @endphp
{!! showMeta('', 'header') !!}
<!-- End meta header  -->
</head>
<body>
@php if(!isset($_SERVER['HTTP_USER_AGENT']) || !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse')): @endphp
{!! getSetting('seo.google-analytics-body') !!}
@php endif @endphp
@include('layout.header')

<article class="offset-header-height">

  @include('layout.elements.meta.text-top')

  {!! $content ?? '' !!}

  @include('layout.elements.meta.text-bottom')
</article>

{!! app(\App\Http\ViewComponents\SidebarStaffViewComponent::class)->toHtml() !!}

@include('includes.modal')
@include('layout.footer')
@include('layout.partials.scripts')
@include('layout.partials.styles-footer')



<!-- meta footer -->
{!! showMeta('', 'footer') !!}
<!-- end meta footer -->

</body>
</html>
