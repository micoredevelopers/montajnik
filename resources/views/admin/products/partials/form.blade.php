@include('admin.partials.crud.elements.name-required')

@include('admin.category.partials.sub-name')

@include('admin.partials.crud.elements.url')

<div class="row">
    <div class="col-md-4">
        @include('admin.partials.crud.elements.active')
    </div>
    <div class="col-md-4">
        @include('admin.products.partials.category')
    </div>
    <div class="col-md-4">

    </div>
</div>

@include('admin.partials.crud.elements.image-upload-group')

@include('admin.partials.crud.textarea.description')

@include('admin.partials.crud.textarea.except')

@include('admin.partials.crud.js.init-description-except')
