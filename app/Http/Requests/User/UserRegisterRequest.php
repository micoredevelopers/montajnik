<?php

namespace App\Http\Requests\User;

use Illuminate\Http\Request;

class UserRegisterRequest extends AbstractUserRequest
{
    /**
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        $personal = $this->getPersonalUserFieldsWithCredentials();

        if (!(int)$request->get('is_company')) {
            return $personal;
        }
        $company = $this->getCompanyUserFields();
        return $company;
    }
}
