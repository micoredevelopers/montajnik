<?php


namespace App\Helpers\Dev;

use App\Traits\SetterAndGetterMethods;
use Illuminate\Support\Str;

class AttributesSettersGettersCreatorFromArray
{
	use SetterAndGetterMethods;

	private $prefix = 'Attribute';

	private $delimiterLines = '<br>';

	private $accessModifier = 'protected';

	public function setPrefix(string $prefix)
	{
		$this->prefix = $prefix;
		return $this;
	}

	public function setDelimiterLines(string $delimiter)
	{
		$this->delimiterLines = $delimiter;
		return $this;
	}

	public function setAccessModifier(string $modifier)
	{
		$this->accessModifier = $modifier;
		return $this;
	}

	public function generate(array $array): string
	{
		$attributes = $getters = $setters = '';
		foreach ($array as $key => $v) {
			$attributes .= $this->accessModifier . ' $' . Str::camel($key) . ';' . $this->delimiterLines;
			//
			$setters .= 'public function ' . $this->getNameSetter($key, $this->prefix) . '($value){
				$this->' . Str::camel($key) . ' = $value;
			return $this;
			}' . $this->delimiterLines;
			//
			$getters .= 'public function ' . $this->getNameGetter($key, $this->prefix) . '(){
				return $this->' . Str::camel($key) . ';
			}' . $this->delimiterLines;
		}
		$delimiter = str_repeat($this->delimiterLines, 5);
		return implode('', [$attributes, $delimiter, $setters, $delimiter, $getters]);
	}

}