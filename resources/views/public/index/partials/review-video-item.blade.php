<div class="video-reviews-slider__item ">
    <div class="mx-lg-auto">
        <div class="iframe-box">
                                <span class="play-logo">
                                    <img class="w-100 play-logo-img" src="{{ asset('img/playLogo.svg') }}" width="106" height="80">
                                </span>
            <img class="video-reviews-iframe w-100" src="{{ getPreviewYoutubeFromLink($testimonialVideo->video) }}"
                 data-src="https://www.youtube.com/embed/{{ getYoutubeVideoCodeFromLink($testimonialVideo->video) }}?autoplay=1">
        </div>
        <p class="text-center text_16-20 mb-0">{{ translateFormat('testimonials.video-from-name', ['%name%' => $testimonialVideo->name,]) }}</p>
    </div>
</div>