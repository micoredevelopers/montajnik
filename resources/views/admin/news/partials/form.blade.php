@include('admin.partials.crud.elements.name')

<div class="row">
    <div class="col-md-4">
        @include('admin.partials.crud.elements.active')
    </div>
    <div class="col-md-4">
        @include('admin.partials.crud.date.date-pub')
    </div>
    <div class="col-md-4">
        @include('admin.news.partials.on-main')
    </div>
</div>
@include('admin.partials.crud.elements.url')

@include('admin.partials.crud.elements.image-upload-group')

@include('admin.partials.crud.textarea.description')

@include('admin.partials.crud.textarea.except')