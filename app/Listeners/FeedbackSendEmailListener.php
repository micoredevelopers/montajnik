<?php

namespace App\Listeners;

use App\Events\FeedbackSubmittedEvent;
use App\Mail\Feedback\FeedbackDefault;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackSendEmailListener
{
	public function __construct()
	{
	}

	/**
	 * Handle the event.
	 *
	 * @param FeedbackSubmittedEvent $event
	 * @return void
	 */
	public function handle(FeedbackSubmittedEvent $event)
	{
		$feedback = $event->getFeedback();
		\Mail::send(new FeedbackDefault($feedback));
	}
}
