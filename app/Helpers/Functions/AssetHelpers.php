<?php

if (!function_exists('assetFileExists')) {
	function assetFileExists($filePath)
	{
		return file_exists(public_path('/' . $filePath));
	}
}
if (!function_exists('assetFilemtime')) {
	function assetFilemtime($filePath)
	{
		if (!assetFileExists($filePath)) {
			return time();
		}

		return filemtime(public_path('/' . $filePath));
	}
}
if (!function_exists('assetVersioned')) {
	function assetVersioned($asset_path): string
	{
		if (assetFileExists($asset_path)) {
			return asset($asset_path) . '?v=' . assetFilemtime($asset_path);
		}
		return '';
	}
}
if (!function_exists('getCurrentLocale')) {
	function getCurrentLocale()
	{
		static $lang = null;
		if ($lang === null) {
			$lang = \LaravelLocalization::getCurrentLocale();
		}

		return $lang;
	}
}