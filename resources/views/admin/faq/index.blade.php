<div class="table-responsive">
	<table class="table table-shopping">
		<thead>
		<tr>
{{--			<th class="">{{ __('form.sorting') }}</th>--}}
			<th class="th-description">@lang('form.title')</th>
			<th>{{ __('form.active') }}</th>
			<th class="text-right">
				<a href="{{ route($routeKey.'.create') }}" class="btn btn-primary">@lang('form.create')</a>
			</th>
		</tr>
		</thead>
		<tbody data-sortable-container="true" data-table="{{ $table }}">
		@foreach($list as $item)
			<?php /** @var $item \App\Models\Faq */ ?>
			<tr class="draggable" data-sort="" data-id="{{ $item->getPrimaryValue() }}">
				{{--<td>
					@include('admin.partials.sort_handle')
				</td>--}}
				<td>
					{{ $item->getQuestionAttribute() }}
				</td>
				<td>
					{{ translateYesNo($item->getAttribute('active')) }}
				</td>
				<td class="text-primary text-right">
					<div class="dropdown menu_drop">
						<button
								class="btn btn-secondary dropdown-toggle" type="button"
								id="dropdownMenuButton_{{ $item->id }}" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false">
							<i class="material-icons">menu</i>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton_{{ $item->id }}">
							{{--<form class="dropdown-item p-0" href="#">--}}
							{{--<button class="drop_menu_button text-left"--}}
							{{--type="submit">Отключить/включить</button>--}}
							{{--</form>--}}
							@can('edit_'. $key)
								<a href="{{ route($routeKey.'.edit',  $item->id)  }}"
								   class="dropdown-item">@lang('form.edit')</a>
							@endcan
							@can('delete_'. $key)
								{!! Form::open( ['method' => 'delete', 'url' => route($routeKey.'.destroy', ['user' => $item->id]), 'class' => 'formDeleteConfirm']) !!}
								<button type="submit" class="dropdown-item">@lang('form.delete')</button>
								{!! Form::close() !!}
							@endcan
						</div>
					</div>
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>
