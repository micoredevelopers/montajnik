<?php

namespace App\Models;

use App\Traits\Models\Scopes\ApplyDateRangeFromRequest;
use App\User;

/**
 * App\Models\Feedback
 *
 * @property int $id
 * @property string $type
 * @property string|null $name
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $address
 * @property string|null $message
 * @property array|null $fields
 * @property array|null $options
 * @property string|null $ip
 * @property int|null $user_id
 * @property string|null $referer
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback applyDateRange($dateRangeFromRequest, $field = 'created_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereFields($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereReferer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereUserId($value)
 * @mixin \Eloquent
 */
class Feedback extends Model
{
	use ApplyDateRangeFromRequest;
	protected $guarded = ['id'];

	protected $casts = [
		'fields'  => 'array',
		'options' => 'array',
	];

	protected static $types = [
		'feedback' => 'Обратная связь',
//		'accident',
//		'business',
//		'buy-car',
	];

	public static function getTypes()
	{
		return self::$types;
	}

	public function getOption($option)
	{
		return \Arr::get($this->getAttribute('options'), $option);
	}

	public function getUploadedFilePath()
	{
		return getStorageFilePath($this->getOption('filepath'));
	}

	public function getUploadedFilename()
	{
		return $this->getOption('filename');
	}

	public function getFieldsAttribute()
	{
		$attributes = \Arr::get($this->attributes, 'fields');
		if (!is_array($attributes) AND isJson($attributes)) {
			\Arr::set($this->attributes, 'fields', json_decode($this->attributes['fields']));
		}

		return (array)\Arr::get($this->attributes, 'fields');
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}


}
