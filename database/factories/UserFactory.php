<?php

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt(\Str::random(10)), // password
        'remember_token' => Str::random(10),
    ];
});

$factory->defineAs(App\User::class, 'superadmin', function(Faker $faker) {
    return [
        'name' => 'Admin',
        'email' => 'superadmin@admin.com',
        'password' => bcrypt(User::getPassword('superadmin')),
        'remember_token' => \Str::random(10),
    ];
});
$factory->defineAs(App\User::class, 'admin', function(Faker $faker) {
    return [
        'name' => 'Admin',
        'email' => 'admin@admin.com',
        'password' => bcrypt(User::getPassword('admin')),
        'remember_token' => \Str::random(10),
    ];
});