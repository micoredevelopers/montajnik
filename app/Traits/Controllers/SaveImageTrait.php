<?php

namespace App\Traits\Controllers;

use App\Config\Media\ThumbnailImagesConfig;
use App\Events\Admin\Image\PathReplacedEvent;
use App\Events\Admin\ImageUploaded;
use App\Helpers\Media\ImageSaver;
use App\Models\Model;
use Illuminate\Http\Request;

/**
 * Trait SaveImageTrait
 * @package App\Traits\Controllers
 */
trait SaveImageTrait
{
	protected function saveImage(Request $request, Model $model): string
	{
		$imagePath = '';
		if ($request->hasFile('image')) {
			$imageSaver = new ImageSaver($request);
			$imageSaver->setFolderName($model->getTable())->withFileName(md5($model->getPrimaryValue()));
			$sizes = array_filter(ThumbnailImagesConfig::getSizesByKey($model->getTable()), 'is_numeric');
			(count($sizes) === 2) ? $imageSaver->setThumbnailSizes(...$sizes) : $imageSaver->setWithThumbnail(false);

			//todo add event on change image path

			$imagePath = $imageSaver->saveFromRequest();
			$model->setAttribute('image', $imagePath);
			event(new PathReplacedEvent($model));
			$model->save();

			event(new ImageUploaded($model, $request));
		}

		return $imagePath;
	}
}
