<?php /** @var $edit \App\Models\Faq */ ?>

<form action="{{ route($routeKey . '.update', $edit->getPrimaryValue() ) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
	@csrf
	@method('patch')

	@include('admin.partials.submit_update_buttons')

	@include('admin.faq.partials.form')

	@include('admin.partials.submit_update_buttons')
</form>

