<?php /** @var $edit \App\Models\Category\Category */ ?>
@include('admin.partials.crud.elements.name')

@include('admin.category.partials.sub-name')

@include('admin.partials.crud.elements.url')

@if(isSuperAdmin())
	@include('admin.partials.crud.default', ['name' => 'icon', 'title' => __('modules.menu.icon_font')])
@endif
<div class="row">
    <div class="col-md-6">
        @include('admin.partials.crud.elements.active')
    </div>
    <div class="col-md-6 d-none">
        <div class="form-group">
            @php
                $size = $edit->childrens_big_size ?? 0;
            @endphp
            <label class="control-label" for="childrens_big_size">Размер товаров</label>
            <select name="childrens_big_size" id="childrens_big_size" class="form-control" autocomplete="off">
                <option value="0" {!! selectedIfTrue($size == 0) !!}>Маленькие</option>
                <option value="1" {!! selectedIfTrue($size == 1) !!}>Большие</option>
            </select>
        </div>
    </div>
</div>

@include('admin.partials.crud.elements.image-upload-group')

@include('admin.partials.crud.textarea', ['name'=>'except', 'title'=>'Краткое описание категории'])

<div class="row">
    <div class="col-md-4">
        @include('admin.category.partials.parent')
    </div>
    <div class="col-md-4">
        @include('admin.menu.partials.page')
    </div>
</div>



