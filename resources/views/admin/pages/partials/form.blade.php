@include('admin.partials.crud.elements.title', isset($edit) ? ['edit' => $edit->lang] : [])

<div class="row">
	<div class="col-md-8">
		@include('admin.partials.crud.elements.url')
	</div>
	<div class="col-md-4">
{{--		@include('admin.partials.crud.checkbox', ['name' => 'manual', 'title' => 'Manual url'])--}}
	</div>
</div>

@include('admin.partials.crud.elements.image-upload-group')

<div class="row">
	<div class="col-3">
		@include('admin.partials.crud.elements.active')
	</div>
</div>
<div class="row">
	@isset($pageTypes)
		<div class="col-4">
			{!! errorDisplay('page_type') !!}
			<label for="page_type">@lang('modules.pages.page_type')</label>
			{!! Form::select('page_type', ['' => __('form.select.empty')] + $pageTypes, $edit->page_type ?? null,
			 ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
		</div>
	@endisset
	<div class="col-4">
		@include('admin.partials.crud.select.parent-id', ['nullable' => true])
	</div>
</div>

@include('admin.partials.crud.textarea.description', isset($edit) ? ['edit' => $edit->lang] : [])

@include('admin.partials.crud.textarea.except', isset($edit) ? ['edit' => $edit->lang] : [])
