<?php

use Illuminate\Database\Seeder;

class TestimonialTypeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'name' => 'Видео отзыв',
                'type' => 'video',
            ],
            [
                'name' => 'Фото отзыв',
                'type' => 'image',
            ],
        ];
        foreach ($types as $type) {
            (new \App\Models\TestimonialType())->fillExisting($type)->save();
        }
    }
}
