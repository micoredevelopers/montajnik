<div class="cursor-pointer" data-id="{{ $item->getPrimaryValue() }}" data-table="{{ $item->getTable() }}" onclick="deleteItem(this)">
    <i class="fa fa-times fa-1x" aria-hidden="true"></i>
</div>
