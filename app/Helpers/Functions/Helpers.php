<?php


if (!function_exists('rowsKeyObject')) {
	function rowsKeyObject($object, $key, $value)
	{
		$return = [];
		foreach ($object as $item) {
			$return[$item->$key] = $item->$value;
		}

		return $return;
	}
}
if (!function_exists('objectKeyBy')) {
	function objectKeyBy($object, $id = 'id')
	{
		$return = [];
		foreach ($object as $item) {
			$return[$item->$id] = $item;
		}

		return $return;
	}
}


if (!function_exists('isAdmin')) {
	function isAdmin()
	{
		static $isAdmin = null;
		if ($isAdmin === null) {
			/** @var  $user \App\User */
			$user = \Auth::user();
			$isAdmin = ($user && $user->isAdmin());
		}
		return $isAdmin;
	}
}
if (!function_exists('isSuperAdmin')) {
	function isSuperAdmin()
	{
		static $result;
		if ($result === null) {
			/** @var $user \App\User */
			$user = \Auth::user();
			$result = $user ? (bool)$user->isSuperAdmin() : false;
		}
		return $result;
	}

}


if (!function_exists('dumpAdmin')) {
	function dumpAdmin()
	{
		if (isSuperAdmin()) {
			dump(debug_backtrace()[0]['file'] . ' | line: ' . debug_backtrace()[0]['line']);
			$vars = func_get_args();
			dump(...$vars);
		}
	}
}
if (!function_exists('getDefaultLang')) {
	function getDefaultLang($column = 'id')
	{
		$lang = \App\Models\Language::default()->first();
		return $column ? Arr::get($lang, $column) : $lang;
	}
}

if (!function_exists('getLang')) {
	function getLang()
	{
		return \App\Helpers\LanguageHelper::getLanguageId() ?? getDefaultLang();
	}
}

if (!function_exists('normalizePath')) {
	function normalizePath($path)
	{
		$replace = [
			'\\'   => '/',
			'\\\\' => '/',
		];

		return str_replace(array_keys($replace), array_values($replace), $path);
	}
}
if (!function_exists('classImplementsInterface')) {
	function classImplementsInterface($class, $interface): bool
	{
		$interfaces = class_implements($class);
		return in_array($interface, $interfaces, true);
	}
}
if (!function_exists('getMeta')) {
	function getMeta()
	{
		return \App\Models\Meta::getMetaData();
	}
}
if (!function_exists('showMeta')) {
	function showMeta($value, $field = 'h1')
	{
		$meta = \Arr::get(getMeta(), $field, $value);
		return $meta ?: $value;
	}
}
if (!function_exists('getYoutubeVideoCodeFromLink')) {
	function getYoutubeVideoCodeFromLink($link)
	{
		$exploded = explode('watch?v=', $link);

		return end($exploded);
	}
}

if (!function_exists('isJson')) {
	function isJson($str)
	{
		if (!is_string($str)) {
			return false;
		}

		\json_decode($str, true);
		return !\json_last_error();
	}
}

if (!function_exists('concatWords')) {
	function concatWords()
	{
		$glue = ' ';
		return implode($glue, func_get_args());
	}
}
if (!function_exists('getCurrencyIcon')) {
	function getCurrencyIcon()
	{
		return getSetting('global.currency');
	}
}


if (!function_exists('toNegative')) {
	function toNegative($number)
	{
		return -1 * abs($number);
	}
}

function replaceLettersSearchRuEn(string $string)
{
	$replace = [
		'ф' => 'a',
		'и' => 'b',
		'с' => 'c',
		'в' => 'd',
		'у' => 'e',
		'а' => 'f',
		'п' => 'g',
		'р' => 'h',
		'ш' => 'i',
		'о' => 'j',
		'л' => 'k',
		'д' => 'l',
		'ь' => 'm',
		'т' => 'n',
		'щ' => 'o',
		'з' => 'p',
		'й' => 'q',
		'к' => 'r',
		'ы' => 's',
		'е' => 't',
		'г' => 'u',
		'м' => 'v',
		'ц' => 'w',
		'ч' => 'x',
		'н' => 'y',
		'я' => 'z',
	];
	return str_replace(array_keys($replace), array_values($replace), $string);
}

function routeKey(string $module, string $action = 'index')
{
	return implode('.', [routeKeys($module), $action]);
}

function routeKeys(string $module)
{
	$prepend = 'admin.';
	$keys = [
		'users'    => 'users',
		'roles'    => 'roles',
		'category' => 'category',
		'products' => 'products',
		'prices'   => 'prices',
		'news'     => 'news',
		'partners' => 'partners',
		'works'    => 'works',
		'sitemap'  => 'other.sitemap',
		'robots'   => 'other.robots',
	];

	return $prepend . ((string)Arr::get($keys, $module));
}

if (!function_exists('grepClassMethods')) {
	function grepClassMethods($class, $needle = ''): array
	{
		$methods = [];
		if (is_object($class) && $needle) {
			$methods = get_class_methods($class);
			$methods = array_filter($methods, static function ($item) use ($needle) {
				$pos = stripos($item, $needle);
				return false !== $pos;
			});
		}
		return $methods;
	}
}

if (!function_exists('is_base64')) {
	function is_base64(string $str)
	{
		if (!is_string($str)) {
			return false;
		}

		return base64_encode(base64_decode($str)) === str_replace(["\n", "\r"], '', $str);
	}
}

function remakeInputKeyDotted($name)
{
	$replace = [
		']' => '',
		'[' => '.',
	];
	return str_replace(array_keys($replace), array_values($replace), $name);
}


function inputNamesManager(\App\Models\Model $model)
{
	return new \App\Helpers\Miscellaneous\InputNamesManager($model);
}

function seedByClass(string $class)
{
	try {
		\Artisan::call('db:seed', ['--class' => $class, '--force' => 'true']);
	} catch (\Exception $e) {
		app('log')->error($e->getMessage());
	}
}

function CRUDLinkByModel(\App\Models\Model $model)
{
	return new \App\Helpers\Route\CRUDLinkByModel($model);
}


function titleFromInputName(string $name)
{
	return \Str::title(str_replace('_', ' ', $name));
}


function prependSymbol(string $string, string $symbol)
{
	return $symbol . ltrim($symbol, $symbol);
}
