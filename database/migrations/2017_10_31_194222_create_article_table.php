<?php

use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTable extends Migration
{

    use MigrationCreateFieldTypes;
    protected $table = 'articles';
    protected $foreign = 'article_id';
    protected $tableLang = 'articles_lang';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->setTable($table);
            $table->increments('id');
            $table->dateTime('published_at')->nullable();
            $table->char('active', 4)->default(0);
            $table->string('url', 128)->unique();
            $table->string('image', 255)->nullable();
            $table->tinyInteger('sort')->default(0);
            $this->createNextPrevFields();
            $table->timestamps();
        });
        //
        Schema::create($this->tableLang, function (Blueprint $table) {
            $this->setTable($table);
            $table->integer($this->foreign)->unsigned();
            $table->integer('language_id');
            $table->string('name', 255)->nullable();
            $table->mediumText('description')->nullable();
            $table->text('except')->nullable();
            $table->text('video')->nullable();
            //
            $table->index($this->foreign);

            $table->index('language_id');

            $table->foreign($this->foreign)
                ->references('id')->on($this->table)
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableLang);
        Schema::dropIfExists($this->table);
    }
}
