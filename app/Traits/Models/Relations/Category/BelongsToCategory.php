<?php


namespace App\Traits\Models\Relations\Category;


use App\Models\Category\Category;

trait BelongsToCategory
{
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}