<?php


	namespace App\Helpers\Route;


	use App\Models\Model;
	use Arr;

	class CRUDLinkByModel
	{

		private $model;

		public function __construct(Model $model)
		{
			$this->model = $model;
		}

		private function routeByModelKey(string $module): string
		{
			$keys = [
				'users'           => 'users',
				'roles'           => 'roles',
				'settings'        => 'settings',
				'translate'       => 'translate',
				'meta'            => 'meta',
				'menus'           => 'menu',
				'categories'      => 'admin.category',
				//
				'pages'           => 'pages',
				'news'            => 'news',
				'feedback'        => 'feedback',
				'testimonials'    => 'admin.testimonials',
				'sliders'         => 'sliders',
				'faqs'            => 'faq',
				'classes'         => 'classes',
				'locations'       => 'locations',
				'promos'          => 'admin.promos',
				'extras'          => 'extras',
				'rates'           => 'rates',
				'cars'            => 'cars',
				'class_miles'     => 'admin.classes.mileage',
				'holidays'        => 'admin.holidays',
				'characteristics' => 'admin.characteristics',
				'blackouts'       => 'blackouts',
				'taxes'           => 'taxes',
			];

			return ((string)Arr::get($keys, $module));
		}

		private function getModel(): Model
		{
			return $this->model;
		}

		private function getPrimaryValue()
		{
			return $this->model->getPrimaryValue();
		}

		private function getModelKey(): string
		{
			return $this->model->getTable();
		}

		private function isValidLinkGenerate(): bool
		{
			if ($this->getModel()->exists && !$this->getPrimaryValue()) {
				return false;
			}

			if (!$route = $this->routeByModelKey($this->getModelKey())) {
				return false;
			}
			return true;
		}

		private function isInvalidLinkGenerate(): bool
		{
			return !$this->isValidLinkGenerate();
		}

		private function defaultLinkWhenInvalid(): string
		{
			return '';
		}

		private function routeKey(string $module, string $action = 'index'): string
		{
			return implode('.', [$this->routeByModelKey($module), $action]);
		}

		private function actionWithoutParameters(string $action): string
		{
			if ($this->isInvalidLinkGenerate()) {
				return $this->defaultLinkWhenInvalid();
			}
			return route(
				$this->routeKey($this->getModelKey(), $action)
			);
		}

		private function actionWithParameters(string $action): string
		{
			if ($this->isInvalidLinkGenerate()) {
				return $this->defaultLinkWhenInvalid();
			}
			return route(
				$this->routeKey($this->getModelKey(), $action)
				, $this->getPrimaryValue()
			);
		}

		public function index(): string
		{
			return $this->actionWithoutParameters('index');
		}

		public function create(): string
		{
			return $this->actionWithoutParameters('create');
		}

		public function store(): string
		{
			return $this->actionWithoutParameters('store');
		}

		public function show(): string
		{
			return $this->actionWithParameters('show');
		}

		public function edit(): string
		{
			return $this->actionWithParameters('edit');
		}

		public function update(): string
		{
			return $this->actionWithParameters('update');
		}

		public function destroy(): string
		{
			return $this->actionWithParameters('destroy');
		}
	}