<?php

namespace App\Traits\Controllers;


use Illuminate\Database\Eloquent\Model;

trait ResourceControllerHelpers
{

    protected function titleEdit(Model $model, $column = 'name'): string
	{
        $prefix = __('modules._.edit') . ' ';
        return $prefix . $model->getAttribute($column);
    }


	protected function titleShow(Model $model, $column = 'name'): string
	{
		$prefix = __('modules._.show') . ' ';
		return $prefix . $model->getAttribute($column);
	}

    protected function resourceRoute(string $action, $parameters = [])
    {
        $key = $this->routeKey ?? $this->key ?? '';
        $route = implode('.', [$key, $action]);
        $route = \Route::has($route) ? route($route, $parameters) : route('admin.index');
	    return $route;
    }

	protected function resourceAbility($action = 'index')
	{
		return $this->permissionKey . '.' . $action;
	}


}
