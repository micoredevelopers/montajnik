<?php

namespace App\Models;


use App\Contracts\HasImagesContract;
use App\Contracts\HasLocalized;
use App\Contracts\Models\HasNextPrevAttributes;
use App\Traits\Models\HasImages;
use App\Traits\Models\ImageAttributeTrait;
use App\Traits\Models\MainOrLangNameAttributeTrait;
use App\Traits\Models\PrevNextAttributes;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Article
 *
 * @property int $id
 * @property string|null $published_at
 * @property string $active
 * @property string $url
 * @property string|null $image
 * @property int $sort
 * @property int|null $prev_id Previous news id
 * @property int|null $next_id Next news id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed|string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \App\Models\Article $nextArticle
 * @property-read \App\Models\Article $prevArticle
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereNextId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article wherePrevId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereUrl($value)
 * @mixin \Eloquent
 */
class Article extends Model implements HasImagesContract, HasNextPrevAttributes, HasLocalized
{

    use ImageAttributeTrait;
    use HasImages;
    use PrevNextAttributes;
    use MainOrLangNameAttributeTrait;

    protected $hasOneLangArguments = [ArticleLang::class];

    protected $guarded = ['id'];

    public function prevArticle(): HasOne
    {
        return $this->hasOne(Article::class, 'prev_id')->with('lang');
    }

    public function nextArticle(): HasOne
    {
        return $this->hasOne(Article::class, 'next_id')->with('lang');
    }
}
