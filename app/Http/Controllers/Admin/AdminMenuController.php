<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\Admin\AdminMenuRepositoryContract;
use App\Models\Admin\Admin_menu;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Collection;
use Illuminate\View\View;

class AdminMenuController extends AdminController
{
	use Authorizable;
	use SaveImageTrait;

	protected $thumbnailWidth = false;

	protected $thumbnailHeight = false;

	private $name = 'Admin menu';

	protected $permissionKey = 'admin-menus';

	protected $routeKey = 'admin.admin-menus';

	protected $key = 'admin-menus';

	public function __construct()
	{
		parent::__construct();
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
	}

	/**
	 * @param Request $request
	 * @return Factory|View
	 */
	public function index(Request $request)
	{
		if ($request->has('seed')) {
			seedByClass('AdminMenuSeeder');
		}

		$title = $this->name;
		$this->setTitle($title);

		$this->addScripts('js/lib/jquery.nestable.js');

		$vars['list'] = Admin_menu::all();
		$data['content'] = view('admin.admin-menus.index', $vars);

		return $this->main($data);
	}

	/**
	 * @param Request $request
	 * @param AdminMenuRepositoryContract $adminMenuRepository
	 * @return RedirectResponse|Redirector
	 */
	public function updateAll(Request $request, AdminMenuRepositoryContract $adminMenuRepository)
	{
		$this->setFailUpdate();
		$adminMenuRepository->dropMenuCache();
		/** @var  $menus Collection */
		$menus = $adminMenuRepository->all();
		if ($menus->isNotEmpty()) {
			/** @var  $menu Admin_menu */
			foreach ($menus as $menu) {
				$inputManager = inputNamesManager($menu);
				$input = $request->input($inputManager->getNameInputRequest());
				if (!$input) {
					continue;
				}
				$input['active'] = $request->input($inputManager->getNameInputRequestByKey('active'), 0);
				$menu->fillExisting($input)->save();
			}
			$this->setSuccessUpdate();
		}

		return redirect()->back()->with($this->getResponseMessage());
	}

	/**
	 * @param Admin_menu $admin_menu
	 * @return Factory|View
	 */
	public function edit(Admin_menu $admin_menu)
	{
		$vars['edit'] = $admin_menu;
		$title = $this->titleEdit($admin_menu);
		$this->addBreadCrumb($title)->setTitle($title);
		$data['content'] = view('admin.admin-menus.edit')->with($vars);
		return $this->main($data);
	}

	/**
	 * @param Request $request
	 * @param Admin_menu $admin_menu
	 * @param AdminMenuRepositoryContract $adminMenuRepository
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update(Request $request, Admin_menu $admin_menu, AdminMenuRepositoryContract $adminMenuRepository)
	{
		$adminMenuRepository->dropMenuCache();
		//
		$input = $request->except('_token');

		$admin_menu->fillExisting($input);
		if ($admin_menu->save()) {
			$this->setSuccessUpdate();
			$this->saveImage($request, $admin_menu);
		}
		if ($request->has('saveClose')) {
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		return redirect()->back()->with($this->getResponseMessage());

	}

	/**
	 * @param Admin_menu $admin_menu
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function create(Admin_menu $admin_menu)
	{
		$attrs = [
			'url'       => '/admin/',
			'gate_rule' => 'view_',
			'active'    => 1,
		];
		$admin_menu->fillExisting($attrs);

		if ($admin_menu->save()) {
			$this->setSuccessStore();
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

	/**
	 * @param Admin_menu $admin_menu
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws \Exception
	 */
	public function destroy(Admin_menu $admin_menu)
	{
		if ($admin_menu->delete())
			$this->setSuccessDestroy();

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

}
