<?php


namespace App\Helpers\View;


class ConcreteColumnWidthCssFramework
{
	public const TYPE_BOOTSTRAP = 'bootstrap';
	private $type;

	public function __construct()
	{
		$this->setFrameworkType(self::TYPE_BOOTSTRAP);
	}

	public function getColNumByCount(int $count): string
	{
		/*
				if ($this->type === self::TYPE_BOOTSTRAP) {
					return $this->getBootstrapColNumByCount($count);
				}
		*/

		return $this->getBootstrapColNumByCount($count);
	}

	public function getBootstrapColNumByCount(int $count): string
	{
		$matches = [
			1 => 4,
		];
		$width = $matches[$count] ?? '';
		$widthStr = $width ? '-' . $width : '';
		return 'col' . $widthStr;
	}

	public function setFrameworkType(string $type): self
	{
		$this->type = $type;
		return $this;
	}

}