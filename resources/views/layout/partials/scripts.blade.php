{{--Seo данные для всех страниц, выводятся из админки, Настройки -> вкладка "seo" --}}
{!! getSetting('seo.footer-global-codes') !!}

@stack('js-prepend')
{{-- <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> --}}

@php if(!isset($_SERVER['HTTP_USER_AGENT']) || !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse')): @endphp
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script defer src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script defer src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<script src="{{ asset('libs/jquery-3.3.1/jquery.min.js') }}"></script>
<script src="{{ mix('js/index.js') }}" defer></script>
<script src="{{ asset('js/plugins/bootstrap-notify.js') }}" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" defer></script>
@php endif; @endphp

@include('partials.flash-message')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>

{{-- scripts from Controllers--}}
{!! $scripts ?? '' !!}
{{-- Scripts from another templates, which "extends" this --}}

@yield('javascript')

@stack('js')

<script>
  $(document).ready(function () {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  });
</script>