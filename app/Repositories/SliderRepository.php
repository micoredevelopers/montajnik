<?php

namespace App\Repositories;

use App\Helpers\Media\ImageSaver;
use App\Models\Category\Category;
use App\Models\Model;
use App\Models\Slider\Slider;
use App\Models\Slider\SliderItem;
use App\Models\Slider\SliderItemLang;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

/**
 * Class MetaRepository.
 */
class SliderRepository extends AbstractRepository {
    /**
     * @return string
     *  Return the model
     */
    public function model() {
        return Slider::class;
    }

    public function getListForAdmin(): LengthAwarePaginator {
        return $this->newQuery()->query->whereNull('sliderable_id')->paginate();
    }

    public function storeSlideItem(Request $request, Slider $slider): bool {
        $sliderItem = new SliderItem();
        $inputItem = $request->input(inputNamesManager($sliderItem)->getNameInputRequest(), []);

        $sliderItem->slider()->associate($slider);
        $result = $sliderItem->fillExisting($inputItem)->save();
        foreach ($this->getLanguagesList() as $language) {
			$sliderItemLang = new SliderItemLang();
            $inputItemLang = $request->input(inputNamesManager($sliderItemLang)->getNameInputRequest(), []);
            $sliderItemLang->fillExisting($inputItemLang);
            $sliderItemLang->sliderItem()->associate($sliderItem);
            $sliderItemLang->associateWithLanguage($language);
            $sliderItemLang->save();
        }
        return $result;
    }

    public function save(Request $request, Slider $slider, Model $belongsTo = null) {
        $sliderItem = new SliderItem();
        $input = $request->input(inputNamesManager($slider)->getNameInputRequest());
        $input['active'] = $request->input(inputNamesManager($slider)->getNameInputRequestByKey('active'), 0);
        $slider->fillExisting($input);
        if ($belongsTo) {
            $slider->sliderable()->associate($belongsTo);
        }
        if ($result = $slider->save()) {
            /** @var $language \App\Models\Language */
            if ($slider->wasRecentlyCreated) {
                $this->storeSlideItem($request, $slider);
            } elseif ($request->has($sliderItem->getTable())) {
                /** @var  $sliderItem SliderItem */
                foreach ($slider->items as $sliderItem) {
                    $itemKey = inputNamesManager($sliderItem)->getNameInputRequest();
                    if ($input = $request->input($itemKey)) {
                        $input['active'] = $request->input(inputNamesManager($sliderItem)->getNameInputRequestByKey('active'), 0);
                        if ($request->hasFile(inputNamesManager($sliderItem)->getNameInputRequestByKey('src'))) {
                            $input['src'] = (new ImageSaver($request, inputNamesManager($sliderItem)->getNameInputRequestByKey('src'), $sliderItem->getTable()))
                                ->setWithThumbnail(false)
                                ->saveFromRequest();
                        }
                        $sliderItem->fillExisting($input);
                        $sliderItem->save();
                        /** @var  $sliderItemLang SliderItemLang */
                        if ($sliderItemLang = $sliderItem->lang) {
                            $inputLang = $request->input(inputNamesManager($sliderItemLang)->getNameInputRequest(), []);
                            $sliderItem->lang->fillExisting($inputLang)->save();
                        }
                    }
                }
            }
        }
        return $result;
    }

    public function findByCategory(Category $category): ?Slider {
        if ($categorySlider = $category->slider) {
            $categorySlider->load('items.lang');
            return $categorySlider;
        }
        return null;
    }

    public function findByCategoryForDisplay(Category $category): ?Slider {
        $slider = $category->slider()->active()->first();
        if ($slider) {
            $this->loadWithScopes($slider);
        }
        return $slider;
    }

    private function loadWithScopes(Slider $slider) {
        $slider->load(['items' => function ($query) {
            $query->active();
        }, 'items.lang',
        ]);
        return $slider;
    }

    public function findById($id) {
        /** @var $slider Slider */
        $slider = $this->newQuery()->where('id', $id)->first();
        if ($slider) {
            $this->loadWithScopes($slider);
        }
        return $slider;
    }

}
