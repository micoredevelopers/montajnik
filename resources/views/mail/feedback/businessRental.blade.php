@extends('mail.feedback.layout')

@section('title')
	Business rental
@stop

@section('content')

    <?php /** @var $feedback \App\Models\Feedback */ ?>
	<table class="table">
		<tbody>
		<tr>
			<td>{{ getTranslate('forms.full-name') }}:</td>
			<td>{{ $feedback->getAttribute('name') }}</td>
		</tr>
		@if ($feedback->getAttribute('email'))
			<tr>
				<td>{{ getTranslate('forms.email') }}:</td>
				<td>{{ $feedback->email }}</td>
			</tr>
		@endif
		@if ($feedback->phone)
			<tr>
				<td>{{ getTranslate('forms.phone') }}:</td>
				<td>{{ $feedback->phone }}</td>
			</tr>
		@endif
		@if ($feedback->getAttribute('address'))
			<tr>
				<td>{{ getTranslate('forms.address') }}:</td>
				<td>{{ $feedback->getAttribute('address') }}</td>
			</tr>
		@endif
		@if ($feedback->getAttribute('message'))
			<tr>
				<td>{{ getTranslate('forms.message') }}:</td>
				<td>{{ $feedback->getAttribute('message') }}</td>
			</tr>
		@endif
		@if ($fields = $feedback->getAttribute('fields') AND is_array($fields))
			@foreach($fields as $name => $field)
				<tr>
					<td>{{ $name }}:</td>
					<td>{{ $field }}</td>
				</tr>
			@endforeach
		@endif
		@if ($feedback->referer)
			<tr>
				<td colspan="2" class="text-center">
					<a href="{{ $feedback->referer }}" target="_blank">Страница заявки</a>
				</td>
			</tr>
		@endif
		</tbody>
	</table>
@stop
