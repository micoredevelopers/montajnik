<?php

namespace App\Traits\Models;

trait HasImages
{
    /**
     * @return mixed
     */
	public function images()
	{
		return $this->morphMany('App\Models\Image', 'imageable');
	}

}