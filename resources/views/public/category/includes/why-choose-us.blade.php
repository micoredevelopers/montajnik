<?php /** @var $category \App\Models\Category\Category */ ?>
<section class="feedback d-lg-flex flex-row-reverse">
  <div class="feedback-img-box">
    <img class="w-100 lazy" data-src="{{ assetWebp('img/feedback.jpg') }}" alt="" width="1152" height="768"/>
  </div>
  <div class="feedback-desc-box text-center text-lg-left d-lg-flex justify-content-end align-items-center">
    <div class="feedback-desc">
      <h2 class="feedback-title">Почему клиенты выбирают нас?</h2>
      <p class="text_white mb-3 mb-lg-4 maxWdt350 feedback-text">Работаем четко, быстро, слаженно, с гарантией качества. Учитываем все
        пожелания клиента. Свяжитесь с нами для уточнения деталей.</p>
      <a href="" class="btn-custom mx-auto mx-lg-0 mr-lg-auto" data-toggle="modal" data-target="#feedBackModalCenter">Связаться</a>
    </div>
  </div>
</section>
