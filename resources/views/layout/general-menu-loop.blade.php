<?php /** @see \App\Http\Controllers\SiteController::main() */ ?>
<? /** @var $category \App\Models\Category\Category */ ?>
@php
    $nestingArr = $nestingArrData ?? [];
@endphp
<ul class="menu-dropdown sub">
    @foreach($categories as $category)
        <li class="menu-dropdown__item sub__item">
            <span class="menu-dropdown-link pl-{!! Arr::get($nestingArr, $nesting, 155)!!}">
              	<a href="{{ route('category.show', $category->url)}}">{{ $category->getCategoryName() }}</a>
				@if($category->allCategories->isNotEmpty())
                    <span class="menu-arrow"></span>
                @endif
        	</span>
            @if($category->allCategories->isNotEmpty() )
                @include('layout.general-menu-loop',['nesting' => ++$nesting, 'categories' => $category->allCategories])
                @php
                    --$nesting;
                @endphp
            @endif
        </li>
    @endforeach
</ul>
