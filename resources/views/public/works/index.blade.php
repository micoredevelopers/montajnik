<?php /** @see App\Http\Controllers\WorksController::show() */ ?>

<?php /** @var $work \App\Models\Work */ ?>
<?php /** @var $workCategory \App\Models\Category\Category */ ?>
<?php /** @var $images Collection */ ?>
<?php /** @var $currentCategory \App\Models\Category\Category */ ?>
<?php /** @var $loop object */ ?>

<main id="works">
  <section class="works-photo-slider-block">
    <div class="container">
      <div class="d-lg-flex flex-row-reverse justify-content-between align-items-start mb-20">
        <div class="works-drop d-none d-lg-block">
          <div class="works-drop-title d-flex justify-content-between align-items-center">
            {{$currentCategory->lang->name}}
            <span class="works-drop-arrow"><img src="{{asset('img/mp/arrow-white.svg')}}" alt="" width="7" height="12"/></span>
          </div>
          <ul class="works-drop-list">
            @include('public.works.partials.loop-works')
          </ul>
        </div>
        <div class="text-left">
          <h1 class="title" style="font-size:32px">
            <span class="decor-bg">{!! showMeta( getTranslate('work.works') ) !!}</span>
            - {{$currentCategory->lang->name}}
          </h1>
          @include('public.partials.breadcrumbs')
        </div>
      </div>
      <div class="works-drop mb-25 d-lg-none">
        <div class="works-drop-title d-flex justify-content-between align-items-center">
          {{$currentCategory->getCategoryName()}}
          <span class="works-drop-arrow"><img src="{{asset('img/mp/arrow-white.svg')}}" alt="" width="7" height="12"/></span>
        </div>
        <ul class="works-drop-list">
          @include('public.works.partials.loop-works')
        </ul>
      </div>
      @php
        $images = $work->images;
        $videos = collect($work->video);
      @endphp

      @if ($images->isNotEmpty())

        <div class="work-photo-slider-first mb-2">
          @foreach ($images as $image)
            @php
              $alt = translateFormat('work.work', ['%category%' => $currentCategory->getCategoryName(), '%number%' =>
              $loop->iteration,]);
            @endphp
            <div class="work-photo-slider-first__item">
              <img alt="{{ $alt }}" class="w-100" data-lazy="{{checkImageWebp(imgPathOriginal($image->image))}}" src="" width="1280" height="960"/>
            </div>
          @endforeach
        </div>
        <div class="work-photo-slider-second mb-20">
          @foreach ($images as $image)
            @php
              $alt = translateFormat('work.work', ['%category%' => $currentCategory->getCategoryName(), '%number%' =>
              $loop->iteration,]);
            @endphp
            <div class="work-photo-slider-second__item px-1">
              <img class="w-100" data-lazy="{{checkImageWebp($image->image)}}" alt="{{ $alt }}" src="" width="400" height="250"/>
            </div>
          @endforeach
        </div>
        @php
          $imagesContainerDefault = 'justify-content-between';
          $imagesContainerBiggerSix = 'justify-content-around justify-content-lg-between';
          $imagesContainerClass = ($images->count() > 6) ? $imagesContainerBiggerSix : $imagesContainerDefault;
        @endphp
        <div class="arrow-box d-flex align-items-center {{ $imagesContainerClass }} material-slider-arrow-box">
          <div class="arrow arrow_prev">
            @include('public.works.partials.svg.arrow-prev')
          </div>
          <div class="count-box">
            <span class="current">1</span>
            <span class="breeze-text">/</span>
            <span class="count" data-count="{{ $images->count() }}">{{ $images->count() }}</span>
          </div>
          <div class="arrow arrow_next">
            @include('public.works.partials.svg.arrow-next')
          </div>
        </div>

      @endif
    </div>
  </section>
  @if ($videos->isNotEmpty())
    <section class="works-video-slider-block">
      <div class="container">
        <div class="text-center mb-20">
          <h2 class="title"><span class="decor-bg">{{ getTranslate('work.works') }}</span></h2>
        </div>

        <div class="work-video-slider-first mb-2">

          @foreach ($videos as $video)
            <div class="work-video-slider-first__item position-relative">
                            <span class="play-logo">
                                <img class="w-100 play-logo-img" src="{{asset('img/playLogo.svg')}}" alt="" width="106" height="80"/>
                            </span>
              <img
                  src="{{getPreviewYoutubeFromLink($video)}}"
                  class="work-video-slider"
                  data-src="{{'https://www.youtube.com/embed/'.getLinkVideo($video).'/?autoplay=1'}}"
                  alt=""
              />
              <iframe
                  class="work-video-slider-iframe "
                  src=""
                  frameborder="0"
                  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen
              ></iframe>
            </div>
          @endforeach
        </div>
        <div class="work-video-slider-second mb-20">
          @foreach ($videos as $video)
            <div class="work-video-slider-second__item px-1">
              <img src="{{getPreviewYoutubeFromLink($video)}}" alt=""/>
            </div>
          @endforeach
        </div>
        @php
          $videosContainerClass = ($videos->count() > 6) ? 'justify-content-center' : 'justify-content-lg-between';
        @endphp
        <div class="arrow-box d-flex align-items-center justify-content-around mb-40 mb-lg-60 material-slider-arrow-box {{ $videosContainerClass }}">
          <div class="arrow arrow_prev">
            @include('public.works.partials.svg.arrow-prev')
          </div>
          <div class="count-box">
            <span class="current">1</span>
            <span class="breeze-text">/</span>
            <span class="count" data-count="{{ $videos->count() }}">{{ $videos->count() }}</span>
          </div>
          <div class="arrow arrow_next">
            @include('public.works.partials.svg.arrow-next')
          </div>
        </div>
      </div>
    </section>
  @endif
</main>
{!! editButtonAdmin(route('admin.works.edit', $work->id)) !!}
