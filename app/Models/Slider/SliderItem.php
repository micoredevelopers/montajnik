<?php


namespace App\Models\Slider;


use App\Contracts\HasLocalized;
use App\Models\Model;

/**
 * App\Models\Slider\SliderItem
 *
 * @property int $id
 * @property int $slider_id
 * @property bool $active
 * @property int|null $sort
 * @property string|null $link
 * @property string $type
 * @property string|null $src
 * @property-read \App\Models\Slider\Slider $slider
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereSliderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class SliderItem extends Model implements HasLocalized
{

	const TYPE_IMAGE = 'image';
	protected $hasOneLangArguments = [SliderItemLang::class];

	protected $guarded = [
		'id',
	];
	protected $casts = [
		'active' => 'boolean',
	];

	public $timestamps = false;

	public function slider()
	{
		return $this->belongsTo(Slider::class);
	}

	public function isTypeImage()
	{
		return $this->type === self::TYPE_IMAGE;
	}
}