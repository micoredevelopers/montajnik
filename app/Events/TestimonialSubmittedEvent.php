<?php

namespace App\Events;

use App\Models\Testimonial;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class TestimonialSubmittedEvent
{
	use Dispatchable, SerializesModels;
	/** @var Testimonial */
	private $testimonial;

	/**
	 * Create a new event instance.
	 *
	 * @param Testimonial $testimonial
	 */
	public function __construct(Testimonial $testimonial)
	{
		$this->testimonial = $testimonial;
	}

	public function getTestimonial(): Testimonial
	{
		return $this->testimonial;
	}

}
