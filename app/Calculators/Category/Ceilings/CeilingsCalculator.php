<?php


	namespace App\Calculators\Category\Ceilings;


	class CeilingsCalculator
	{
		/**
		 * @var CalculatorData
		 */
		private $data;
		/**
		 * @var CeilingsPrices
		 */
		private $prices;

		public function __construct(CalculatorDataContract $data, CeilingsPrices $prices)
		{
			$this->setData($data);
			$this->setPrices($prices);
		}

		/**
		 * @return CalculatorData
		 */
		public function getData(): CalculatorData
		{
			return $this->data;
		}

		/**
		 * @param CalculatorData $data
		 */
		public function setData(CalculatorData $data): void
		{
			$this->data = $data;
		}

		public function getPerimeterLength(): int
		{
			$length = $this->getData()->getLength();
			return ($length * 2);
		}

		public function getPerimeterWidth(): int
		{
			$width = $this->getData()->getWidth();
			return ($width * 2);
		}

		public function getPerimeter(): int
		{
			return $this->getPerimeterWidth() + $this->getPerimeterLength();
		}

		public function getSquare(): int
		{
			$length = $this->getData()->getLength();
			$width = $this->getData()->getWidth();
			return ($length * $width);
		}

		/**
		 * @return CeilingsPrices
		 */
		public function getPrices(): CeilingsPrices
		{
			return $this->prices;
		}

		/**
		 * @param CeilingsPrices $prices
		 */
		public function setPrices(CeilingsPrices $prices): void
		{
			$this->prices = $prices;
		}

		public function getCostCeilingType()
		{
			// Тип потолка, от площади, м2
			$perSquare = $this->getPrices()->getPriceType($this->getData()->getType());
			return $perSquare * $this->getSquare();
		}

		public function getCostGapless(): int
		{
			// Бесщелевые потолки, м2
			$perSquare = $this->getPrices()->getPriceGapless();
			return $perSquare * $this->getData()->getGapless();
		}

		public function getCostStarSky(): int
		{
			// Звездное небо, м2
			$perSquare = $this->getPrices()->getPriceStarSky();
			return $perSquare * $this->getData()->getStarSky();
		}

		public function getCostAngles(): int
		{
			// количество углов, больше 4, оплачивается отдельно, за шт
			$angles = $this->getData()->getAngles();
			if ($angles <= PropertiesCalculationSpecificContract::ANGLES_FREE_TO) {
				return 0;
			}
			$angles -= PropertiesCalculationSpecificContract::ANGLES_FREE_TO;
			$perAngle = $this->getPrices()->getPriceAngles();
			return $perAngle * $angles;
		}

		public function getCostLight(): int
		{
			// Количество светильников, за шт
			$perItem = $this->getPrices()->getPriceLight();
			return $perItem * $this->getData()->getLight();
		}

		public function getCostPipes(): int
		{
			// Количество труб, за шт
			$perItem = $this->getPrices()->getPricePipe();
			return $perItem * $this->getData()->getPipe();
		}

		public function getCostCurvature(): int
		{
			// Количество криволинейностей, за шт
			$perItem = $this->getPrices()->getPriceCurvature();
			return $perItem * $this->getData()->getCurvature();
		}

		public function getCostBaguette(): int
		{
			// Багет (резиночка), за м/п
			$withBaguette = $this->getData()->getBaguette();
			if (!$withBaguette){
				return 0;
			}
			$perItem = $this->getPrices()->getPriceBaguette();
			return $perItem * $this->getPerimeter();
		}

		public function getCostProfile(): int
		{
			// Тип профиля, за шт
			$perItem = $this->getPrices()->getPriceProfile($this->getData()->getProfile());
			return $perItem * $this->getPerimeter();
		}

		public function getTotal(): int
		{
			$total = 0;
			$total += $this->getCostCeilingType();
			$total += $this->getCostProfile();
			$total += $this->getCostGapless();
			$total += $this->getCostStarSky();
			$total += $this->getCostAngles();
			$total += $this->getCostLight();
			$total += $this->getCostPipes();
			$total += $this->getCostCurvature();
			$total += $this->getCostBaguette();
			return $total;
		}

	}