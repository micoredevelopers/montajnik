<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PartnerRequest;
use App\Models\Partner;
use App\Repositories\CategoryRepository;
use App\Repositories\PartnersRepository;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;

class PartnerController extends AdminController
{
    use Authorizable;
    use SaveImageTrait;

    private $name = 'Партнеры';

    protected $key = 'partners';

    protected $routeKey = 'admin.partners';

    protected $permissionKey = 'partners';

    public function __construct(CategoryRepository $categoryRepository)
    {
        parent::__construct();
        $this->addBreadCrumb($this->name, $this->resourceRoute('index'));
        $this->shareViewModuleData();
        \View::share('categories', $categoryRepository->all());
    }

    public function index(PartnersRepository $partnersRepository)
    {
        $this->setTitle($this->name);
        $vars['list'] = $partnersRepository->getListForAdmin();
        $data['content'] = view('admin.partners.index', $vars);
        return $this->main($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['content'] = view('admin.partners.create');
        return $this->main($data);
    }

    /**
     * @param PartnerRequest $request
     * @param Partner $partner
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PartnerRequest $request, Partner $partner)
    {
        $input = $request->except('image');
        if ($partner->fillExisting($input)->save()) {
            $this->setSuccessStore();
            $this->saveImage($request, $partner);
            $this->fireEvents();
        }
        if ($request->has('createOpen')) {
            return redirect($this->resourceRoute('edit', $partner->id))->with($this->getResponseMessage());
        }

        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
    }

    /**
     * @param $id
     * @param PartnersRepository $partnersRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, PartnersRepository $partnersRepository)
    {
        $edit = $partnersRepository->findForEdit($id);

        $title = $this->titleEdit($edit);
        $this->addBreadCrumb($title)->setTitle($title);

        $data['content'] = view('admin.partners.edit', compact(
            'edit'
        ));
        return $this->main($data);
    }

    /**
     * @param PartnerRequest $request
     * @param \App\Http\Controllers\Admin\Partner $partner
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PartnerRequest $request, Partner $partner)
    {
        $input = $request->except('image');
        //
        $this->saveImage($request, $partner);
        $partner->fillExisting($input);
        if ($partner->save()) {
            $this->setSuccessUpdate();
            $this->fireEvents();
        }

        if ($request->has('saveClose')) {
            return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
        }

        return redirect()->back()->with($this->getResponseMessage());
    }

    /**
     * @param Partner $partner
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Partner $partner)
    {
        if ($partner->delete()) {
            $this->photoModel->deleteImageStorage($partner->image);
            foreach ($partner->images as $image) {
                $this->photoModel->deleteImageStorage($image->image);
            }
            $this->setSuccessDestroy();
            $this->fireEvents();
        }

        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
    }

    private function fireEvents()
    {

    }
}
