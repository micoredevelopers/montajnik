<?php

namespace App\Http\Controllers\Admin\Category;


use App\Containers\Admin\Category\SearchDataContainer;
use App\Events\Admin\CategoriesChanged;
use App\Helpers\Media\ImageMeta;
use App\Http\Controllers\Admin\AdminController;
use App\Http\ImageThumbnail;
use App\Http\Requests\Admin\Category\CategoryRequest;
use App\Models\Admin\Photo;
use App\Models\Category\Category;
use App\Models\Category\CategoryLang;
use App\Models\Category\CategoryPage;
use App\Models\Language;
use App\Modules\Transfer\SliderTransfer;
use App\Repositories\CategoryRepository;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CategoryController extends AdminController
{
	use Authorizable;

	use SaveImageTrait;

	protected $routeKey = 'admin.category';

	protected $permissionKey = 'categories';

	protected $key = 'category';

	protected $name = 'Категории';

	/**
	 * @var CategoryRepository
	 */
	private $categoryRepository;

	public function __construct(CategoryRepository $categoryRepository)
	{
		parent::__construct();
		$this->categoryRepository = $categoryRepository;
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));

		$this->shareViewModuleData();

	}
	public function search(SearchDataContainer $dataContainer, Request $request){

		$this->setTitle('Поиск')->addBreadCrumb('Поиск');
		$dataContainer->setSearch((string)$request->get('search'));
		$list = $this->categoryRepository->search($dataContainer);
		$data['content'] = view('admin.category.search', compact('list'));
		return $this->main($data);
	}

	/**
	 * @return Factory|View
	 */
	public function index()
	{
		$this->setTitle($this->name);
		$this->addScripts('libs/nestable2/jquery.nestable.min.js');
		$this->addCss('libs/nestable2/jquery.nestable.min.css');
		$list = $this->categoryRepository->getCategories();
		$data['content'] = view('admin.category.index', compact('list'));
		return $this->main($data);
	}

	/**
	 * @return Factory|View
	 */
	public function create()
	{
		$vars['categories'] = $this->categoryRepository->getCategories();
		$data['content'] = view('admin.category.create')->with($vars);

		return $this->main($data);
	}

	public function store(CategoryRequest $request, Category $category)
	{
		$input = $request->all();
		if ($category->fillExisting($input)->save()) {
			$this->setSuccessStore();
			/** @var $language  Language */
			foreach ($this->languagesList as $language) {
				($categoryLang = new CategoryLang())->fillExisting($input);
				$categoryLang->associateWithLanguage($language);
				$categoryLang->category()->associate($category);
				$categoryLang->save();
			}
			event(new CategoriesChanged());
		}
		$this->saveImage($request, $category);

		if ($request->has('createOpen')) {
			return redirect($this->resourceRoute('edit', $category->getPrimaryValue()))->with($this->getResponseMessage());
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

	/**
	 * @param $id
	 * @param SliderTransfer $sliderTransfer
	 * @return Factory|RedirectResponse|\Illuminate\Routing\Redirector|View
	 */
	public function edit($id, SliderTransfer $sliderTransfer)
	{
		$vars['edit'] = $category = $this->categoryRepository->findForEdit((int)$id);
		if (!$category) {
			$this->setMessage(__('modules._.record-not-finded'));
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		$categories = $this->categoryRepository->getCategoryParents($category, false);
		/** @var  $categoryParent Category */
		foreach ($categories as $categoryParent) {
			$this->addBreadCrumb($categoryParent->getNameAttribute(), route(routeKey('category', 'edit'), $categoryParent->getPrimaryValue()));
		}

		$title = $this->titleEdit($category);
		$this->addBreadCrumb($title)->setTitle($title);
		$vars['slider'] = $sliderTransfer->getForm($category);

		$data['content'] = view('admin.category.edit', $vars)->with($vars);

		return $this->main($data);
	}

	public function update(CategoryRequest $request, Category $category, SliderTransfer $sliderTransfer)
	{
		$category = $this->categoryRepository->findForEdit((int)$category->id);

		$input = $request->all();
		$category->fillExisting($input);
		if ($category->save()) {
			if (($categoryPages = $category->categoryPages()->get())->isNotEmpty()) {
				/** @var  $categoryPage CategoryPage */
				foreach ($categoryPages as $index => $categoryPage) {
					$description = \Arr::get($request->get('description'), $index);
					$categoryPage->lang->setAttribute('description', $description);
					$categoryPage->lang->save();
				}
			}
			$this->setSuccessUpdate();
			$category->lang($this->langAdmin)->first()->fillExisting($input)->save();
			$this->saveImage($request, $category);

			$sliderTransfer->saveData($request, $category);

			event(new CategoriesChanged());
		}
		if ($request->has('saveClose')) {
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		return redirect()->back()->with($this->getResponseMessage());

	}

	/**
	 * @param Category $category
	 * @param Photo $photo
	 * @return RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws Exception
	 */
	public function destroy(Category $category, Photo $photo)
	{
		if ($category->categories()->count()) {
			$this->setMessage('Нельзя удалить категорию имеющую дочерние категории');
		} else if ($category->delete()) {
			$photo->deleteImageStorage($category->getImageAttribute());
			$this->setSuccessDestroy();
			event(new CategoriesChanged());
		}

		return back()->with($this->getResponseMessage());
	}


	public function nesting(Request $request): array
	{
		$requestData = $request->get('categories');
		Category::nestable($requestData);
		$this->setSuccessUpdate();

		event(new CategoriesChanged());

		return $this->getResponseMessageForJson();
	}

	public function show(Category $category)
	{

	}
}
