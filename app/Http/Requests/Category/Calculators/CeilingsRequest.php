<?php

	namespace App\Http\Requests\Category\Calculators;

	use App\Calculators\Category\Ceilings\CeilingsProfileTypes;
	use App\Calculators\Category\Ceilings\CeilingsTypes;
	use App\Http\Requests\AbstractRequest;

	class CeilingsRequest extends AbstractRequest
	{
		protected $toIntegers = [
			'length',
			'width',
			'baguette',
			'angles',
			'light',
			'pipe',
			'curvature',
		];

		/**
		 * Get the validation rules that apply to the request.
		 *
		 * @return array
		 */
		public function rules()
		{
			return [
				'perimeter_profile' => ['required', 'in:' . implodeComma(CeilingsProfileTypes::getTypesKeys())],
				'type'              => ['required', 'in:' . implodeComma(CeilingsTypes::getTypesKeys())],
				'length'            => ['required', 'int', 'gt:0'],
				'width'             => ['required', 'int', 'gt:0'],
				'gapless'           => ['required', 'int', 'gt:-1'],
				'star_sky'          => ['required', 'int', 'gt:-1'],
				'angles'            => ['required', 'int', 'gt:-1'],
				'light'             => ['required', 'int', 'gt:-1'],
				'pipe'             => ['required', 'int', 'gt:-1'],
				'curvature'             => ['required', 'int', 'gt:-1'],
			];
		}

		protected function mergeRequestValues()
		{
			$this->merge([
				'gapless'  => abs((int)$this->get('gapless')),
				'star_sky' => abs((int)$this->get('star_sky')),
			]);
		}

	}
