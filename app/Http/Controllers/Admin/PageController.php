<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PageRequest;
use App\Models\Admin\Photo;
use App\Models\Page;
use App\Models\PageLang;
use App\Repositories\PageRepository;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;
use Illuminate\Http\Request;

class PageController extends AdminController
{
	use SaveImageTrait;
	use Authorizable;

	private $name = 'Страницы';

	protected $key = 'pages';

	protected $permissionKey = 'pages';

	protected $routeKey = 'admin.pages';

	public function __construct()
	{
		parent::__construct();
		$this->name = __('modules.pages.title');
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
	}

	public function index(Request $request, PageRepository $pageRepository)
	{
		$this->setTitle($this->name);
		$vars['list'] = $pageRepository->getListForAdmin($request);
		$vars['request'] = $request;

		$data['content'] = view('admin.pages.index', $vars);
		return $this->main($data);
	}

	/**
	 * @param PageRepository $pageRepository
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create(PageRepository $pageRepository)
	{
		$pageTypes = Page::getPageTypes();
		$list = $pageRepository->getForAdmin();
		$data['content'] = view('admin.pages.create')->with(compact('pageTypes', 'list'));
		return $this->main($data);
	}

	/**
	 * @param PageRequest $request
	 * @param Page $page
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store(PageRequest $request, Page $page)
	{
		$input = $request->except('image');
		if ($page->fillExisting($input)->save()) {
			$this->setSuccessStore();
			foreach ($this->languagesList as $language) {
				$pageLang = new PageLang();
				$pageLang->associateWithLanguage($language);
				$pageLang->page()->associate($page);
				$pageLang->fillExisting($input)->save();
			}
			$this->saveImage($request, $page);
		}

		$redirectTo = $request->has('createOpen')
			? $this->resourceRoute('edit', $page->getPrimaryValue())
			: $this->resourceRoute('index');

		return redirect($redirectTo)->with($this->getResponseMessage());
	}


	/**
	 * @param Page $page
	 * @param PageRepository $pageRepository
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit(Page $page, PageRepository $pageRepository)
	{
		$pageTypes = Page::getPageTypes();
		$vars['edit'] = $page;
		$list = $pageRepository->getForAdmin($page->id);
		$title = $this->titleEdit($page->lang, 'title');
		$this->addBreadCrumb($title)->setTitle($title);

		$data['content'] = view('admin.pages.edit', $vars)->with(compact('pageTypes', 'list'));
		return $this->main($data);
	}

	/**
	 * @param PageRequest $request
	 * @param Page $page
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update(PageRequest $request, Page $page)
	{
		$input = $request->except('image');
		$page->fillExisting($input);
		if ($page->save()) {
			$page->lang($this->langAdmin)->first()->fillExisting($input)->save();
			$this->setSuccessUpdate();
		}
		$this->saveImage($request, $page);

		if ($request->hasFile('images')) {
			$this->saveAdditionalImages($page, $request);
		}

		if ($request->has('saveClose')) {
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		return redirect()->back()->with($this->getResponseMessage());
	}

	/**
	 * @param Page $page
	 * @param Photo $photo
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws \Exception
	 */
	public function destroy(Page $page, Photo $photo)
	{
		if ($page->delete()) {
			$photo->deleteImageStorage($page->getImageAttribute());
			$this->setSuccessDestroy();
		}
		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

}
