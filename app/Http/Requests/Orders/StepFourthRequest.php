<?php

namespace App\Http\Requests\Orders;

use App\Helpers\Order\OrderStepsFields;
use App\Helpers\Order\OrderStepsRules;

class StepFourthRequest extends AbstractOrderRequest
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getFillableRequestFields($except = []): array
	{
		$fields = array_flip(OrderStepsFields::getFieldsStepFourth($this));
		$fields = $this->exceptFields($fields, $except);
		$fields = array_flip($fields);
		return $fields;
	}

	/**
	 * @return array
	 */
	public function rules()
	{
		return OrderStepsRules::getRulesStepFourth($this);
	}
}
