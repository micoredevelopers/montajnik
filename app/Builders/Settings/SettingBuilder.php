<?php

	namespace App\Builders\Settings;


	use Illuminate\Support\Str;

	class SettingBuilder implements \ArrayAccess
	{
		private $key = '';

		private $value = '';

		private $type = 'text';

		private $displayName = '';

		public function setKey(string $key): self
		{
			$this->key = $key;
			return $this;
		}

		public function setType(string $type): self
		{
			$this->type = $type;
			return $this;
		}

		public function setValue(string $value): self
		{
			$this->value = $value;
			return $this;
		}

		public function setDisplayName(string $displayName): self
		{
			$this->displayName = $displayName;
			return $this;
		}

		public function build()
		{
			return [
				'key'          => $this->key,
				'value'        => $this->value,
				'type'         => $this->type,
				'display_name' => $this->displayName,
			];
		}

		public function offsetExists($offset)
		{
			return property_exists($this, $offset);
		}

		public function offsetGet($offset)
		{
			return $this->offsetExists($offset) ? $this->{$offset} : null;
		}

		public function offsetSet($offset, $value)
		{
			$method = Str::camel('set'. ucfirst($offset));
			if (method_exists($this, $method)){
				$this->$method($value);
			}
		}

		public function offsetUnset($offset)
		{
			if ($this->offsetExists($offset)) {
				$this->{$offset} = null;
			}
		}


	}