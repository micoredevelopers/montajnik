@if ($paginator->hasPages())
	<div class="col-12 col-lg-5 mb-30">
		<div class="pagin d-flex justify-content-left align-items-center mx-0">
		@if ($paginator->onFirstPage())
			<a href="" class="pagin-arrow pagin-prev" disabled>
				<img class="" src="{{asset('img/mp/arrow.svg')}}" alt="" />
			</a>
		@else
			<a href="{{ $paginator->previousPageUrl() }}" class="pagin-arrow pagin-prev">
				<img class="" src="{{asset('img/mp/arrow.svg')}}" alt="" />
			</a>
		@endif



			<div class="pagin-num-box d-flex justify-content-between mx-4">
			{{-- Pagination Elements --}}
			@foreach ($elements as $element)
				{{-- "Three Dots" Separator --}}
				@if (is_string($element))
					<li><span class="pagination-ellipsis"><span>{{ $element }}</span></span></li>
				@endif

				{{-- Array Of Links --}}
				@if (is_array($element))
					@foreach ($element as $page => $url)
						@if ($page == $paginator->currentPage())
								<a class="pagin-num pagin-num_active" href="">{{ $page }}</a>

						@else
								<a  class="pagin-num" href="{{ $url }}">{{ $page }}</a>

						@endif
					@endforeach
				@endif
			@endforeach
			</div>
			@if ($paginator->hasMorePages())
				<a href="{{ $paginator->nextPageUrl() }}" class="pagin-arrow pagin-next">
					<img class="" src="{{asset('img/mp/arrow.svg')}}" alt="" />
				</a>
			@else
				<a href="" class="pagin-arrow pagin-next" disabled>
					<img class="" src="{{asset('img/mp/arrow.svg')}}" alt="" />
				</a>
			@endif
		</div>
	</div>
@endif

