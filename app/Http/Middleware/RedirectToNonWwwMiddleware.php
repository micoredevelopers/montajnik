<?php


namespace App\Http\Middleware;

use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class RedirectToNonWwwMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 * @return mixed
	 */
	public function handle($request, \Closure $next)
	{
		if (Str::startsWith($request->header('host'), 'www.')) {
			$to = Str::replaceFirst('www.', '', url($request->path()));
			return redirect($to, Response::HTTP_MOVED_PERMANENTLY);
		}

		return $next($request);
	}
}