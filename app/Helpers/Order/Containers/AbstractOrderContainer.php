<?php


namespace App\Helpers\Order\Containers;

use App\Traits\SetterAndGetterMethods;

class AbstractOrderContainer
{
	use SetterAndGetterMethods;

	public function __construct()
	{
	}

	/** get all properties, which have getter */
	public function all(): array
	{
		$properties = collect(get_object_vars($this));
		$properties = $properties->filter(function ($value, $key) {
			return $this->hasGetter($key);
		})->keyBy(function ($value, $key) {
			$key = \Str::snake($key);
			return $key;
		});
		return $properties->toArray();
	}

	protected function fillFromArray(array $array)
	{
		foreach (\Arr::wrap($array) as $key => $value) {
			$this->setField($key, $value);
		}
	}

	protected function getField(string $key)
	{
		if ($this->hasGetter($key)) {
			return $this->{$this->getNameGetter($key)}();
		}
		return null;
	}

	protected function setField(string $key, $value)
	{
		if ($this->hasSetter($key)) {
			return $this->{$this->getNameSetter($key)}($value);
		}
		return null;
	}

}