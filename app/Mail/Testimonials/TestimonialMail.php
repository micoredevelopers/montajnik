<?php

namespace App\Mail\Testimonials;

use App\Mail\MailAbstract;

class TestimonialMail extends MailAbstract
{
	protected $testimonial;

    /**
     * Testimonial constructor.
     * @param \App\Models\Testimonial $testimonial
     */
	public function __construct(\App\Models\Testimonial $testimonial)
	{
		parent::__construct();
		$this->testimonial = $testimonial;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		debugInfo($this->getEmailTo());
		return $this->from($this->getEmailFrom(), $this->getNameFrom())
			->to($this->getEmailTo())
			->view('mail.testimonials.testimonial')
            ->with(['testimonial' => $this->testimonial]);
	}
}
