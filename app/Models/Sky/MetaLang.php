<?php

namespace App\Models\Sky;

use Illuminate\Support\Collection;

/**
 * App\Models\Sky\MetaLang
 *
 * @property int $meta_id
 * @property string|null $title
 * @property string|null $body
 * @property string|null $keywords
 * @property string|null $description
 * @property string $excerpt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\MetaLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\MetaLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\AbstractSkyModel onlyMain()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\MetaLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\MetaLang whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\MetaLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\MetaLang whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\MetaLang whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\MetaLang whereMetaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\MetaLang whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class MetaLang extends AbstractSkyModel
{

    protected $primaryKey = 'meta_id';

    protected $table = 'ru_meta';

    protected $unsanitizeFields = ['body', 'title', 'keywords', 'description', 'excerpt'];

    protected $replaceAttributesKeys = [
        'excerpt' => 'text_top',
        'body'    => 'text_bottom',
    ];

    public $timestamps = false;

    public function remake()
    {
        $comments = self::all();
        $this->unsanitizeList($comments);
    }


}
