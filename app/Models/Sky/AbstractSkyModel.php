<?php

namespace App\Models\Sky;


use App\Helpers\Miscellaneous\Strings;
use App\Models\Model;
use Illuminate\Support\Collection;

/**
 * App\Models\Sky\AbstractSkyModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\AbstractSkyModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\AbstractSkyModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\AbstractSkyModel onlyMain()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\AbstractSkyModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class AbstractSkyModel extends Model
{
    protected $relationChildrensAttribute = 'childrens';
    protected $replaceRelations = [];
    protected $unsanitizeFields = [];
    protected $replaceAttributesKeys = [];

    protected $connection = 'mysql_sky';

    public function __construct()
    {
        parent::__construct();
    }

    protected function unSanitize(string $value)
    {
        return Strings::unSanitize($value);
    }

    protected function unsanitizeList(Collection $collection)
    {
        $collection->map(function (AbstractSkyModel $model) {
            foreach ($this->unsanitizeFields as $attribute) {
                $model->setAttribute($attribute, $this->unSanitize((string)$model->getAttribute($attribute)));
            }
            $model->save();
            return $model;
        });
    }

    protected function remakeColumnsName(Collection $collection, bool $asArray)
    {
        $collection->map(function (AbstractSkyModel $model) use ($asArray) {
            foreach ($this->replaceAttributesKeys as $old => $attribute) {
                $model->setAttribute($attribute, $model->getAttribute($old));
            }
            if ($childrens = ($model->{$this->relationChildrensAttribute} ?? null)) {
                $this->remakeColumnsName($childrens, $asArray);
            }

            \Arr::forget($model, array_keys($this->replaceAttributesKeys));
            return $model;
        });

        if ($asArray) {
            $collection = $collection->toArray();
        }
        return $collection;
    }

    protected function remakeRelationNames(Model $model)
    {
        foreach ($this->replaceRelations as $old => $relation) {
            try{
                $model->setRelation($relation, $model->getRelation($old));
                \Arr::forget($model, $old);
            } catch (\Exception $e){
                continue;
            }
        }
    }

    public function scopeOnlyMain($query)
    {
        return $query->whereNull('sub');
    }


}
