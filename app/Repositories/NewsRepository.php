<?php

namespace App\Repositories;

use App\Models\News;
use App\Scopes\PublishedAtOrderScope;
use App\Traits\Repositories\RebuildNextAndPrev;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class NewsRepository.
 */
class NewsRepository extends AbstractRepository
{
	use RebuildNextAndPrev;

	/**
	 * @return string
	 *  Return the model
	 */
	public function model()
	{
		return News::class;
	}

	public function getNewForMainPage(): Collection
	{
		News::initScopesPublic();
		$newChecked = $this->newQuery()->WhereIsPublished()->where('on_main', 1)->first();
		if (!$newChecked AND getSetting('main.fallback-news-last')) {
			$newChecked = $this->newQuery()->WhereIsPublished()->first();
		}
		/** @var  $news */
		$news = $newChecked ? collect([$newChecked]) : collect([]);
		return $news;
	}

	public function getListForAdmin(): LengthAwarePaginator
	{
		/** @var  $list */
		$list = News::GetLang()->withGlobalScope('orderPublish', new PublishedAtOrderScope())->paginate();
		return $list;
	}

	/**
	 * @param $id
	 * @return News
	 */
	public function findForEdit($id): ?News
	{
		/** @var  $edit News*/
		$edit = News::GetLang()->find($id);
		return $edit;
	}

	public function allQuery(array $columns = ['*'])
	{
		return News::orderBy('published_at', 'desc')->select($columns);
	}

	public function all(array $columns = ['*'])
	{
		return $this->allQuery($columns)->get();
	}

	public function findByUrl(string $url): News
	{
		News::initScopesPublic();
		$new = $this->newQuery()->with(['lang', 'prevNew', 'nextNew'])->whereUrl($url)->firstOrFail();
		return $new;
	}


	public function getListForPublic(): LengthAwarePaginator
	{
		News::initScopesPublic();
		/** @var  $news */
		$news = $this->newQuery()->with(['lang'])->paginate(12);
		return $news;
	}

	public function getAllForNextPrev(): Collection
	{
		return $this->allQuery()->where('active', 1)->get();
	}

}
