<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Repositories\NewsRepository;

class NewsController extends SiteController {

    public function __construct() {
        parent::__construct();
        $this->addBreadCrumb(getTranslate('news.news'), route('news.index'));
    }

    public function index(NewsRepository $newsRepository) {
        $news = $newsRepository->getListForPublic();
        $this->setNextPrevLinkForPagination($news);

        $page = $news->currentPage();

        $title = getTranslate('news.meta_title');
        $description = getTranslate('news.meta_description');
        if (1 < $page) {
            $title = str_replace('%page%', $page, getTranslate('news.meta_title_paged'));
            $description = str_replace('%page%', $page, getTranslate('news.meta_description_paged'));
        }

        $this->setTitle($title);
        $this->setDescription($description);

        $data['content'] = view('public.news.index')->with(compact('news'));
        return $this->main($data);
    }

    public function show($url, NewsRepository $newsRepository) {
        $new = $newsRepository->findByUrl($url);
        $this->setTitle($new->name)->addBreadCrumb($new->name, route('news.show', $new->url));
        $data['content'] = view('public.news.show', compact('new'));
        return $this->main($data);
    }

}
