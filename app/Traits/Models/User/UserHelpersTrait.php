<?php

namespace App\Traits\Models\User;


use App\Models\Model;

trait UserHelpersTrait
{
	public function groupExists($group)
	{
		return in_array($group, $this->groups);
	}

	public function isCompany()
	{
		return (int)$this->getAttribute('is_company');
	}

	public function isPersonal()
	{
		return !$this->isCompany();
	}

	private function getDiscount(): ?Model
	{
		$discount = null;
		if ($this->discount AND $this->discount->value) {
			$discount = $this->discount;
		} elseif ($this->userGroup AND $this->userGroup->value) {
			$discount = $this->userGroup;
		}
		return $discount;
	}

	public function getDiscountValue(): float
	{
		if ($discount = $this->getDiscount()) {
			return (float)\Arr::get($discount, 'value');
		}
		return 0;
	}

	public function isUserDiscountPercent(): bool
	{
		if ($discount = $this->getDiscount()) {
			return (int)\Arr::get($discount, 'percent');
		}
		return false;
	}
}
