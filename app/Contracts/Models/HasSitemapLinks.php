<?php


namespace App\Contracts\Models;


interface HasSitemapLinks
{
	public function getSiteMapLinks();
}