<?php

namespace App\Observers\Admin;

use App\Models\Category\CategoryPage;
use App\Models\Category\CategoryPageLang;
use App\Models\Language;
use App\Repositories\Admin\LanguageRepository;

class CategoryPageObserver
{
    /**
     * @var \Illuminate\Support\Collection
     */
    private $languages;

    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languages = $languageRepository->getForCreateEntity();
    }

    /**
     * Handle the category page "created" event.
     *
     * @param \App\Models\Category\CategoryPage $categoryPage
     * @return void
     */
    public function created(CategoryPage $categoryPage)
    {
        if (!$categoryPage->lang) {
            /** @var  $language Language*/
            foreach ($this->languages as $language) {
                $categoryPageLang = new CategoryPageLang();
                $categoryPageLang->associateWithLanguage($language);
                $categoryPage->lang()->save($categoryPageLang);
            }
        }
    }

    /**
     * Handle the category page "updated" event.
     *
     * @param \App\Models\Category\CategoryPage $categoryPage
     * @return void
     */
    public function updated(CategoryPage $categoryPage)
    {
        //
    }

    /**
     * Handle the category page "deleted" event.
     *
     * @param \App\Models\Category\CategoryPage $categoryPage
     * @return void
     */
    public function deleted(CategoryPage $categoryPage)
    {
        //
    }

    /**
     * Handle the category page "restored" event.
     *
     * @param \App\Models\Category\CategoryPage $categoryPage
     * @return void
     */
    public function restored(CategoryPage $categoryPage)
    {
        //
    }

    /**
     * Handle the category page "force deleted" event.
     *
     * @param \App\Models\Category\CategoryPage $categoryPage
     * @return void
     */
    public function forceDeleted(CategoryPage $categoryPage)
    {
        //
    }
}
