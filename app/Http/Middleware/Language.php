<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Language
{
	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 * @param string|null $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$languageList = \App\Models\Language::active(1)->get();
		if ($locale = getCurrentLocale()) {
			$language = $languageList->where('key', $locale)->first();
			if ($language) {
				\App\Helpers\LanguageHelper::setLanguageId((int)$language->id);
			}
		}
		if (!getLang()) {
			$language = $languageList->where('default', 1)->first();
			if (!$language) {
				abort(404);
			}
		} else {
			$language = $languageList->where('id', getLang())->first();
		}
		\App\Helpers\LanguageHelper::setLanguageId((int)$language->id);

		return $next($request);
	}
}
