<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAuthenticatedAdmin
{
	public function handle($request, Closure $next)
	{
		if (Auth::check() && (!isAdmin() && !isSuperAdmin())) {
			Auth::logout();
			return redirect()->route('admin.login');
		}

		return $next($request);
	}
}
