$(document).ready(function (e) {
  function dropToggle() {
    $(".works-drop-list").slideUp();
    $(".works-drop-title").click(function() {
      $(this)
          .siblings(".works-drop-list")
          .slideToggle(300);
    });
  }

  dropToggle();
  function slickEvents(slider, currentEl, countEl) {
    slider
        .on("init", function(event, slick) {
          const currentSlide = $(slick.$slides[slick.currentSlide]).index();
          // console.log(currentSlide);
          const count = $(slick.slideCount);
          currentEl.text(currentSlide + 1);
          countEl.text(count[0]);
        })
        .on("beforeChange", function(event, slick, currentSlide, nextSlide) {
          currentEl.text(nextSlide + 1);
        });
  }
  slickEvents(
      $(".work-photo-slider-first"),
      $(".works-photo-slider-block .current"),
      $(".works-photo-slider-block .count")
  );

  $(".work-photo-slider-first").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: ".work-photo-slider-second",
    prevArrow: $(".works-photo-slider-block .arrow_prev"),
    nextArrow: $(".works-photo-slider-block .arrow_next")
  });
  $(".work-photo-slider-second").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: ".work-photo-slider-first",
    focusOnSelect: true,
    arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4
        }
      }
    ]
  });

  slickEvents(
      $(".work-video-slider-first"),
      $(".works-video-slider-block .current"),
      $(".works-video-slider-block .count")
  );

  $(".work-video-slider-first").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    prevArrow: $(".works-video-slider-block .arrow_prev"),
    nextArrow: $(".works-video-slider-block .arrow_next"),
    asNavFor: ".work-video-slider-second"
  });
  $(".work-video-slider-second").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: ".work-video-slider-first",
    arrows: false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4
        }
      }
    ]
  });

  function setIframe() {
    function getTarget(target) {
      if (target.classList.contains("work-video-slider")) {
        return $(target);
      } else if (target.classList.contains("play-logo-img")) {
        return $(target)
            .parent()
            .siblings(".work-video-slider");
      } else {
        return false;
      }
    }
    function getParametrs(elem) {
      return {
        width: elem.width(),
        height: elem.height(),
        url: elem.attr("data-src")
      };
    }

    function showIframe({ width, height, url }, elem, img, icon) {
      elem.width(width);
      elem.height(height);
      elem.attr("src", url);
      elem.show();
      $(img).hide();
      $(icon).hide();
    }
    $(".work-video-slider-first").click(function({ target }) {
      const elemTarget = getTarget(target);
      const parametrs = getParametrs(elemTarget);
      const iframe = $(target)
          .parents(".slick-active")
          .find("iframe");
      const icon = $(target)
          .parents(".slick-active")
          .find(".play-logo-img");
      showIframe(parametrs, iframe, elemTarget, icon);
    });
    $(".work-video-slider-second").click(function() {
      $(".work-video-slider-first .slick-active")
          .find("iframe")
          .attr("src", "")
          .hide();
      $(".work-video-slider-first .slick-active")
          .find("img")
          .show();
    });
  }

  setIframe();

})