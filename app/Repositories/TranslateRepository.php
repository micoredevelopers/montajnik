<?php

namespace App\Repositories;

use App\Models\Translate;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class TranslateRepository
{
    /**
     * @param Request $request
     * @return Collection
     */
    public function getForAdminDisplay(Request $request): Collection
    {
        $query = Translate::GetLang()->with(['user', 'deletedBy'])->latest();
        if ($request->has('with_trashed')) {
            $query->withTrashed();
        }
        if ($search = $request->get('search')) {
            $query->where(static function (Builder $builder) use ($search) {
                $builder->where('key', 'like', '%' . $search . '%')
                    ->orWhere('value', 'like', '%' . $search . '%')
                    ->orWhere('group', 'like', '%' . $search . '%');
            });
        }
		return $query->get();

    }
}
