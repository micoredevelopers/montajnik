<?php

namespace App\Observers;

use App\Models\Testimonial;
use App\Models\TestimonialType;

class TestimonialObserver
{

	public function creating(Testimonial $testimonial)
	{
		try{
			if (!$testimonial->type_id){
				$testimonial->setAttribute('type_id', TestimonialType::TYPE_IMAGE_ID);
			}
			if (!$testimonial->getAttribute('date')){
				$testimonial->setAttribute('date', now());
			}
		} catch (\Exception $e){
		    logger()->error($e);
		}
	}
    /**
     * Handle the testimonial "created" event.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return void
     */
    public function created(Testimonial $testimonial)
    {
        //
    }

    /**
     * Handle the testimonial "updated" event.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return void
     */
    public function updating(Testimonial $testimonial)
    {
		try{
			if (!$testimonial->getAttribute('date')){
				$testimonial->setAttribute('date', now());
			}
		} catch (\Exception $e){
		    logger()->error($e);
		}
    }

    /**
     * Handle the testimonial "deleted" event.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return void
     */
    public function deleted(Testimonial $testimonial)
    {
        //
    }

    /**
     * Handle the testimonial "restored" event.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return void
     */
    public function restored(Testimonial $testimonial)
    {
        //
    }

    /**
     * Handle the testimonial "force deleted" event.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return void
     */
    public function forceDeleted(Testimonial $testimonial)
    {
        //
    }
}
