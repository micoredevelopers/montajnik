<?php

namespace App\Models;

use App\Traits\EloquentMultipleForeignKeyUpdate;
use App\Traits\Models\BelongsToLanguage;

/**
 * App\Models\NewsLang
 *
 * @property int $news_id
 * @property int $language_id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $except
 * @property array|null $video
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereSubDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereVideo($value)
 * @mixin \Eloquent
 * @property int $article_id
 * @property-read \App\Models\Article $article
 * @property-read \App\Models\Language $language
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleLang whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleLang whereExcept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 */
class ArticleLang extends Model
{
    use BelongsToLanguage;
    use EloquentMultipleForeignKeyUpdate;

    public $table = 'articles_lang';

    protected $guarded = [];

	protected $casts = [
		'video' => 'array'
	];

	public $incrementing = false;

	public $timestamps = false;

	protected $primaryKey = ['article_id', 'language_id'];


    public function article()
    {
        return $this->belongsTo(Article::class);
	}

}
