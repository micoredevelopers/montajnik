<?php

	namespace App\Http\Requests\Admin;

	use App\Contracts\Requests\RequestParameterModelable;
	use App\Helpers\Validation\ValidationMaxLengthHelper;
	use App\Http\Requests\AbstractRequest;
	use App\Models\Meta;
	use App\Traits\Requests\Helpers\GetActionModel;

	class MetaRequest extends AbstractRequest implements RequestParameterModelable
	{
		protected $checkboxes = [
			'active'
		];
		protected $requestKey = 'meta';

		use GetActionModel;

		/**
		 * Get the validation rules that apply to the request.
		 *
		 * @return array
		 */
		public function rules()
		{
			$rules = [
				'h1'               => ['max:255'],
				'meta_title'       => ['max:255'],
				'meta_keywords'    => ['max:500'],
				'meta_description' => ['max:1000'],
				'url'              => ['max:160', 'unique:meta,url'],
				'header'           => ['max:65000'],
				'footer'           => ['max:65000'],
				'text_top'         => ['max:65000'],
				'text_text_bottom' => ['max:' . ValidationMaxLengthHelper::MEDIUMTEXT],
			];

			/** @var $meta Meta */
			if ($this->isActionUpdate() && $meta = $this->getActionModel()) {
				$rules['url'] = ['required', 'unique:meta,url,' . $meta->getPrimaryValue(), 'max:160'];
			}

			return $rules;
		}


		protected function mergeRequestValues()
		{
			if ($this->isActionUpdate()){
				return;
			}
			$url = Meta::makeUrlClear($this->get('url'));
			$this->merge(['url' => $url]);
		}

		public function messages()
		{
			$metaExisted = Meta::findByUrl($this->get('url'));
			if (!$metaExisted) {
				return [];
			}
			return [
				'url.unique' => sprintf('Url already exists, <a href="%s" class="btn btn-success">Edit</a>', route('admin.meta.edit', $metaExisted->id)),
			];
		}


	}
