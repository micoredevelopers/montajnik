@php
    $messages = [];
    if (session('success')) $messages[] = ['message' => session('success'), 'status' => 'success'];
    if (session('message')) $messages[] = ['message' => session('message'), 'status' => 'info'];
    if (session('error')) $messages[] = ['message' => session('error'), 'status' => 'danger'];
    if ($errors->any()){
        foreach ($errors->all() as $error){
             $messages[] = ['message' => $error, 'status' => 'danger'];
        }
    }
@endphp
@if ($messages)
    <script type="text/javascript" defer>
        $(document).ready(function () {
            var messages = @json($messages, JSON_PRETTY_PRINT );
            messages.forEach(function (item) {
                $.notify(item.message, {
                    type: item.status,
                    mouse_over: 'pause',
                    delay: 15 * 1000,
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                });
            });
        });
    </script>
@endif
