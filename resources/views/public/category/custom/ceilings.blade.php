@extends('public.category.custom.layout.custom-category-layout')

@section('beforeSubcategories')

@stop

@section('subcategories')
  <div class="category-advantages">
    <div class="container">
      <div class="breadcrumbs-category">
        @include('public.partials.breadcrumbs')
      </div>
    </div>
  </div>
  @include('public.category.includes.subcategories')

  {{--    @include('public.category.includes.subcategories-without-child')--}}

  @include('public.index.includes.advantages')
  @php if(!isset($_SERVER['HTTP_USER_AGENT']) || !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse')): @endphp
  <section class="category-desc position-relative">
    <div id="particles-js"></div>
    <div class="container pt-4 py-lg-5 bg-white position-relative">
      <h1 class="text-center">{{ showMeta($category->getCategoryName()) }}</h1>
      <div class="row">
        <div class="col-lg-8 offset-lg-2">
          <div class="decodeWrapper">
            {!! $category->getDescriptionByIndex(0) !!}
          </div>
        </div>
        <div class="col-lg-8 offset-lg-2">
          @include('public.category.includes.prices')
        </div>
      </div>
    </div>
  </section>
  @php endif @endphp
@stop
