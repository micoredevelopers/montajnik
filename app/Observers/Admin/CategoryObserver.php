<?php

namespace App\Observers\Admin;

use App\Models\Category\Category;
use App\Models\Category\CategoryPage;
use App\Models\Slider\Slider;
use App\Repositories\SliderRepository;
use Illuminate\Http\Request;

class CategoryObserver
{
	/** @var SliderRepository */
	private $sliderRepository;

	public function __construct(SliderRepository $sliderRepository)
	{
		$this->sliderRepository = $sliderRepository;
	}

	/**
	 * Handle the category "created" event.
	 *
	 * @param \App\Models\Category\Category $category
	 * @return void
	 */
	public function created(Category $category)
	{
		if ($category->categoryPages->isEmpty()) {
			$categoryPages = [
				[], [], [],
			];
			$category->categoryPages()->createMany($categoryPages);
		}
		if ($category->isMainCategory()) {
			$request = new Request();
			$slider = new Slider();
			$this->sliderRepository->save($request, $slider, $category);
		}
	}

	/**
	 * Handle the category "updated" event.
	 *
	 * @param \App\Models\Category\Category $category
	 * @return void
	 */
	public function updated(Category $category)
	{

	}

	/**
	 * Handle the category "deleted" event.
	 *
	 * @param \App\Models\Category\Category $category
	 * @return void
	 */
	public function deleted(Category $category)
	{
		//
	}

	/**
	 * Handle the category "restored" event.
	 *
	 * @param \App\Models\Category\Category $category
	 * @return void
	 */
	public function restored(Category $category)
	{
		//
	}

	/**
	 * Handle the category "force deleted" event.
	 *
	 * @param \App\Models\Category\Category $category
	 * @return void
	 */
	public function forceDeleted(Category $category)
	{
		//
	}

}
