<?php /** @var $category \App\Models\Category\Category */ ?>
<?php /** @var $chunkedSubCategory \App\Models\Category\Category */ ?>
<?php /** @var $chunkedCategories \Illuminate\Support\Collection */ ?>
<?php /** @var $chunkCategoryContainer \App\Helpers\Order\Containers\Category\ChunkCategoryContainer */ ?>
@if (isset($chunkedCategories) && ($chunkedCategories)->isNotEmpty())
    <section class="category-advantages">
        <div class="container">
            @foreach($chunkedCategories as $chunkCategoryContainer)
                <div class="text-center">
                    <a href="{{ route('category.show', $chunkCategoryContainer->getCategory()->url) }}"
                       class="title text-center mb-20 mb-lg-5">
                        <span class="decor-bg">{{ $chunkCategoryContainer->getCategory()->name }}</span>
                    </a>
                </div>
                @foreach($chunkCategoryContainer->getChunks() as $chunk)
                    @php
                        $cssCol = $chunkCategoryContainer->getColByChunk($chunk);
                    @endphp
                    <div class="row">
                        @foreach($chunk as $chunkedSubCategory)
                            @include('public.category.partials.product-or-category-item',
                            [
                                'model' => $chunkedSubCategory,
                                'url' => route('category.show', $chunkedSubCategory->url),
                            ])
                        @endforeach
                    </div>
                @endforeach
            @endforeach
        </div>
    </section>
@endif