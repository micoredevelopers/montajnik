<?php

use Illuminate\Database\Seeder;

class MenuGroupSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$groups = [
			['name' => 'Основное меню', 'role' => 'main_menu'],

        ];
        foreach ($groups as $group) {
			\App\Models\MenuGroup::create($group);
		}
    }
}
