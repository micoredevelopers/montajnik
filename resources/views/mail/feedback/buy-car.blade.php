@extends('mail.feedback.layout')

@section('title')
	Buy a car
@stop

@section('content')

    <?php /** @var $feedback \App\Models\Feedback */ ?>
	<table class="table">
		<tbody>
		@include('mail.feedback.partials.fields')
		@if ($car = $feedback->getOption('car') AND is_array($car))
			<tr>
				<td>
					<a href="{{ route('cars.edit', Arr::get($car, 'id', 0)) }}">
						<img src="{{ checkImage(Arr::get($car, 'image')) }}" alt="" class="img-fluid" width="100">

					</a>
				</td>
				<td>
					<span>{{ Arr::get($car, 'name', 'car') }}</span>
				</td>
			</tr>
		@endif
		<tr class="text-center">
			<td><a href="{{ route('feedback.index') }}">Feedback</a></td>
		</tr>
		</tbody>
	</table>
@stop
