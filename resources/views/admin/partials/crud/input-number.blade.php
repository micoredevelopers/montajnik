<div class="form-group">
    @if ($errors->has($name)) <p class="text-danger">{{ $errors->first($name) }}</p> @endif
    <label for="{{ $name }}" class="control-label">{{ $title ?? '' }}</label>
    <input type="number" id="{{ $name }}" name="{{ $name }}"
           class="form-control"
           autocomplete="off"
           step="{{ $step ?? '0.01' }}"
           {!! $props ?? '' !!}
           min="0"
           value="{{ old($name, $value ?? ($edit->{$name} ?? '' ) ) }}"
    >
</div>