<?php

namespace App\Events\Admin\Module\Transfer\Meta;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class EntityMetableEditedEvent extends AbstractEntityMetable
{
    use Dispatchable, SerializesModels;
}
