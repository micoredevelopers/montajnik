<?php

namespace App\Http\Controllers;

use App\Models\Gallery;

class GalleryController extends SiteController
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->setTitle(getTranslate('gallery.gallery'));
		$vars = $data = [];
		$vars['galleries'] = Gallery::with('photos')->GetLang($this->lang)->Active(1)->get();
		$data['content'] = view('public.gallery.index', $vars);
		return $this->main($data);
	}
}
