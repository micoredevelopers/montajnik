<?php /** @var $testimonialVideo \App\Models\Testimonial */ ?>
@if(isset($testimonialsVideo) AND $testimonialsVideo->isNotEmpty())
    <section class="video-reviews">
        <div class="text-center">
            <h2 class="title text-center mb-25 mb-lg-5">
                <span class="decor-bg">Видео-отзывы</span>
            </h2>
        </div>
        <div class="container">
            <div class="video-reviews-slider">
                @foreach($testimonialsVideo as $testimonialVideo)
                    @include('public.index.partials.review-video-item')
                @endforeach
            </div>
        </div>
    </section>
@endif