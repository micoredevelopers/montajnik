@include('admin.partials.crud.elements.name')

@include('admin.partials.crud.elements.url')

@include('admin.partials.crud.elements.active')

@include('admin.partials.crud.elements.image-upload-group')

@include('admin.partials.crud.textarea.description')

@include('admin.partials.crud.textarea.except')