<?php

namespace App\Console\Commands;

use App\Helpers\UrlReplacer;
use Illuminate\Console\Command;

class RemoveUpperUrl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'url:remove-upper';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all upper url in application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = new UrlReplacer();
        $url->setRedirectUpperToLower();
    }
}
