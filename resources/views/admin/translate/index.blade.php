<div class="panel panel-default">
    <div class="panel-body clearfix">
        <div class="row">
            <div class="col-4">
                @can('create_translate')
                    <a href="{{route($routeKey.'.create')}}" class="btn btn-default">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        {{ __('form.create') }}</a>
                @endcan
                @can('edit_translate')
                    <button class="btn btn-primary" type="submit" form="translate_form">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i>
                        {{ __('form.save') }}</button>
                @endcan
                <a class="btn btn-warning" href="{{ route($routeKey.'.index') }}?seed=true">Seed</a>
            </div>
            <div class="col-8">
                <form action="" method="get">
                    <div class="form-group">
                        <div class="row">
                            @if (isSuperAdmin())
                                <div class="col-2">
                                    @include('admin.partials.crud.checkbox', ['name' => 'with_trashed', 'title' => 'With deleted', 'checked' => $request->has('with_trashed') ])
                                </div>
                            @endif
                            <div class="col-7">
                                @include('admin.partials.crud.default', ['name' => 'search', 'title' => 'Search', 'value' => $request->get('search'),])
                            </div>
                            <div class="col-3">
                                <button class="btn btn-danger" type="submit">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    @lang('form.search')
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<form action="{{route($routeKey. '.update', '*')}}" method="post" data-send-only-changed="true" id="translate_form">
    <input type="hidden" name="_method" value="PATCH">
    <input type="hidden" name="translate_tab" class="translate_tab" value="{{ $active }}">
    @csrf
    <ul class="nav nav-tabs my-5" id="translates" role="tablist">
        @foreach($groups as $group)
            @php
                $isActive = ($active === $group);
            @endphp
            <li class="nav-item">
                <a class="nav-link text-dark @if($isActive) active @endif" id="{{ $group }}-tab" data-toggle="tab"
                   href="#group-{{ $group }}" role="tab"
                   aria-controls="group-{{ $group }}" aria-selected="@if($isActive) true @endif">{{ $group }}</a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content" id="translatesContent">
        @foreach($groups as $group)
            @php
                $isActive = ($active === $group);
            @endphp
            <div class="tab-pane @if($isActive) active @endif" id="group-{{ $group }}" role="tabpanel">
                <div class="table-responisve">
                    <table class="table table-hover table-condensed table-striped">
                        <tr class="active">
                            <th>ID</th>
                            <th>{{ __('modules.localization.key') }}</th>
                            <th>{{ __('modules.localization.value') }}</th>
                            @superadmin
                            <th>{{ __('modules.localization.group') }}</th>@endsuperadmin
                        </tr>
                        @if(isset($list))
                            @forelse($list->where('group', $group) as $translate)
								<?php /** @var $translate \App\Models\Translate */ ?>
                                <tr data-id="{{ $translate->getPrimaryValue() }}" data-deleteable-row="">
                                    <td width="20">
                                        {{$translate->getPrimaryValue()}}
                                        <input type="hidden" name="translate[{{$translate->getPrimaryValue()}}][id]"
                                               value="{{$translate->getPrimaryValue()}}">
                                    </td>
                                    <td width="200">
                                        <input type="hidden" name="translate[{{ $translate->getPrimaryValue() }}][key]"
                                               value="{{$translate->key}}">
                                        <input type="text" value="{{$translate->key}}" class="form-control" readonly=""
                                               title="{{ $translate->comment }}"
                                               onclick="copyClickBoard(this)" autocomplete="off"
                                               data-prepend="{{ $translate->getPrependClickBoardText() }}"
                                               data-append="{{ $translate->getAppendClickBoardText() }}">
                                    </td>
                                    <td title="{{ $translate->comment }}">
                                        @if ($translate->isTypeTextarea())
                                            <textarea name="translate[{{ $translate->getPrimaryValue() }}][value]"
                                                      cols="30" rows="6"
                                                      class="form-control"
                                                      maxlength="65000">{!! $translate->getValueAttribute() !!}</textarea>
                                        @else
                                            <input type="text"
                                                   name="translate[{{$translate->getPrimaryValue()}}][value]"
                                                   value="{{$translate->getValueAttribute()}}"
                                                   class="form-control">
                                        @endif
                                    </td>
                                    <td width="200">
                                        @superadmin
                                        <div class="d-inline-block">
                                            <select name="translate[{{$translate->getPrimaryValue()}}][group]"
                                                    class="form-control group_select">
                                                @forelse ($groups as $group)
                                                    <option value="{{ $group }}"
                                                            @if($group == $translate->group) selected="selected" @endif>{{
														$group }}
                                                    </option>
                                                @empty
                                                    <option value="">No</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="d-inline-block">
                                            @include('admin.partials.delete_ajax_button', ['item' => $translate])
                                        </div>
                                        @endsuperadmin
                                    </td>
                                </tr>
                            @empty
                                <h3>Пусто</h3>
                            @endforelse
                        @endif
                    </table>
                </div>
            </div>
        @endforeach
    </div>
</form>

@section('javascript')
    <script>
        $(document).ready(function () {
            $(".group_select").not('.group_select_new').select2({
                tags: true,
                width: 'resolve'
            });

            $('[data-toggle="tab"]').click(function () {
                $(".translate_tab").val($(this).html());
            });
        });
    </script>
@stop
