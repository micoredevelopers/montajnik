<?php

namespace App\Http\Requests\Admin;

use App\Contracts\Requests\RequestParameterModelable;
use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;
use App\Models\News;
use App\Traits\Requests\Helpers\GetActionModel;

class PartnerRequest extends AbstractRequest implements RequestParameterModelable
{
    use GetActionModel;

    protected $toBooleans = ['active'];

    protected $requestKey = 'partner';

    public function rules()
    {
        $rules = [
            'name'         => ['required', 'max:255'],
//            'url'          => ['required', 'unique' => 'unique:news,url'],
            'description'  => ['max:' . ValidationMaxLengthHelper::TEXT],
        ];

//        if ($this->isActionUpdate() AND $news = $this->getActionModel()) {
//            /** @var  $news News */
//            $rules['url']['unique'] = 'unique:news,url,' . $news->id;
//        }

        return $rules;
    }


    protected function mergeRequestValues()
    {
//        $this->mergeUrlFromName();
        $this->merge([
            'video'        => $this->get('video'),
        ]);
    }
}
