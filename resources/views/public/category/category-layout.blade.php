<?php /** @var $category \App\Models\Category\Category */ ?>
<?php /** @var $slider \App\Models\Slider\Slider */ ?>

<main id="category" class="category">
    @if ($slider && $slider->getValidItems()->isNotEmpty())
        <section class="main-page-top">
            @include('public._includes.slider')
        </section>
    @endif

    @yield('beforeSubcategories')

    @yield('subcategories')

    @include('public.category.includes.why-choose-us')

    @if ($showCeilingsFrame ?? false)
        <div class="container">
            <iframe class="hidden-xs hidden-sm" src="/ceilings-colors.html"
                    style="border: 0; width: 100%; height: 760px;"></iframe>
        </div>
    @endif

    @php
        $descriptionByIndex = $category->getDescriptionByIndex(1);
    @endphp
    @if($descriptionByIndex !== '')
        <section class="calc-block">
            <div class="container bg-white pt-4 py-lg-5">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2 ">
                        <div class="decodeWrapper">
                            {!! $descriptionByIndex !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @include('public.category.includes.calculators')

    @include('public.category.includes.work')

    <section class="materials pb-3 pb-lg-5">
        <div class="container bg-white py-4">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 ">
                    @include('public.category.includes.partners')
                    <div class="decodeWrapper">
                        {!! $category->getDescriptionByIndex(2) !!}
                    </div>
                </div>
            </div>
            @include('public.category.includes.video-reviews')
            @include('public.category.includes.image-reviews')

        </div>
    </section>
</main>
@section('javascript')
    <script src="//cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
@stop

{!! editButtonAdmin(route(routeKey('category', 'edit'), $category->getPrimaryValue())) !!}
