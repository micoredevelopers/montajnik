<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'url' => 'balkon',
                'image' => NULL,
                'active' => 1,
                'sort' => 0,
                'created_at' => '2019-10-03 09:58:52',
                'updated_at' => '2019-10-03 11:04:19',
                'category_id' => 223,
            ),
            1 => 
            array (
                'id' => 2,
                'url' => 'rewrw',
                'image' => 'products/c81e728d9d4c2f636f067f89cc14862c_s.png',
                'active' => 1,
                'sort' => 0,
                'created_at' => '2019-10-03 15:15:59',
                'updated_at' => '2019-10-04 11:09:37',
                'category_id' => 223,
            ),
        ));
        
        
    }
}