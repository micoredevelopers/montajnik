<?php


namespace App\Http\Requests\Feedback;


use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;

abstract class AbstractFeedbackRequest extends AbstractRequest
{

	/**
	 * @param array $except
	 * @return array
	 */
	public function getBusinessRental(array $except = []): array
	{
		$personal = [
			'company_name' => ['required', 'string', 'max:255'],
			'job_position' => ['required', 'string', 'max:255'],
			'address'      => ['required', 'string', 'max:255'],
			'message'      => ['required', 'string', 'max:' . ValidationMaxLengthHelper::TEXT],
		];
		$personal = $personal + $this->getMinimalRules();
		return $this->exceptFields($personal, $except);
	}

	public function getMinimalRules(array $except = []): array
	{
		$personal = [
			'name'    => ['required', 'string', 'max:255'],
			'phone'   => ['required', 'string', 'max:255'],
			'email'   => ['required', 'string', 'email', 'max:255'],
			'message' => ['string', 'max:' . ValidationMaxLengthHelper::TEXT],
		];
		return $this->exceptFields($personal, $except);
	}


}
