<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Repositories\PageRepository;
use Illuminate\Http\Request;

class PageController extends SiteController
{

	/**
	 * @param PageRepository $pageRepository
	 * @param null $url
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show(PageRepository $pageRepository, $url = null)
	{
		$page = $pageRepository->findPageByUrl($url);
		if (!$page) {
			abort(404);
		}
		$vars = [
			'page' => $page,
		];
		$this->addBreadCrumbsByPage($page, $pageRepository);
		$vars = array_merge($vars, $this->loadPageDataByPage($page));
		$title = $page->lang->getAttribute('title');
		$this->setTitle($title);
		$data['content'] = view('public.pages.page')->with($vars);

		return $this->main($data);

	}

	/**
	 * @param Page $page
	 * @return array
	 */
	private function loadPageDataByPage(Page $page): array
	{
		$pageType = $page->getAttribute('page_type');

		return [];
	}


	private function addBreadCrumbsByPage(Page $page, PageRepository $pageRepository): void
	{
		$pages = $pageRepository->getPageParents($page);
		$pages->push($page);
		/** @var $parentPage Page */
		foreach ($pages as $parentPage) {
			$this->addBreadCrumb($parentPage->lang->getAttribute('title'), $parentPage->getRouteUrl());
		}
	}

	/**
	 * @param Request $request
	 * @param PageRepository $pageRepository
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function manualUrl(Request $request, PageRepository $pageRepository)
	{
		$url = $request->getRequestUri();
		if (!$pageRepository->findManualPageByUrl($url)) {
			$url = null;
		}
		return $this->show($pageRepository, $url);
	}
}
