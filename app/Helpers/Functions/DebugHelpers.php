<?php

if (!function_exists('d')) {
	function d()
	{
		if (isLocalEnv()) {
			$backtrace = debug_backtrace();
			$trace = array_shift($backtrace);
			$prevTrace = array_shift($backtrace);
			$methodName = Arr::get($prevTrace, 'function');
			$traceText = Arr::get($trace, 'file') . ' ' . $methodName . ' ' . Arr::get($trace, 'line');
			dd($traceText, ...func_get_args());
		}
	}

}

if (!function_exists('isLocalEnv')) {
	function isLocalEnv()
	{
		return env('APP_ENV') === 'local';
	}
}


if (!function_exists('debugInfo')) {
	function debugInfo()
	{
		\Debugbar::info(...func_get_args());
	}
}

