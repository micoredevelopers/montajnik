<?php

namespace App\Models\Sky;

use Illuminate\Support\Collection;

/**
 * App\Models\Sky\Comment
 *
 * @property int $id
 * @property string $author
 * @property string $phone
 * @property string $email
 * @property string $file
 * @property string $text
 * @property string $date
 * @property int $like_nums
 * @property string $active
 * @property int $moderator_id
 * @property int $sub
 * @property int $content_id
 * @property int $type_id
 * @property string $session_id
 * @property string $type
 * @property string $mail
 * @property string $language
 * @property string $author2
 * @property string $text2
 * @property string $photo
 * @property string $referer
 * @property-read \App\Models\Sky\Category $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\AbstractSkyModel onlyMain()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereAuthor2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereContentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereLikeNums($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereMail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereModeratorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereReferer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereSessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereSub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereText2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Comment whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class Comment extends AbstractSkyModel
{
    protected $unsanitizeFields = ['text'];

    protected $replaceAttributesKeys = [
        'text'       => 'message',
        'author'     => 'name',
        'content_id' => 'category_id',
    ];

    public $timestamps = false;

    public function remake()
    {
        $comments = self::with('category')->all();
        $this->unsanitizeList($comments);
    }

    public function getList(): Collection
    {
        $comments = self::with('category')->active()->get();

        $this->unsanitizeList($comments);
        $this->remakeColumnsName($comments, false);
        return $comments;
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'content_id', 'id');
    }
}
