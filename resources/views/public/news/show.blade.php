<?php /** @var $news \Illuminate\Pagination\LengthAwarePaginator */ ?>
<?php /** @var $new \App\Models\News */ ?>

<main id="news-open">
    <section class="news-open">
        <div class="container bg-white pt-3 pt-lg-5">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="d-lg-flex flex-row-reverse justify-content-between align-items-center mb-20 mb-lg-45 flex-lg-row">
                        <div class="breadcrumbs  mb-25 m-lg-0 breadcrumbs-wrap">
                            @include('public.partials.breadcrumbs')
                        </div>

                        @if(Agent::isMobile())
                            <h1 class="title mb-3 d-lg-none">{!! showMeta($new->getNameAttribute()) !!}</h1>
                        @endif
                        <span class="news-date-box mb-0">{{ $new->published_at->formatLocalized('%d %b %Y') }}</span>
                    </div>
                    @if(!Agent::isMobile())
                        <h1 class="title mb-35 d-none d-lg-inline-block">{!! showMeta($new->getNameAttribute()) !!}</h1>
                    @endif
                    <div class="news-open-img-wrap mb-lg-30">
                        <img class="w-100 lazy" data-src="{{ checkImage($new->image) }}" src="{{ checkImage('') }}"
                             alt="{{ translateFormat('news.image-alt', ['%name%' => $new->getNameAttribute()]) }}"/>
                    </div>
                    <div class="news-open-text mb-5">{!! $new->getDescription() !!}</div>
                    {{--
                              <h5 class="news-open-small-title">Как правильно обшить балкон?</h5>
                              <p class="news-open-text"></p>--}}
                </div>
            </div>
        </div>
    </section>

    <section class="other-news">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 text-center text-lg-left">
                    @if ($new->prevNew)
                        @include('public.news.includes.prev', ['new' => $new->prevNew])
                    @endif
                </div>
                <div class="col-lg-6 text-center text-lg-left">
                    @if ($new->nextNew)
                        @include('public.news.includes.next', ['new' => $new->nextNew])
                    @endif
                </div>
            </div>
        </div>
    </section>
</main>
{!! editButtonAdmin(route(routeKey('news', 'edit'), $new->getPrimaryValue())) !!}