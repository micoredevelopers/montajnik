<?php

namespace App\Http\Requests\Orders;

use App\Helpers\Order\OrderStepsFields;
use App\Helpers\Order\OrderStepsRules;

class StepThreeRequest extends AbstractOrderRequest
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getFillableRequestFields($except = []): array
	{
		return $this->exceptFields(OrderStepsFields::getFieldsStepThree($this), $except);
	}

	/**
	 * @return array
	 */
	public function rules()
	{
		return OrderStepsRules::getRulesStepThree($this);
	}
}
