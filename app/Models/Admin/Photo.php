<?php

namespace App\Models\Admin;

use App\Contracts\HasImagesContract;
use App\Helpers\Media\ImageRemover;
use App\Helpers\Media\ImageSaver;
use App\Http\ImageThumbnail;
use App\Models\Model;
use App\Models\Setting;
use App\Traits\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Storage;
use DB;
use Image;

/**
 * App\Models\Admin\Photo
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Photo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Photo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Photo query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 */
class Photo extends Model
{
	use Builder;

	protected $watermarkConfig = null;

	public const DEFAULT_EXT = 'jpg';


	public function setImage($image)
	{
		return $this->setData('image', $image);
	}

	public function saveImageFromBase64($cols = 250, $rows = 250): string
	{
		$base64 = $this->getData('image');
		$directory = $this->getData('directory');
		$id = md5($this->getData('id'));

		$imageSaver = new ImageSaver();
		return $imageSaver->setFolderName($directory)
			->setThumbnailSizes($cols, $rows)
			->withFileName($id)
			->saveFromBase64($base64)
			;
	}

	/**
	 * Метод является оберткой для saveImageFromBase64, и в общем то пока не имеет смысла в своем существовании
	 */
	public function saveAdditionalImageFromBase64()
	{
		$photo = $this->getData('image');
		$table = $this->getData('table');
		$directory = $this->getData('directory', $table);
		$primary_id = $this->getData('primary_id');
		if (!$photo) {
			return false;
		}
		$directory .= '/' . $primary_id;
		$this->setData('directory', $directory);
		return $this->saveImageFromBase64();
	}

	/**
	 * @param HasImagesContract $belongToModel
	 * @param Request $request
	 * @param string $requestKey
	 * @return Collection
	 */
	public function saveAdditionPhotos(HasImagesContract $belongToModel, Request $request, $requestKey = 'images'): Collection
	{
		if (!$request->hasFile($requestKey)) {
			return null;
		}
		/** @var $image UploadedFile */
		/** @var $imageModel \App\Models\Image */
		/** @var $belongToModel \App\Models\Model */
		$images = collect([]);
		$imageSaver = new ImageSaver($request);
		$imageSaver->setFolderName($belongToModel->getTable() . DIRECTORY_SEPARATOR . $belongToModel->getPrimaryValue());
		foreach ($request->file($requestKey) as $index => $image) {
			$photoName = (string)$image->getClientOriginalName();
			$imageModel = $belongToModel->images()->create(['name' => $photoName,]);
			if ($imageModel) {
				$imageSaver->setNameKey('images.' . $index);
				$photoPath = $imageSaver->saveFromRequest();
				$imageModel->setAttribute('image', $photoPath);
				$imageModel->save();
				$images->push($imageModel);
			}
		}
		$this->clearBuilderData();
		return $images;
	}

	public function deletePhoto($id, $table, $deleteFromDatabase = false): bool
	{
		if (!\Schema::hasTable($table)) {
			return false;
		}
		$photo = DB::table($table)->where('id', (int)$id)->first();
		$column = 'image';

		if ($photo && $column) {
			$path = Arr::get((array)$photo, $column);
			$this->deleteImageStorage($path);
			// Delete record if that additional image
			$query = DB::table($table)->where('id', $id);
			$deleteFromDatabase ? $query->delete() : $query->update([$column => '']);

			return true;
		}
		return false;
	}

	public function updateImagePath(Request $request, $imagePath): void
	{
		$column = 'image';
		$table = $request->get('table');
		$id = $request->get('id');
		$columnId = 'id';
		if (Schema::hasTable($table) AND Schema::hasColumn($table, $column)) {
			if ($id AND $record = DB::table($table)->where($columnId, $id)->first()) {
				DB::table($table)->where($columnId, $id)->update([$column => $imagePath]);
				if ($oldPhoto = $record->$column AND imgPathOriginal($oldPhoto) !== imgPathOriginal($imagePath)) {
					$this->deleteImageStorage($oldPhoto);
				}
			}
		}
	}

	public function deleteImageStorage($imagePath): void
	{
		(new ImageRemover())->removeImage($imagePath);
	}
}

