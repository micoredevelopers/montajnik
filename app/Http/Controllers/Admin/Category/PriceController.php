<?php

namespace App\Http\Controllers\Admin\Category;


use App\Events\Admin\CategoriesChanged;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests\Admin\Category\PriceRequest;
use App\Models\Category\Category;
use App\Models\Category\CategoryPage;
use App\Models\Category\Price;
use App\Models\Category\PriceItem;
use App\Repositories\CategoryRepository;
use App\Repositories\PriceRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class PriceController extends AdminController
{
    use Authorizable;

    protected $routeKey = 'admin.prices';

    protected $permissionKey = 'prices';

    protected $key = 'prices';

    private $name = 'Доп работы (категория)';

    /**
     * @var PriceRepository
     */
    private $priceRepository;

    public function __construct(PriceRepository $priceRepository, CategoryRepository $categoryRepository)
    {
        parent::__construct();
        $this->priceRepository = $priceRepository;
        $this->addBreadCrumb($this->name, $this->resourceRoute('index'));
        $this->shareViewModuleData();
        \View::share('categories', $priceRepository->getNotAssociatedCategories());
        \View::share('pricesUnits', $this->priceRepository->getDistinctUnits());
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->setTitle($this->name);
        $list = $this->priceRepository->all();
        $data['content'] = view('admin.prices.index', compact('list'));
        return $this->main($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->shareAssets();
        $data['content'] = view('admin.prices.create');

        return $this->main($data);
    }

    /**
     * @param PriceRequest $request
     * @param Price $price
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PriceRequest $request, Price $price)
    {
        $input = $request->all();

        if ($price->fillExisting($input)->save()) {
            $this->setSuccessStore();
            $priceItem = new PriceItem();
            $priceItem->price()->associate($price);
            $priceItem->fillExisting($input);
            $priceItem->save();
        }

        if ($request->has('createOpen')) {
            return redirect($this->resourceRoute('edit', $price->getPrimaryValue()))->with($this->getResponseMessage());
        }

        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $vars['edit'] = $price = $this->priceRepository->findForEdit((int)$id);
        if (!$price) {
            $this->setMessage(__('modules._.record-not-finded'));
            return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
        }
        $this->shareAssets();

        $categories = \Arr::get(\View::getShared(), 'categories');
        $categories->prepend($price->category);

        $title = $this->titleEdit($price);
        $this->addBreadCrumb($title)->setTitle($title);

        $data['content'] = view('admin.prices.edit', $vars)->with($vars);

        return $this->main($data);
    }

    public function update(PriceRequest $request, Price $price)
    {
        $price = $this->priceRepository->findForEdit((int)$price->id);

        $input = $request->all();
        $price->fillExisting($input);
        if ($price->save()) {
            if ($priceItems = $price->items) {
                /** @var  $priceItem PriceItem */
                foreach ($priceItems as $index => $priceItem) {
                    $priceItem->fillExisting($request->input('price_items.' . $priceItem->id, []));
                    $priceItem->save();
                }
            }
            $this->setSuccessUpdate();
        }
        if ($request->has('saveClose')) {
            return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
        }

        return redirect()->back()->with($this->getResponseMessage());

    }

    /**
     * @param Price $price
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Price $price)
    {
        if ($price->delete()) {
            $this->setSuccessDestroy();
        }

        return back()->with($this->getResponseMessage());
    }

    public function storeItem(Request $request, Price $price)
    {
        $price->items()->create($request->only(['name', 'price', 'unit']));
        $this->setSuccessStore();
        return redirect()->back()->with($this->getResponseMessage());
    }

    private function shareAssets()
    {
        $this->addScripts([
            'js/lib/select2/select2.full.min.js',
            'js/lib/select2/ru.js',
        ]);
        $this->addCss([
            'css/lib/select2.min.css',
        ]);
    }

}
