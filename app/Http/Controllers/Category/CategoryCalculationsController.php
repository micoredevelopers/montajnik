<?php

	namespace App\Http\Controllers\Category;

	use App\Calculators\Category\Ceilings\CalculatorData;
	use App\Calculators\Category\Ceilings\CalculatorDataContract;
	use App\Calculators\Category\Ceilings\CeilingsCalculator;
	use App\Http\Controllers\SiteController;
	use App\Http\Requests\Category\Calculators\CeilingsRequest;
	use App\Repositories\SliderRepository;
	use App\Traits\Controllers\Category\BreadcrumbsTree;

	class CategoryCalculationsController extends SiteController
	{
		use BreadcrumbsTree;

		private $categoryRepository;
		/**
		 * @var SliderRepository
		 */
		private $sliderRepository;

		public function __construct()
		{
			parent::__construct();
		}

		public function ceilings(CeilingsRequest $request)
		{
			app()->singleton(CalculatorDataContract::class, CalculatorData::class);
			$calculatorData = app(CalculatorDataContract::class);
			/** @var $calculatorData CalculatorData */
			$calculatorData
				->setLength((int)$request->get('length'))
				->setWidth((int)$request->get('width'))
				->setType($request->get('type'))
				->setProfile($request->get('perimeter_profile'))
				->setGapless((int)$request->get('gapless'))
				->setStarSky((int)$request->get('star_sky'))
				->setAngles((int)$request->get('angles'))
				->setLight((int)$request->get('light'))
				->setPipe((int)$request->get('pipe'))
				->setCurvature((int)$request->get('curvature'))
				->setBaguette((int)$request->get('baguette'))
			;

			$calculator = app(CeilingsCalculator::class);

			$total = $calculator->getTotal();
			debugInfo($total);
			return [
				'price'         => $total,
				'priceText'     => sprintf('%d %s', $total, getCurrencyIcon()),
				'perimeter'     => $calculator->getPerimeter(),
				'perimeterText' => sprintf('%d м', $calculator->getPerimeter()),
				'square'        => $calculator->getSquare(),
				'squareText'    => sprintf('%d м2', $calculator->getSquare()),
			];
		}

	}
