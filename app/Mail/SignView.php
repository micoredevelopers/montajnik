<?php

namespace App\Mail;

use App\Models\Feedback;

class SignView extends MailAbstract
{
	protected $feedback;

    /**
     * SignView constructor.
     * @param Feedback $testimonial
     */
	public function __construct(Feedback $testimonial)
	{
		parent::__construct();
		$this->feedback = $testimonial;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->from($this->getEmailFrom(), $this->getNameFrom())
			->to($this->getEmailTo())
			->view('mail.sign-view')->with(['feedback' => $this->feedback]);
	}
}
