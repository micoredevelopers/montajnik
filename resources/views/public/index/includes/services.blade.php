<?php /** @var $loop object */ ?>
<section class="services position-relative">
    <div id="particles-js"></div>
    <div class="container ">
        <div class="text-center">
            <h2 class="title title_after text-center mb-20">
                <span class="decor-bg">Услуги</span>
            </h2>
        </div>
        <div class="row">
            @php
                $matches = [6,6,4,4,4,7,5,4,4,4,5,7];
            @endphp
            @foreach($categories as $category)
                @php
                    $colNumber = Arr::get($matches, $loop->index, 4);
                @endphp
                <div class="col-lg-{{ $colNumber }}">
                    <a href="{{ route('category.show', $category->getUrlAttribute()) }}" class="service__item d-flex flex-column justify-content-center">
                        <img alt="{{ $category->getNameAttribute() }}"
                             src="{{ checkImageWebp('') }}"
                             class="service__img service__img_drill lazy"
                             data-src="{{ checkImageWebp($category->getImageAttribute(), asset('img/services/potolki.png')) }}"/>
                        <div class="">
                            <h3 class="service-item-title">{{ $category->getNameAttribute() }}</h3>
                            <p class="main-services-text">
                                <span>{!! $category->getExceptAttribute() !!}</span>
                            </p>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</section>
