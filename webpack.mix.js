const mix = require('laravel-mix');

// mix.disableNotifications();

// mix.webpackConfig({
//     devtool: 'inline-source-map'
// });
//Admin side
mix.scripts([
    'public/js/core/popper.min.js',
    'public/js/core/bootstrap-material-design.min.js',
    'public/js/plugins/bootstrap-notify.js',
    'public/js/plugins/bootstrap-selectpicker.js',
    'public/js/lib/jquery.fancybox.min.js',
    'public/js/lib/bootstrap-filestyle.min.js',
    'public/js/lib/Sortable.min.js',
], 'public/js/admin/libraries.js');

mix.sass('resources/sass/material-dashboard.scss', 'public/css/admin')
    .sass('resources/sass/admin-main.scss', 'public/css/admin')
    .sass('resources/sass/styles.scss', 'public/css/admin')
    .sourceMaps();
//End Admin side

mix.copyDirectory('resources/img', 'public/img');
mix.copyDirectory('resources/libs', 'public/libs');
mix.copyDirectory('resources/fonts', 'public/fonts');
mix.sass('resources/sass/style.scss', 'public/css')
    .js('resources/js/index.js', 'public/js')
    .sourceMaps()
    .version();

