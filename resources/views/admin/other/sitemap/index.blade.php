<form action="{{ route($routeKey . '.store') }}" method="post">
    @csrf

    <button class="btn btn-warning btn-wide">
        <i class="fa fa-file-text-o" aria-hidden="true"></i>
        Generate sitemap</button>

</form>