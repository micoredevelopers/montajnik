<?php

namespace App\Http\Controllers\Admin;


use App\Helpers\Dev\Database\DatabaseHelper;
use App\Widgets\Chart\FeedbackChart;
use Illuminate\Http\Request;

class IndexController extends AdminController
{
    public function index(Request $request, DatabaseHelper $databaseHelper)
    {


        $this->setTitle(__('modules._.dashboard'));

		$charts = collect([]);
		$feedbackCharts = (new FeedbackChart(['chart_title' => 'Заявки обратной связи']))->chart();
		$charts->push($feedbackCharts);
		$data['content'] = view('admin.index.dashboard')->with(compact('charts'));
        return $this->main($data);
    }

    public function clearCache()
    {
        $this->setMessage('Cache Cleared!')->setStatus(true);
        \Artisan::call('cache:clear');
        return redirect()->back()->with($this->getResponseMessage());
    }

    public function clearView()
    {
        \Artisan::call('view:clear');
        $this->setMessage('Cache views cleared!')->setStatus(true);
        return redirect()->back()->with($this->getResponseMessage());
    }

}
