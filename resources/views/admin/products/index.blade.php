<?php /** @var $permissionKey string */ ?>
<?php use App\Models\Category\Category;

/** @var $category Category */ ?>
<?php /** @var $item \App\Models\Category\Product */ ?>
@php
    $canEdit = Gate::allows('edit_'. $permissionKey );
    $canDelete = Gate::allows('delete_'. $permissionKey );
@endphp
<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th class="">@lang('form.image.image')</th>
            <th class="th-description">@lang('form.title')</th>
            <th class="th-description">{{ __('modules.category.title_singular') }}</th>
            <th class="th-description"></th>
            <th class="text-right">
                <a href="{{ route($routeKey.'.create') }}" class="btn btn-primary">@lang('form.create')</a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
            <tr>
                <td>
                    <div class="img-container">
                        <a href="{{ imgPathOriginal(checkImage($item->image)) }}" class="fancy" data-fancybox="news-image">
                            <img src="{{ checkImage($item->image) }}" alt=""/>
                        </a>
                    </div>
                </td>
                <td>
                    <a href="{{ route($routeKey.'.edit', $item->id) }}">{{ $item->name }}</a>
                </td>
                <td>
                    @if ($category = $item->category)
                        <a href="{{ route(routeKey('category', 'edit'), $category->id) }}">{{ $category->getCategoryName() }}</a>
                    @endif
                </td>
                <td>
                    @include('admin.partials.preview-button', ['link' => route($key . '.show', $item->url),])
                </td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{{$list->render()}}