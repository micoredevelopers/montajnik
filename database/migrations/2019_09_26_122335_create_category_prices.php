<?php

use App\Traits\Migrations\CategoryForeignKey;
use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryPrices extends Migration
{
    use MigrationCreateFieldTypes;
    use CategoryForeignKey;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $this->setTable($table);

            $this->createName();
            $table->bigIncrements('id');
            $this->addForeignCategory();
            $table->timestamps();
        });

        Schema::create('price_items', function (Blueprint $table) {
            $this->setTable($table);
            $table->bigIncrements('id');
            $table->unsignedBigInteger('price_id')->index();
            $table->foreign('price_id')->references('id')->on('prices')->onDelete('cascade');

            $this->createName();
            $table->char('price')->nullable();
            $table->char('unit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_items');
        Schema::dropIfExists('prices');
    }
}
