<?php

	namespace App\Http\Middleware;

	use Closure;
	use Illuminate\Support\Str;
	use Symfony\Component\HttpFoundation\Response;

	class ToLowerCaseUrlMiddleware
	{
		public function __construct()
		{
		}

		/**
		 * Handle an incoming request.
		 *
		 * @param \Illuminate\Http\Request $request
		 * @param \Closure                 $next
		 * @return mixed
		 */
		public function handle($request, Closure $next)
		{
			if ($this->supports()) {
				return $this->redirect();
			}

			return $next($request);
		}

		private function supports()
		{
			$url = $this->getUrl();
			return ($url !== $this->getLowerUrl());
		}

		private function getUrl(): string
		{
			return getUrlWithoutHost(getNonLocaledUrl());
		}

		private function getLowerUrl(): string
		{
			return Str::lower($this->getUrl());
		}

		private function redirect()
		{
			return redirect($this->getLowerUrl(), Response::HTTP_MOVED_PERMANENTLY);
		}
	}
