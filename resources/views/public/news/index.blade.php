<?php /** @var $news \Illuminate\Pagination\LengthAwarePaginator */ ?>
<?php /** @var $new \App\Models\News */ ?>
<main id="news">
    <section class="news-page">
        <div class="container ">

            <div class="d-lg-flex flex-row-reverse justify-content-between align-items-center mb-25 mb-lg-35">
                @include('public.partials.breadcrumbs')
                <h1 class="title"><span class="decor-bg">{!! showMeta(getTranslate('news.news')) !!}</span></h1>
            </div>

            <div class="row">
            @foreach($news as $new)
                @php
                    $link = route('news.show', $new->getUrlAttribute());
                @endphp
                    <div class="col-lg-6">
                        <div class="news__item">
                            <a href="{{ $link }}" class="news-img-box d-block">
                                <img class="lazy" data-src="{{ checkImage($new->image) }}" src="{{ checkImage('') }}"
                                     alt="{{ translateFormat('news.image-alt', ['%name%' => $new->getNameAttribute()]) }}"/>
                            </a>
                            <div class="news-desc-box">
                                <span class="news-date-box">
                                {{ $new->published_at->formatLocalized('%d %b %Y') }}
							</span>
                                <h4 class="news-title">{{ $new->getNameAttribute() }}</h4>
                                <p class="mb-lg-0">{{ Str::limit($new->getExcept(), 250) }}</p>
                                <a href="{{ $link }}"
                                   class="btn-custom maxWdt100 d-lg-none">{{ getTranslate('news.new-detail') }}</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="container d-flex justify-content-center">
           
            @if ($news->hasPages())
                {!! $news->links() !!}
            @endif
        </div>
    </section>
</main>
