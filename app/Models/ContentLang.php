<?php

namespace App\Models;

/**
 * App\Models\Pages
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 * @property int $content_id
 * @property int $language_id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $except
 * @property array|null $video
 * @property array|null $options
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContentLang whereContentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContentLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContentLang whereExcept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContentLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContentLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContentLang whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContentLang whereVideo($value)
 */

class ContentLang extends Model
{

	protected $table = 'contents_lang';
	protected $casts = [
		'options' => 'array',
		'video' => 'array',
	];

	protected $guarded = [];

	public function contentable()
	{
		$this->morphTo();
	}
}
