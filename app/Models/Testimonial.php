<?php

	namespace App\Models;


	use App\Contracts\HasImagesContract;
	use App\Models\Category\Category;
	use App\Scopes\WhereActiveScope;
	use App\Traits\Models\HasImages;
	use Illuminate\Database\Eloquent\Builder;
	use Illuminate\Database\Eloquent\Relations\BelongsTo;

	/**
	 * App\Models\Testimonial
	 *
	 * @property int                                                               $id
	 * @property string|null                                                       $name
	 * @property string|null                                                       $phone
	 * @property string|null                                                       $email
	 * @property string|null                                                       $message
	 * @property int                                                               $active
	 * @property \Illuminate\Support\Carbon|null                                   $date
	 * @property int|null                                                          $sort
	 * @property string|null                                                       $referer
	 * @property \Illuminate\Support\Carbon|null                                   $created_at
	 * @property \Illuminate\Support\Carbon|null                                   $updated_at
	 * @property int|null                                                          $category_id
	 * @property int                                                               $type_id
	 * @property string|null                                                       $video
	 * @property-read \App\Models\Category\Category|null                           $category
	 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
	 * @property-read int|null                                                     $images_count
	 * @property-read \App\Models\TestimonialType                                  $type
	 * @method static Builder|Model active($active = 1)
	 * @method static Builder|Model default()
	 * @method static Builder|Model getLang($languageId = null)
	 * @method static Builder|Testimonial newModelQuery()
	 * @method static Builder|Testimonial newQuery()
	 * @method static Builder|Model orWhereLike($column, $value)
	 * @method static Builder|Model parentMenu($parentId = 0)
	 * @method static Builder|Testimonial query()
	 * @method static Builder|Model sortOrder($sort = 'asc', $id = 'desc')
	 * @method static Builder|Testimonial whereActive($value)
	 * @method static Builder|Testimonial whereCategoryId($value)
	 * @method static Builder|Testimonial whereCreatedAt($value)
	 * @method static Builder|Testimonial whereDate($value)
	 * @method static Builder|Testimonial whereEmail($value)
	 * @method static Builder|Testimonial whereId($value)
	 * @method static Builder|Model whereIdIn($ids, $field = 'id')
	 * @method static Builder|Model whereIsPublished($column = 'published_at')
	 * @method static Builder|Model whereLanguage($languageId)
	 * @method static Builder|Model whereLike($column, $value)
	 * @method static Builder|Testimonial whereMessage($value)
	 * @method static Builder|Testimonial whereName($value)
	 * @method static Builder|Testimonial wherePhone($value)
	 * @method static Builder|Testimonial whereReferer($value)
	 * @method static Builder|Testimonial whereSort($value)
	 * @method static Builder|Testimonial whereTypeId($value)
	 * @method static Builder|Testimonial whereUpdatedAt($value)
	 * @method static Builder|Model whereUrl($url)
	 * @method static Builder|Testimonial whereVideo($value)
	 * @mixin \Eloquent
	 */
	class Testimonial extends Model implements HasImagesContract
	{
		use HasImages;

		protected $guarded = ['id'];

		protected $casts = [
			'date' => 'datetime',
		];

		public function getDateAttribute()
		{
			return checkDateCarbon($this->attributes['date'] ?? null);
		}

		public function type()
		{
			return $this->belongsTo(TestimonialType::class, 'type_id', 'id')->limit(1);
		}

		public static function getForDisplay()
		{
			return self::active()->get();
		}

		public static function getForEdit()
		{
			return self::latest()->paginate();
		}

		public function category(): BelongsTo
		{
			return $this->belongsTo(Category::class, 'category_id', 'id')->limit(1);
		}

		public static function initScopesPublic()
		{
			static::addGlobalScope(new WhereActiveScope());
		}

		public function getPublishedAt()
		{
			return $this->getAttribute('date');
		}

	}
