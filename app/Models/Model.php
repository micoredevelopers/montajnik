<?php

namespace App\Models;

use App\Models\Category\PriceItem;
use App\Models\Slider\SliderItem;
use App\Traits\EloquentExtend;
use App\Traits\EloquentScopes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as BaseModel;

/**
 * App\Models\Model
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class Model extends BaseModel
{

	use EloquentScopes;
	use EloquentExtend;

	/**
	 * @var array
	 */
	protected $hasOneLangArguments = [];

	public function __construct(array $attributes = [], $table = false)
	{
		if ($table) {
			$this->setTable($table);
		}
		parent::__construct($attributes);
	}

	public static function updateNested(array $todos, $parent_id = 0)
	{
		foreach ($todos as $num => $todo) {
			if ($findTodo = static::find($todo['id'])) {
				if ($findTodo->sort != $num)
					$findTodo->sort = $num;
				$findTodo->parent_id = (int)$parent_id;
				$findTodo->save();
				if (isset($todo['children']))
					static::updateNested($todo['children'], $todo['id']);
			}
		}
	}

	/**
	 * @param string $table
	 * @return Model|null
	 */
	public static function getModelByTable(string $table): ?Model
	{
		$className = self::getModelClassNameByTable($table);
		$model = null;
		if ($className) {
			try {
				$model = app()->make($className);
			} catch (\Exception $e) {

			}
		}
		return $model;
	}

	/**
	 * @param string $table
	 * @return string|null
	 */
	public static function getModelClassNameByTable(string $table): ?string
	{
		$model = null;
		switch ($table) {
			case 'faq':
				$model = Faq::class;
				break;
			case 'faq_items':
				$model = FaqItems::class;
				break;
			case 'price_items':
				$model = PriceItem::class;
				break;
			case 'slider_items':
				$model = SliderItem::class;
				break;
			case 'translate':
				$model = Translate::class;
				break;
		}
		return $model;
	}

	/**
	 * @param null $language_id
	 * @return Builder
	 */
	public function lang($language_id = null)
	{
		if (!$language_id) {
			$language_id = getLang();
		}
		return $this->hasOne(...$this->hasOneLangArguments)->WhereLanguage($language_id);
	}

}



