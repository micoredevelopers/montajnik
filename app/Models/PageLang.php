<?php

namespace App\Models;


use App\Traits\EloquentMultipleForeignKeyUpdate;
use App\Traits\Models\BelongsToLanguage;
use App\Traits\Models\TitleAttributeTrait;

/**
 * App\Models\PageLang
 *
 * @property int $page_id
 * @property int $language_id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $except
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Page $page
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang whereExcept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageLang whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class PageLang extends Model
{
    use TitleAttributeTrait;
    use BelongsToLanguage;
	use EloquentMultipleForeignKeyUpdate;

	public $incrementing = false;

	public $timestamps = false;

    protected $table = 'pages_lang';

	protected $guarded = [];

	protected $primaryKey = ['page_id', 'language_id'];

    public function page()
    {
        return $this->belongsTo(Page::class);
	}


}
