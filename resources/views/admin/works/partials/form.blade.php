<?php /** @var $categories Collection */ ?>
<?php use App\Models\Work;

/** @var $edit Work */ ?>
<?php /** @var $category \App\Models\Category\Category */ ?>
@include('admin.partials.crud.elements.url')

@include('admin.partials.crud.elements.active')

{{--@include('admin.partials.crud.elements.image-upload-group')--}}

@isset($categories)
    <div class="row">
        <div class="col-6">
            {!! errorDisplay('category_id') !!}
            <label for="category">Категория</label>
            <select name="category_id" id="category" data-live-search="true" class="form-control selectpicker"
                    title="Категория">
                @php
                    $editCategoryId = old('category_id', ($edit->category_id ?? null));
                @endphp
                @foreach($categories as $category)
                    <option
                            {!! selectedIfTrue($category->id == $editCategoryId) !!} value="{{$category->id}}">{{$category->getCategoryName()}}</option>
                @endforeach
            </select>
        </div>
        @if (isset($edit) AND $edit->category)
            <a class="mt-5" href="{{ route('admin.category.edit', $edit->category->id) }}">{{ $edit->category->name }}</a>
        @endif
    </div>
@endisset