<?php

use Illuminate\Database\Seeder;

class PartnersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('partners')->delete();
        
        \DB::table('partners')->insert(array (
            0 => 
            array (
                'id' => 1,
                'image' => 'partners/c4ca4238a0b923820dcc509a6f75849b_s.jpg',
                'active' => 0,
                'sort' => 0,
                'name' => 'sdasda',
                'description' => 'asdad',
                'except' => NULL,
                'created_at' => '2019-10-03 11:05:12',
                'updated_at' => '2019-10-03 11:05:12',
                'category_id' => 223,
            ),
            1 => 
            array (
                'id' => 2,
                'image' => 'partners/c81e728d9d4c2f636f067f89cc14862c_s.jpg',
                'active' => 0,
                'sort' => 0,
                'name' => '1234',
                'description' => 'asdaad',
                'except' => NULL,
                'created_at' => '2019-10-03 16:14:04',
                'updated_at' => '2019-10-03 16:14:05',
                'category_id' => 223,
            ),
        ));
        
        
    }
}