<?php

namespace App\Models;


use App\Traits\EloquentMultipleForeignKeyUpdate;
use App\Traits\Models\BelongsToLanguage;

/**
 * App\Models\MetaLang
 *
 * @property int $meta_id
 * @property string|null $h1
 * @property string|null $title
 * @property string|null $keywords
 * @property string|null $description
 * @property string|null $text_top
 * @property string|null $text_bottom
 * @property int $language_id
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Meta $meta
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereMetaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereTextBottom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereTextTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class MetaLang extends Model
{
    use BelongsToLanguage;
	use EloquentMultipleForeignKeyUpdate;

	protected $table = 'meta_lang';
	protected $primaryKey = ['meta_id', 'language_id'];

	public $incrementing = false;

	protected $guarded = [];

	public $timestamps = false;

	public function meta(){
		return $this->belongsTo(Meta::class);
	}
}
