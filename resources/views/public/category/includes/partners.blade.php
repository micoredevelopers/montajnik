<?php /** @var $category \App\Models\Category\Category */ ?>
<?php /** @var $partner \App\Models\Partner */ ?>
@if (($partners = $category->partners)->isNotEmpty())
    <div class="partners">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h2 class="category-title mb-25">{{ getTranslate('category.partners-title') }}</h2>
                <div class="row mb-4">
                    @foreach($partners as $partner)
                        @php
                            $alt = translateFormat('category.our-partner-alt', ['%партнер%' => $partner->name]);
                        @endphp
                        <div class="col-6">
                            <div class="materials-box" data-description="{{ $partner->getAttribute('description') }}"
                                 title="{{ $partner->getAttribute('description') }}">
                                <img
                                        class="lazy materials-box_img"
                                        data-src="{{ checkImage($partner->image )}}"
                                        alt="{{ $alt }}" src="{{ checkImage('') }}"
                                />
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif
