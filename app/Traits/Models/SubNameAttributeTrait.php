<?php

namespace App\Traits\Models;


trait SubNameAttributeTrait
{

	public function getSubNameAttribute()
	{
	    $column = 'sub_name';
		return $this->getLangColumn($column);
	}

}