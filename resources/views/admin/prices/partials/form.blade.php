<?php /** @var $edit \App\Models\Category\Price */ ?>
<?php /** @var $priceItem \App\Models\Category\PriceItem */ ?>
<?php /** @var $category \App\Models\Category\Category */ ?>
<div class="row">
    <div class="col-md-6">
        @include('admin.partials.crud.select.select', ['name' => 'category_id', 'title' => 'Категория', 'list' => $categories, 'column' => 'name',])
    </div>
    <div class="col-md-6 mt-5">
        @if (isset($edit) AND $category = $edit->category)
            <a class=""
               href="{{ route('admin.category.edit', $category->id) }}">{{ $category->getCategoryName() }}</a>
        @endif
    </div>
</div>
@include('admin.partials.crud.elements.name')

@if(isset($edit))
    <div class="card">
        <div class="card-header">
            Доп работы
        </div>
        <div class="card-body">
            @include('admin.prices.partials.titles')
            @foreach($edit->items as $priceItem)
                <div class="row" data-id="{{ $priceItem->getPrimaryValue() }}" data-deleteable-row="">
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="price_items[{{ $priceItem->id }}][name]"
                               value="{{ $priceItem->name }}">
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="price_items[{{ $priceItem->id }}][price]"
                               value="{{ $priceItem->price }}">
                    </div>
                    <div class="col-md-4">
                        <select name="price_items[{{ $priceItem->id }}][unit]" class="select2 form-control"
                                autocomplete="off">
                            @forelse($pricesUnits as $pricesUnit)
                                <option {!! selectedIfTrue($pricesUnit->unit == $priceItem->unit) !!} value="{{ $pricesUnit->unit }}">{{ $pricesUnit->unit }}</option>
                            @empty
                                <option value=""></option>
                            @endforelse
                        </select>
                    </div>
                    <div class="col-md-1">
                        @include('admin.partials.delete_ajax_button', ['item' => $priceItem])
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@else
    @include('admin.prices.partials.create-item')
@endif
