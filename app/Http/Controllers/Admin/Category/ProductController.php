<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests\Admin\Category\CategoryRequest;
use App\Http\Requests\Admin\Category\ProductRequest;
use App\Models\Admin\Photo;
use App\Models\Category\Category;

use App\Models\Category\Product;
use App\Models\Category\ProductLang;
use App\Models\Language;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;
use Illuminate\Http\Request;

class ProductController extends AdminController
{
    use Authorizable;

    use SaveImageTrait;

    protected $routeKey = 'admin.products';

    protected $permissionKey = 'products';

    protected $key = 'products';

    protected $name = 'Товары';

    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        parent::__construct();
        $this->productRepository = $productRepository;
        $this->addBreadCrumb($this->name, $this->resourceRoute('index'));
        $this->shareViewModuleData();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->setTitle($this->name);
        $this->addScripts('libs/nestable2/jquery.nestable.min.js');
        $this->addCss('libs/nestable2/jquery.nestable.min.css');

        $categories = $this->productRepository->getCategoriesForProducts();
        $list = $this->productRepository->paginate();
        $data['content'] = view('admin.products.index', compact('categories', 'list'));
        return $this->main($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $vars['categories'] = $this->productRepository->getCategoriesForProducts();
        $data['content'] = view('admin.products.create')->with($vars);

        return $this->main($data);
    }

    /**
     * @param ProductRequest $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ProductRequest $request, Product $product)
    {
        $input = $request->all();
        if ($product->fillExisting($input)->save()) {
            $this->setSuccessStore();
            /** @var $language  Language */
            foreach ($this->languagesList as $language) {
                ($categoryLang = new ProductLang())->fillExisting($input);
                $categoryLang->associateWithLanguage($language);
                $categoryLang->product()->associate($product);
                $categoryLang->save();
            }
            $this->fireEvents();
        }
        $this->saveImage($request, $product);

        if ($request->has('createOpen')) {
            return redirect($this->resourceRoute('edit', $product->getPrimaryValue()))->with($this->getResponseMessage());
        }

        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $vars['edit'] = $product = $this->productRepository->findForEdit((int)$id);

        if (!$product) {
            $this->setMessage(__('modules._.record-not-finded'));
            return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
        }
        $title = $this->titleEdit($product);
        $this->addBreadCrumb($title)->setTitle($title);
        $vars['categories'] = $this->productRepository->getCategoriesForProducts();
        $data['content'] = view('admin.products.edit', $vars)->with($vars);

        return $this->main($data);
    }

    public function update(ProductRequest $request, Product $product)
    {
        $product = $this->productRepository->findForEdit((int)$product->id);

        $input = $request->all();
        $product->fillExisting($input);
        if ($product->save()) {
            $this->setSuccessUpdate();
            $product->lang($this->langAdmin)->first()->fillExisting($input)->save();
            $this->saveImage($request, $product);

            $this->fireEvents();
        }
        if ($request->has('saveClose')) {
            return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
        }

        return redirect()->back()->with($this->getResponseMessage());

    }

    /**
     * @param Category $category
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Category $category, Photo $photo)
    {
        if ($category->delete()) {
            $photo->deleteImageStorage($category->getImageAttribute());
            $this->setSuccessDestroy();
            $this->fireEvents();
        }

        return back()->with($this->getResponseMessage());
    }


    public function nesting(Request $request)
    {
        $requestData = $request->get('categories');
        Category::nestable($requestData);
        $this->setSuccessUpdate();

        $this->fireEvents();

        return $this->getResponseMessage();
    }

    private function fireEvents()
    {

    }
}
