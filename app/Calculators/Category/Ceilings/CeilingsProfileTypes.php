<?php


	namespace App\Calculators\Category\Ceilings;


	class CeilingsProfileTypes
	{
		public static function getTypes(): array
		{
			return [
				CeilingsProfileTypesContract::PLASTIC   => _('Пластиковый'),
				CeilingsProfileTypesContract::ALUMINIUM => _('Алюминиевый'),
			];
		}

		public static function getTypesKeys(): array
		{
			return array_keys(self::getTypes());
		}

	}