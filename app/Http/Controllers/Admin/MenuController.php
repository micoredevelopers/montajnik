<?php

namespace App\Http\Controllers\Admin;

use App\Events\Admin\MenusChanged;
use App\Helpers\ResponseHelper;
use App\Http\Requests\Admin\MenuRequest;
use App\Models\Admin\Photo;
use App\Models\Language;
use App\Models\Menu;
use App\Models\MenuGroup;
use App\Models\MenuLang;
use App\Repositories\PageRepository;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;
use Illuminate\Http\Request;

class MenuController extends AdminController
{
    use SaveImageTrait;
    use Authorizable;

    public $thumbnailWidth = false;
    public $thumbnailHeight = false;


    protected $routeKey = 'admin.menu';

    protected $permissionKey = 'menu';

    protected $menuGroup;

    private $name = 'Меню';

    protected $key = 'menu';

    public function __construct(Request $request)
    {
        parent::__construct();
        $this->name = __('modules.menu.title');
        $this->addBreadCrumb($this->name, $this->resourceRoute('index'));
        $this->menuGroup = MenuGroup::find((int)$request->get('group'));
        if ($this->menuGroup) {
            $this->addBreadCrumb($this->menuGroup->name, $this->routeWithGroup($this->resourceRoute('index')));
        }
        $this->shareViewModuleData();
        \View::share('group', $this->menuGroup);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    private function checkGroup()
    {
        if (!$this->menuGroup) {
            return redirect()->back()->with('error', 'group menu not selected')->send();
        }
    }

    private function routeWithGroup($url)
    {
        return $url . '?group=' . $this->menuGroup->id;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $vars['groups'] = MenuGroup::all();
        $this->setTitle($this->name);
        $this->addScripts('libs/nestable2/jquery.nestable.min.js');
        $this->addCss('libs/nestable2/jquery.nestable.min.css');

        $query = Menu::with('allMenus')->GetLang($this->langAdmin)->ParentMenu()->SortOrder();

        $view = 'admin.menu.index-group';
        if ($this->menuGroup) {
            $view = 'admin.menu.index';
            $query->where('menu_group_id', $this->menuGroup->id);
        }
        $vars['list'] = $query->get();
        $data['content'] = view($view, $vars);
        return $this->main($data);
    }

    /**
     * @param PageRepository $pageRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(PageRepository $pageRepository)
    {
        $this->checkGroup();
        $vars['pages'] = $pageRepository->getForAdmin();
        $vars['menus'] = Menu::getForDisplayEdit();
        $data['content'] = view('admin.menu.create')->with($vars);

        return $this->main($data);
    }

    /**
     * @param MenuRequest $request
     * @param Menu $menu
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(MenuRequest $request, Menu $menu)
    {
        $this->checkGroup();

        $input = $request->except('image');
        if ($menu->fillExisting($input)->menuGroup()->associate($this->menuGroup)->save()) {
            $this->setSuccessStore();
            /** @var $language  Language */
            foreach ($this->languagesList as $language) {
                (new MenuLang())->fillExisting($input)
                    ->associateWithLanguage($language)
                    ->menu()->associate($menu)
                    ->save();
            }
            $this->fireEvents();
        }
        $this->saveImage($request, $menu);
        if ($request->has('createOpen')) {
            return redirect($this->routeWithGroup($this->resourceRoute('edit', $menu->id)))->with($this->getResponseMessage());
        }

        return redirect($this->routeWithGroup($this->resourceRoute('index')))->with($this->getResponseMessage());
    }

    /**
     * @param $id
     * @param PageRepository $pageRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, PageRepository $pageRepository)
    {
        $this->checkGroup();
        $vars['groups'] = MenuGroup::all();
        $vars['edit'] = Menu::GetLang($this->langAdmin)->findOrFail($id);
        $title = $this->titleEdit($vars['edit']);
        $this->addBreadCrumb($title)->setTitle($title);

        $vars['menus'] = Menu::getForDisplayEdit();
        $vars['pages'] = $pageRepository->getForAdmin();
        $data['content'] = view('admin.menu.edit', $vars)->with($vars);

        return $this->main($data);
    }

    /**
     * @param MenuRequest $request
     * @param Menu $menu
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(MenuRequest $request, Menu $menu)
    {
        $this->checkGroup();

        $input = $request->all();
        $menu->fillExisting($input);
        if ($menu->save()) {
            $this->setSuccessUpdate();
            $menu->lang($this->langAdmin)->first()->fillExisting($input)->save();
            $this->saveImage($request, $menu);

            $this->fireEvents();
        }
        if ($request->has('saveClose')) {
            return redirect($this->routeWithGroup($this->resourceRoute('index')))->with($this->getResponseMessage());
        }

        return redirect()->back()->with($this->getResponseMessage());
    }


    /**
     * @param Menu $menu
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Menu $menu, Photo $photo)
    {
        if ($menu->delete()) {
            $photo->deleteImageStorage($menu->getImageAttribute());
            $this->setSuccessDestroy();
            $this->fireEvents();
        }

        return back()->with($this->getResponseMessage());
    }

    public function nesting(Request $request)
    {
        $requestData = $request->get('menus');
        Menu::nestable($requestData);
        $this->setMessage(__('generic.successfully_updated'))->setStatus(true);

        $this->fireEvents();

        return $this->getResponseMessageForJson();
    }

    protected function fireEvents()
    {
        event(new MenusChanged());
    }
}
