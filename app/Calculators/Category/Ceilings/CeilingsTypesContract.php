<?php


	namespace App\Calculators\Category\Ceilings;


	interface CeilingsTypesContract
	{
		public const STANDART = 'STANDART';
		public const PREMIUM = 'PREMIUM';
		public const VIP = 'VIP';

	}