<section class="our-works">
  <div class="our-works-slider-bg ">
    <img class="w-100 h-100" src="" alt=""/>
  </div>
  <div class="container position-relative">
    <div class="our-works-slider">
      <div class="our-works-slider__item">
        <img class="w-100 " data-lazy="{{ assetWebp('img/our-works.png') }}" alt=""/>
      </div>
      <div class="our-works-slider__item">
        <img class="w-100 " data-lazy="{{ assetWebp('img/feedback.png') }}" alt=""/>
      </div>
      <div class="our-works-slider__item">
        <img class="w-100 " data-lazy="{{ assetWebp('img/slider-img.png') }}" alt=""/>
      </div>
      <div class="our-works-slider__item">
        <img class="w-100 " data-lazy="{{ assetWebp('img/photo_.png') }}" alt=""/>
      </div>
    </div>
    <a href="#" class="btn-custom btn-custom_our-works d-none d-lg-flex">ВСЕ РАБОТЫ</a>
    <div class="our-works-slider-arrow-box d-flex justify-content-around justify-content-lg-between mb-25 ">
      <div class="our-works__arrows our-works__arrows_prev">
        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="16" viewBox="0 0 26 16">
          <path d="M7.638 15.834c-.109.11-.271.166-.488.166-.217 0-.38-.055-.487-.166-4.28-4.808-6.446-7.24-6.5-7.295C.054 8.429 0 8.263 0 8.04c0-.22.054-.386.163-.497l6.5-7.295c.325-.332.65-.332.975 0 .325.331.325.663 0 .995L2.194 7.378H25.35c.433 0 .65.221.65.663 0 .443-.217.664-.65.664H2.194l5.444 6.134c.325.332.325.664 0 .995z"/>
        </svg>
      </div>
      <div class="our-works-slider-count-box">
        <span class="current"></span> /
        <span class="count"></span>
      </div>
      <div class="our-works__arrows our-works__arrows_next">
        <svg xmlns="http://www.w3.org/2000/svg" style="transform:rotate(180deg)" width="26" height="16" viewBox="0 0 26 16">
          <path d="M7.638 15.834c-.109.11-.271.166-.488.166-.217 0-.38-.055-.487-.166-4.28-4.808-6.446-7.24-6.5-7.295C.054 8.429 0 8.263 0 8.04c0-.22.054-.386.163-.497l6.5-7.295c.325-.332.65-.332.975 0 .325.331.325.663 0 .995L2.194 7.378H25.35c.433 0 .65.221.65.663 0 .443-.217.664-.65.664H2.194l5.444 6.134c.325.332.325.664 0 .995z"/>
        </svg>
      </div>
    </div>
    <div class="our-works-desc">
      <h2 class="title text-left mb-20">
        <span class="decor-bg ">Наши работы</span>
      </h2>
      <p class="text_light mb-25 our-works-text">
        Компания «Монтажник» выполняет полный перечень услуг по ремонту
        и строительству. Строительство жилых домов и строений, ремонтные,
        подготовительные, высотные работы. Оказываем помощь в
        оформлении самостроев, лоджий и балконов. <br>
        <br>
        Проводим электромонтажные, малярные, сантехнические работы,
        монтаж ламината, межкомнатных перегородок, металлоконструкций,
        утепление стен, установку кондиционеров, дверей, жалюзи и ролетов.
        Разрабатываем индивидуальный дизайн интерьера для домов, офисов,
        квартир. Ознакомьтесь с примерами наших работ.
      </p>
      <a href="#" class="btn-custom mx-auto d-lg-none">ВСЕ РАБОТЫ</a>

    </div>
  </div>
  <div class="our-works-decor our-works-decor_first"></div>
  <div class="our-works-decor our-works-decor_second"></div>
</section>