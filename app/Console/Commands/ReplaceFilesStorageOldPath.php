<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ReplaceFilesStorageOldPath extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'files:replace-path {--from=https://montajnik.od.ua/files/storage} {--to=/storage/files/storage}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * @throws \Exception
	 */
	public function handle(): void
	{
		try {
			\DB::beginTransaction();
			foreach ($this->getReplacingTables() as $replacingTable => $columns) {
				foreach ($columns as $column) {
					$sql =
						sprintf('UPDATE %s SET `%s` = REPLACE(`%s`, "%s", "%s")', $replacingTable, $column, $column, $this->getReplaceFrom(), $this->getReplaceTo());
					\DB::update($sql);
					$this->info($this->getReplaceFrom() . ' => ' . $this->getReplaceTo() . ' || ' . $replacingTable . ':' . $column);

				}
			}
			if ($this->confirm('Commit changes?')) {
				\DB::commit();
				$this->info('Changes commited');
			} else{
				\DB::rollBack();
				$this->messageRollBack();
			}
		} catch (\Exception $e) {
			$this->error($e->getMessage());
			\DB::rollBack();
			$this->messageRollBack();
		}
	}

	private function messageRollBack(): void
	{
		$this->info('Changes rollBacked');
	}

	private function getReplacingTables(): array
	{
		return [
			'news_lang'          => [
				'description',
				'except',
			],
			'meta_lang'          => [
				'description',
				'text_bottom',
			],
			'category_page_lang' => [
				'description',
			],
		];
	}

	private function getReplaceFrom(): string
	{
		return $this->option('from');
	}

	private function getReplaceTo(): string
	{
		return $this->option('to');
	}
}
