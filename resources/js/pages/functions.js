// global script
const axios = require("axios").default;
window.addEventListener("load", function() {
  $(document).ready(function() {
    //lazy-load
    $(".lazy").Lazy();
    //burger
    $(".burger").click(function() {
      $(this).toggleClass("open");
    });

    // menu
    $(".sub").slideUp();
    $(".menu-dropdown-link span").click(function(e) {
      $(this)
        .closest(".menu-dropdown-link")
        .siblings(".sub")
        .slideToggle();
      $(this)
        .closest(".sub__item")
        .toggleClass("menu-dropdown__item_open");
      $(this).toggleClass("rotate90");
      $(this)
        .closest(".menu-dropdown-link")
        .toggleClass("menu-dropdown-link_active");
    });

    ////
    // $(".menu-input-wrap-mobile").slideUp();
    $(".menu-dropdown-wrap,.burger").click(function() {
      $(this)
        .children(".menu-dropdown-title")
        .toggleClass("menu-dropdown-title_active");
      $(".menu-dropdown_position").toggleClass("menu-dropdown_position-active");
      $(".overlay").toggleClass("overlay_active");
      $(".menu-input-wrap-mobile").slideToggle();
      if ($(".overlay").hasClass("overlay_active") && $(window).width() > 992) {
        $("header, main").css({
          filter: "grayscale(1)"
        });
      } else
        $("header, main").css({
          filter: "grayscale(0)"
        });
    });

    //////
    $(".overlay").click(function() {
      $(".menu-dropdown-wrap")
        .children(".menu-dropdown-title")
        .toggleClass("menu-dropdown-title_active");
      $(".menu-dropdown_position").toggleClass("menu-dropdown_position-active");
      $(".overlay").toggleClass("overlay_active");
      $(".menu-input-wrap-mobile").slideToggle();
      if ($(".overlay").hasClass("overlay_active") && $(window).width() > 992) {
        $("header, main").css({
          filter: "grayscale(1)"
        });
      } else
        $("header, main").css({
          filter: "grayscale(0)"
        });
    });
  });
  //// search
  function HeaderSearchFunction() {
    let interval = null;

    function request(url, data, successFn, input) {
      $.ajax({
        url,
        data,
        success: res => successFn(res, input)
      });
    }

    function successFn(res, input) {
      let valueArray = res.map(elem => {
        return elem.name;
      });
      // console.log(input);
      valueArray.length === 0
        ? $(".menu-input-wrap span").show()
        : $(".menu-input-wrap span").hide();

      input.autocomplete({
        source: valueArray,
        select: function(event, { item: { value } }) {
          const selectedItem = res.find(elem => {
            return (elem.name = value);
          });
          window.open(selectedItem.url, "_top");
        }
      });
      const e = jQuery.Event("keydown");
      e.which = 40;
      input.trigger(e);
    }

    $(".menu__input").on("input", function() {
      clearInterval(interval);
      const $this = $(this);
      const value = $(this).val();
      const length = value.length;
      const url = $(this).attr("data-url");
      const data = {
        search: value
      };

      interval = setTimeout(function() {
        length >= 2 && request(url, data, successFn, $this);
      }, 600);
    });
  }

  HeaderSearchFunction();

  function sendForm(formSelector, modalSelector) {
    $(formSelector).submit(function(e) {
      e.preventDefault();
      const inputFile = e.target.querySelector('input[type="file"]');
      let $this = $(this);
      let url = $this.attr("action");
      let data = $this.serializeArray();
      let formdata = new FormData();

      data.forEach(function(field) {
        formdata.append(field.name, field.value);
      });

      if (inputFile) {
        formdata.append(
          "image[]",
          e.target.querySelector('input[name="image[]"]').files[0]
        );
      }

      axios({
        url,
        data: formdata,
        method: "POST"
      })
        .then(({ status, data: { message } }) => {
          if (status === 200) {
            fedbackSuccess(message, modalSelector, $this);
          }
        })
        .catch(({ response }) => console.log(response));
    });
  }

  function fedbackSuccess(message, modal, form) {
    $("#success-modal").modal("show");
    $(modal).modal("hide");
    $("#success-modal .modal-content").text(message);
    const fieldArr = form.find("input, textarea");
    $(fieldArr).each((i, item) => {
      $(item).val("");
    });
  }
  sendForm(".feedBack-modal-form", "#feedBackModal");
  sendForm(".callback-form", "#feedBackModalCenter");

  function setSameHeight(divSelector) {
    const height = getMaxHeight();

    function getMaxHeight() {
      let maxHeight = 0;
      divSelector.each((i, item) => {
        // $(item).height()>maxHeight&& maxHeight = $(item).height();
        if ($(item).height() > maxHeight) {
          maxHeight = $(item).height();
        }
      });
      return maxHeight;
    }
    divSelector.height(height);
    // console.log(divSelector);
  }
  setSameHeight($(".reviews-slider__desc"));
  setSameHeight($(".review__item"));
});

$('input[name="phone"]').mask('+38 (099) 999-99-99')

$(function () {
  $('.selectpicker').selectpicker()
});
