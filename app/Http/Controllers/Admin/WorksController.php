<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\WorksRequest;
use App\Models\Work;
use App\Repositories\CategoryRepository;
use App\Repositories\WorkRepository;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;
use Illuminate\Http\Request;

class WorksController extends AdminController
{
    use SaveImageTrait;
    use Authorizable;

    private $name = 'Работы';

    protected $key = 'works';

    protected $routeKey = 'admin.works';

    protected $permissionKey = 'works';

    private $workRepository;

    public function __construct(WorkRepository $workRepository)
    {
        parent::__construct();
        $this->workRepository = $workRepository;
        $this->addBreadCrumb($this->name, $this->resourceRoute('index'));
        $this->shareViewModuleData();
        \View::share('categories', $workRepository->getCategoriesForEdit());
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->setTitle($this->name);
        $vars['list'] = Work::with('category.lang')->paginate();
        $data['content'] = view('admin.works.index', $vars);
        return $this->main($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['content'] = view('admin.works.create');
        return $this->main($data);
    }

    /**
     * @param WorksRequest $request
     * @param CategoryRepository $categoryRepository
     * @param Work $work
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(WorksRequest $request, CategoryRepository $categoryRepository, Work $work)
    {
        $category = $categoryRepository->findById((int)$request->get('category_id'));

        $input = $request->except('_token', 'image');

        if ($work->fillExisting($input)->save()) {
            $work->category()->associate($category)->save();
            $this->setSuccessStore();
            $this->saveImage($request, $work);
        }

        if ($request->has('createOpen')) {
            return redirect($this->resourceRoute('edit', $work->id))->with($this->getResponseMessage());
        }

        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
    }


    /**
     * @param Work $work
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Work $work)
    {
        $vars['edit'] = $work;
        $vars['photosList'] = $work->images;
        $title = $this->titleEdit($work);
        $this->addBreadCrumb($title)->setTitle($title);
        if ($work->category) {
            $categories = \Arr::get(\View::getShared(), 'categories');
            $categories->prepend($work->category);
        }

        $data['content'] = view('admin.works.edit', $vars);
        return $this->main($data);
    }

    /**
     * @param Request $request
     * @param Work $work
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(WorksRequest $request, Work $work)
    {
        $input = $request->except('_token', 'image');
        //
        $this->saveImage($request, $work);
        $work->fillExisting($input);
        if ($work->save()) {
            $this->setSuccessUpdate();
        }
        if ($request->hasFile('images')) {
            $this->saveAdditionalImages($work, $request);
        }

        if ($request->has('saveClose')) {
            return redirect(route('works.index'))->with($this->getResponseMessage());
        }

        return redirect()->back()->with($this->getResponseMessage());
    }

    public function destroy(Work $work)
    {
        if ($work->delete()) {
            $this->photoModel->deleteImageStorage($work->image);
            foreach ($work->images as $image) {
                $this->photoModel->deleteImageStorage($image->image);
            }
            $this->setSuccessDestroy();
        }

        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
    }
}
