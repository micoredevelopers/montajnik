<?php /** @var $feedback \App\Models\Feedback */ ?>
@if ($feedback->getAttribute('name'))
	<p><b>{{ getTranslate('forms.name') }}</b>: {{ $feedback->getAttribute('name') }}</p>
@endif
@if ($feedback->getAttribute('email'))
	<p><b>{{ getTranslate('forms.email') }}</b>: {{ $feedback->getAttribute('email') }}</p>
@endif
