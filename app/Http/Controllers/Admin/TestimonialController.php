<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use App\Http\Requests\Admin\TestimonialRequest;
use App\Models\Admin\Photo;
use App\Models\Category\Category;
use App\Models\Image;
use App\Models\Sky\Comment;
use App\Models\Testimonial;
use App\Models\TestimonialType;
use App\Repositories\CategoryRepository;
use App\Traits\Authorizable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;

class TestimonialController extends AdminController
{
    use Authorizable;

    protected $key = 'testimonials';

    protected $name = 'Отзывы';

    protected $routeKey = 'admin.testimonials';

    protected $permissionKey = 'testimonials';
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        parent::__construct();

        $this->categoryRepository = $categoryRepository;
        $this->addBreadCrumb($this->name, $this->resourceRoute('index'));
        $this->shareViewModuleData();
    }

    public function index()
    {
        $this->setTitle($this->name);
        $list = Testimonial::getForEdit();
        $data['content'] = view('admin.testimonials.index')->with(compact('list'));
        return $this->main($data);
    }

    public function create()
    {
        $categories = $this->categoryRepository->getCategories();
        $types = TestimonialType::all();
        $data['content'] = view('admin.testimonials.create', compact('categories', 'types'));

        return $this->main($data);
    }

    /**
     * @param TestimonialRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(TestimonialRequest $request)
    {
        $input = $request->except('_token');
        ($testimonial = new Testimonial())->fillExisting($input);
        if ($testimonial->save()) {
            $this->setSuccessStore();
        }

        if ($request->has('createOpen')) {
            return redirect($this->resourceRoute('edit', $testimonial->getPrimaryValue()))->with($this->getResponseMessage());
        }

        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
    }


    /**
     * @param Testimonial $testimonial
     * @return Factory|\Illuminate\View\View
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function edit(Testimonial $testimonial)
    {
        $vars['edit'] = $testimonial->load('type');
        $title = $this->titleEdit($testimonial);
        $vars['photosList'] = $testimonial->images;
        $vars['categories'] = $this->categoryRepository->getCategories();
        $vars['types'] = TestimonialType::all();
        $this->addBreadCrumb($title)->setTitle($title);
        $data['content'] = view('admin.testimonials.edit', $vars);

        return $this->main($data);
    }

    /**
     * @param TestimonialRequest $request
     * @param Testimonial $testimonial
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(TestimonialRequest $request, Testimonial $testimonial)
    {
        $input = $request->except('_token');
        $testimonial->fillExisting($input);
        if ($testimonial->save()) {
            $this->setSuccessUpdate();
            if ($request->hasFile('images')) {
                $this->saveAdditionalImages($testimonial, $request);
            }
        }
        if ($request->has('saveClose')) {
            return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
        }
        return redirect()->back()->with($this->getResponseMessage());
    }

    /**
     * @param Testimonial $testimonial
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Testimonial $testimonial, Photo $photo)
    {
        if ($testimonial->delete()) {
            $this->setSuccessDestroy();
            if ($testimonial->images) {
                foreach ($testimonial->images as $image) {
                    /** @var $image Image */
                    $photo->deleteImageStorage($image->getImageAttribute());
                }
            }
        }

        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
    }
}
