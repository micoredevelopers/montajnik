<?php

namespace App\Models\Category;

use App\Contracts\HasLocalized;
use App\Contracts\Models\HasNextPrevAttributes;
use App\Models\Model;
use App\Scopes\SortOrderScope;
use App\Scopes\WhereActiveScope;
use App\Traits\Models\ImageAttributeTrait;
use App\Traits\Models\PrevNextAttributes;
use App\Traits\Models\Relations\Category\BelongsToCategory;
use App\Traits\Models\SubNameAttributeTrait;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * App\Models\Category\Product
 *
 * @property int $id
 * @property string|null $url
 * @property string|null $image
 * @property int $active
 * @property int|null $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $category_id
 * @property-read \App\Models\Category\Category|null $category
 * @property-read mixed $description
 * @property-read mixed $except
 * @property-read mixed $lower_name
 * @property-read mixed $name
 * @property-read mixed $sub_name
 * @property-write mixed $next_id
 * @property-write mixed $prev_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Product whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Product whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Product whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Product whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Product whereUrl($value)
 * @mixin \Eloquent
 */
class Product extends Model implements HasNextPrevAttributes, HasLocalized
{
    use BelongsToCategory;
    use PrevNextAttributes;
    use SubNameAttributeTrait;

    protected $hasOneLangArguments = [ProductLang::class];

    /** @var $categoryPages Collection */

    use ImageAttributeTrait;

    protected $guarded = ['id'];

    /**
     * @return mixed
     */
    public function getProductName()
    {
        return $this->getLangColumn('name');
    }

    public function getLowerNameAttribute()
    {
        return Str::lower($this->getProductName());
    }

    public function getNameAttribute()
    {
        return $this->getProductName();
    }

    public function getExceptAttribute()
    {
        return $this->getLangColumn('except');
    }

    public function getDescriptionAttribute()
    {
        return $this->getLangColumn('description');
    }


    public static function boot()
    {
        parent::boot();

        self::addGlobalScope(new SortOrderScope());
    }

    public static function initScopesPublic()
    {
        if (!self::hasGlobalScope(new WhereActiveScope())) {
            self::addGlobalScope(new WhereActiveScope());
        }
    }
}
