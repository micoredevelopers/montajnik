<?php

namespace App\Repositories;

use App\Models\Redirect;

/**
 * Class RedirectRepository.
 */
class RedirectRepository extends AbstractRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Redirect::class;
    }

    private function initScopes()
    {
        Redirect::initScopesPublic();
    }

    public function findByUrl(string $url)
    {
        $this->initScopes();
        $data = $this->where('from', $url)->first();
        if(is_null($data))
        {
            return null;
        }
        return $data->from === $url? $data : null;
    }
}
