<?php


namespace App\Traits\Repositories;


use App\Contracts\Models\HasNextPrevAttributes;
use App\Models\Model;
use Illuminate\Support\Collection;

trait RebuildNextAndPrev
{

	public function rebuildNextPrev(): void
	{
		$list = $this->getAllForNextPrev();
		/** @var  $model Model */
		foreach ($list as $index => $model) {
			if (!classImplementsInterface($model, HasNextPrevAttributes::class)) {
				continue;
			}
			/** @var  $prev Model */
			$prevId = ($prev = $list->get($index + 1)) ? $prev->getPrimaryValue() : null;
			$model->setPrevIdAttribute($prevId);
			/** @var  $next Model */
			$nextId = ($next = $list->get($index - 1)) ? $next->getPrimaryValue() : null;
			$model->setNextIdAttribute($nextId);

			$model->save();
		}
	}

	public function getAllForNextPrev(): Collection
	{
		return $this->all();
	}

}