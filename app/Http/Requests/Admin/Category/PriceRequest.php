<?php

namespace App\Http\Requests\Admin\Category;

use App\Contracts\Requests\RequestParameterModelable;
use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;
use App\Models\Category\Category;
use App\Traits\Requests\Helpers\GetActionModel;

class PriceRequest extends AbstractRequest
{

    protected $requestKey = 'price';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'category_id'         => ['required', 'exists:categories,id'],
            'price_items.*.name'  => ['max:255'],
            'price_items.*.price' => ['max:255'],
            'price_items.*.unit'  => ['max:255'],
        ];
        return $rules;
    }

}
