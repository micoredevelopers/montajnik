<?php


	namespace App\Http\ViewComponents;


	use App\Contracts\Admin\AdminMenuRepositoryContract;
	use App\Models\Meta;
	use Illuminate\Contracts\Support\Htmlable;
	use Illuminate\Support\Str;

	class SidebarStaffViewComponent implements Htmlable
	{
		private static $alreadyRendered = false;

		/**
		 * @inheritDoc
		 */
		public function toHtml(): string
		{
			if (static::$alreadyRendered || !isAdmin()) {
				return '';
			}
			static::$alreadyRendered = true;
			$data = [];
			if ($meta = Meta::getMetaData()) {
				$data['meta'] = $meta;
			} else {
				$data['metaUrlCreate'] = getUrlWithoutHost(getNonLocaledUrl());
			}
			if (isSuperAdmin()){
				$data['adminMenu'] = app(AdminMenuRepositoryContract::class)->getNestedMenu();
			}
			$robotsClass = file_exists('robots.txt') ? 'btn-success' : 'btn-danger';
			$sitemapClass = file_exists('sitemap.xml') ? 'btn-success' : 'btn-danger';
			$productionLink = $this->getProductionLink();
			$data = array_merge($data, compact(
				'robotsClass'
				, 'sitemapClass'
				, 'productionLink'
			));

			return view('public.dev.sidebar')
				->with($data)
				->render()
				;
		}

		private function getProductionLink(): string
		{
			if (!(isLocalEnv() && env('PRODUCTION_APP_URL'))) {
				return '';
			}
			return Str::replaceFirst(env('APP_URL'), env('PRODUCTION_APP_URL'), request()->fullUrl());
		}

	}
