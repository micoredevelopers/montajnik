<p class=" mb-1 breadcrumbs-wrap">
    @foreach(($breadcrumbs ?? \Breadcrumbs::getBreadcrumbs()) as $item)
        @if (!$loop->last)
            <a href="{{\Arr::get($item, 'url')}}">{{\Arr::get($item, 'name')}}</a>
        @else
            <span>{{\Arr::get($item, 'name')}}</span>
        @endif
        @if(!$loop->last)<span> — </span>@endif
    @endforeach
</p>