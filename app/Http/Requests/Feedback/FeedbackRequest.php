<?php

namespace App\Http\Requests\Feedback;

use App\Models\Feedback;

class FeedbackRequest extends AbstractFeedbackRequest
{

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = $this->getMinimalRules(['email']);
		return $rules;
	}
}
