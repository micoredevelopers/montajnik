<?php /** @var $feedback \App\Models\Feedback */ ?>
<div class="col-md-3">
	@include('admin.feedback.types.partials.name_email')
	@include('admin.feedback.types.partials.phone_address')
</div>
<div class="col-md-3">
	<p>	{{ getTranslate('forms.message') }}: {{ $item->message }}</p>
	@include('admin.feedback.types.partials.fields')
</div>
