<?php


namespace App\Repositories;


use App\Models\Language;
use App\Models\Model;
use Illuminate\Support\Collection;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;

abstract class AbstractRepository extends BaseRepository
{

	public function model()
	{
		return Model::class;
	}

	protected function getLanguagesList(): Collection
	{
		return Language::all();
	}

	public function WhereIsPublished($column = 'published_at')
	{
		return $this->where($column, now(), '<');
	}

	public function first(array $columns = ['*'])
	{
		$this->newQuery()->eagerLoad()->setClauses()->setScopes();

		$model = $this->query->first($columns);

		$this->unsetClauses();

		return $model;
	}

	public function firstOrFail(array $columns = ['*'])
	{
		return parent::first($columns);
	}

	public function whereUrl(string $url)
	{
		return $this->where('url', $url);
	}

	public function findByUrl(string $url)
	{
		return $this->whereUrl($url)->first();
	}
}
