<?php /** @var $category \App\Models\Category\Category */ ?>
<?php /** @var $product \App\Models\Category\Product */ ?>
<main id="product" class="product">
    {{--    <section class="main-page-top">--}}
    {{--        @include('public.category.includes.slider')--}}
    {{--    </section>--}}

    <section class="category-desc">
        <div class="container pt-4 py-lg-5 bg-white">

            <div class="breadcrumbs-category">
                @include('public.partials.breadcrumbs')
            </div>

            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    {!! $product->getDescriptionAttribute() !!}
                </div>
            </div>
        </div>
    </section>

    @include('public.category.includes.why-choose-us')

    <section class="materials pb-3 pb-lg-5">
        <div class="container bg-white py-4">
            @include('public.category.includes.video-reviews')

            @include('public.category.includes.image-reviews')
        </div>
    </section>
</main>

{!! editButtonAdmin(route(routeKey('products', 'edit'), $product->getPrimaryValue())) !!}