<?php

use App\Traits\Migrations\CategoryForeignKey;
use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    use MigrationCreateFieldTypes;
    use CategoryForeignKey;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $this->setTable($table);
            $table->bigIncrements('id');
//            $this->createUniqueUrl();
            $this->createImage();
            $this->createActive();
            $this->createSort();

            $this->createName();
            $this->createDescription();
            $this->createExcept();

            $table->timestamps();

            $this->addForeignCategory();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }

}
