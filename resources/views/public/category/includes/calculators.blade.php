@if ($includeCeilingsCalculator ?? false)
    <section class="calc-block">
        <div class="container bg-white pt-4 py-lg-5">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                @include('public.category.includes.calculators.ceilings')
                </div>
            </div>
        </div>
    </section>
@endif