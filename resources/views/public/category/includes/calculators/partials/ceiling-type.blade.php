<div class="col-lg-6">
     <span class="category-dropdown-title">
       Выберите тип натяжного потолка
     </span>
</div>
<div class="col-lg-6 ">
    <select class="selectpicker" name="type" title="Натяжные потолки" required
            data-style="btn-group category-dropdown" data-width="300px">
        @foreach(\App\Calculators\Category\Ceilings\CeilingsTypes::getTypes() as $id => $typeName)
            <option value="{{ $id }}"
                    @if($loop->first) selected @endif
                    data-style="dropdown-item">{{ $typeName }}</option>
        @endforeach
    </select>
</div>