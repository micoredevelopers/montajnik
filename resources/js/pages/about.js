$(".docs-slider").slick({
    arrows: false,
    slidesToShow: 4,
    infinity: true,
    arrows:true,
    prevArrow:`<div class="docs-slider-arrow arrowPrev"><img src="./img/bigArrow.svg"/></div>`,
    nextArrow:`<div class="docs-slider-arrow arrowNext"><img src="./img/bigArrow.svg"/></div>`,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true,
          arrows:false,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          arrows:false,
        }
      }
    ]
  });