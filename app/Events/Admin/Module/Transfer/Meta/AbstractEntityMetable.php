<?php

namespace App\Events\Admin\Module\Transfer\Meta;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class AbstractEntityMetable
{
    use Dispatchable, SerializesModels;

    private $url;

    private $metaData;

    /**
     * EntityMetableEdited constructor.
     * @param string $url
     * @param array $metaData
     */
    public function __construct(string $url, array $metaData)
    {
        $this->url = $url;
        $this->metaData = $$metaData;
    }

    public function getUrl(): string
    {
        return (string)$this->url;
    }

    public function getMetaData(): array
    {
        return (array)$this->metaData;
    }
}
