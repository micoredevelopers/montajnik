<?php

namespace App\Http\Middleware;

use App\Models\Redirect;
use App\Repositories\RedirectRepository;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RedirectsMiddleware
{
	private $redirectRepository;

	public function __construct(RedirectRepository $redirectRepository)
	{
		$this->redirectRepository = $redirectRepository;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($this->supports($request)){
			$url = getUrlWithoutHost(getNonLocaledUrl());
			$redirect = $this->redirectRepository->findByUrl($url);
			if ($redirect) {
				return redirect($redirect->to, $redirect->code);
			}
		}
		return $next($request);
	}

	private function supports(Request $request): bool
	{
		return !Str::startsWith($request->getPathInfo(), '/admin');
	}
}
