<?php


namespace App\Repositories;


use App\Models\Menu;
use App\Models\MenuGroup;
use App\Models\Page;
use Illuminate\Support\Collection;

class MenuRepository extends AbstractRepository
{
	public function __construct()
	{
		parent::__construct();
	}

	protected function initScopes()
	{
		Page::initScopesPublic();
		Menu::initScopesPublic();
	}


	/**
	 * @return array|null
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function getMenus(): ?array
	{
		$this->initScopes();
		$cacheKey = Page::$publicMenusCacheKey;
		if (!$menus = \Cache::get($cacheKey)) {
			$menus = [];
			$headerLeft = MenuGroup::getByRoleName('main_menu');
			if ($headerLeft) {
				$menus['main_menu'] = $headerLeft->getGroupWithNestedMenu()->menus;
				$menus['main_menu']->load(['lang', 'allMenus']);
			}
			try {
				\Cache::set($cacheKey, $menus);
			} catch (\Exception $e) {
				\Cache::set($cacheKey, []);
			}
		}

		return $menus;
	}
}
