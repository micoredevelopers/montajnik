<?php

namespace App\Traits\Models;

use App\Helpers\MoneyTransformer;

trait CurrencyValueAttributeTrait
{
	public function setValueAttribute($value)
	{
		$value = (float)$value;
		$value = MoneyTransformer::dollarToCent($value);
		$this->attributes['value'] = $value;

		return $this;
	}

	public function getValueAttribute()
	{
		$column = 'value';
		$value = MoneyTransformer::centsToDollar(\Arr::get($this->attributes, $column));
		return $value;
	}
}
