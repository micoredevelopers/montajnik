<?php

	use Illuminate\Database\Seeder;
	use Symfony\Component\HttpFoundation\Response;

	class RedirectsTableSeeder extends Seeder
	{
		private $exists = [];

		public function __construct(\App\Repositories\RedirectRepository $repository)
		{
			$this->repository = $repository;
			$this->exists = $this->repository->all()->keyBy('from');
		}

		/**
		 * Auto generated seed file
		 *
		 * @return void
		 */
		public function run()
		{

			try {
				\DB::table('redirects')->insert($this->getFormattedRedirects());
			} catch (\Throwable $e) {
				dump($e);
				logger($e);
			}


		}

		private function getFormattedRedirects()
		{
			$arr = $this->getOldRedirects();
			$fn = function (&$to, $from) {
				$to = [
					'from' => $from,
					'to'   => $to,
					'code' => (string)Response::HTTP_MOVED_PERMANENTLY,
				];
			};
			array_walk($arr, $fn);
			$arr = array_filter($arr, function ($item) {
				return !$this->redirectExists($item['from']);
			});
			return $arr;
		}

		private function getOldRedirects()
		{
			return $redirects = [
				'/pochemu-nelzya-obsit-balkon-samomu-bez-opyta'                        => '/voprosy-otvety/pochemu-nelzya-obsit-balkon-samomu-bez-opyta',
				'/chem-obshit-balkon-dlya-teploizolyacii'                              => '/voprosy-otvety/chem-obshit-balkon-dlya-teploizolyacii',
				'/chem-obshivat-balkon-vnutri'                                         => '/voprosy-otvety/chem-obshivat-balkon-vnutri',
				'/kakoi-okonnyi-profil-vybrat-v-kvartiru-ofis'                         => '/voprosy-otvety/kakoi-okonnyi-profil-vybrat-v-kvartiru-ofis',
				'/kak-montiruet-kryshu-i-pol-na-balkone-ili-lodzhii-firma-montazhnik'  => '/voprosy-otvety/kak-montiruet-kryshu-i-pol-na-balkone-ili-lodzhii-firma-montazhnik',
				'/kak-pravilno-uteplit-balkon-lodzhiiu-v-odesse'                       => '/voprosy-otvety/kak-pravilno-uteplit-balkon-lodzhiiu-v-odesse',
				'/pochemu-natiazhnoi-potolok-luchshe-chem-gipsokarton'                 => '/voprosy-otvety/pochemu-natiazhnoi-potolok-luchshe-chem-gipsokarton',
				'/kakuiu-moskitnuiu-setku-vybrat'                                      => '/voprosy-otvety/kakuiu-moskitnuiu-setku-vybrat',
				'/daiut-li-shumoizoliatsiiu-natiazhnye-potolki'                        => '/voprosy-otvety/daiut-li-shumoizoliatsiiu-natiazhnye-potolki',
				'/chto-delat-esli-zatopilo-natiazhnye-potolki'                         => '/voprosy-otvety/chto-delat-esli-zatopilo-natiazhnye-potolki',
				'/balconies'                                                           => '/catalog/balconies',
				'/balconies/balconies-from-scratch'                                    => '/catalog/balconies-from-scratch',
				'/balconies/takeaway-balcony'                                          => '/catalog/takeaway-balcony',
				'/balconies/take-out-window-sill'                                      => '/catalog/take-out-window-sill',
				'/balconies/sheathing-balcony'                                         => '/catalog/sheathing-balcony',
				'/balconies/lighting-electrician'                                      => '/catalog/lighting-electrician',
				'/balconies/glazing-of-balconies-and-loggias'                          => '/catalog/glazing-of-balconies-and-loggias',
				'/balconies/info'                                                      => '/catalog/info',
				'/balconies/drying-on-the-balcony'                                     => '/catalog/drying-on-the-balcony',
				'/balconies/the-insulation-of-the-balconies'                           => '/catalog/the-insulation-of-the-balconies',
				'/balconies/french-balcony'                                            => '/catalog/french-balcony',
				'/balconies/wardrobe-on-the-balcony-and-a-loggia'                      => '/catalog/wardrobe-on-the-balcony-and-a-loggia',
				'/doors'                                                               => '/catalog/doors',
				'/doors/locks-and-fittings'                                            => '/catalog/locks-and-fittings',
				'/doors/categories-doors-guardian'                                     => '/catalog/categories-doors-guardian',
				'/doors/categories-doors-mottura-is'                                   => '/catalog/categories-doors-mottura-is',
				'/doors/categories-doors-multilok'                                     => '/catalog/categories-doors-multilok',
				'/doors/categories-doors-stallard'                                     => '/catalog/categories-doors-stallard',
				'/doors/categories-doors-standard'                                     => '/catalog/categories-doors-standard',
				'/doors/categories-doors-economy'                                      => '/catalog/categories-doors-economy',
				'/doors/the-range'                                                     => '/catalog/the-range',
				'/blinds-rolety'                                                       => '/catalog/blinds-rolety',
				'/blinds-rolety/bamboo-and-jute'                                       => '/catalog/bambukovye-i-dzhutovye-292',
				'/blinds-rolety/vertical-blinds'                                       => '/catalog/vertikalnye-zhalyuzi-293',
				'/blinds-rolety/horizontal-blinds'                                     => '/catalog/gorizontalnye-zhalyuzi-294',
				'/blinds-rolety/closed-systems-blinds'                                 => '/catalog/zakrytye-sistemy-rulonnye-shtory-295',
				'/blinds-rolety/roof-systems'                                          => '/catalog/mansardnye-sistemy-296',
				'/blinds-rolety/open-systems-blinds'                                   => '/catalog/otkrytye-sistemy-rulonnye-shtory-297',
				'/blinds-rolety/rolety-day-night'                                      => '/catalog/rolety-den-noch-298',
				'/blinds-rolety/roller-blinds'                                         => '/catalog/rulonnye-shtory-299',
				'/air-conditioning'                                                    => '/catalog/air-conditioning',
				'/air-conditioning/dismantling'                                        => '/catalog/demontazh-300',
				'/air-conditioning/bookmark'                                           => '/catalog/zakladka-301',
				'/air-conditioning/refill'                                             => '/catalog/zapravka-302',
				'/air-conditioning/installation'                                       => '/catalog/montazh-kondicionerov-303',
				'/air-conditioning/repair'                                             => '/catalog/remont-kondicionerov-304',
				'/air-conditioning/service'                                            => '/catalog/servis-305',
				'/ceilings'                                                            => '/catalog/ceilings',
				'/ceilings/bezshchelevye-stretch-ceilings'                             => '/catalog/bezschelevye-natyazhnye-potolki-306',
				'/ceilings/glossy-ceilings'                                            => '/catalog/glyancevye-natyazhnye-potolki-307',
				'/ceilings/how-to-wash-the-suspended-ceiling'                          => '/catalog/kak-myt-natyazhnoy-potolok-308',
				'/ceilings/what-preliminary-work-must-be-done'                         => '/catalog/kakie-predvaritelnye-raboty-dolzhny-byt-proizvedeny-309',
				'/ceilings/matt-ceilings'                                              => '/catalog/matovye-natyazhnye-potolki-310',
				'/ceilings/stretch-ceilings'                                           => '/catalog/natyazhnye-potolki-311',
				'/ceilings/sateen-tension-ceilings'                                    => '/catalog/natyazhnye-potolki-311',
				'/ceilings/stretch-ceilings-with-the-effect-of-starry-sky'             => '/catalog/natyazhnye-potolki-s-effektom-quot-zvezdnoe-nebo-quot-312',
				'/ceilings/palette'                                                    => '/catalog/palitra-313',
				'/ceilings/pariashchie-potolki-innovatsionnaia-tekhnologiia-otdelki'   => '/catalog/paryaschie-potolki',
				'/ceilings/info'                                                       => '/catalog/poleznaya-informaciya-o-natyazhnyh-potolkah-315',
				'/ceilings/ceiling-with-illumination'                                  => '/catalog/potolok-s-podsvetkoy-316',
				'/ceilings/preimushchestva-dvukhurovnevykh-natiazhnykh-potolkov'       => '/catalog/preimuschestva-dvuhurovnevyh-natyazhnyh-potolkov-317',
				'/ceilings/advantages-ceilings'                                        => '/catalog/preimuschestva-potolkov-318',
				'/window'                                                              => '/catalog/window',
				'/window/montazh-okon'                                                 => '/catalog/montazh-okon-319',
				'/window/podokonniki'                                                  => '/catalog/podokonniki-320',
				'/window/kommerling'                                                   => '/catalog/Kommerling-321',
				'/window/rehau'                                                        => '/catalog/Rehau-322',
				'/window/osnova'                                                       => '/catalog/Osnova-323',
				'/window/wds'                                                          => '/catalog/WDS-324',
				'/window/kbe'                                                          => '/catalog/KBE-325',
				'/window/info'                                                         => '/catalog/poleznye-stati-o-metalloplastikovyh-oknah-326',
				'/window/repair-of-windows'                                            => '/catalog/remont-okon-327',
				'/window/rules-of-exploitation-of-window'                              => '/catalog/pravila-ekspluatacii-okon-328',
				'/window/window-fittings'                                              => '/catalog/okonnaya-furnitura-329',
				'/window-accessories'                                                  => '/catalog/window-accessories',
				'/window-accessories/vnutriramnaia-net'                                => '/catalog/vnutriramnaya-setka-330',
				'/window-accessories/mosquito-net-on-pockets'                          => '/catalog/moskitnaya-setka-na-karmashkah-331',
				'/window-accessories/accessories'                                      => '/catalog/okonnaya-furnitura-332',
				'/window-accessories/the-ebb-and-visors'                               => '/catalog/otlivy-i-kozyrki-333',
				'/window-accessories/window-sills'                                     => '/catalog/podokonniki-334',
				'/window-accessories/grid-hinges'                                      => '/catalog/setki-na-petlyah-335',
				'/reshetki'                                                            => '/catalog/reshetki',
				'/construction-and-repair'                                             => '/catalog/construction-and-repair',
				'/construction-and-repair/floor-leveling'                              => '/catalog/vyravnivanie-pola-336',
				'/construction-and-repair/the-alignment-of-the-ceiling'                => '/catalog/vyravnivanie-potolka-337',
				'/construction-and-repair/alignment-of-walls'                          => '/catalog/vyravnivanie-sten-338',
				'/construction-and-repair/dismantling'                                 => '/catalog/demontazhnye-raboty-339',
				'/construction-and-repair/wallpapering'                                => '/catalog/pokleyka-oboev-340',
				'/construction-and-repair/painting-ceilings'                           => '/catalog/pokraska-potolkov-341',
				'/construction-and-repair/info'                                        => '/catalog/info-remont',
				'/construction-and-repair/laying-ceramic-tile-odessa'                  => '/catalog/ukladka-keramicheskoy-plitki-343',
				'/construction-and-repair/installation-of-sockets-and-switches-odessa' => '/catalog/ustanovka-rozetok-i-vyklyuchateley-344',
				'/insulation-of-walls'                                                 => '/catalog/insulation-of-walls',
				'/insulation-of-walls/sealing-repair-of-seams-between-panels'          => '/catalog/germetizaciya-remont-mezhpanelnyh-shvov-345',
				'/insulation-of-walls/warming-of-apartments'                           => '/catalog/uteplenie-kvartir-346',
				'/insulation-of-walls/insulation-of-walls'                             => '/catalog/uteplenie-sten-347',
				'/insulation-of-walls/insulation-of-walls-plaster'                     => '/catalog/uteplenie-sten-shtukaturkoy-348',
				'/insulation-of-walls/the-insulation-of-the-facade'                    => '/catalog/uteplenie-fasada-349',
				'/insulation-of-walls/to-insulate-the-house-from-the-outside'          => '/catalog/uteplit-dom-snaruzhi-350',
				'/photo-gallery'                                                       => '/photos/nashi-raboty',
				'/feedback'                                                            => '/comments/all',
				'/video'                                                               => '/video/all',
				'/site-news'                                                           => '/news/all',
				'/balconies/info/kozyrki-i-kryshi-na-balkon'                           => '/catalog/kozyrki-i-kryshi-na-balkon',
				'/balconies/info/neobychnye-dizainy-balkonov-i-lodzhii-odessa'         => '/catalog/neobychnye-dizainy-balkonov-i-lodzhii-odessa',
				'/balconies/info/osteklenie-verand-i-terras-v-odesse'                  => '/catalog/osteklenie-verand-i-terras-v-odesse',
				'/balconies/info/teploe-osteklenie-balkonov-v-odesse'                  => '/catalog/teploe-osteklenie-balkonov-v-odesse',
				'/ceilings/info/mnogourovnevye-natiazhnye-potolki'                     => '/catalog/mnogourovnevye-natiazhnye-potolki',
				'/window/info/kakie-byvaiut-plastikovye-okna'                          => '/catalog/kakie-byvaiut-plastikovye-okna',
				'/window/info/kak-provesti-zamer-okon-samomu'                          => '/catalog/kak-provesti-zamer-okon-samomu',
				'/window/info/kalkuliator-stoimosti-plastikovykh-okon-v-odesse'        => '/catalog/kalkuliator-stoimosti-plastikovykh-okon-v-odesse',
				'/window/info/plastikovye-okna-ekonom-klassa-odessa'                   => '/catalog/plastikovye-okna-ekonom-klassa-odessa',
				'/window/info/sravnenie-pvkh-profilei-odessa'                          => '/catalog/sravnenie-pvkh-profilei-odessa',
				'/window/info/tonirovka-plastikovykh-okon-v-odesse'                    => '/catalog/tonirovka-plastikovykh-okon-v-odesse',
				'/window/info/tsvetnye-plastikovye-okna-v-odesse'                      => '/catalog/tsvetnye-plastikovye-okna-v-odesse',
				'/window/info/zerkalnye-steklopakety-odessa'                           => '/catalog/zerkalnye-steklopakety-odessa',
				'/site-news/1003'                                                      => '/news/oformlenie-balkonov-tendencii-stilya-v-2017-godu',
				'/site-news/996'                                                       => '/news/glavnye-osobennosti-remonta-kvartiry',
				'/window-accessories/accessories/378'                                  => '/catalog/window-accessories',
				'/window-accessories/window-sills/382'                                 => '/catalog/window-accessories',
				'/site-news/385'                                                       => '/news/385',
				'/insulation-of-walls/insulation-of-walls-in-odessa'                   => '/catalog/insulation-of-walls-in-odessa',
			];
		}

		private function redirectExists($urlFrom)
		{
			return $this->exists->offsetExists($urlFrom);
		}
	}