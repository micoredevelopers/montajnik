<?php


namespace App\Helpers\Miscellaneous;


use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class Breadcrumbs
{
    public static $template = 'public.partials.breadcrumbs';

    protected static $breadcrumbsData = [];

    public static function addBreadCrumb($breadName, $breadUrl = ''): void
	{
        self::$breadcrumbsData[] = ['name' => $breadName, 'url' => $breadUrl];
    }

    public static function dropLastBreadCrumb(): void
	{
        if (self::$breadcrumbsData) {
            array_pop(self::$breadcrumbsData);
        }
    }

    public static function dropAllBreadCrumbs(): void
	{
        self::$breadcrumbsData = [];
    }

    /**
     * @param bool $name
     * @return array
     */
    public static function getBreadCrumbs($name = false): array
	{
        if ($name) {
            self::addBreadCrumb($name);
        }

        return self::$breadcrumbsData;
    }

    /**
     * @return Factory|View
     */
    public static function render()
    {
        return view(self::$template)->with(['breadcrumbs' => self::getBreadCrumbs()]);
    }

}