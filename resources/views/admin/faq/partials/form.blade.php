<div class="row">
    <div class="col-6">
		@include('admin.partials.crud.default', ['name'=>'question','title'=>'Вопрос'])
    </div>
    <div class="col-6">
        @include('admin.partials.crud.elements.active')
    </div>
	<div class="col-6">
		@include('admin.partials.crud.default',['name'=>'url','title'=>'Ссылка'])
	</div>

</div>
<div class="row">

	<div class="col-12">
		@include('admin.partials.crud.textarea',['name'=>'answer','title'=>'Ответ'])
	</div>
</div>
@section('javascript')

	<script defer>
		$(document).ready(function (e) {
			{!! showEditor('answer') !!}
		})
	</script>
@stop
