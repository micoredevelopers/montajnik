<?php

namespace App\Traits\Models;


trait ImageAttributeTrait
{
	public function setImageAttribute($name)
	{
		if (is_string($name)) {
			$column = 'image';
			$this->attributes[$column] = $name;
		}
		return $this;
	}

	public function getImageAttribute()
	{
		$column = 'image';
		$name = \Arr::get($this->attributes, $column);
		return $name;
	}

}