<?php

namespace App\Models;

use App\Scopes\SortOrderScope;

/**
 * App\Models\FaqItems
 *
 * @property-read \App\Models\Faq $faq
 * @property mixed $answer
 * @property mixed $question
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqItems newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqItems newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqItems query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class FaqItems extends Model
{
	protected $guarded = [];

	public function faq()
	{
		return $this->belongsTo(Faq::class);
	}

	public function getQuestionAttribute()
	{
		return \Arr::get($this->attributes, 'question');
	}

	public function setQuestionAttribute($question)
	{
		$this->attributes['question'] = \Str::limit($question, 1000, '');
		return $this;
	}

	public function getAnswerAttribute()
	{
		return \Arr::get($this->attributes, 'answer');
	}

	/**
	 * @param $question
	 * @return $this
	 * limit symbols - pseudo validation
	 */
	public function setAnswerAttribute($question)
	{
		$this->attributes['answer'] = \Str::limit($question, 65000, '');
		return $this;
	}


	public static function boot()
	{
		parent::boot();

		self::addGlobalScope(new SortOrderScope());
	}

}
