<?php


namespace App\Http\Requests;


use App\Traits\Requests\Helpers\IsAction;
use App\Traits\Requests\Helpers\RequestAttributesCaster;
use Illuminate\Foundation\Http\FormRequest;

class AbstractRequest extends FormRequest
{
	protected $toBooleans = [];

	use RequestAttributesCaster;

	use IsAction;

	protected $requestKey = '';

	public function authorize()
	{
		return true;
	}

	/**
	 * @param $fields = [ 'email' => ['required', 'email', '...'] ]
	 * @param array $except = ['password', 'email', '...']
	 * @return array
	 */
	protected function exceptFields($fields, $except = []): array
	{
		return array_diff_key($fields, array_flip($except));
	}


	public function getRequestKey(): string
	{
		return $this->requestKey;
	}

	protected function prepareForValidation()
	{
		$this->mergeRequestValues();
		$this->mapCasts();
	}

	protected function mergeRequestValues() { }

	protected function castCheckboxes(array $checkboxes)
	{
		foreach ($checkboxes as $checkbox) {
			$this->merge([$checkbox => $this->toCheckbox($this->get($checkbox))]);
		}
	}

	protected function toCheckbox($value)
	{
		return (int)$value;
	}

	protected function mergeUrlFromName()
	{
		$this->mergeUrlFromField();
	}

	protected function mergeUrlFromTitle()
	{
		$this->mergeUrlFromField('title');
	}

	protected function mergeUrlFromField($field = 'name')
	{
		if (!$this->get('url')) {
			$this->merge(['url' => $this->get($field)]);
		}
		$this->merge([
			'url' => \Str::slug($this->get('url')),
		]);
	}


	protected function forgetData($keys): void
	{
		if (is_array($keys)) {
			foreach ($keys as $key) {
				$this->forgetData($key);
			}
		}
		$all = $this->all();
		\Arr::forget($all, $keys);
		$this->replace($all);
	}
}
