<?php /** @var $edit \App\Models\Category\Category */ ?>
<?php /** @var $category \App\Models\Category\Category */ ?>
<div role="tabpanel" class="tab-pane active" id="dataDefault">
    <div class="panel-body">

        @include('admin.category.partials.form')

        @if ($edit->categories->isNotEmpty())
            <div class="form-group">
                <h3>{{ __('modules.category.childrens') }}</h3>
                @foreach($edit->categories as $category)
                    @php
                    $classBadge = $category->active ? 'success' : 'danger';
                    @endphp
                    <a href="{{ route($routeKey.'.edit', $category->id) }}"
                       class="btn btn-{{ $classBadge }} btn-sm">{{ $category->name }}</a>
                @endforeach
            </div>
        @endif
    </div>
</div>