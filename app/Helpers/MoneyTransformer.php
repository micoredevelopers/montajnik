<?php

namespace App\Helpers;


class MoneyTransformer
{

	/**
	 * @param $dollars
	 * @return float|int
	 * применяется для методов Set
	 */
	public static function dollarToCent($dollars)
	{
		$dollars = (float)$dollars;
		$cents = $dollars * 100;
		$cents = (int)$cents;
		return $cents;
	}

	/**
	 * @param $cents
	 * @return float|int
	 * применяется для методов Get
	 */
	public static function centsToDollar($cents)
	{
		$cents = (int)$cents;
		$dollars = $cents / 100;
		$dollars = (float) $dollars;
		return $dollars;
	}


}