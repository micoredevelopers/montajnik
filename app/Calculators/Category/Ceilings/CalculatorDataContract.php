<?php


	namespace App\Calculators\Category\Ceilings;


	interface CalculatorDataContract{
		public function getType();
		public function setType($type): CalculatorDataContract;

		public function getLength(): int;
		public function setLength(int $length): CalculatorDataContract;

		public function getWidth(): int;
		public function setWidth(int $width): CalculatorDataContract;

		public function getProfile();
		public function setProfile($perimeterProfile): self;

		public function getGapless(): int;
		public function setGapless(int $gapless): self;

		public function getStarSky(): int;
		public function setStarSky(int $starSky): self;

		public function getAngles(): int;
		public function setAngles(int $angles): self;

		public function getLight(): int;
		public function setLight(int $light): self;

		public function getPipe(): int;
		public function setPipe(int $pipe): self;

		public function getCurvature(): int;
		public function setCurvature(int $curvature): self;

		public function getBaguette(): int;
		public function setBaguette(int $baguette): self;

	}