$(document).ready(function (e) {
  function videoReviewSlider() {
    $(".video-reviews-slider").slick({
      slidesToShow: 2,
      arrows: false,
      infinite: false,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 600,
          settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  }
  $(".video-reviews-slider").click(function(e) {
    let that;
    if (e.target.classList.contains("video-reviews-iframe")) {
      that = $(e.target);
    } else if (e.target.classList.contains("play-logo-img")) {
      that = $(e.target)
          .parent()
          .siblings(".video-reviews-iframe");
    } else {
      return false;
    }

    const width = that.width();
    const height = that.height();
    const dataSrc = that.attr("data-src");
    const iframe = `<iframe
                    width=${width}
                    height=${height}
                    src="${dataSrc}"
                    frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                  ></iframe>`;
    that.closest(".iframe-box").html(iframe);
  });
  videoReviewSlider()
  $("#product .reviews-slider").slick({
    lazyLoad: "ondemand",
    arrows: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    prevArrow:
        '<div class="reviews-slider-arrow reviews-slider-arrow_prev"></div>',
    nextArrow:
        '<div class="reviews-slider-arrow reviews-slider-arrow_next"></div>',
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
  });

})