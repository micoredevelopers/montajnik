<?php

namespace App\Traits\Models\User;

trait UserAccessorsTrait
{
	private static $passwords = [
		'superadmin' => '&<cS#p#Z+Ed$7W9#B5FjwU9.MpXZ/$KU', // Passwords for get him from CLI if forget
		'admin' => 'GKqFzL3Tx^n5Z~?(',
	];

	public static function getPassword($userName = 'admin')
	{
		return \Arr::get(self::$passwords, $userName);
	}

}
