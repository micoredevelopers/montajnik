<?php

	namespace App\Models;

	use App\Contracts\Models\HasNextPrevAttributes;
	use App\Scopes\SortOrderScope;
	use App\Traits\Models\PrevNextAttributes;
	use App\Traits\Models\Scopes\Init\InitPublicScopeActive;
	use App\Traits\Models\UrlAttributeTrait;

	/**
	 * App\Models\Faq
	 *
	 * @property int                             $id
	 * @property string|null                     $question
	 * @property string|null                     $answer
	 * @property int                             $sort
	 * @property bool                            $active
	 * @property mixed                           $url
	 * @property int|null                        $prev_id Previous news id
	 * @property int|null                        $next_id Next news id
	 * @property \Illuminate\Support\Carbon|null $created_at
	 * @property \Illuminate\Support\Carbon|null $updated_at
	 * @property-read \App\Models\Faq            $nextFaq
	 * @property-read \App\Models\Faq            $prevFaq
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq newModelQuery()
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq newQuery()
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq query()
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereActive($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereAnswer($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereCreatedAt($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereId($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereNextId($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq wherePrevId($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereQuestion($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereSort($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereUpdatedAt($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereUrl($value)
	 * @mixin \Eloquent
	 */
	class Faq extends Model implements HasNextPrevAttributes
	{
		use InitPublicScopeActive;
		use PrevNextAttributes;
		use UrlAttributeTrait;

		protected $casts = [
			'active' => 'boolean',
		];

		protected $guarded = ['id'];

		public static function getForDisplay()
		{
			return self::active()->get();
		}

		public function getQuestionAttribute()
		{
			return \Arr::get($this->attributes, 'question');
		}

		public function setQuestionAttribute($question)
		{
			$this->attributes['question'] = \Str::limit($question, 1000, '');
			return $this;
		}

		public function getAnswerAttribute()
		{
			return \Arr::get($this->attributes, 'answer');
		}

		/**
		 * @param $question
		 * @return $this
		 * limit symbols - pseudo validation
		 */
		public function setAnswerAttribute($question)
		{
			$this->attributes['answer'] = \Str::limit($question, 65000, '');
			return $this;
		}

		public function prevFaq()
		{
			return $this->hasOne(Faq::class, 'prev_id')->select(['id', 'prev_id', 'next_id', 'question', 'url']);
		}

		public function nextFaq()
		{
			return $this->hasOne(Faq::class, 'next_id')->select(['id', 'prev_id', 'next_id', 'question', 'url']);
		}

		public static function boot()
		{
			parent::boot();

			self::addGlobalScope(new SortOrderScope());
		}
	}
