<?php


namespace App\Repositories\Admin;

use \App\Contracts\Admin\AdminMenuRepositoryContract;
use App\Contracts\HasMenuContent;
use App\Models\Admin\Admin_menu;
use App\Repositories\AbstractRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;

class AdminMenuRepository extends BaseRepository implements AdminMenuRepositoryContract
{

	private $cacheKey = 'admin.menu.nested';

	private $activeMenus = [];

	public function model()
	{
		return Admin_menu::class;
	}

	public function dropMenuCache(): bool
	{
		return \Cache::forget($this->cacheKey);
	}

	private function getMenuFromCache()
	{
		if (!$menus = \Cache::get($this->cacheKey)) {
			$menus = (Admin_menu::with('childrens')->active()->ParentMenu()->get());
			$menus = $this->checkMenuValid($menus);
			$menus = $this->getMenuSubContent($menus);
			$this->addParentLink($menus);
			\Cache::set($this->cacheKey, $menus);
		}
		return $menus;
	}

	public function getNestedMenu(): Collection
	{
		/** @var $menus Collection */
		$menus = $this->getMenuFromCache();
		$menus = $this->checkMenuAccess($menus);
		$this->detectCurrentMenu($menus);

		return $menus;
	}

	private function addParentLink(Collection $menus, ?Admin_menu $parent = null)
	{
		$menus->map(function (Admin_menu $menu) use ($parent) {
			$menu->setRelation('parent', $parent);
			if ($menu->childrens->isNotEmpty()) {
				$this->addParentLink($menu->childrens, $menu);
			}
		});
	}

	private function detectCurrentMenu(Collection $menus)
	{
		$this->checkForCurrentMenu($menus);
		if ($this->activeMenus) {
			$this->matchActiveMenu($this->activeMenus);
		}
	}


	private function matchActiveMenu(array $array)
	{
		$menus = Collection::wrap($array);
		$menus = $menus->sortByDesc(static function (Admin_menu $item) {
			return mb_strlen($item->url);
		});
		if ($activeMenu = $menus->first()) {
			$this->activateCurrentMenu($activeMenu);
		}
	}

	private function activateCurrentMenu($menu)
	{
		$menu->current = 1;
		$this->setActiveChainParentsForCurrentMenu($menu);
	}

	private function checkForCurrentMenu(Collection $menus)
	{
		$menus->map(function ($menu) {
			if (isMenuActiveByUrl($menu->url)) {
				$this->addActiveMenu($menu);
			}
			if ($menu->childrens->isNotEmpty()) {
				$this->checkForCurrentMenu($menu->childrens);
			}
			return $menu;
		});
	}

	private function setActiveChainParentsForCurrentMenu(Admin_menu $menu)
	{
		/** @var  $parent Admin_menu */
		if ($parent = $menu->parent) {
			$parent->setAttribute('show', true);
			if ($parent->parent) {
				$this->setActiveChainParentsForCurrentMenu($parent);
			}
		}
	}

	private function checkMenuValid(Collection $menus)
	{
		foreach ($menus as $num => $menu) {
			$valid = 1;
			if (!$menu->url) {
				$valid = 0;
			}

			if (!$valid) {
				$menus->forget($num);
				continue;
			}

			if ($menu->childrens->isNotEmpty()) {
				$menu->childrens = $this->checkMenuValid($menu->childrens);
			}
		}
		return $menus;
	}

	private function checkMenuAccess(Collection $menus)
	{
		$menus = $menus->filter(static function ($menu) {
			try {
				return \Gate::allows($menu->gate_rule);
			} catch (\Exception $e) {
				return false;
			}
		});
		return $menus;
	}

	private function getMenuSubContent(Collection $menus): Collection
	{
		$menus = $menus->map(function ($menu) {
			$menu->content = $this->getMenuContentProvider($menu);
			if ($menu->childrens->isNotEmpty()) {
				$menu->childrens = $this->getMenuSubContent($menu->childrens);
			}
			return $menu;
		});

		return $menus;
	}

	private function getMenuContentProvider(Admin_menu $menu)
	{
		$content = '';
		if ($menu->content_provider) {
			$class = $menu->content_provider;
			try {
				$interface = HasMenuContent::class;
				if (class_exists($class) AND classImplementsInterface($class, $interface)) {
					/** @var  $instance HasMenuContent*/
					$instance = new $class;
					$content = $instance->getMenuContent();
				}
			} catch (\Error $e) {
				//
			}
		}
		return $content;
	}

	private function addActiveMenu(Admin_menu $menu)
	{
		$this->activeMenus[] = $menu;
	}
}
