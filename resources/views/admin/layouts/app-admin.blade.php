<!DOCTYPE html>
<html lang="{{ getCurrentLocale() }}">
<head>
	<meta charset="utf-8"/>
	<title>{!! \Artesaos\SEOTools\Facades\SEOMeta::getTitle() !!}</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no"
		  name="viewport"/>
	<link rel="stylesheet" href="{{ assetVersioned('css/admin/admin-main.css') }}"/>
	<link rel="stylesheet" href="{{ assetVersioned('css/admin/material-dashboard.css') }}"/>
	<link rel="stylesheet" href="{{ assetVersioned('css/admin/styles.css') }}"/>
	<script type="text/javascript" src="{{ asset( 'js/core/jquery.min.js') }}"></script>

	{!! $styles !!}
</head>
<body>

@include('admin.partials.preloader')

<div class="wrapper blurred-app" id="app">

	<div class="sidebar" data-color="purple" data-background-color="white" data-image="">
		<div class="logo text-center">
			<a href="{{ route('home')}}" class="simple-text logo-normal">{{ Str::words(getSetting('global.sitename'), 2) }}</a>
			<small>{{ Auth::user()->email }}</small>
		</div>
		<div class="sidebar-wrapper position-static">
			@include('admin.menu')
		</div>
	</div>
	<div class="main-panel">

		@include('admin.header')

		@if (\Arr::has($sections, 'contentHasWrapper'))
			{!! $content ?? '' !!}
		@else
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-10 offset-1 ">
							<div class="card">
								<div class="card-header card-header-primary">
									<h4 class="card-title">{{ $cardTitle ?? '' }}</h4>
								</div>
								<div class="card-body">
									{!! $content ?? '' !!}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
	</div>
</div>

@include('admin.langlist')

@include('admin.layouts.partials.styles')

@include('admin.layouts.partials.scripts')

@include('admin.partials.flash-message')
</body>
</html>















