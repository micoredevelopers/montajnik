<?php

namespace App\Traits;


use App\Contracts\HasLocalized;
use App\Models\Model;
use Illuminate\Database\Eloquent\Scope;

trait EloquentExtend
{

	protected static $schema = [];

	public function getTableColumns()
	{
		if (!\Arr::has(static::$schema, $this->getTable())) {
			$columns = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
			\Arr::set(static::$schema, $this->getTable(), $columns);
		}
		return \Arr::get(static::$schema, $this->getTable());
	}


	/**
	 * @param array $attributes
	 * @param bool $override
	 * @return $this
	 */
	public function fillExisting(array $attributes, $override = true): self
	{
		$schema = array_flip($this->getTableColumns());
		$attributes = array_filter($attributes, function ($value, $column) use ($schema) {
			$exists = \Arr::exists($schema, $column);
			$isGuarded = $this->isGuarded($column);
			return ($exists AND !$isGuarded);
		}, ARRAY_FILTER_USE_BOTH);

		foreach ($attributes as $attribute => $value) {
			if (!$override AND $this->isDirty($attribute)){
				continue;
			}
			$this->setAttribute($attribute, $value);
		}

		return $this;
	}

	public function getPrimaryValue()
	{
		$key = $this->getKeyName();
		$key = \Arr::wrap($key);
		$key = array_shift($key);
		return $this->getAttribute($key);
	}

	public static function findByUrl(string $url): ?self
	{
		return self::whereUrl($url)->first();
	}

	protected static function removeGlobalScope($scope, \Closure $implementation = null): void
	{
		$key = false;
		if (is_string($scope) && $implementation !== null) {
			$key = $implementation;
		} else if ($scope instanceof \Closure) {
			$key = spl_object_hash($scope);
		} else if ($scope instanceof Scope) {
			$key = get_class($scope);
		}

		if ($key !== false && \Arr::has(static::$globalScopes, static::class)) {
			\Arr::forget(static::$globalScopes[static::class], $key);
		}
	}

	public function getLangColumn(string $column)
	{
		if (classImplementsInterface($this, HasLocalized::class) && $this->lang) {
			return $this->lang->{$column} ?? '';
		}
		if (\Arr::has($this->attributes, $column)) {
			return \Arr::get($this->attributes, $column);
		}
		return '';
	}

	public function hasAttribute(string $attribute):bool
	{
		return array_key_exists($attribute, $this->attributes);
	}

}

