<?php


namespace App\Repositories;


use App\Models\Page;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;

class PageRepository extends AbstractRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function initScopes()
    {
        Page::initScopesPublic();
    }

    public function getForPublic(): ?Collection
    {
        $this->initScopes();
        return Page::GetLang()->get();
    }

    public function getForAdmin($pageId = []): ?Collection
    {
        $query = Page::GetLang();
        if ($pageId) {
            $query->whereNotIn('id', [$pageId]);
        }
        return $query->get();
    }

    public function getListForAdmin(Request $request)
    {
        $query = Page::with('lang')->latest()->GetLang();
        if ($request->has('search')) {
            $search = $request->get('search');
            $query->where(function (Builder $builder) use ($search) {
                $builder->where('title', 'like', '%' . $search . '%')
                    ->orWhere('url', 'like', '%' . $search . '%')
                    ->orWhere('description', 'like', '%' . $search . '%');
            });
        }
        return $query->paginate();
    }

    public function findPageByUrl($url): ?Page
    {
        $this->initScopes();
        return Page::where('url', $url)->GetLang()->first();
    }

    public function findManualPageByUrl(string $url): ?Page
    {
        $this->initScopes();
        return Page::where('url', $url)->GetLang()->first();
    }

    public function getPageParents(Page $page): Collection
    {
        $parents = collect([]);
        if ($page->parent) {
            $parent = $page->parent;
            $parents->push($parent);
            while (!is_null($parent->parent)) {
                $parents->push($parent->parent);
                $parent = $parent->parent;
            }
        }
        return $parents->reverse();
    }

}
