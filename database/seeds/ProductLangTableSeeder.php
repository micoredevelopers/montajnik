<?php

use Illuminate\Database\Seeder;

class ProductLangTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_lang')->delete();
        
        \DB::table('product_lang')->insert(array (
            0 => 
            array (
                'name' => 'балкон',
                'description' => '<p>ывыфвфв</p>',
                'except' => '<p>фывффвв</p>',
                'language_id' => 1,
                'product_id' => 1,
            ),
            1 => 
            array (
                'name' => 'rewrw',
                'description' => '<p>asdasdsdasasda</p>',
                'except' => '<p>asdaddasd</p>',
                'language_id' => 1,
                'product_id' => 2,
            ),
        ));
        
        
    }
}