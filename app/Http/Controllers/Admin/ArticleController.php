<?php

namespace App\Http\Controllers\Admin;

use App\Events\Admin\ArticlesChangedEvent;
use App\Http\Requests\Admin\ArticleRequest;
use App\Models\Article;
use App\Models\ArticleLang;
use App\Repositories\ArticleRepository;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;

class ArticleController extends AdminController
{
    use Authorizable;

    use SaveImageTrait;

    protected $routeKey = 'admin.articles';

    protected $permissionKey = 'articles';

    protected $name = 'Статьи';

    protected $key = 'articles';


    public function __construct()
    {
        parent::__construct();
        $this->addBreadCrumb($this->name, $this->resourceRoute('index'));
        $this->shareViewModuleData();
    }

    /**
     * @param ArticleRepository $articleRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ArticleRepository $articleRepository)
    {
        $this->setTitle($this->name);
        $vars['list'] = Article::GetLang($this->langAdmin)->orderBy('published_at', 'desc')->paginate();
        $data['content'] = view('admin.articles.index', $vars);
        return $this->main($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['content'] = view('admin.articles.create');
        return $this->main($data);
    }

    /**
     * @param ArticleRequest $request
     * @param Article $article
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ArticleRequest $request, Article $article)
    {
        $input = $request->except('image');

        if ($article->fillExisting($input)->save()) {
            $this->setSuccessStore();
            /** @var $language \App\Models\Language */
            foreach ($this->languagesList as $language) {
                $articleLang = new ArticleLang();
                $articleLang->fillExisting($input)->article()->associate($article);
                $articleLang->associateWithLanguage($language)->save();
            }
            $this->saveImage($request, $article);
            $this->fireEvents();
        }
        if ($request->has('createOpen')) {
            return redirect($this->resourceRoute('edit', $article->id))->with($this->getResponseMessage());
        }

        return redirect()->back()->with($this->getResponseMessage());
    }

    /**
     * @param $id
     * @param ArticleRepository $articleRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, ArticleRepository $articleRepository)
    {
        $edit = $articleRepository->findForEditById($id);
        $vars['photosList'] = $edit->images;
        $title = $this->titleEdit($edit);
        $this->addBreadCrumb($title)->setTitle($title);

        $data['content'] = view('admin.articles.edit', compact('edit'));
        return $this->main($data);
    }

    /**
     * @param ArticleRequest $request
     * @param Article $article
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(ArticleRequest $request, Article $article)
    {
        $input = $request->except('image');
        //
        $article->fillExisting($input);
        if ($article->save()) {
            $article->lang($this->langAdmin)->first()->fillExisting($input)->save();
            $this->setSuccessUpdate();
            $this->fireEvents();

            $this->saveImage($request, $article);
        }

        if ($request->hasFile('images')) {
            $this->saveAdditionalImages($article, $request);
        }

        if ($request->has('saveClose')) {
            return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
        }

        return redirect()->back()->with($this->getResponseMessage());
    }

    public function destroy(Article $article)
    {
        if ($article->delete()) {
            $this->fireEvents();
            $this->photoModel->deleteImageStorage($article->image);
            foreach ($article->images as $image) {
                $this->photoModel->deleteImageStorage($image->image);
            }
            $this->setSuccessDestroy();
        }

        return redirect(route('articles.index'))->with($this->getResponseMessage());
    }

    private function fireEvents()
    {
        event(new ArticlesChangedEvent);
    }
}
