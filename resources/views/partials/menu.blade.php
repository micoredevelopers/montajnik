@isset($menus)
    <?php /** @var $menu \App\Models\Menu */ ?>
    @foreach($menus as $menu)
        <div class="global-menu__nav-list__item">
            <a href="{{ $menu->getFullUrl() }}" {!! $menu->getRel() !!}
            {!! $menu->getTarget() !!} title="{{ $menu->name }}">
                <img src="{{ checkImage($menu->image, $menu->image) }}" alt="Menu icon" uk-svg>
                <span>{{ $menu->name }}</span>
            </a>
        </div>
    @endforeach
@endisset