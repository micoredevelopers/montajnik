<?php

namespace App\Listeners;

use App\Events\TestimonialSubmittedEvent;
use App\Mail\Testimonials\TestimonialMail;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestimonialSendEmailListener
{
	private $request;

	/**
	 * Create the event listener.
	 *
	 */
	public function __construct()
	{
	}

	/**
	 * Handle the event.
	 *
	 * @param TestimonialSubmittedEvent $event
	 * @return void
	 */
	public function handle(TestimonialSubmittedEvent $event)
	{
		$testimonial = $event->getTestimonial();
		\Mail::send(new TestimonialMail($testimonial));
	}
}
