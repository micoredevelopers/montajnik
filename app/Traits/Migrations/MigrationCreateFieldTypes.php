<?php

namespace App\Traits\Migrations;

use Illuminate\Database\Schema\Blueprint;

trait MigrationCreateFieldTypes
{
	/**
	 * @var Blueprint
	 */
	protected $tableBlueprint;

	protected function setTable(Blueprint $table)
	{
		return $this->tableBlueprint = $table;
	}

	protected function createActive()
	{
		return $this->table()->tinyInteger('active')->default(1);
	}

	protected function createImage()
	{
		$this->createNullableString('image', 255);
	}

	protected function createNullableChar(string $column)
	{
		return $this->table()->char($column, 255)->nullable();
	}

	protected function createName()
	{
		$this->createNullableString('name', 255);
	}

	protected function createPrice()
	{
		$this->table()->unsignedInteger('price');
	}

	protected function createDescription()
	{
		$this->createNullableText('description');
	}

	protected function createExcept()
	{
		$this->createNullableText('except');
	}

	protected function createMediumDescription()
	{
		return $this->table()->mediumText('description')->nullable();
	}

	protected function createLongDescription()
	{
		return $this->table()->longText('description')->nullable();
	}

	protected function createUrl($unique = false)
	{
		$column = $this->table()->string('url', 160)->nullable();
		if ($unique) {
			$column->unique();
		}
		return $column;
	}

	protected function createUniqueUrl()
	{
		$this->createUrl(true);
	}

	protected function createSort()
	{
		return $this->table()->smallInteger('sort')->nullable()->default(0);
	}

	protected function createVideo()
	{
		return $this->table()->text('video')->nullable();
	}

	protected function createLanguageKey()
	{
		$this->table()->smallInteger('language_id')->default(1);
		$this->table()->index('language_id');
	}

	protected function createNextPrevFields()
	{
		$this->table()->unsignedInteger('prev_id')->nullable()->comment('Previous news id');
		$this->table()->unsignedInteger('next_id')->nullable()->comment('Next news id');
	}

	/**
	 * @param string $column
	 * @return \Illuminate\Database\Schema\ColumnDefinition
	 */
	protected function createNullableText(string $column)
	{
		return $this->table()->text($column)->nullable();
	}

	protected function createNullableString(string $column, int $length = 500)
	{
		return $this->table()->string($column, $length)->nullable();
	}

	protected function table(): Blueprint
	{
		return $this->tableBlueprint;
	}

}