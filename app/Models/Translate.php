<?php

namespace App\Models;

use App\Traits\Models\Relations\BelongsToUser;
use App\Traits\Models\Relations\DeletedByUser;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;


/**
 * App\Models\Translate
 *
 * @property int $id
 * @property string|null $key
 * @property string|null $comment
 * @property int $module_id
 * @property string|null $group
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereModuleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $user_id Last edited by user
 * @property int|null $deleted_by User id deleted translate
 * @property-read \App\User|null $deletedBy
 * @property-read mixed $value
 * @property-read \App\User|null $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Translate onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Translate withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Translate withoutTrashed()
 */
class Translate extends Model
{
	use DeletedByUser;
	use BelongsToUser;
	use SoftDeletes;
	use Singleton;

	private static $translates;
	//
	protected $table = 'translate';

	protected $guarded = ['id'];


	public function lang($language_id = null)
	{
		$language_id = $language_id ?: getLang();
		return $this->hasOne(TranslateLang::class)->WhereLanguage($language_id);
	}

	private static function loadTranslatesOfLanguageIfNotLoaded($languageKey): void
	{
		if (!Arr::has(static::$translates, $languageKey)) {
			$translates = static::GetLang($languageKey)->get()->keyBy('key');
			Arr::set(static::$translates, $languageKey, $translates);
		}
	}

	private static function isExistsTranslateByLangAndKey($langId, $key): bool
	{
		if (!\Arr::has(self::$translates, $langId)) {
			return false;
		}
		return \Arr::has(\Arr::get(self::$translates, $langId), $key);
	}

	private static function getTranslateByTranslateByLangAndKey($langId, $key): ?self
	{
		return (!self::isExistsTranslateByLangAndKey($langId, $key)) ? null : \Arr::get(\Arr::get(self::$translates, $langId), $key);
	}

	public static function getTranslates($langId, $key = false)
	{
		static::loadTranslatesOfLanguageIfNotLoaded($langId);

		if (!$key) {
			return Arr::get(static::$translates, $langId);
		}
		if (!static::isExistsTranslateByLangAndKey($langId, $key)) {
			return null;
		}
		return static::getTranslateByTranslateByLangAndKey($langId, $key);

	}

	public static function getTranslate($key, $asObject = false)
	{
		$res = $asObject
			? static::getTranslates(getLang(), $key)
			: Arr::get(static::getTranslates(getLang(), $key), 'value');
		return $res;
	}




	public function format(array $replace)
	{
		try {
			return str_replace(array_keys($replace), array_values($replace), $this->getValueAttribute());
		} catch (\Exception $exception) {
			return null;
		}
	}

	public function isFormatable(): bool
	{
		if (!is_string($this->getValueAttribute())) {
			return false;
		}
		/** @var  $contains */
		$contains = \Str::contains($this->getValueAttribute(), ['%']);
		return $contains;
	}

	public function getValueAttribute()
	{
		return $this->attributes['value'];
	}

	public function isTypeTextarea(): bool
	{
		return $this->type === 'textarea';
	}

	public function isTypeText(): bool
	{
		return $this->type === 'text';
	}

	public static function getGroups(): \Illuminate\Support\Collection
	{
		return Translate::distinct('group')->pluck('group');
	}

	public function setDeletedByAttribute($value)
	{
		$this->attributes['deleted_by'] = $value;
	}

	public function getPrependClickBoardText(): string
	{
		return $this->isFormatable() ? "translateFormat('" : "getTranslate('";
	}

	public function getAppendClickBoardText(): string
	{
		return $this->isFormatable() ? $this->getAppendArguments() : "')";
	}

	protected function getAppendArguments(): string
	{
		$str = '';
		$needle = $this->getAppendNeedle();
		$vars = $this->getVarsFromValue($this->lang->value);
		if ($vars) {
			$str = "', [";
			foreach ($vars as $var) {
				$str .= sprintf("'%s%s%s' => $%s, ", $needle, $var, $needle, $var);
			}
			$str .= '])';
		}
		return $str;
	}

	protected function getVarsFromValue(string $value): array
	{
		try {
			$needle = $this->getAppendNeedle();
			preg_match_all('/(\\' . $needle . '[a-z]+' . $needle . ')/i', $value, $matches);
			$vars = array_map(static function ($variable) use ($needle) {
				return str_replace($needle, '', $variable);
			}, $matches[0]);
		} catch (\Exception $e) {
			$vars = [];
		}
		return $vars;
	}

	protected function getAppendNeedle(): string
	{
		return '%';
	}
}
