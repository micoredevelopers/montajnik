<?php


	namespace App\Repositories;


	use App\Containers\Admin\Category\SearchDataContainer;
	use App\Helpers\Order\Containers\Category\ChunkCategoryContainer;
	use App\Helpers\View\ColumnWidthChunker;
	use App\Helpers\View\ConcreteColumnWidthCssFramework;
	use App\Models\Category\Category;
	use App\Scopes\SortOrderScope;
	use Illuminate\Database\Eloquent\Relations\HasMany;
	use Illuminate\Support\Collection;

	class CategoryRepository extends AbstractRepository
	{
		private $cacheKey = 'public.categories.tree';

		protected function getSingleWith()
		{
			return ['lang', 'categoryPages.lang', 'prices.items', 'work.images', 'products' => function ($query) {
				return $query->active();
			}, 'products.lang', 'partners',
			];
		}

		public function model()
		{
			return Category::class;
		}

		private function filterTreeForActive(Collection $categories)
		{
			$categories = $categories->filter(function (Category $item) {
				if (!(int)$item->active) {
					return false;
				}
				if ($item->allCategories->isNotEmpty()) {
					$item->allCategories = $this->filterTreeForActive($item->allCategories);
				}
				return true;
			});
			return $categories;
		}

		public function search(SearchDataContainer $dataContainer)
		{
			$search = $dataContainer->getSearch();
			if (!$search) {
				return collect([]);
			}
			return Category::whereLike('url', $search)
				->orWhereHas('lang', function ($q) use ($search) {
					$q->whereLike('name', $search);
				})->orWhereHas('categoryPages.lang', function ($q) use ($search) {
					$q->whereLike('description', $search);
				})->with('lang')->get()
				;
		}

		public function getCategories(): ?Collection
		{
			try {
				if (!$categories = \Cache::get($this->cacheKey)) {
					$categories = Category::with(['lang', 'allCategories' => function ($query) {
						return $query->where('active', 1);
					},
					])->active()->ParentMenu()->get()
					;
					$categories = $this->filterTreeForActive($categories);
					\Cache::set($this->cacheKey, $categories);
				}
			} catch (\Exception $e) {
				$categories = collect([]);
			}

			return $categories;
		}

		public function dropCacheTree(): void
		{
			\Cache::forget($this->cacheKey);
		}

		public function rebuildCategoriesTree(): void
		{
			$this->dropCacheTree();
			$this->getCategories();
		}

		/**
		 * @param int $id
		 * @return Category|null
		 */
		public function findForEdit(int $id): ?Category
		{
			return Category::with($this->getSingleWith())->find($id);
		}

		public function findById(int $id): ?Category
		{
			return Category::find($id);
		}

		public function findByUrl(string $url): Category
		{
			/** @var  $category Category */
			$category = Category::with($this->getSingleWith())->active()->WhereUrl($url)->firstOrFail();
			return $category;
		}

		public function all(array $columns = ['*'])
		{
			$this->newQuery()->eagerLoad();

			$this->with('lang');

			$this->query->withGlobalScope('sortOrder', new SortOrderScope());

			$models = $this->query->get($columns);

			$this->unsetClauses();

			return $models;
		}

		public function getCategoriesWithoutChildrens(): Collection
		{
			$cacheKey = 'admin.categories.last';
			if (!$categories = \Cache::get($cacheKey)) {
				$allCategories = $this->with('lang')->where('parent_id', '0', '>')->all();
				Category::dropGlobalScopes(new SortOrderScope());
				$allCategories->loadCount('categories');
				Category::bootGlobalScopes();

				$categories = $allCategories->filter(function (Category $category) use ($allCategories) {
					return !$category->categories_count;
				});
				\Cache::set($cacheKey, $categories);
			}

			return $categories;
		}

		public function getCategoryParents(Category $category, $onlyActive = true): Collection
		{
			/** @var  $parent Category */
			$parents = collect([]);
			if ($category->parent) {
				$parent = $category->parent;
				$parents->push($parent);
				while ($parent->parent !== null) {
					$parents->push($parent->parent);
					$parent = $parent->parent;
				}
				if ($onlyActive) {
					$parents = $parents->filter(static function (Category $category) {
						return (int)$category->getAttribute('active');
					});
				}
			}
			return $parents->reverse();
		}

		public function getMainCategories(): Collection
		{
			/** @var  $mainCategories */
			$mainCategories = $this->newQuery()->query->where('parent_id', 0)->orWhereNull('parent_id')->get();
			return $mainCategories;
		}

		public function isOrBelongsToCeilingsCategory(Category $category): bool
		{
			/** @var  $mainCategory Category */
			$mainCategory = null;
			if ($category->isMainCategory()) {
				$mainCategory = $category;
			} else {
				$parents = $this->getCategoryParents($category);
				$mainCategory = $parents->first();
			}

			return $mainCategory ? $mainCategory->getPrimaryValue() === Category::CEILING_ID : false;
		}

		public function loadForCategoryShow(Category $category): Category
		{
			$category->load(['testimonialsImage.images']);
			if ($category->isCategoryWithoutChild()) {
				$category->load('products');
				return $category;
			}
			$category->load(['categories' => static function (HasMany $query) {
				$query->active();
				$query->with(['categories' => static function (HasMany $query) {
					$query->active();
				}, 'categories.lang',
				]);
			}, 'categories.lang',
			]);
			return $category;
		}

		public function getCategoriesChunkedContainer(Category $category, Collection $categories, ColumnWidthChunker $chunker): ChunkCategoryContainer
		{
			return new ChunkCategoryContainer($category, collect($chunker->chunk($categories)), new ConcreteColumnWidthCssFramework);
		}

		public function getChunkedSubCategories(Collection $categories, ColumnWidthChunker $chunker): Collection
		{
			$chunkedCategories = collect([]);
			foreach ($categories as $subCategory) {
				$chunkCategoryContainer = $this->getCategoriesChunkedContainer($subCategory, $subCategory->categories, $chunker);
				$chunkedCategories->push($chunkCategoryContainer);
			}
			return $chunkedCategories;
		}

		public function getUrlMethodMatches(): array
		{
			return [
				'balconies'                                      => 'balconies',
				'ceilings'                                       => 'ceilings',
				'window'                                         => 'window',
				'doors'                                          => 'doors',
				'dizayn-interera-479'                            => 'designInterior',
				'blinds-rolety'                                  => 'blindsRolety',
				'stroitelstvo-domov'                             => 'stroitelstvoDomov',
				'air-conditioning'                               => 'airConditioning',
				'metallokonstruktsii'                            => 'metallokonstruktsii',
				'insulation-of-walls'                            => 'insulationOfWalls',
				'construction-and-repair'                        => 'constructionAndRepair',
				'uzakonivanie-samostroya-balkonov-i-lodzhiy-437' => 'uzakonivanieSamostroyaBalkonovLodzhiy',
			];
		}

	}