@include('admin.partials.crud.elements.name-required')
<div class="row">
    <div class="col-md-6">
        @include('admin.partners.partials.category')
    </div>
</div>
@include('admin.partials.crud.elements.image-upload-group')

@include('admin.partials.crud.textarea.description')
