<?php /** @var $feedback \App\Models\Feedback */ ?>
@if ($feedback->getAttribute('phone'))
	<p><b>{{ getTranslate('forms.phone') }}</b>: {{ $feedback->getAttribute('phone') }}</p>
@endif
@if ( $feedback->getAttribute('address') )
	<p><b>Address</b>: {{ $feedback->getAttribute('address') }}</p>
@endif
