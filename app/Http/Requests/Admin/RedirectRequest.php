<?php

namespace App\Http\Requests\Admin;

use App\Contracts\Requests\RequestParameterModelable;
use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;
use App\Models\Meta;
use App\Models\Redirect;
use App\Traits\Requests\Helpers\GetActionModel;

class RedirectRequest extends AbstractRequest implements RequestParameterModelable
{
    protected $requestKey = 'redirect';

    protected $toBooleans = ['active'];

    use GetActionModel;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $codes = array_keys(Redirect::getCodes());
        $rules = [
            'from' => ['required', 'unique' => 'unique:redirects,from', 'max:190'],
            'to'   => ['required', 'max:190'],
            'code' => ['required', 'in:' . implodeComma($codes)],
        ];

        /** @var $redirects Redirect */
        if ($this->isActionUpdate() AND $redirects = $this->getActionModel()) {
            $rules['from']['unique'] = 'unique:redirects,from,' . $redirects->getPrimaryValue();
            $rules['to']['unique'] = 'unique:redirects,to,' . $redirects->getPrimaryValue();
        }

        return $rules;
    }

    protected function mergeRequestValues()
    {
        $from = getUrlWithoutHost(getNonLocaledUrl($this->get('from')));
        $to = getUrlWithoutHost(getNonLocaledUrl($this->get('to')));
        $this->merge([
            'from' => $from,
            'to'   => $to,
        ]);
    }
}
