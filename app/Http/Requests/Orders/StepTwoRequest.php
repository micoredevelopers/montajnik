<?php

namespace App\Http\Requests\Orders;

use App\Helpers\Order\OrderStepsFields;
use App\Helpers\Order\OrderStepsRules;

class StepTwoRequest extends AbstractOrderRequest
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getFillableRequestFields($except = []): array
	{
		return $this->exceptFields(OrderStepsFields::getFieldsStepTwo(), $except);

	}

	/**
	 * @return array
	 */
	public function rules()
	{
		return OrderStepsRules::getRulesStepTwo();
	}
}
