<?php

namespace App\Models;


use App\Traits\EloquentMultipleForeignKeyUpdate;
use App\Traits\Models\BelongsToLanguage;

/**
 * App\Models\GalleriesLang
 *
 * @property int $galleries_id
 * @property int $language_id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $except
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang whereGalleriesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang whereSubDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 * @property int $gallery_id
 * @property-read \App\Models\Gallery $gallery
 * @property-read \App\Models\Language $language
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang whereExcept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang whereGalleryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 */
class GalleriesLang extends Model
{
    use EloquentMultipleForeignKeyUpdate;
    use BelongsToLanguage;

    protected $guarded = [];

    protected $table = 'galleries_lang';

    public $incrementing = false;

    protected $primaryKey = ['gallery_id', 'language_id'];

    public $timestamps = false;

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

}
