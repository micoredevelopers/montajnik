@include('admin.prices.partials.titles')
<div class="row">
    <div class="col-md-4">
        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
    </div>
    <div class="col-md-4">
        <input type="text" class="form-control" name="price" value="{{ old('price') }}">
    </div>
    <div class="col-md-4">
        <select name="unit" class="select2 form-control">
            @forelse($pricesUnits as $pricesItem)
                <option {!! selectedIfTrue($pricesItem->unit == old('unit')) !!} value="{{ $pricesItem->unit }}">{{ $pricesItem->unit }}</option>
            @empty
                <option value=""></option>
            @endforelse
        </select>
    </div>
</div>