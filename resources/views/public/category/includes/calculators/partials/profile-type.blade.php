<div class="col-12">
    <div class="row">
        <div class="col-lg-6">
            <span class="category-dropdown-title">Выберите тип профиля</span>
        </div>
        <div class="col-lg-6 ">
            <select class="selectpicker" name="perimeter_profile" title="{{ __('Профиль') }}"
                    data-style="btn-group category-dropdown" data-width="300px" required>
                @foreach(\App\Calculators\Category\Ceilings\CeilingsProfileTypes::getTypes() as $id => $typeName)
                    <option value="{{ $id }}"
                            @if($loop->first) selected @endif
                            data-style="dropdown-item">{{ $typeName }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>