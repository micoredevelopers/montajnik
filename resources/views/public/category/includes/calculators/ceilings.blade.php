<h2 class="title-category mb-3 mb-lg-4">Калькулятор натяжного потолка</h2>
<form id="ceilingsCalculatorForm" action="{{ route('category.calculations.ceilings') }}" method="post">
    @csrf
    <div class="row">
        @includeIf('public.category.includes.calculators.partials.ceiling-type')

        @includeIf('public.category.includes.calculators.partials.profile-type')

        @includeIf('public.category.includes.calculators.partials.baguette')

        @includeIf('public.category.includes.calculators.partials.length')

        @includeIf('public.category.includes.calculators.partials.width')

        @includeIf('public.category.includes.calculators.partials.star_sky')

        @includeIf('public.category.includes.calculators.partials.gapless')

        @includeIf('public.category.includes.calculators.partials.angles')

        @includeIf('public.category.includes.calculators.partials.light')

        @includeIf('public.category.includes.calculators.partials.pipe')

        @includeIf('public.category.includes.calculators.partials.curvature')

        <div class="col-lg-6 d-flex justify-content-between align-items-center mb-20 mb-lg-40">
            <span>Площадь потолка</span>
            <span class="text_bold" id="squareText"></span>
        </div>
        <div class="col-lg-6 d-flex justify-content-between align-items-center mb-20 mb-lg-40">
            <span>Периметр потолка</span>
            <span class="text_bold" id="perimeterText"></span>
        </div>
        <div class="col-12 d-flex justify-content-between align-items-center mb-20 mb-lg-0">
            <span class="text_bold text-lg-20">Стоимость потолка под ключ</span>
            <span class="text_bold text-lg-24" id="totalText"></span>
        </div>
    </div>
</form>
@push('js')
    <script type="text/javascript">
        $(document).ready(function (e) {
            let $timeout = setInterval(function () {
            }, 1);
            const onSuccess = function (res) {
                const $totalElm = $('#totalText')
                const $perimeterElm = $('#perimeterText')
                const $squareElm = $('#squareText')
                $totalElm.text(res.priceText)
                $perimeterElm.text(res.perimeterText)
                $squareElm.text(res.squareText)
            }
            $('#ceilingsCalculatorForm').on('change', function (e) {
                const $form = $(this);
                const $url = $form.attr('action');
                const $data = $form.serialize();

                clearTimeout($timeout)
                $timeout = setTimeout(function () {
                    $.post($url, $data).done(function (res) {
                        onSuccess(res);
                    }).fail(function (res) {
                        $.notify("Ошибка на сервере, пожалуйста попробуйте позже");
                    })
                }, 600);
            })
            $('#ceilingsCalculatorForm').change();
        })
    </script>
@endpush