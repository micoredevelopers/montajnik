<?php /** @var $category \App\Models\Category\Category */ ?>
<?php /** @var $work \App\Models\Work */ ?>
<?php /** @var $image \App\Models\Image */ ?>
@if ($work = $category->work AND $work->images->isNotEmpty())

    <section class="our-works pb-4 custom-bg">
        <div class="our-works-slider-bg ">
            <img class="w-100 h-100" src="{{ checkImage($work->images->first()->image) }}" alt=""/>
        </div>


        <div class="container position-relative ">
            <div class="our-works-slider">
                @foreach($work->images as $image)
                    @php
                        $alt = translateFormat('work.work', ['%category%' => $category->getCategoryName(), '%number%' => $loop->iteration]);
                    @endphp
                    <div class="our-works-slider__item">
                        <img class="w-100 lazy" data-lazy="{{ checkImageWebp($image->image) }}" alt="{{ $alt }}"/>
                    </div>
                @endforeach
            </div>
            <a href="{{ route('works.index') }}"
               class="btn-custom btn-custom_our-works d-none d-lg-flex">{{ getTranslate('work.all-works') }}</a>
            <div class="our-works-slider-arrow-box d-flex justify-content-around justify-content-lg-between mb-25 ">
                <div class="our-works__arrows our-works__arrows_prev">
                    <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="26"
                            height="16"
                            viewBox="0 0 26 16"
                    >
                        <g>
                            <g>
                                <path
                                        d="M7.638 15.834c-.109.11-.271.166-.488.166-.217 0-.38-.055-.487-.166-4.28-4.808-6.446-7.24-6.5-7.295C.054 8.429 0 8.263 0 8.04c
0-.22.054-.386.163-.497l6.5-7.295c.325-.332.65-.332.975 0 .325.331.325.663 0 .995L2.194 7.378H25.35c.433 0 .65.221.65.663 0 .443-.217.664-.65.664H2.194l5.44
4 6.134c.325.332.325.664 0 .995z"
                                />
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="our-works-slider-count-box">
                    <span class="current"></span> /
                    <span class="count"></span>
                </div>
                <div class="our-works__arrows our-works__arrows_next">
                    <svg
                            xmlns="http://www.w3.org/2000/svg"
                            style="transform:rotate(180deg)"
                            width="26"
                            height="16"
                            viewBox="0 0 26 16"
                    >
                        <g>
                            <g>
                                <path
                                        d="M7.638 15.834c-.109.11-.271.166-.488.166-.217 0-.38-.055-.487-.166-4.28-4.808-6.446-7.24-6.5-7.295C.054 8.429 0 8.263 0 8.04c
0-.22.054-.386.163-.497l6.5-7.295c.325-.332.65-.332.975 0 .325.331.325.663 0 .995L2.194 7.378H25.35c.433 0 .65.221.65.663 0 .443-.217.664-.65.664H2.194l5.44
4 6.134c.325.332.325.664 0 .995z"
                                />
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="our-works-desc">
                <h2 class="title text-left mb-20">
                    <span class="decor-bg">{{ getTranslate('work.works') }}</span>
                </h2>
                <div class="text_light mb-25">{!! getTranslate('work.all-description') !!}</div>
                <a href="#" class="btn-custom mx-auto d-lg-none">{{ getTranslate('work.all-works') }}</a>


            </div>
        </div>
        <div class="our-works-decor our-works-decor_first"></div>
        <div class="our-works-decor our-works-decor_second"></div>
    </section>
@endif