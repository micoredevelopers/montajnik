<section class="made-booking">
    <div class="container">
        <div class="text-center">
            <h2 class="title mb-20 mb-lg-5 text-center">
                <span class="decor-bg">Как оформить заказ</span>
            </h2>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="made-booking__item mb-20">
                    <div class="d-flex align-items-center">
                        <img class="made-booking-img lazy" data-src="{{ asset('img/FirstStep.jpg') }}" alt="" src=""/>
                        <h4 class="made-booking-title">Этап 1</h4>
                    </div>
                    <p class="text_light howBookingOrderText">
                        Позвоните нам или оставьте заявку на сайте
                    </p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="made-booking__item mb-20">
                    <div class="d-flex align-items-center">
                        <img class="made-booking-img lazy" data-src="{{ asset('img/SecondStep.jpg') }}" alt="" src=""/>
                        <h4 class="made-booking-title">Этап 2</h4>
                    </div>
                    <p class="text_light">
                        Выезд специалиста на замеры, согласование задач, объема работы,
                        предварительный расчет, согласование сроков, подписание договора
                    </p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="made-booking__item mb-20">
                    <div class="d-flex align-items-center">
                        <img class="made-booking-img lazy" data-src="{{ asset('img/ThirdStep.jpg') }}" alt="" src=""/>
                        <h4 class="made-booking-title">Этап 3</h4>
                    </div>
                    <p class="text_light">
                        Выполнение работ согласно договору
                    </p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="made-booking__item mb-20">
                    <div class="d-flex align-items-center">
                        <img alt="" src="" class="made-booking-img lazy" data-src="{{ asset('img/FourStep.jpg') }}"/>
                        <h4 class="made-booking-title">Этап 4</h4>
                    </div>
                    <p class="text_light">
                        Оплата услуг
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>