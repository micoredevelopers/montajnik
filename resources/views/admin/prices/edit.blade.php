<?php /** @var $edit \App\Models\Category\Category */ ?>
<form action="{{ route($routeKey . '.update', $edit->id) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @method('patch')

    @include('admin.prices.partials.form')

    @include('admin.partials.submit_update_buttons')
</form>

<div class="card mt-5">
    <div class="card-header">Добавление нового пункта</div>
    <div class="card-body">
        <form action="{{ route($routeKey . '.store-item', $edit->id) }}" method="post" class="form-horizontal"
              enctype="multipart/form-data">
            @csrf

            @include('admin.prices.partials.create-item')
            <button class="btn btn-primary">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Создать</button>
        </form>
    </div>
</div>
@section('javascript')
    <script defer>
        $(document).ready(function () {
            $('.select2').select2({
                tags: true,
                width: 'resolve'
            });
        });
    </script>
@stop