@extends('mail.feedback.layout')

@section('title')
	Accident
@stop

@section('content')

    <?php /** @var $feedback \App\Models\Feedback */ ?>
	<table class="table">
		<tbody>
		<tr>
			<td>File:</td>
			<td>
				<a href="{{ $feedback->getUploadedFilePath() }}">{{ $feedback->getOption('filename') }}</a>
			</td>
		</tr>
		</tbody>
	</table>
@stop
