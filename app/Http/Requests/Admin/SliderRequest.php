<?php

namespace App\Http\Requests\Admin;

use App\Contracts\Requests\RequestParameterModelable;
use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;
use App\Models\News;
use App\Traits\Requests\Helpers\GetActionModel;

class SliderRequest extends AbstractRequest implements RequestParameterModelable
{
	use GetActionModel;

	protected $requestKey = 'slider';

//	protected $toBooleans = ['active'];

	public function rules()
	{
		$rules = [
//            '*.name'         => ['required', 'max:255'],
//            '*.except'       => ['max:' . ValidationMaxLengthHelper::TEXT],
//            '*.description'  => ['max:' . ValidationMaxLengthHelper::TEXT],
		];
		if ($this->isActionUpdate() AND $slider = $this->getActionModel()) {

		}

		return $rules;
	}


	protected function mergeRequestValues()
	{

	}
}
