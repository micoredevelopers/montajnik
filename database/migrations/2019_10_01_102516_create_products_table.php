<?php

use App\Traits\Migrations\CategoryForeignKey;
use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{

    use MigrationCreateFieldTypes;
    use CategoryForeignKey;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $this->setTable($table);
            $table->bigIncrements('id');
            $this->createUniqueUrl();
            $this->createImage();
            $this->createActive();
            $this->createSort();

            $table->timestamps();

            $this->addForeignCategory();
        });

        Schema::create('product_lang', function (Blueprint $table) {
            $this->setTable($table);

            $this->createName();
            $this->createNullableChar('sub_name');
            $this->createMediumDescription();
            $this->createExcept();

            $this->createLanguageKey();
            $this->addForeignProduct();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }

    private function addForeignProduct()
    {
        $this->table()->unsignedBigInteger('product_id')->index()->nullable();
        $this->table()->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
    }
}
