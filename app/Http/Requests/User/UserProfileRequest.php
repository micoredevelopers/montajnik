<?php

namespace App\Http\Requests\User;

use App\Traits\Requests\User\GetUser;
use App\User;

class UserProfileRequest extends AbstractUserRequest
{
	use GetUser;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /** @var $user User */
        $user = $this->getUser();
        if ($user->isPersonal()) {
            return $this->getPersonalUserFields();
        }

        return $this->getCompanyUserFields();
    }


}
