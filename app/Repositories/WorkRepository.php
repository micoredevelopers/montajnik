<?php

namespace App\Repositories;

use App\Models\Category\Category;
use App\Models\Work;
use Illuminate\Support\Collection;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;

/**
 * Class WorkRepository
 * @package App\Repositories
 */
class WorkRepository extends BaseRepository
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    private function initScopes()
    {
        Work::initScopesPublic();
    }

    public function __construct(CategoryRepository $categoryRepository)
    {
        parent::__construct();
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Work::class;
    }


    public function getCategories(): Collection
    {
        $categories = $this->categoryRepository->getCategories();
        return $categories;
    }

    public function getCategoriesForEdit(): Collection
    {
        $worksWithCategories = Work::all();
        $categories = $this->categoryRepository->all();
        $categories = $categories->filter(function (Category $category) use ($worksWithCategories) {
            $notExists = !$worksWithCategories->where('category_id', $category->id)->first();
            return $notExists;
        });
        return $categories;
    }

    public function getListForDisplay(): Collection
    {
        $this->initScopes();
        $works = Work::all();
        return $works;
    }

    public function getListForDisplayWithoutCurrent(?Work $work): Collection
    {
        $works = $this->getListForDisplay();
        if ($work) {
            $works = $works->filter(function (Work $workFromList) use ($work) {
                $notMatches = $workFromList->id !== $work->id;
                return $notMatches;
            });
        }
        $works->load('category.lang');
        return $works;
    }

    public function findByUrl(string $url): ?Work
    {
        $this->initScopes();
        $work = Work::whereUrl($url)->first();
        return $work;
    }
}
