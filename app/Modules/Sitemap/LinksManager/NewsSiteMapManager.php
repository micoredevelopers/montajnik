<?php

namespace App\Modules\Sitemap\LinksManager;

use App\Contracts\Models\HasSitemapLinks;
use App\Models\News;
use Illuminate\Support\Collection;

class NewsSiteMapManager implements HasSitemapLinks
{
	public function getSiteMapLinks(): Collection
	{
		$links = collect([]);
		$news = News::active()->get();
		$news->map(static function (News $news) use ($links) {
			$item = [
				'loc'     => route('news.show', $news->url),
				'lastmod' => checkDateCarbon($news->updated_at),
			];
			$links->push($item);
		});
		return $links;
	}

}