<?php

namespace App\Http\Controllers\Category;

use App\Helpers\View\ColumnWidthChunker;
use App\Http\Controllers\SiteController;
use App\Models\Category\Category;
use App\Models\Category\Interfaces\CategoryNamed;
use App\Models\Slider\Slider;
use App\Models\TestimonialType;
use App\Repositories\CategoryRepository;
use App\Repositories\SliderRepository;
use App\Repositories\TestimonialRepository;
use App\Traits\Controllers\Category\BreadcrumbsTree;
use Illuminate\View\View;

class CategoryCustomizeController extends SiteController
{
	use BreadcrumbsTree;
	private $categoryRepository;
	/**
	 * @var SliderRepository
	 */
	private $sliderRepository;

	private $dataView = [];
	/**
	 * @var TestimonialRepository
	 */
	private $testimonialRepository;
	/**
	 * @var ColumnWidthChunker
	 */
	private $columnWidthChunker;


	private $view = 'public.category.custom.default';

	public function __construct(
		CategoryRepository $categoryRepository,
		SliderRepository $sliderRepository,
		TestimonialRepository $testimonialRepository,
		ColumnWidthChunker $columnWidthChunker
	)
	{
		parent::__construct();
		$this->categoryRepository = $categoryRepository;
		$this->sliderRepository = $sliderRepository;
		$this->testimonialRepository = $testimonialRepository;
		$this->columnWidthChunker = $columnWidthChunker;
	}

	private function _show(Category $category)
	{
		$this->categoryRepository->loadForCategoryShow($category);
		$testimonialsImage = $category->testimonialsImage;
		$this->addDataToView(compact('testimonialsImage'));
		view()->composer('includes.modal', static function (View $view) use ($category) {
			$view->with('category', $category);
		});

		$slider = $this->sliderRepository->findByCategoryForDisplay($category);
		$this->addBreadCrumbsTree($category, $this->categoryRepository);
		$testimonialsVideo = $this->testimonialRepository->getTestimonialsForMainPage(TestimonialType::TYPE_VIDEO_ID);

		$this->setTitle($category->name);
		$this->addDataToView(compact(
			'category'
			, 'testimonialsVideo'
			, 'slider'
		));

		$data['content'] = view($this->view, $this->dataView);
		return $this->main($data);
	}

	private function addDataToView(array $data): void
	{
		$this->dataView = array_merge($this->dataView, $data);
	}

	public function ceilings()
	{
		$category = $this->categoryRepository->findByUrl($this->_getUrlByMethod(__METHOD__));
		$this->addDataToView(['includeCeilingsCalculator' => true, 'showCeilingsFrame' => true]);
		$this->addDataToView($this->getDefaultChunkedCategories($category));
		$this->view = 'public.category.custom.ceilings';

		return $this->_show($category);
	}

	public function window()
	{
		$category = $this->categoryRepository->findByUrl($this->_getUrlByMethod(__METHOD__));

		$topCategories = $category->getActiveSubcategories()->filter(function (Category $category) {
			$ids = [CategoryNamed::WINDOWS_OSNOVA_ID, CategoryNamed::WINDOWS_REHAU_ID,];
			return in_array($category->id, $ids, true);
		})
		;
		$topCategories = $this->categoryRepository->getCategoriesChunkedContainer($category, $topCategories, $this->columnWidthChunker);
		/** @var  $furnitureCategory Category */

		$clonedCategory = clone $category;
		$clonedCategory->lang->setAttribute('name', 'Комплектующие, монтаж и установка');
		$secondCategories = $category->getActiveSubcategories()->filter(function (Category $category) {
			$ids = [CategoryNamed::WINDOWS_OSNOVA_ID, CategoryNamed::WINDOWS_REHAU_ID,];
			return !in_array($category->id, $ids, true);
		})
		;
		$secondCategories = $this->categoryRepository->getCategoriesChunkedContainer($clonedCategory, $secondCategories, $this->columnWidthChunker);
		$this->addDataToView(compact('topCategories', 'secondCategories'));
		1 ?: view('public.category.custom.window');//Just fast link
		$this->view = 'public.category.custom.window';
		return $this->_show($category);
	}

	private function getDefaultChunkedCategories(Category $category)
	{
		$subCatsWithChilds = $this->getSubCatsWithChilds($category);
		$subCatsWithoutChilds = $this->getSubCatsWithoutChilds($category);

		$chunkedCategories = $this->categoryRepository->getChunkedSubCategories($subCatsWithChilds, $this->columnWidthChunker);
		$chunkedCategoriesWithoutChild = $this->categoryRepository->getCategoriesChunkedContainer($category, $subCatsWithoutChilds, $this->columnWidthChunker);
		return compact('chunkedCategories', 'chunkedCategoriesWithoutChild');
	}

	public function constructionAndRepair()
	{
		$category = $this->categoryRepository->findByUrl($this->_getUrlByMethod(__METHOD__));
		$chunkedCategories = $this->categoryRepository->getChunkedSubCategories($this->getSubCatsWithChilds($category), $this->columnWidthChunker);

		$first = collect([$chunkedCategories->pop()]);
		$this->addDataToView([
			'firstCategory'    => $first,
			'secondCategories' => $chunkedCategories,
		]);
		$this->view = 'public.category.custom.construction';
		return $this->_show($category);
	}

	private function getSubCatsWithChilds(Category $category)
	{
		return $category->getActiveSubcategories()->filter(static function (Category $category) {
			return $category->getActiveSubcategories()->isNotEmpty();
		})
			;
	}

	private function getSubCatsWithoutChilds(Category $category)
	{
		return $category->getActiveSubcategories()->filter(static function (Category $category) {
			return $category->getActiveSubcategories()->isEmpty();
		})
			;
	}


	public static function routes(): void
	{
		$categories = static::getUrlMethodMatches();
		\Route::namespace('Category')
			->prefix('catalog')
			->group(static function () use ($categories) {
				foreach ($categories as $url => $method) {
					\Route::get(sprintf('/%s', $url), 'CategoryCustomizeController@' . $method);
				}
			})
		;
	}

	private function _extractMethod(string $method)
	{
		return getLastFromExploded($method, '::');
	}

	private function _getUrlByMethod(string $method)
	{
		$method = $this->_extractMethod($method);
		return array_search($method, static::getUrlMethodMatches(), true);
	}

	private function _getDefaultDataToDefaultView(Category $category)
	{
		$subCats = $this->categoryRepository->getCategoriesChunkedContainer($category, $category->getActiveSubcategories(), $this->columnWidthChunker);
		$this->addDataToView(['chunkedCategoriesWithoutChild' => $subCats]);
		return $this->_show($category);
	}

	public static function getUrlMethodMatches(): array
	{
		static $matches;
		if (null === $matches) {
			/** @var  $categoryRepository CategoryRepository */
			$categoryRepository = resolve(CategoryRepository::class);
			$matches = $categoryRepository->getUrlMethodMatches();
		}
		return $matches;
	}


	public function __call($method, $parameters)
	{
		$category = $this->categoryRepository->findByUrl($this->_getUrlByMethod($method));
		return $this->_getDefaultDataToDefaultView($category);
	}

}
