<?php /** @var $feedback \App\Models\Feedback */ ?>
@if ($feedback->user)
	<a href="{{ route('users.edit', $feedback->user->id) }}">{{ $feedback->user->fields->name }}</a>
@endif
<div>{{ $feedback->created_at->format('d M Y / H:i') }}</div>
<div class="badge {{ $badge ?? 'badge-secondary' }} no-radius">{{ $feedback->type }}</div>
