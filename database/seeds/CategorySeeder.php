<?php

use App\Helpers\Miscellaneous\Strings;
use App\Repositories\Admin\LanguageRepository;
use App\Repositories\Exports\Seeder\PageSeedRepository;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    private $languages;

	public function countCategoriesRecursive($tree)
	{
		static $count = 0;
		$count += count($tree);
		foreach ($tree as $item) {
			if (\Arr::has($item,'categories')){
				$this->countCategoriesRecursive(Arr::wrap(Arr::get($item,'categories')));
			}
		}
		return $count;
	}

    private $pageSeedRepository;

    public function __construct(PageSeedRepository $seedRepository, LanguageRepository $languageRepository)
    {
        $this->pageSeedRepository = $seedRepository;
        $this->languages = $languageRepository->getForCreateEntity();
    }

    /**
     *
     */
    public function run()
    {
        $categories = $this->pageSeedRepository->getTree();
        $count = $this->countCategoriesRecursive($categories);

        $this->command->info('Categories started, progress shows main categories save');
        $this->command->getOutput()->progressStart($count);

        $this->loop($categories);

        event(new \App\Events\Admin\CategoriesChanged());

        $this->command->getOutput()->progressFinish();

    }

    /**
     * @param array $categories
     * @param \App\Models\Category\Category|null $parentCategory
     */
    private function loop(array $categories, \App\Models\Category\Category $parentCategory = null)
    {
        foreach ($categories as $category) {
			$this->command->getOutput()->progressAdvance();
			$pageModel = $this->saveCategory($category, $parentCategory);

            if (Arr::has($category, 'categories')) {
                $this->loop(Arr::get($category, 'categories'), $pageModel);
            }
        }
    }

    private function saveCategory(array $item, \App\Models\Category\Category $parentCategory = null): \App\Models\Category\Category
    {
        ($category = new \App\Models\Category\Category())->fillExisting($item);
        $parentCategory ? $parentCategory->categories()->save($category) : $category->save();

        $categoryPages = $category->categoryPages()->get();
        if ($categoryPage = $categoryPages->first()) {
            $categoryPage->lang->description = Strings::unSanitize(Arr::get($item, 'description'));
            $categoryPage->lang->save();
        }

        foreach ($this->languages as $language) {
            Arr::set($item, 'name', Strings::unSanitize($item['name']));
            ($categoryLang = new \App\Models\Category\CategoryLang())->fillExisting($item);
            $categoryLang->category()->associate($category);
            $categoryLang->associateWithLanguage($language)
                ->save();
        }

        return $category;
    }
}
