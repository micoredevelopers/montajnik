<section class="main-page-top">
    <div class="top-slider">
        <div class="top-slider__item_wrap">
            <div class="top-slider__item">
                <img class="top-slider-bg " src="{{ asset('img/slider-img.png') }}" alt=""/>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 d-flex flex-column justify-content-center">
                            <h2 class="top-slider-title">Балконы и лоджии</h2>
                            <p class="mb-lg-30">
                                Не следует, однако забывать, что сложившаяся структура
                                организации играет важную роль в формировании дальнейших
                                направлений развития. Идейные соображения высшего порядка, а
                                также начало повседневной работы по формированию.
                            </p>
                            <a href="" class="btn-custom">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="top-slider__item_wrap">
            <div class="top-slider__item">
                <img class="top-slider-bg " src="{{ asset('img/slider-img.png') }}" alt=""/>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 d-flex flex-column justify-content-center">
                            <h2 class="top-slider-title">Балконы и лоджии</h2>
                            <p>
                                Не следует, однако забывать, что сложившаяся структура
                                организации играет важную роль в формировании дальнейших
                                направлений развития. Идейные соображения высшего порядка, а
                                также начало повседневной работы по формированию.
                            </p>
                            <a href="" class="btn-custom">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="top-slider__item_wrap">
            <div class="top-slider__item">
                <img class="top-slider-bg " src="{{ asset('img/slider-img.png') }}" alt=""/>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 d-flex flex-column justify-content-center">
                            <h2 class="top-slider-title">Балконы и лоджии</h2>
                            <p>
                                Не следует, однако забывать, что сложившаяся структура
                                организации играет важную роль в формировании дальнейших
                                направлений развития. Идейные соображения высшего порядка, а
                                также начало повседневной работы по формированию.
                            </p>
                            <a href="" class="btn-custom">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="top-slider__item_wrap">
            <div class="top-slider__item">
                <img class="top-slider-bg " src="{{ asset('img/slider-img.png') }}" alt=""/>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 d-flex flex-column justify-content-center">
                            <h2 class="top-slider-title">Балконы и лоджии</h2>
                            <p>
                                Не следует, однако забывать, что сложившаяся структура
                                организации играет важную роль в формировании дальнейших
                                направлений развития. Идейные соображения высшего порядка, а
                                также начало повседневной работы по формированию.
                            </p>
                            <a href="" class="btn-custom">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="top-slider-navigation-box">
        <div class="container d-flex justify-content-center justify-content-lg-start align-items-center">
            <div class="top-slider-arrow top-slider-arrow_prev">
                <img class="svg d-block" src="{{ asset('img') }}/mp/arrow.svg" alt="">
            </div>
            <div class="top-slider-dots__wrap"></div>
            <div class="top-slider-arrow top-slider-arrow_next">
                <img class="svg d-block" src="{{ asset('img') }}/mp/arrow.svg" alt="">
            </div>
        </div>
    </div>
</section>