<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use App\Http\Requests\Admin\TranslateRequest;
use App\Models\Language;
use App\Models\Translate;
use App\Models\TranslateLang;
use App\Repositories\TranslateRepository;
use App\Traits\Authorizable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Input;
use Illuminate\View\View;

class TranslateController extends AdminController
{
	use Authorizable;

	private $name = 'Локализация';

	protected $tb;

	protected $routeKey = 'translate';

	protected $permissionKey = 'translate';

	public function __construct()
	{
		parent::__construct();
		$this->name = __('modules.localization.title');
		$this->tb = 'translate';
		$this->addBreadCrumb($this->name, route($this->tb . '.index'));
		$this->addScripts([
			'js/lib/select2/select2.full.min.js',
			'js/lib/select2/ru.js',
		]);
		$this->addCss([
			'css/lib/select2.min.css',
		]);
		$this->shareViewModuleData();
	}

	public function index(Request $request, TranslateRepository $translateRepository)
	{
		if ($request->has('seed')) {
			seedByClass('TranslateTableSeeder');
			return redirect($this->resourceRoute('index'));
		}
		$list = $translateRepository->getForAdminDisplay($request);

		$groups = $list->pluckDistinct('group');
		$groups = collect($groups);
		$this->setTitle($this->name);
		$active = request()->session()->get('translate_tab', old('translate_tab', ($groups->first())));
		$vars = compact(
			'list',
			'request',
			'active'
			, 'groups'
		);
		$data['content'] = view('admin.translate.index', $vars);

		return $this->main($data);
	}

	/**
	 * @return Factory|View
	 */
	public function create()
	{
		$vars['groups'] = Translate::getGroups();
		$data['content'] = view('admin.translate.add')->with($vars);
		return $this->main($data);
	}


	/**
	 * @param TranslateRequest $request
	 * @return RedirectResponse|Redirector
	 */
	public function store(TranslateRequest $request)
	{
		$data = $request->all();
		$translate = new Translate();
		if ($translate->fillExisting($data)->save()) {
			$this->setSuccessStore();
			$data[$translate->getForeignKey()] = $translate->getPrimaryValue();
			foreach ($this->languagesList as $language) {
				/** @var $language Language */
				$data[$language->getForeignKey()] = $language->getPrimaryValue();
				(new TranslateLang())->fillExisting($data)->save();
			}
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}


	/**
	 * @param Request $request
	 * @return RedirectResponse|Redirector
	 */
	public function update(Request $request)
	{
		if ($request->has('translate')) {
			$ids = array_keys($request->get('translate'));
			$this->setSuccessUpdate();
			$translates = Translate::with('lang')->find($ids);
			$translateRequest = $request->get('translate');
			foreach ($translates as $translate) {
				/** @var $translate TranslateLang */
				$id = $translate->getPrimaryValue();
				$data = \Arr::get($translateRequest, $id);
				$translate->fillExisting($data)->save();
				$translate->lang->fillExisting($data)->save();
			}
		}
		request()->flashOnly('translate_tab');
		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());

	}

}
