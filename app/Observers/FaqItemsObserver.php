<?php

namespace App\Observers;

use App\Models\FaqItems;

class FaqItemsObserver
{
    /**
     * Handle the faq items "created" event.
     *
     * @param  \App\Models\FaqItems  $faqItems
     * @return void
     */
    public function created(FaqItems $faqItems)
    {
        //
    }

    /**
     * Handle the faq items "updated" event.
     *
     * @param  \App\Models\FaqItems  $faqItems
     * @return void
     */
    public function updating(FaqItems $faqItems)
    {
    	if ( $faqItems->getAttribute('from_user') AND $faqItems->getAnswerAttribute() ){
			$faqItems->from_user = false;
		}
    }

    /**
     * Handle the faq items "updated" event.
     *
     * @param  \App\Models\FaqItems  $faqItems
     * @return void
     */
    public function updated(FaqItems $faqItems)
    {
        //
    }

    /**
     * Handle the faq items "deleted" event.
     *
     * @param  \App\Models\FaqItems  $faqItems
     * @return void
     */
    public function deleted(FaqItems $faqItems)
    {
        //
    }

    /**
     * Handle the faq items "restored" event.
     *
     * @param  \App\Models\FaqItems  $faqItems
     * @return void
     */
    public function restored(FaqItems $faqItems)
    {
        //
    }

    /**
     * Handle the faq items "force deleted" event.
     *
     * @param  \App\Models\FaqItems  $faqItems
     * @return void
     */
    public function forceDeleted(FaqItems $faqItems)
    {
        //
    }
}
