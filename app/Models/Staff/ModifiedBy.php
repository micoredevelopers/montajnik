<?php


namespace App\Models\Staff;

/**
 * App\Models\Staff\ModifiedBy
 *
 * @property-read \App\Models\Staff\ModifiedBy $modifiable
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\ModifiedBy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\ModifiedBy newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff\ModifiedBy query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class ModifiedBy extends \App\Models\Model
{
	protected $guarded = ['id'];

	public function modifiable(): \Illuminate\Database\Eloquent\Relations\MorphTo
	{
		return $this->morphTo('modifiable');
	}

	public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
	{
		return $this->belongsTo(\App\User::class);
	}
}