<?php

namespace App\Observers;

use App\Models\Translate;
use App\User;

class TranslateObserver
{
    /**
     * Handle the translate "created" event.
     *
     * @param  \App\Models\Translate  $translate
     * @return void
     */
    public function creating(Translate $translate)
    {
        if (!\Arr::get($translate, 'group') AND \Str::contains($translate->key, '.')){
            $parts = explode('.', $translate->key);
            $translate->group = reset($parts);
        }

        $this->addEditedBy($translate);
    }

    public function created(Translate $translate)
    {
        //
    }

    /**
     * Handle the translate "updated" event.
     *
     * @param  \App\Models\Translate  $translate
     * @return void
     */
    public function updated(Translate $translate)
    {
        //
    }


    /**
     * Handle the translate "restored" event.
     *
     * @param  \App\Models\Translate  $translate
     * @return void
     */
    public function restored(Translate $translate)
    {
        //
    }

    /**
     * Handle the translate "force deleted" event.
     *
     * @param  \App\Models\Translate  $translate
     * @return void
     */
    public function forceDeleted(Translate $translate)
    {
        //
    }


    public function updating(Translate $translate)
    {
        $this->addEditedBy($translate);
    }

    private function addEditedBy(Translate $translate)
    {
        /** @var  $user User*/
        if ($user = \Auth::user()){
            $userId = $user->getPrimaryValue();
            $translate->setAttribute('user_id', $userId);
        }
    }

    public function deleted(Translate $translate)
    {
        $this->addDeletedBy($translate);
    }

    private function addDeletedBy(Translate $translate)
    {
        /** @var  $user User*/
        if ($user = \Auth::user()){
            $userId = $user->getPrimaryValue();
            $translate->setDeletedByAttribute($userId);
            $translate->save();
        }

    }
}
