<?php /** @var $feedback \App\Models\Feedback */ ?>
<div class="row mt-3">

	@include('admin.feedback.types.partials.default_fields')

	<div class="col-md-3">
		@include('admin.feedback.types.partials.user_date_type')
	</div>
	<div class="hr"></div>
</div>
