<?php

namespace App\Http\Controllers;

use App\Events\TestimonialSubmittedEvent;
use App\Helpers\Media\ImageSaver;
use App\Http\Requests\TestimonialRequest;
use App\Models\Testimonial;
use App\Models\TestimonialType;
use Carbon\Carbon;

class TestimonialController extends SiteController {

    public function index() {
        $vars['breadcrumbs'] = $this->getBreadCrumbs();
        $vars['testimonials'] = TestimonialType::where('type', 'image')->first()->testimonials()->with(['category.lang', 'images'])->latest()->active()->paginate();
        $this->setNextPrevLinkForPagination($vars['testimonials']);

        $page = $vars['testimonials']->currentPage();
        $title = getTranslate('testimonials.meta_title');
        $description = getTranslate('testimonials.meta_description');
        if (1 < $page) {
            $title = str_replace('%page%', $page, getTranslate('testimonials.meta_title_paged'));
            $description = str_replace('%page%', $page, getTranslate('testimonials.meta_description_paged'));
        }

        $this->setTitle($title);
        $this->setDescription($description);
        $this->addBreadCrumb('Отзывы');

        $data['content'] = view('public.testimonials.index', $vars);
        return $this->main($data);
    }

    public function testimonialAdd(TestimonialRequest $request, Testimonial $testimonial, ImageSaver $imageSaver) {
        $this->setMessage(getTranslate('feedback.send-failed'));
        $testimonial->fillExisting($request->except('image'));
        if ($testimonial->save()) {
            if ($request->hasFile('image')) {
                $image = $imageSaver
                    ->setNameKey('image.0')
                    ->setFolderName($testimonial->getTable() . DIRECTORY_SEPARATOR . $testimonial->getPrimaryValue())
                    ->saveFromRequest();
                $testimonial->images()->create(['image' => $image]);
                $testimonial->setAttribute('src', $image);
            }
            event(new TestimonialSubmittedEvent($testimonial));
            $this->setMessage(getTranslate('feedback.send-success'))->setStatus(true);
        }

        if ($request->expectsJson()) {
            return $this->getResponseMessageForJson();
        }
        return redirect()->back()->with($this->getResponseMessage());
    }
}
