<?php

namespace App\Traits\Migrations;

use Illuminate\Database\Schema\Blueprint;

trait CategoryForeignKey
{
    protected function addForeignCategory(): void
	{
		/** @var  $table Blueprint*/
		$table = $this->table();
		$table->unsignedInteger('category_id')->index()->nullable();
		$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
    }
}