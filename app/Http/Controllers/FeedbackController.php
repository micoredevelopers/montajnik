<?php

namespace App\Http\Controllers;

use App\Events\FeedbackSubmittedEvent;
use App\Helpers\ResponseHelper;
use App\Http\Requests\Feedback\FeedbackRequest;
use App\Mail\Feedback\FeedbackDefault;
use App\Models\Feedback;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class FeedbackController extends SiteController
{
	/** @var Request */
	protected $request;

	public function __construct(Request $request)
	{
		parent::__construct();
		$this->request = $request;
	}

	/**
	 * @param FeedbackRequest $request
	 * @return array|\Illuminate\Http\RedirectResponse
	 */
	public function feedback(FeedbackRequest $request)
	{
		$ip = \Request::ip();
		if ($feedback = Feedback::where('ip', $ip)->where('created_at', '>', (string)now()->subMinute())->first()) {
			$this->setMessage(getTranslate('feedback.throlle-message'));
			return $this->response();
		}
		$data = $request->only([
			'name', 'phone', 'message',
		]);

		$data = $this->checkDataForSuperAdmin($data);

		$data = $this->addStaffInfo($data, $request);

		$this->saveFeedbackWithSendMail($data);

		return $this->response();
	}


	private function saveFeedbackWithSendMail(array $data)
	{
		$this->setFail();

		$feedback = new Feedback();
		try {
			if ($feedback->fillExisting($data)->save()) {
				event(new FeedbackSubmittedEvent($feedback));
				$this->setSuccess();
			}
		} catch (\Exception $e) {
			logger($e);
		}
	}

	/**
	 * @return array|\Illuminate\Http\RedirectResponse
	 */
	private function response()
	{
		if ($this->request->expectsJson()) {
			return $this->getResponseMessageForJson();
		}

		return redirect()->back()->with($this->getResponseMessage())->withInput(isLocalEnv() ? $this->request->input() : []);
	}

	/**
	 *
	 */
	private function setSuccess()
	{
		$this->setMessage(getTranslate('feedback.send-success'))->setStatus(true);
	}

	/**
	 *
	 */
	private function setFail()
	{
		$this->setMessage(getTranslate('feedback.send-failed'))->setStatus(false);
	}

	private function addStaffInfo(array $data, Request $request): array
	{
		$data['referer'] = $request->server('HTTP_REFERER');
		$data['ip'] = $request->ip();

		return $data;

	}

	private function checkDataForSuperAdmin($data)
	{
		if (isSuperAdmin()) {
			Arr::forget($data, 'email');
		}
		return $data;
	}
}












