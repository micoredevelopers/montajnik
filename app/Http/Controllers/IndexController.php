<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Models\Category\Category;
use App\Models\Category\Product;
use App\Models\TestimonialType;
use App\Repositories\CategoryRepository;
use App\Repositories\NewsRepository;
use App\Repositories\SliderRepository;
use App\Repositories\TestimonialRepository;
use Illuminate\Support\Facades\Cache;

class IndexController extends SiteController
{

	private $cacheKey = 'views.public.index.index';

	/**
	 * @param CategoryRepository $categoryRepository
	 * @param NewsRepository $newsRepository
	 * @param TestimonialRepository $testimonialRepository
	 * @param SliderRepository $sliderRepository
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 * @throws \Throwable
	 */

	public function index(
		CategoryRepository $categoryRepository,
		NewsRepository $newsRepository,
		TestimonialRepository $testimonialRepository,
		SliderRepository $sliderRepository
	)
	{
		$this->dropLastBreadCrumb();
		$cacheKey = 'views.public.index.main';
		if ( !getSetting('main.cache-enabled') || !Cache::has($cacheKey)) {
			$categories = $categories = $categoryRepository->getCategories();
			$newsMain = $newsRepository->getNewForMainPage();
			$testimonialsImage = $testimonialRepository->getTestimonialsForMainPage(TestimonialType::TYPE_IMAGE_ID);
			$testimonialsVideo = $testimonialRepository->getTestimonialsForMainPage(TestimonialType::TYPE_VIDEO_ID);
			$slider = $sliderRepository->findById((int)getSetting('_.main-page-slider-id'));
			$content = view('public.index.main', compact(
				'categories',
				'newsMain',
				'testimonialsImage',
				'testimonialsVideo',
				'slider'))->render();
			Cache::set($cacheKey, $content);
		}
		$data['content'] = Cache::get($cacheKey);

		return $this->main($data);
	}

	public function search(SearchRequest $request)
	{
		$search = $request->get('search');
		$likeSearch = '%' . $search . '%';
		$list = collect([]);
		$categories = Category::with('lang')->whereHas('lang', function ($query) use ($likeSearch) {
			$query->where('name', 'like', $likeSearch);
		})
			->active()
			->limit(20)
			->get();
		$categories = $categories->map(static function (Category $category) {
			return [
				'name'  => $category->getCategoryName(),
				'url'   => route('category.show', $category->getUrlAttribute()),
				'image' => checkImage($category->getImageAttribute()),
			];
		});
		if ($limit = (20 - $categories->count())) {
			$products = Product::with('lang')->whereHas('lang', function ($query) use ($likeSearch) {
				$query->where('name', 'like', $likeSearch);
			})
				->active()
				->limit($limit)
				->get();

			$products = $products->map(static function (Product $product) {
				return [
					'name'  => $product->getNameAttribute(),
					'url'   => route('product.show', $product->url),
					'image' => checkImage($product->getImageAttribute()),
				];
			});
			$list = $list->merge($products);
		}
		$list = $list->merge($categories);
		return $list;
	}
}
