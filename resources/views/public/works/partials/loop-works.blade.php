<?php /** @var $work \App\Models\Work */ ?>
<?php /** @var $workCategory \App\Models\Category\Category */ ?>

@foreach ($works as $work)
    @php
        $workCategory = $work->category;
        if (!$workCategory){
            continue;
        }
    @endphp
    <li class="works-drop-list-item">
        <a href="{{route('works.show', $work->url)}}">{{ $workCategory->getCategoryName() }}</a>
    </li>
@endforeach