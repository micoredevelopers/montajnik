<?php

if (!function_exists('isStringUrl')) {
	function isStringUrl($string)
	{
		$http = \Str::startsWith($string, 'http://');
		$https = \Str::startsWith($string, 'https://');
		$double = \Str::startsWith($string, '//');
		return ($http OR $https OR $double);
	}
}

if (!function_exists('getHost')) {
	function getHost()
	{
		return request()->getHost();
	}
}
if (!function_exists('getProtocol')) {
	function getProtocol()
	{
		return request()->isSecure() ? 'https' : 'http';
	}
}


if (!function_exists('langUrl')) {
	function langUrl($url, $locale = false): string
	{
		$localeCode = $locale ?: getCurrentLocale();

		return \LaravelLocalization::getLocalizedURL($localeCode, $url, [], false);
	}
}
if (!function_exists('getNonLocaledUrl')) {
	function getNonLocaledUrl($url = null)
	{
		if ($url === null) {
			$url = request()->getPathInfo();
		}

		return \LaravelLocalization::getNonLocalizedURL($url);
	}
}
if (!function_exists('urlWithoutPublic')) {
	function urlWithoutPublic($url)
	{
		return \Str::replaceFirst('/public', '', $url);
	}
}
if (!function_exists('getUrlWithoutHost')) {
	function getUrlWithoutHost($url)
	{
		$domain = env('APP_URL');
		$url = urlWithoutPublic($url);
		return \Str::replaceFirst($domain, '', $url);
	}
}


function urlClearSlashes(string $url)
{
	return trim($url, '/');
}