<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use App\Models\Article;
use App\Models\Feedback;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class FeedbackController extends AdminController
{
    protected $routeKey = 'admin.feedback';

    protected $permissionKey = 'feedback';

	protected $name = 'Feedback';

	protected $key = 'feedback';

	public function __construct()
	{
		parent::__construct();
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index(Request $request)
	{
		$title = $this->name;
		$this->setTitle($title);
		$search = '';
		$query = Feedback::with('user.fields')->latest();
		if ($request->get('search')) {
			$search = $request->get('search');
			$query->where(function (Builder $builder) use ($search) {
				$builder->where('name', 'like', '%' . $search . '%')
					->orWhere('email', 'like', '%' . $search . '%')
					->orWhere('phone', 'like', '%' . $search . '%')
					->orWhere('address', 'like', '%' . $search . '%')
					->orWhere('message', 'like', '%' . $search . '%');
			});
		}
		if ($request->get('type')){
			$query->where('type' , $request->get('type'));
		}
		if ($request->get('date')){
			$query->ApplyDateRange( $request->get('date'));
		}
		$vars['request'] = $request;
		$vars['types'] = Feedback::getTypes();
		$vars['list'] = $query->paginate();
		$vars['search'] = $search;
		$data['content'] = view('admin.feedback.index')->with($vars);

		return $this->main($data);
	}

	/**
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function create()
	{
		return back();
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(Request $request)
	{
		return back();
	}

	/**
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function show($id)
	{
		return back();
	}

	/**
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function edit($id)
	{
		return back();
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Request $request, $id)
	{
		return back();
	}

	/**
	 * @param Feedback $feedback
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws \Exception
	 */
	public function destroy(Feedback $feedback)
	{
		if ($feedback->delete()) {
			$this->setSuccessDestroy();
		}

		return redirect(route('feedback.index'))->with($this->getResponseMessage());
	}
}
