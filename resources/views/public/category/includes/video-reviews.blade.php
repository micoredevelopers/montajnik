<?php /** @var $category \App\Models\Category\Category */ ?>
@if(isset($testimonialsVideo) AND $testimonialsVideo->isNotEmpty())
    <div class="text-center">
        <h2 class="title text-center mb-25 mb-lg-5">
            <span class="decor-bg">Видео-отзывы</span>
        </h2>
    </div>
    <div class="col-lg-8 offset-lg-2 category-video-reviews-slider-wrap mb-25 mb-lg-40">
        <div class="video-reviews-slider">
            @foreach($testimonialsVideo as $testimonialVideo)
                @include('public.index.partials.review-video-item')
            @endforeach
        </div>
    </div>
@endif