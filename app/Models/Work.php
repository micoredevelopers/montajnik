<?php

namespace App\Models;

use App\Contracts\HasImagesContract;
use App\Contracts\Models\HasSitemapLinks;
use App\Models\Category\Category;
use App\Traits\Models\HasImages;
use App\Traits\Models\ImageAttributeTrait;
use App\Traits\Models\Scopes\Init\InitPublicScopeActive;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Work
 *
 * @property int $id
 * @property int $active
 * @property string|null $image
 * @property string|null $url
 * @property int|null $sort
 * @property array|null $video
 * @property int|null $category_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Category\Category|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Work newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Work newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Work query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Work whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Work whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Work whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Work whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Work whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Work whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Work whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Work whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Work whereVideo($value)
 * @mixin \Eloquent
 */
class Work extends Model implements HasImagesContract
{
    use ImageAttributeTrait;
    use HasImages;
    use InitPublicScopeActive;

    protected $table = 'works';
    protected $guarded = [
        'id',
    ];

    protected $casts = [
        'video' => 'array',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }


    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setVideoAttribute($value)
    {
        $attribute = 'video';
        $value = array_filter($value);
        $value = $this->castAttributeAsJson($attribute, $value);
        $this->attributes[$attribute] = $value;
    }

}
