<?php

namespace App\Traits\Models;


trait HoursFromToAttributes
{

	/**
	 * @param $from
	 * @return HoursFromToAttributes
	 */
	public function setHoursFromAttribute($from): self
	{
		$this->attributes['hours_from'] = $from;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getHoursFromAttribute()
	{
		return \Arr::get($this->attributes, 'hours_from');
	}

	/**
	 * @param $to
	 * @return HoursFromToAttributes
	 */
	public function setHoursToAttribute($to): self
	{
		$this->attributes['hours_to'] = $to;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getHoursToAttribute()
	{
		return \Arr::get($this->attributes, 'hours_to');
	}

}