<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersAddLocaleField extends Migration
{
	public function up()
	{
		Schema::table('users', function (Blueprint $table) {
			$table->string('locale', 20)->default(\Config::get('app.locale'));
		});
	}

	public function down()
	{
		Schema::table('users', function (Blueprint $table) {
			$table->dropColumn('locale');
		});
	}
}
