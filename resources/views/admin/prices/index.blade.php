<?php /** @var $permissionKey string */ ?>
@php
    $canEditCategory = Gate::allows('edit_'. $permissionKey );
    $canDeleteCategory = Gate::allows('delete_'. $permissionKey );
@endphp

<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th class="">Категория</th>
            <th class="">Дата создания</th>
            <th class="text-right">
                <a href="{{ route($routeKey.'.create') }}" class="btn btn-primary">@lang('form.create')</a>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php /** @var $item \App\Models\Category\Price */ ?>
        <?php /** @var $category \App\Models\Category\Category */ ?>
        @foreach($list as $item)
            <tr>
                <td>
                    @if ($category = $item->category)
                        <a href="{{ route('admin.category.edit', $category->id) }}">{{ $category->getCategoryName() }}</a>
                    @endif
                </td>
                <td>
                    {{ $item->created_at }}
                </td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>