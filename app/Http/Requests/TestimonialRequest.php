<?php

namespace App\Http\Requests;

use App\Repositories\CategoryRepository;

class TestimonialRequest extends AbstractRequest
{
	/**
	 * @var CategoryRepository
	 */
	private $categoryRepository;

	public function __construct(CategoryRepository $categoryRepository)
	{
		parent::__construct();
		$this->categoryRepository = $categoryRepository;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$auth = \Auth::check();
		$rules = [
			'name'        => ['required', 'string', 'min:2', 'max:100'],
			'message'     => ['required', 'string', 'min:10', 'max:1000'],
			'category_id' => ['nullable', 'exists:categories,id'],
		];
		if ($this->has('image') && $this->hasFile('image.0')) {
			$rules['image.*'] = ['nullable', 'image', 'mimes:jpeg,png,jpg', 'max:2048'];
		}
		if ($auth) {
			return $rules;
		}
		return [
				'phone' => ['required', 'string', 'min:2', 'max:100'],
				'email' => ['required', 'string', 'email', 'min:5', 'max:100'],
			] + $rules;
	}

	public function messages()
	{
		return [
			'comment' => 'текст',
		];
	}

	/*
	 * public function prepareForValidation(): void
	{
		if (!$categoryId = $this->get('category_id')) {
			return;
		}
		$category = $this->categoryRepository->findById($categoryId);
		if (!$category && $category->isMainCategory()) {
			return;
		}
		$category = $this->categoryRepository->getCategoryParents($category)->first();
		if (!$category) {
			return;
		}
		$this->merge(['category_id' => $category->id]);
	}
	*/
}
