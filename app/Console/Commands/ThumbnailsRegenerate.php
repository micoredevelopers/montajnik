<?php

namespace App\Console\Commands;

use App\Config\Media\ThumbnailImagesConfig;
use App\Helpers\Media\ImageMeta;
use App\Models\Model;
use App\Repositories\AbstractRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\NewsRepository;
use App\Repositories\ProductRepository;
use App\Repositories\WorkRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class ThumbnailsRegenerate extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'app.thumbnails:regenerate {module}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Regenerate thumbnails by module';

	private $supports = [
		'categories',
		'products',
		'news',
		'works',
	];

	private $matches = [
		'categories' => CategoryRepository::class,
		'products'   => ProductRepository::class,
		'news'       => NewsRepository::class,
		'works'      => WorkRepository::class,
	];


	/**
	 * Execute the console command.
	 */
	public function handle(): void
	{
		$type = $this->argument('module');
		if (!$this->supports($type)) {
			$err = sprintf('Module "%s" does not supports, available:[%s]', $type, implode(',', $this->supports));
			$this->info($err);
			return;
		}
		$records = $this->getRecordsByModuleKey($type);
		[$width, $height] = $this->getThumbnailSizes($type);
		$imageMeta = new ImageMeta();
		$imageMeta->setWidth($width)->setHeight($height);
		$imageThumbnailer = new \App\Helpers\Media\ImageThumbnail($imageMeta);
		/** @var  $record Model */
		foreach ($records as $record) {
			if (storageFileExists(imgPathOriginal($record->image))) {
				$imagePath = \Storage::path(imgPathOriginal($record->image));
				\Storage::put($record->image, $imageThumbnailer->makeThumbnail($imagePath));
				$this->info($record->image);
			}
		}
	}

	private function getRecordsByModuleKey(string $type): Collection
	{
		/** @var  $repo AbstractRepository */
		$repo = app()->make($this->matches[$type]);
		return $repo->all();
	}

	protected function supports(string $type): bool
	{
		return in_array($type, $this->supports, true);
	}

	protected function getThumbnailSizes(string $type): array
	{
		return ThumbnailImagesConfig::getSizesByKey($type);
	}
}
