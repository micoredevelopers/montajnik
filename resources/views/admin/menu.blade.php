<?php /** @var $item \App\Models\Admin\Admin_menu */ ?>
<ul class="nav pb-5 sidebar-menu dropdownMenu">
    @if(isset($menu) AND $menu)
        @foreach($menu as $item)
            @include('admin.admin-menu-nested')
        @endforeach
    @endif

    <li class="nav-item mt-5 text-center">
        <form action="{{ route('admin.logout') }}" method="POST">
            @csrf
            <button class="btn btn-primary">
                <b>{{ __('Выйти') }}</b>
            </button>
        </form>
    </li>
</ul>
