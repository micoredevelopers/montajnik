<?php /** @var $new \App\Models\News */ ?>
@if ($newsMain)
    <section class="news">
        <div class="container">
            @foreach($newsMain as $new)
                <div class="row">
                    <div class="col-lg-5">
                        <div class="news-img-wrap mb-30">
                            <img src="{{ checkImage('') }}" class=" lazy" data-src="{{ checkImage($new->image) }}"
                                 alt="{{ translateFormat('news.new-single', ['%name%' => $new->getNameAttribute()]) }}"/>
                        </div>
                    </div>
                    <div class="col-lg-7 ">
                        <div class="news-desc-wrap">
                            <div class="title title_drop-decor mb-15">{{ $new->getNameAttribute() }}</div>
                            <div class="text_light mb-20">{!! $new->getExcept() !!}</div>
                            <div class="d-flex justify-content-between justify-content-lg-start">
                                <a href="{{ route('news.show', $new->getUrlAttribute()) }}"
                                   class="btn-custom w-158 mr-lg-5">{{ getTranslate('news.new-detail') }}</a>
                                <a href="{{ route('news.index') }}"
                                   class=" w-158 btn-custom_inverse"
                                ><span>{{ getTranslate('news.news-all') }}</span></a
                                >
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endif
