<?php

namespace App\Http\Middleware;

class AdminAuthenticated extends Authenticate
{
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('admin.login');
        }
        return '';
    }
}
