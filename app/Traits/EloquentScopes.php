<?php
/**
 * Created by PhpStorm.
 * User: aljajazva
 * Date: 2019-05-22
 * Time: 13:34
 */

namespace App\Traits;


trait EloquentScopes
{

	public function scopeWhereUrl($query, $url)
	{
		return $query->where('url', '=', $url);
	}

	public function scopeWhereIdIn($query, $ids, $field = 'id')
	{
		if (is_string($ids) AND $ids) $ids = explode(',', $ids);

		return $query->whereIn($field, $ids);
	}

	public function scopeActive($query, $active = 1)
	{
		return $query->where('active', '=', $active);
	}

	public function scopeParentMenu($query, $parentId = 0)
	{
		return $query->where(function ($query) use ($parentId) {
			return $query->where('parent_id', $parentId)->orWhereNull('parent_id');
		});
	}

	public function scopeDefault($query)
	{
		return $query->where('default', '=', 1);
	}

	public function scopeWhereLanguage($query, $languageId)
	{
		return $query->where('language_id', '=', $languageId);
	}

	public function scopeGetLang($query, $languageId = null)
	{
		if (!$languageId){
			$languageId = getLang();
		}
		$tableLang = $this->tableLang ?? $this->getTable() . '_lang';
		$primary = $this->getTable() . '.' . $this->getKeyName();
		$foreign = $tableLang . '.' . $this->getForeignKey();

		return $query->leftJoin($tableLang, $foreign, $primary)
			->where('language_id', '=', (int)$languageId);
	}

	public function scopeWhereId($query, $id)
	{
		return $query->where('id', '=', $id);
	}

	public function scopeSortOrder($query, $sort = 'asc', $id = 'desc')
	{
		return $query->orderBy('sort', $sort)->orderBy('id', $id);
	}

	public function setTimestamps($flag = false)
	{
		$this->timestamps = $flag;
	}

	public function scopeWhereIsPublished(\Illuminate\Database\Eloquent\Builder $query, $column = 'published_at')
	{
		return $query->where($column, '<', now());
	}


	public function scopeWhereLike(\Illuminate\Database\Eloquent\Builder $query, $column, $value)
	{
		return $query->where($column, 'like', '%'.$value.'%');
	}

	public function scopeOrWhereLike($query, $column, $value)
	{
		return $query->orWhere($column, 'like', '%'.$value.'%');
	}

}
