<?php /** @var $request \Illuminate\Http\Request */ ?>
<?php /** @var $item \App\Models\Feedback */ ?>
<form action="">
	<div class="form-group">
		<div class="row">
			<div class="col-5">
				<label for="search">Поиск</label>
				<input type="text" class="form-control mt-3" name="search" value="{{ $search ?? '' }}" autocomplete="off"
					   placeholder="Search" id="search">
			</div>
			<div class="col-3">
				@include('admin.partials.crud.date.date-interval', ['value' => $request->get('date')])
			</div>
			<div class="col-2">
				<label for="type">Тип</label>
				<select name="type" id="type" class="form-control selectpicker" title="type">
					@foreach($types as $type => $name)
						<option {!! selectedIfTrue($type == $request->get('type')) !!} value="{{ $type }}">{{ $name }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-2 pt-4">
				<button class="btn btn-danger" type="submit"><i class="fa fa-search" aria-hidden="true"></i>{{ __('form.search') }}</button>
			</div>
		</div>
	</div>
</form>
@foreach($list as $item)
	@if (View::exists('admin.feedback.types.' . $item->type))
		@include('admin.feedback.types.' . $item->type, ['feedback' => $item])
	@else
		@include('admin.feedback.types.feedback', ['feedback' => $item])
	@endif
@endforeach

{{$list->render()}}
