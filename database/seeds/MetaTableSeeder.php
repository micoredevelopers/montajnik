<?php

use Illuminate\Database\Seeder;

class MetaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('meta')->delete();
        
        \DB::table('meta')->insert(array (
            0 => 
            array (
                'id' => 1,
                'url' => '/catalog/all',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'url' => '/photos',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'url' => '/catalog/ceilings',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжные потолки",
"image": "https://montajnik.od.ua/storage/category/267_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 4.8,
"ratingCount": 45
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "159",
"highPrice": "2000",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'url' => '/catalog/glazing-of-balconies-and-loggias',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Остекленение балкона и лоджии",
"image": "https://montajnik.od.ua/storage/category/278_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'url' => '/catalog/takeaway-balcony',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Вынос балкона",
"image": "https://montajnik.od.ua/storage/category/274_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'url' => '/catalog/sheathing-balcony',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Обшивка балкона",
"image": "https://montajnik.od.ua/storage/category/276_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'url' => '/catalog/the-insulation-of-the-balconies',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Утепление балкона",
"image": "https://montajnik.od.ua/storage/category/281_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'url' => '/catalog/balconies-from-scratch',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Балконы с нуля",
"image": "https://montajnik.od.ua/storage/category/273_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'url' => '/catalog/okonnaya-furnitura-329',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Оконная фурнитура",
"image": "https://montajnik.od.ua/storage/category/329_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'url' => '/catalog/french-balcony',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Французский балкон",
"image": "https://montajnik.od.ua/storage/category/282_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'url' => '/catalog/potolok-s-podsvetkoy-316',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжной потолок с подсветкой:",
"image": "https://montajnik.od.ua/storage/category/316_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'url' => '/catalog/blinds-rolety',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Жалюзи, ролеты",
"image": "https://montajnik.od.ua/storage/category/265_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'url' => '/catalog/gorizontalnye-zhalyuzi-294',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Горизонтальные жалюзи",
"image": "https://montajnik.od.ua/storage/category/294_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'url' => '/catalog/vertikalnye-zhalyuzi-293',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Вертикальные жалюзи",
"image": "https://montajnik.od.ua/storage/category/293_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'url' => '/catalog/rulonnye-shtory-299',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Рулонные шторы",
"image": "https://montajnik.od.ua/storage/category/299_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'url' => '/catalog/otkrytye-sistemy-rulonnye-shtory-297',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Открытые системы рулонные шторы",
"image": "https://montajnik.od.ua/storage/category/297_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'url' => '/catalog/zakrytye-sistemy-rulonnye-shtory-295',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Закрытые системы рулонные шторы",
"image": "https://montajnik.od.ua/storage/category/295_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'url' => '/catalog/mansardnye-sistemy-296',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Мансардные системы",
"image": "https://montajnik.od.ua/storage/category/296_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'url' => '/catalog/bambukovye-i-dzhutovye-292',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Бамбуковые и джутовые",
"image": "https://montajnik.od.ua/storage/category/292_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'url' => '/catalog/drying-on-the-balcony',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Сушилка на балкон",
"image": "https://montajnik.od.ua/storage/category/280_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'url' => '/catalog/lighting-electrician',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Освещение и электрика",
"image": "https://montajnik.od.ua/storage/category/277_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'url' => '/catalog/wardrobe-on-the-balcony-and-a-loggia',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Шкафы на балконе и лоджии",
"image": "https://montajnik.od.ua/storage/category/283_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'url' => '/catalog/otlivy-i-kozyrki-333',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Отливы и козырьки",
"image": "https://montajnik.od.ua/storage/category/333_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'url' => '/catalog/vnutriramnaya-setka-330',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Внутрирамная сетка",
"image": "https://montajnik.od.ua/storage/category/330_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'url' => '/catalog/take-out-window-sill',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Вынос подоконника",
"image": "https://montajnik.od.ua/storage/category/275_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'url' => '/catalog/balconies',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Балконы и лоджии",
"image": "https://montajnik.od.ua/storage/category/263_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'url' => '/catalog/matovye-natyazhnye-potolki-310',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Матовые натяжные потолки:",
"image": "https://montajnik.od.ua/storage/category/310_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'url' => '/catalog/remont-okon-327',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Ремонт окон",
"image": "https://montajnik.od.ua/storage/category/327_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'url' => '/catalog/pravila-ekspluatacii-okon-328',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Правила эксплуатации окон",
"image": "https://montajnik.od.ua/storage/category/328_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'url' => '/catalog/insulation-of-walls',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Утепление стен",
"image": "https://montajnik.od.ua/storage/category/272_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'url' => '/catalog/ukladka-keramicheskoy-plitki-343',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Укладка керамической плитки",
"image": "https://montajnik.od.ua/storage/category/343_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'url' => '/catalog/vyravnivanie-sten-338',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Выравнивание стен",
"image": "https://montajnik.od.ua/storage/category/338_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'url' => '/catalog/montazh-kondicionerov-303',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Монтаж и установка кондиционера",
"image": "https://montajnik.od.ua/storage/category/303_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'url' => '/catalog/zakladka-301',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Закладка магистрали кондиционера",
"image": "https://montajnik.od.ua/storage/category/301_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'url' => '/catalog/demontazh-300',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Демонтаж кондиционера с сохранением фриона",
"image": "https://montajnik.od.ua/storage/category/300_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'url' => '/catalog/servis-305',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Обслуживание кондиционера",
"image": "https://montajnik.od.ua/storage/category/305_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'url' => '/catalog/zapravka-302',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Заправка кондиционера",
"image": "https://montajnik.od.ua/storage/category/302_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'url' => '/catalog/remont-kondicionerov-304',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Ремонт кондиционера",
"image": "https://montajnik.od.ua/storage/category/304_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'url' => '/catalog/air-conditioning',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Кондиционеры",
"image": "https://montajnik.od.ua/storage/category/266_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'url' => '/catalog/categories-doors-economy',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Категории дверей ЭКОНОМ",
"image": "https://montajnik.od.ua/storage/category/290_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'url' => '/catalog/categories-doors-standard',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Категории дверей СТАНДАРТ",
"image": "https://montajnik.od.ua/storage/category/289_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'url' => '/catalog/categories-doors-guardian',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Категории дверей ГАРДИАН",
"image": "https://montajnik.od.ua/storage/category/285_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'url' => '/catalog/categories-doors-mottura-is',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Категории дверей МОТТУРА",
"image": "https://montajnik.od.ua/storage/category/286_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'url' => '/catalog/categories-doors-multilok',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Категории дверей МУЛЬТИЛОК",
"image": "https://montajnik.od.ua/storage/category/287_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'url' => '/catalog/doors',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Бронированные двери",
"image": "https://montajnik.od.ua/storage/category/264_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'url' => '/catalog/reshetki',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Решетки и ворота",
"image": "https://montajnik.od.ua/storage/category/270_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'url' => '/catalog/Kommerling-321',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Оконный профиль Kommerling",
"image": "https://montajnik.od.ua/storage/category/321_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'url' => '/catalog/window',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Металлопластиковые окна",
"image": "https://montajnik.od.ua/storage/category/268_s.png",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'url' => '/catalog/Osnova-323',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Оконный профиль Osnova",
"image": "https://montajnik.od.ua/storage/category/323_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'url' => '/catalog/podokonniki-320',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Подоконники",
"image": "https://montajnik.od.ua/storage/category/320_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'url' => '/catalog/montazh-okon-319',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Монтаж окон",
"image": "https://montajnik.od.ua/storage/category/319_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'url' => '/catalog/Rehau-322',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Оконный профиль Rehau",
"image": "https://montajnik.od.ua/storage/category/322_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'url' => '/catalog/WDS-324',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Оконный профиль WDS",
"image": "https://montajnik.od.ua/storage/category/324_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'url' => '/catalog/KBE-325',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Оконный профиль KBE",
"image": "https://montajnik.od.ua/storage/category/325_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'url' => 'http://montajnik.od.ua/news/akciya',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'url' => 'http://montajnik.od.ua/catalog/kondicioner-Gree-GWHN24JDNK1A1A-040-do-65-kv-m-041-411',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'url' => 'http://montajnik.od.ua/catalog/construction-and-repair',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'url' => 'http://montajnik.od.ua/catalog/window',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'url' => 'http://montajnik.od.ua/news/akciya-14',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'url' => 'http://montajnik.od.ua/news/akciya-11',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'url' => 'http://montajnik.od.ua/news/akciya-13',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'url' => '/news/akciya-15',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'url' => 'http://montajnik.od.ua/catalog/kondicioner-Gree-GWHN12JANK3A1B-silver-040-do-35-kv-m-041-410',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'url' => 'http://montajnik.od.ua/catalog/kondicioner-Gree-GWHN12JANK3A1B-silver-040-do-35-kv-m-041-404',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'url' => 'http://montajnik.od.ua/catalog/kondicioner-Gree-GWHN12JBNK1A1A-040-do-35-kv-m-041-409',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'url' => 'http://montajnik.od.ua/catalog/kondicioner-Gree-GWHN12JBNK1A1A-040-do-35-kv-m-041-403',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'url' => 'http://montajnik.od.ua/catalog/kondicioner-Gree-GWHN09JANK3A1B-silver-040-do-25-kv-m-041-408',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'url' => 'http://montajnik.od.ua/catalog/kondicioner-Gree-GWHN09JANK3A1B-silver-040-do-25-kv-m-041-402',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'url' => 'http://montajnik.od.ua/catalog/otlivy-i-kozyrki-333',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'url' => 'http://montajnik.od.ua/catalog/french-balcony',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-04 16:30:05',
                'header' => NULL,
                'footer' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'url' => '/catalog/plastikovye-okna-ekonom-klassa-odessa',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Пластиковые окна эконом-класса Одесса",
"image": "https://montajnik.od.ua/storage/category/432_s.png",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'url' => '/catalog/poleznye-stati-o-metalloplastikovyh-oknah-326',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Полезные статьи о металлопластиковых окнах",
"image": "https://montajnik.od.ua/storage/category/326_s.png",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'url' => '/catalog/satinovye-natyazhnye-potolki-441',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Сатиновые натяжные потолки:",
"image": "https://montajnik.od.ua/storage/category/441_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'url' => '/catalog/reznye-natyazhnye-potolki-Apply-442',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:05',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Резные натяжные потолки Apply:",
"image": "https://montajnik.od.ua/storage/category/442_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'url' => '/catalog/natyazhnoy-potolok-zvezdnoe-nebo-montazh-443',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжной потолок звёздное небо монтаж:",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'url' => '/catalog/fakturnye-natyazhnye-potolki-444',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Фактурные натяжные потолки: ",
"image": "https://montajnik.od.ua/storage/category/444_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'url' => '/catalog/natyazhnoy-potolok-nebo-s-oblakami-445',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжной потолок небо с облаками:",
"image": "https://montajnik.od.ua/storage/category/445_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'url' => '/catalog/natyazhnoy-potolok-zamshevyy-446',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжной потолок замшевый:",
"image": "https://montajnik.od.ua/storage/category/446_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'url' => '/catalog/montazh-natyazhnogo-potolka-raduga-447',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжной потолок &quot;Радуга&quot;: ",
"image": "https://montajnik.od.ua/storage/category/447_s.png",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'url' => '/catalog/poluprozrachnyy-natyazhnoy-potolok-448',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Полупрозрачный натяжной потолок: ",
"image": "https://montajnik.od.ua/storage/category/448_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'url' => '/catalog/3D-natyazhnye-potolki-449',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "3D натяжные потолки:",
"image": "https://montajnik.od.ua/storage/category/449_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'url' => '/catalog/dvuhurovnevye-natyazhnye-potolki-s-fotopechatyu-450',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Двухуровневые натяжные потолки с фотопечатью:",
"image": "https://montajnik.od.ua/storage/category/450_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'url' => '/catalog/natyazhnye-potolki-s-uzorom-451',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжные потолки с узором:",
"image": "https://montajnik.od.ua/storage/category/451_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'url' => '/catalog/tkanevye-natyazhnye-potolki-pod-pokrasku-452',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Тканевые натяжные потолки под покраску:",
"image": "https://montajnik.od.ua/storage/category/452_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'url' => '/catalog/natyazhnye-potolki-s-volnoy-453',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжные потолки &quot;Волна&quot;:",
"image": "https://montajnik.od.ua/storage/category/453_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'url' => '/catalog/mnogourovnevye-natyazhnye-potolki-454',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Многоуровневые натяжные потолки:",
"image": "https://montajnik.od.ua/storage/category/454_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'url' => '/catalog/istochniki-osvescheniya-dlya-natyazhnyh-potolkov-455',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Источники освещения для натяжных потолков",
"image": "https://montajnik.od.ua/storage/category/455_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'url' => '/testovaya',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'url' => '/video/24',
                'type' => 0,
                'active' => 0,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'url' => 'http://montajnik.od.ua/video/24',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'url' => '/',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'url' => 'http://montajnik.od.ua/video/23',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'url' => 'http://montajnik.od.ua/video/22',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'url' => 'http://montajnik.od.ua/video/21',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'url' => 'http://montajnik.od.ua/video/20',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'url' => 'http://montajnik.od.ua/video/19',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'url' => 'http://montajnik.od.ua/video/18',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'url' => 'http://montajnik.od.ua/video/17',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'url' => 'http://montajnik.od.ua/video/16',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'url' => 'http://montajnik.od.ua/video/15',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'url' => 'http://montajnik.od.ua/video/14',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'url' => 'http://montajnik.od.ua/video/13',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'url' => 'http://montajnik.od.ua/video/12',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'url' => 'http://montajnik.od.ua/video/11',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'url' => 'http://montajnik.od.ua/video/10',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            105 => 
            array (
                'id' => 106,
                'url' => 'http://montajnik.od.ua/video/9',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            106 => 
            array (
                'id' => 107,
                'url' => 'http://montajnik.od.ua/video/all',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            107 => 
            array (
                'id' => 108,
                'url' => '/comments/all',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            108 => 
            array (
                'id' => 109,
                'url' => '/contacts',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            109 => 
            array (
                'id' => 110,
                'url' => '/voprosy-otvety',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            110 => 
            array (
                'id' => 111,
                'url' => '/photos/nashi-raboty',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            111 => 
            array (
                'id' => 112,
                'url' => '/about-us',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            112 => 
            array (
                'id' => 113,
                'url' => '/sitemap',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            113 => 
            array (
                'id' => 114,
                'url' => '/video/all',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            114 => 
            array (
                'id' => 115,
                'url' => '/news/all',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            115 => 
            array (
                'id' => 116,
                'url' => '/news/mnogourovnevye-natyazhnye-potolki',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            116 => 
            array (
                'id' => 117,
                'url' => '/catalog/bezschelevye-natyazhnye-potolki-306',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Беcщелевые натяжные потолки:",
"image": "https://montajnik.od.ua/storage/category/306_s.png",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            117 => 
            array (
                'id' => 118,
                'url' => '/catalog/natyazhnye-potolki-s-effektom-quot-zvezdnoe-nebo-quot-312',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжные потолки &quot;Звездное небо&quot;:",
"image": "https://montajnik.od.ua/storage/category/312_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            118 => 
            array (
                'id' => 119,
                'url' => '/catalog/paryaschie-potolki',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Парящие потолки – инновационная технология:",
"image": "https://montajnik.od.ua/storage/category/314_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            119 => 
            array (
                'id' => 120,
                'url' => '/catalog/preimuschestva-dvuhurovnevyh-natyazhnyh-potolkov-317',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Двухуровневые натяжные потолки:",
"image": "https://montajnik.od.ua/storage/category/317_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            120 => 
            array (
                'id' => 121,
                'url' => '/catalog/window-accessories',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Оконные комплектующие",
"image": "https://montajnik.od.ua/storage/category/269_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            121 => 
            array (
                'id' => 122,
                'url' => '/comments/1014',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            122 => 
            array (
                'id' => 123,
                'url' => '/comments/390',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            123 => 
            array (
                'id' => 124,
                'url' => '/comments/387',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            124 => 
            array (
                'id' => 125,
                'url' => '/comments/389',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            125 => 
            array (
                'id' => 126,
                'url' => '/comments/388',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            126 => 
            array (
                'id' => 127,
                'url' => '/catalog/info',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Полезная информация о строительстве балконов",
"image": "https://montajnik.od.ua/storage/category/279_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            127 => 
            array (
                'id' => 128,
                'url' => '/catalog/kozyrki-i-kryshi-na-balkon',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Козырьки и крыши на балкон",
"image": "https://montajnik.od.ua/storage/category/424_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            128 => 
            array (
                'id' => 129,
                'url' => '/catalog/neobychnye-dizainy-balkonov-i-lodzhii-odessa',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Необычные дизайны балконов и лоджий Одесса",
"image": "https://montajnik.od.ua/storage/category/425_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            129 => 
            array (
                'id' => 130,
                'url' => '/catalog/osteklenie-verand-i-terras-v-odesse',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Остекление веранд и террас в Одессе",
"image": "https://montajnik.od.ua/storage/category/426_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            130 => 
            array (
                'id' => 131,
                'url' => '/catalog/teploe-osteklenie-balkonov-v-odesse',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Теплое остекление балконов в Одессе",
"image": "https://montajnik.od.ua/storage/category/427_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            131 => 
            array (
                'id' => 132,
                'url' => '/catalog/kak-provesti-zamer-okon-samomu',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Как провести замер окон самому",
"image": "https://montajnik.od.ua/storage/category/430_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            132 => 
            array (
                'id' => 133,
                'url' => '/catalog/kakie-byvaiut-plastikovye-okna',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Какие бывают пластиковые окна?",
"image": "https://montajnik.od.ua/storage/category/429_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            133 => 
            array (
                'id' => 134,
                'url' => '/catalog/kalkuliator-stoimosti-plastikovykh-okon-v-odesse',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Калькулятор стоимости пластиковых окон в Одессе",
"image": "https://montajnik.od.ua/storage/category/431_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            134 => 
            array (
                'id' => 135,
                'url' => '/catalog/sravnenie-pvkh-profilei-odessa',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Сравнение ПВХ профилей Одесса",
"image": "https://montajnik.od.ua/storage/category/433_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            135 => 
            array (
                'id' => 136,
                'url' => '/catalog/tonirovka-plastikovykh-okon-v-odesse',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Тонировка пластиковых окон в Одессе",
"image": "https://montajnik.od.ua/storage/category/434_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            136 => 
            array (
                'id' => 137,
                'url' => '/catalog/tsvetnye-plastikovye-okna-v-odesse',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Цветные пластиковые окна в Одессе",
"image": "https://montajnik.od.ua/storage/category/435_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            137 => 
            array (
                'id' => 138,
                'url' => '/catalog/zerkalnye-steklopakety-odessa',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Зеркальные стеклопакеты Одесса",
"image": "https://montajnik.od.ua/storage/category/436_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            138 => 
            array (
                'id' => 139,
                'url' => '/catalog/categories-doors-stallard',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Категории дверей СтальГард",
"image": "https://montajnik.od.ua/storage/category/288_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            139 => 
            array (
                'id' => 140,
                'url' => '/catalog/locks-and-fittings',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Замки и фурнитура",
"image": "https://montajnik.od.ua/storage/category/284_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            140 => 
            array (
                'id' => 141,
                'url' => '/catalog/the-range',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Модельный ряд",
"image": "https://montajnik.od.ua/storage/category/291_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            141 => 
            array (
                'id' => 142,
                'url' => '/catalog/vyravnivanie-pola-336',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Выравнивание пола",
"image": "https://montajnik.od.ua/storage/category/336_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            142 => 
            array (
                'id' => 143,
                'url' => '/catalog/vyravnivanie-potolka-337',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Выравнивание потолка",
"image": "https://montajnik.od.ua/storage/category/337_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            143 => 
            array (
                'id' => 144,
                'url' => '/catalog/pokleyka-oboev-340',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Поклейка обоев",
"image": "https://montajnik.od.ua/storage/category/340_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            144 => 
            array (
                'id' => 145,
                'url' => '/catalog/pokraska-potolkov-341',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Покраска потолков",
"image": "https://montajnik.od.ua/storage/category/341_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            145 => 
            array (
                'id' => 146,
                'url' => '/catalog/ustanovka-rozetok-i-vyklyuchateley-344',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Установка розеток и выключателей",
"image": "https://montajnik.od.ua/storage/category/344_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            146 => 
            array (
                'id' => 147,
                'url' => '/catalog/kupit-natyazhnye-potolki-na-kuhnyu-v-odesse-460',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Купить натяжные потолки на кухню в Одессе",
"image": "https://montajnik.od.ua/storage/category/460_s.png",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            147 => 
            array (
                'id' => 148,
                'url' => '/catalog/natyazhnoy-potolok-v-koridore-odessa-461',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжной потолок в коридоре Одесса",
"image": "https://montajnik.od.ua/storage/category/461_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            148 => 
            array (
                'id' => 149,
                'url' => '/catalog/natyazhnye-potolki-v-zal-odessa-montazh-za-1-den-462',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжные потолки в зал Одесса: монтаж за 1 день",
"image": "https://montajnik.od.ua/storage/category/462_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            149 => 
            array (
                'id' => 150,
                'url' => '/catalog/natyazhnye-potolki-v-spalne-odessa-463',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжные потолки в спальне Одесса",
"image": "https://montajnik.od.ua/storage/category/463_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            150 => 
            array (
                'id' => 151,
                'url' => '/catalog/natyazhnye-potolki-v-detskoy-odessa-464',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжные потолки в детской Одесса",
"image": "https://montajnik.od.ua/storage/category/464_s.png",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            151 => 
            array (
                'id' => 152,
                'url' => '/catalog/ustanavlivaem-natyazhnye-potolki-v-vannuyu-odessa',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Устанавливаем натяжные потолки в ванную Одесса за 1 день",
"image": "https://montajnik.od.ua/storage/category/465_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            152 => 
            array (
                'id' => 153,
                'url' => '/pochemu-natiazhnoi-potolok-luchshe-chem-gipsokarton',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            153 => 
            array (
                'id' => 154,
                'url' => '/chto-delat-esli-zatopilo-natiazhnye-potolki',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            154 => 
            array (
                'id' => 155,
                'url' => '/daiut-li-shumoizoliatsiiu-natiazhnye-potolki',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            155 => 
            array (
                'id' => 156,
                'url' => '/news/kak-vybrat-natyazhnoy-potolok',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            156 => 
            array (
                'id' => 157,
                'url' => '/news/kak-uteplit-balkon-iznutri',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            157 => 
            array (
                'id' => 158,
                'url' => '/catalog/podokonniki-334',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Подоконники",
"image": "https://montajnik.od.ua/storage/category/334_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            158 => 
            array (
                'id' => 159,
                'url' => '/news/kak-vybrat-okna',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            159 => 
            array (
                'id' => 160,
                'url' => '/news/kak-pravilno-obshit-balkon',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-04 16:30:06',
                'header' => NULL,
                'footer' => NULL,
            ),
            160 => 
            array (
                'id' => 161,
                'url' => '/catalog/glyancevye-natyazhnye-potolki-307',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Глянцевые натяжные потолки:",
"image": "https://montajnik.od.ua/storage/category/307_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            161 => 
            array (
                'id' => 162,
                'url' => '/catalog/chistka-kondicionera-470',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Чистка кондиционера",
"image": "https://montajnik.od.ua/storage/category/470_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            162 => 
            array (
                'id' => 163,
                'url' => '/catalog/construction-and-repair',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:06',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Строительство и ремонт",
"image": "https://montajnik.od.ua/storage/category/271_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            163 => 
            array (
                'id' => 164,
                'url' => '/catalog/demontazhnye-raboty-339',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Демонтажно-подготовительные работы",
"image": "https://montajnik.od.ua/storage/category/339_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            164 => 
            array (
                'id' => 165,
                'url' => '/catalog/santehnicheskie-raboty-473',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Сантехнические работы",
"image": "https://montajnik.od.ua/storage/category/473_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            165 => 
            array (
                'id' => 166,
                'url' => '/catalog/elektromontazhnye-raboty-474',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Электромонтажные работы",
"image": "https://montajnik.od.ua/storage/category/474_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            166 => 
            array (
                'id' => 167,
                'url' => '/catalog/gipsokartonnye-raboty-476',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Гипсокартонные работы",
"image": "https://montajnik.od.ua/storage/category/476_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            167 => 
            array (
                'id' => 168,
                'url' => '/catalog/plitochnye-raboty-478',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:34:50',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Плиточные работы",
"image": "https://montajnik.od.ua/storage/category/478_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            168 => 
            array (
                'id' => 169,
                'url' => '/catalog/dizayn-interera-479',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:37:49',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Дизайн интерьера",
"image": "https://montajnik.od.ua/storage/category/479_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            169 => 
            array (
                'id' => 170,
                'url' => '/catalog/ceny-stroitelnyh-uslug-480',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Цены строительных услуг",
"image": "https://montajnik.od.ua/storage/category/480_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            170 => 
            array (
                'id' => 171,
                'url' => '/catalog/shtukaturnye-raboty-475',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Штукатурные работы",
"image": "https://montajnik.od.ua/storage/category/475_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            171 => 
            array (
                'id' => 172,
                'url' => '/catalog/malyarnye-raboty-040-shpaklevka-pokraska-oboi-laminat-041-477',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Малярные работы&#040;шпаклевка, покраска,обои,ламинат&#041;",
"image": "https://montajnik.od.ua/storage/category/477_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            172 => 
            array (
                'id' => 173,
                'url' => '/catalog/po-materialu-456',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжной потолок по материалу",
"image": "https://montajnik.od.ua/storage/category/456_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            173 => 
            array (
                'id' => 174,
                'url' => '/catalog/po-tipu-poverhnosti-457',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжной потолок по типу поверхности",
"image": "https://montajnik.od.ua/storage/category/457_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            174 => 
            array (
                'id' => 175,
                'url' => '/catalog/po-konstrukcii-458',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжной потолок по  конструкции",
"image": "https://montajnik.od.ua/storage/category/458_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            175 => 
            array (
                'id' => 176,
                'url' => '/catalog/po-primeneniyu-459',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:02',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Натяжной потолок по  применению",
"image": "https://montajnik.od.ua/storage/category/459_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            176 => 
            array (
                'id' => 177,
                'url' => '/catalog/moskitnaya-setka-na-karmashkah-331',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Москитная сетка на кармашках",
"image": "https://montajnik.od.ua/storage/category/331_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            177 => 
            array (
                'id' => 178,
                'url' => '/catalog/okonnaya-furnitura-332',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Оконная фурнитура",
"image": "https://montajnik.od.ua/storage/category/332_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            178 => 
            array (
                'id' => 179,
                'url' => '/catalog/setki-na-petlyah-335',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Сетки на петлях",
"image": "https://montajnik.od.ua/storage/category/335_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            179 => 
            array (
                'id' => 180,
                'url' => '/catalog/shpaklevka-sten',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Шпаклевка стен",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            180 => 
            array (
                'id' => 181,
                'url' => '/catalog/ustanovka-boylera',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Установка бойлера",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            181 => 
            array (
                'id' => 182,
                'url' => '/catalog/ukladka-plitki',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Укладка плитки",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            182 => 
            array (
                'id' => 183,
                'url' => '/catalog/styazhka-pola',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Стяжка пола",
"image": "https://montajnik.od.ua/storage/category/484_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            183 => 
            array (
                'id' => 184,
                'url' => '/catalog/razvodka-santehniki',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Разводка сантехники",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            184 => 
            array (
                'id' => 185,
                'url' => '/catalog/montazh-teplogo-pola',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Монтаж теплого пола",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            185 => 
            array (
                'id' => 186,
                'url' => '/catalog/montazh-gipsokartona',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Монтаж гипсокартона",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            186 => 
            array (
                'id' => 187,
                'url' => '/catalog/dizayn-kvartir',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Дизайн квартир",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            187 => 
            array (
                'id' => 188,
                'url' => '/catalog/demontazh-sten',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Демонтаж стен",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            188 => 
            array (
                'id' => 189,
                'url' => '/catalog/demontazh-plitki',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Демонтаж плитки",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            189 => 
            array (
                'id' => 190,
                'url' => '/catalog/dizaynerskiy-remont',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Дизайнерский ремонт квартир",
"image": "https://montajnik.od.ua/storage/category/491_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            190 => 
            array (
                'id' => 191,
                'url' => '/catalog/optimalnyy-remont',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Оптимальный ремонт квартир",
"image": "https://montajnik.od.ua/storage/category/492_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            191 => 
            array (
                'id' => 192,
                'url' => '/catalog/nedorogoy-remont',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Недорогой ремонт квартир",
"image": "https://montajnik.od.ua/storage/category/493_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            192 => 
            array (
                'id' => 193,
                'url' => '/catalog/standartnyy-remont-kvartir',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Стандартный ремонт квартир",
"image": "https://montajnik.od.ua/storage/category/494_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            193 => 
            array (
                'id' => 194,
                'url' => '/catalog/eksklyuzivnyy-remont',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Эксклюзивный ремонт квартир",
"image": "https://montajnik.od.ua/storage/category/495_s.jpg",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            194 => 
            array (
                'id' => 195,
                'url' => '/catalog/zamena-provodki',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Замена проводки",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            195 => 
            array (
                'id' => 196,
                'url' => '/catalog/pokraska-sten',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Покраска стен",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            196 => 
            array (
                'id' => 197,
                'url' => '/catalog/ukladka-laminata',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Укладка ламината",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            197 => 
            array (
                'id' => 198,
                'url' => '/catalog/ustanovka-unitaza',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Установка унитаза",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            198 => 
            array (
                'id' => 199,
                'url' => '/catalog/shtukaturka-sten',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Штукатурка стен",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            199 => 
            array (
                'id' => 200,
                'url' => '/catalog/stroitelstvo-domov-iz-gazobetona',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Строительство домов из газобетона",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            200 => 
            array (
                'id' => 201,
                'url' => '/catalog/stroitelstvo-domov-iz-penoblokov',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Строительство домов из пеноблоков",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            201 => 
            array (
                'id' => 202,
                'url' => '/catalog/stroitelstvo-domov-iz-kirpicha',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Строительство домов из кирпича",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            202 => 
            array (
                'id' => 203,
                'url' => '/catalog/stroitelstvo-bani-sauny',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Строительство бани, сауны",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            203 => 
            array (
                'id' => 204,
                'url' => '/catalog/ustanovka-elektroshchita',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Установка электрощита",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            204 => 
            array (
                'id' => 205,
                'url' => '/catalog/ustanovka-vanny',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Установка ванны",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            205 => 
            array (
                'id' => 206,
                'url' => '/catalog/ustanovka-dushevoy-kabiny',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Установка душевой кабины",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            206 => 
            array (
                'id' => 207,
                'url' => '/catalog/ustanovka-lystryu',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Установка люстры",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            207 => 
            array (
                'id' => 208,
                'url' => '/catalog/Ustanovka-smesitelya',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Установка смесителя",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
            208 => 
            array (
                'id' => 209,
                'url' => '/catalog/ustanovka-umyvalnika',
                'type' => 0,
                'active' => 1,
                'created_at' => '2019-10-04 16:30:07',
                'updated_at' => '2019-10-14 13:04:03',
                'header' => '<script type="application/ld+json">{
"@context": "http://schema.org/",
"@type": "Service",
"name": "Установка умывальника",
"image": "https://montajnik.od.ua/",
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": 0,
"ratingCount": 0
},
"offers": {
"@type": "AggregateOffer",
"lowPrice": "0",
"highPrice": "0",
"priceCurrency": "UAH"
}
}</script>',
                'footer' => NULL,
            ),
        ));
        
        
    }
}