<?php /** @var $category \App\Models\Category\Category */ ?>
<?php /** @var $prices \Illuminate\Support\Collection */ ?>
<?php /** @var $price \App\Models\Category\Price */ ?>
<?php /** @var $priceItem \App\Models\Category\PriceItem */ ?>
@if (($prices = $category->prices)->isNotEmpty())
    <h2 class="title-category mb-20">{{ translateFormat('category.price-head', ['%категория%' => $category->getLowerNameAttribute()]) }}</h2>
    @foreach($prices as $price)
        <ul class="category-price">
            <li class="category-price__item_first">{{ $price->name }}</li>
            @foreach($price->items as $priceItem)
                <li class="category-price__item">
                    <span>{{ $priceItem->name }}</span>
                    <span>{{ $priceItem->price }}</span>
                    <span>{{ $priceItem->unit }}</span>
                </li>
            @endforeach
        </ul>
    @endforeach
@endif