$(document).ready(function () {
  const topSlider = $('.top-slider')
  const videoSlider = $('.video-reviews-slider')
  const worksSlider = $('.our-works-slider')
  const reviewSlider = $('.category-reviews-slider')

  function topSlick() {
    topSlider.slick({
      lazyLoad: 'ondemand',
      arrows: true,
      prevArrow: $('.top-slider-arrow_prev'),
      nextArrow: $('.top-slider-arrow_next'),
      dots: true,
      appendDots: $('.top-slider-dots__wrap'),
      customPaging: function (slider, i) {
        return `<a>0${i + 1}</a>`
      }
    })
  }
  function ourWorkSlider() {
    worksSlider
        .on('init', function (event, slick) {
          const currentSlide = $(slick.$slides[slick.currentSlide]).index()
          const count = $(slick.slideCount)
          $('.current').text(currentSlide + 1)
          $('.count').text(count[0])
          const currentEl = $(slick.$slides[currentSlide])
          const src = $(currentEl)
              .find('img')
              .attr('data-lazy')
          $('.our-works-slider-bg img').attr('src', src)
        })
        .on('beforeChange', function () {
          $('.our-works-slider-bg').addClass('our-works-slider-bg_active')
          $('.our-works__arrows').css({
            pointerEvents: 'none'
          })
          setTimeout(function () {
            $('.our-works-slider-bg').removeClass('our-works-slider-bg_active')
            $('.our-works__arrows').css({
              pointerEvents: 'auto'
            })
          }, 1000)
        })
        .on('afterChange', function (event, slick, currentSlide) {
          $('.current').text(currentSlide + 1)
          const currentEl = $(slick.$slides[currentSlide])
          const currentImg = $(currentEl).find('img')
          const currentImgSrc = currentImg[0].src
          $('.our-works-slider-bg img').attr('src', currentImgSrc)
        })

    worksSlider.slick({
      arrows: true,
      lazyLoad: 'ondemand',
      fade: true,
      prevArrow: $('.our-works__arrows_prev'),
      nextArrow: $('.our-works__arrows_next')
    })
  }
  function videoReviewSlider() {
    videoSlider.slick({
      slidesToShow: 2,
      arrows: false,
      infinite: false,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 600,
          settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    })
  }
  function reviewSliderInit() {
    reviewSlider.slick({
      lazyLoad: 'ondemand',
      arrows: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: false,
      prevArrow:
          '<div class="reviews-slider-arrow reviews-slider-arrow_prev"></div>',
      nextArrow:
          '<div class="reviews-slider-arrow reviews-slider-arrow_next"></div>',
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    })
  }

  topSlick()
  ourWorkSlider()
  videoReviewSlider()
  reviewSliderInit()

  videoSlider.click(function (e) {
    let that
    if (e.target.classList.contains('video-reviews-iframe')) {
      that = $(e.target)
    } else if (e.target.classList.contains('play-logo-img')) {
      that = $(e.target)
          .parent()
          .siblings('.video-reviews-iframe')
    } else {
      return false
    }

    const width = that.width()
    const height = that.height()
    const dataSrc = that.attr('data-src')
    const iframe = `<iframe
                    width=${width}
                    height=${height}
                    src="${dataSrc}"
                    frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                  ></iframe>`
    that.closest('.iframe-box').html(iframe)
  })
})