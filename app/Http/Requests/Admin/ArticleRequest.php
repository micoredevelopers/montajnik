<?php

namespace App\Http\Requests\Admin;

use App\Contracts\Requests\RequestParameterModelable;
use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;
use App\Models\Article;
use App\Traits\Requests\Helpers\GetActionModel;

class ArticleRequest extends AbstractRequest implements RequestParameterModelable
{

    protected $toBooleans = ['active'];

    use GetActionModel;

    protected $requestKey = 'article';


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'          => ['required', 'max:255'],
            'url'           => ['required', 'unique:articles,url'],
            'except'        => ['max:' . ValidationMaxLengthHelper::TEXT],
            'description.*' => ['max:' . ValidationMaxLengthHelper::TEXT],
        ];

        if ($this->isActionUpdate() AND $article = $this->getActionModel()) {
            /** @var  $article Article */
            $rules['url'] = ['required', 'unique:articles,url,' . $article->id];
        }

        return $rules;
    }

    protected function mergeRequestValues()
    {
        if (!$this->get('url')) {
            $this->merge(['url' => $this->get('name'),]);
        }
        $this->merge([
            'url'          => \Str::slug($this->get('url')),
            'published_at' => checkDateCarbon($this->get('published_at')),
            'video'        => $this->get('video'),
        ]);
    }
}
