@include('admin.partials.crud.elements.name')

@include('admin.partials.crud.elements.url')

<div class="row">
    <div class="col-md-6">
        @include('admin.partials.crud.elements.active')
    </div>
    <div class="col-md-6">
        @include('admin.partials.crud.date.date-pub')
    </div>
</div>

@include('admin.partials.crud.elements.image-upload-group')


@include('admin.partials.crud.textarea.description')

@include('admin.partials.crud.textarea.except')