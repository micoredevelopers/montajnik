<?php

namespace App\Http\Requests\Admin\Category;

use App\Contracts\Requests\RequestParameterModelable;
use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;
use App\Models\Category\Category;
use App\Traits\Requests\Helpers\GetActionModel;

class ProductRequest extends AbstractRequest implements RequestParameterModelable
{

    protected $toBooleans = ['active'];

    use GetActionModel;

    protected $requestKey = 'product';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'          => ['required', 'max:255'],
            'url'           => ['required', 'unique:products,url'],
            'category_id'   => ['required', 'exists:categories,id'],
            'except'        => ['max:' . ValidationMaxLengthHelper::TEXT],
            'description.*' => ['max:' . ValidationMaxLengthHelper::TEXT],
        ];

        if ($this->isActionUpdate() AND $category = $this->getActionModel()) {
            /** @var  $category Category */
            $rules['url'] = ['required', 'unique:products,url,' . $category->id];
        }

        return $rules;
    }

    protected function mergeRequestValues()
    {
        $this->mergeUrlFromName();
        $this->merge([
        ]);
    }
}
