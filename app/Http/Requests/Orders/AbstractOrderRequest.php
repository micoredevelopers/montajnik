<?php

namespace App\Http\Requests\Orders;

use App\Http\Requests\AbstractRequest;

abstract class AbstractOrderRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    abstract public function getFillableRequestFields($except = []):array ;

}
