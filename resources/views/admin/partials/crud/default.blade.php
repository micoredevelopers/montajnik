<?php /** @var $edit \App\Models\Model */ ?>
@php
    $name = $name ?? $edit->getKeyName();
    $keyDotted = remakeInputKeyDotted($name);
    $attributeKey = getLastFromExploded($keyDotted);
    $title = $title ?? ucfirst(titleFromInputName($attributeKey));
    $value = old($keyDotted, ($value ?? (isset($edit) ? $edit->getAttribute($attributeKey) : '' ) ) );
    $required = $required ?? false;
@endphp

<div class="form-group">
    {!! errorDisplay($keyDotted) !!}
    <label for="{{ $name }}" class=" control-label">{{ $title ?? '' }}</label>
    {!! $beforeInput ?? '' !!}
    <input type="{{ $type ?? 'text' }}" id="{{ $name }}" name="{{ $name }}"
           class="{{ $inputClass ?? 'form-control' }}"
           @if($required) required="required" @endif
           autocomplete="{{ $autocomplete ?? 'off' }}"
           {!! $props ?? '' !!}
           {!! $jsEvents ?? '' !!}
           value="{{ $value }}"
    >
    {!! $afterInput ?? '' !!}
</div>