<?php

namespace App\Listeners\Admin\Article;

use App\Repositories\ArticleRepository;
use App\Repositories\FaqRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FaqRebuildNextPrevListener
{

    private $faqRepository;

    public function __construct(FaqRepository $faqRepository)
    {
        $this->faqRepository = $faqRepository;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $this->faqRepository->rebuildNextPrev();
    }
}
