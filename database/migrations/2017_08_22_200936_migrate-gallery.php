<?php

use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrateGallery extends Migration
{

    use MigrationCreateFieldTypes;
    
    protected $table = 'galleries';
    protected $tableLang = 'galleries_lang';
    protected $foreign = 'gallery_id';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->setTable($table);
            $table->increments('id');
            $table->dateTime('published_at')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->string('image', 255)->nullable();
            $table->string('url', 128)->nullable()->unique();
            $table->smallInteger('sort')->nullable()->default(0);
			$table->text('video')->nullable();

            $table->timestamps();
        });
        
        Schema::create($this->tableLang, function (Blueprint $table) {
            $this->setTable($table);
            $table->integer($this->foreign)->unsigned();
            $table->integer('language_id');
            $table->string('name', 255)->nullable();
            $table->longText('description')->nullable();
            $table->longText('except')->nullable();
            //
            $table->index($this->foreign);
            $table->index('language_id');
            $table->foreign($this->foreign)
                ->references('id')->on($this->table)
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableLang);
        Schema::dropIfExists($this->table);
    }
}
