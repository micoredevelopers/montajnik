<?php

namespace App\Http\Requests\User;


use App\Traits\Requests\User\GetUser;
use App\Traits\Requests\User\IsOnlyChangeEmail;
use App\Traits\Requests\User\IsPasswordWasSend;

class UserCredentialsRequest extends AbstractUserRequest
{
	use GetUser;
	use IsOnlyChangeEmail;
	use IsPasswordWasSend;

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		/*
		 * 1) Is changes only email
		 * 2) Changes password AND (OR) email
		 * */
		$user = $this->getUser();
		$rules = [
			'email' => ['email', 'required', 'unique:users,email,' . $user->getAttribute('id')],
		] + $this->getCurrentPasswordRule();

		if ($this->isOnlyNewPasswordsWasSend()) {
			$rules += $this->getChangePasswordRules();
		}
		return $rules;
	}
}
