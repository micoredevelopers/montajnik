<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use App\Repositories\FaqRepository;
use App\Repositories\TestimonialRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AnswerController extends SiteController
{

	public function __construct()
	{
		parent::__construct();
		$this->addBreadCrumb(getTranslate('faq.faq'), route('answer.index'));

	}

	public function index()
	{
		$this->setTitle(getTranslate('faq.faq'));
		$faq = Faq::getForDisplay();
		$data['content'] = view('public.answer.index',compact('faq'));
		return $this->main($data);
	}

    public function show($url, FaqRepository $faqRepository)
    {
        $faq = $faqRepository->findByUrl($url);
        if (!$faq){
        	return abort(Response::HTTP_NOT_FOUND);
		}
        $this->setTitle($faq->question)->addBreadCrumb($faq->question, route('answer.show', $faq->url));

        $data['content'] = view('public.answer.show')->with(compact('faq'));
        return $this->main($data);
    }
}
