<form action="{{ route('infoblocks.update', $edit->id) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @method('PATCH')

	@include('admin.infoblocks.partials._form')

    @include('admin.partials.submit_update_buttons')
</form>

@include('admin.partials.crud.js.init-description-except')
