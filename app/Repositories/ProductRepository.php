<?php


namespace App\Repositories;


use App\Models\Category\Category;
use App\Models\Category\Product;
use App\Traits\Repositories\RebuildNextAndPrev;
use Illuminate\Support\Collection;

class ProductRepository extends AbstractRepository
{
    use RebuildNextAndPrev;

    private $cacheKey = 'public.categories.tree';

    public function __construct()
    {
        parent::__construct();
    }

    public function model()
    {
        return Product::class;
    }

    protected function initScopes()
    {

    }

    /**
     * @param int $id
     * @return Product|null
     */
    public function findForEdit(int $id): ?Product
    {
        $product = Product::with(['lang', 'category.lang'])->find($id);
        return $product;
    }

    public function findById(int $id): ?Product
    {
        $category = Product::find($id);
        return $category;
    }

    public function findByUrl(string $url)
    {
        return $this->newQuery()->where('url', $url)->first();
    }

    public function getCategoriesForProducts(Product $product = null): Collection
    {
        /** @var  $categoryRepository CategoryRepository */
        $categoryRepository = resolve(CategoryRepository::class);
        $categories = $categoryRepository->getCategoriesWithoutChildrens();

        return $categories;
    }

}


