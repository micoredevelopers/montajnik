<?php /** @var $meta \App\Models\Meta */ ?>

<div class="admin-sidebar">
    <div class="admin-sidebar-container">
        <div class="button-close">
            <button class="btn"><i class="fa fa-times"></i></button>
        </div>
        {{--            --}}
        <div class="card">
            <div class="card-header">SEO</div>
            <div class="card-body">
                @isset($meta)
                    <a href="{{ route('admin.meta.edit', $meta->id) }}" class="btn btn-default" target="_blank">Edit
                        meta</a>
                @else
                    <a class="btn btn-primary text-uppercase"
                       href="{{ route('admin.meta.create') }}?url={{ $metaUrlCreate ?? '' }}" target="_blank"><i
                                class="fa fa-plus"></i> Create meta</a>
                @endisset
                <a href="{{ asset('robots.txt') }}" target="_blank" class="btn {{ $robotsClass ?? '' }}">Robots.txt
                    <i class="fa fa-external-link" aria-hidden="true"></i></a>
                <a href="{{ asset('sitemap.xml') }}" target="_blank" class="btn {{ $sitemapClass ?? '' }}">Sitemap
                    <i class="fa fa-external-link" aria-hidden="true"></i></a>
            </div>
        </div>
        {{--                --}}
        <div class="card mt-3">
            <div class="card-header">Settings</div>
            <div class="card-body">
                <form action="{{ route('settings.index') }}" method="get" target="_blank" class="form-inline">
                    <div class="form-group">
                        <input type="text" name="search" class="form-control">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </form>
            </div>
        </div>
        {{--            --}}
        <div class="card mt-3">
            <div class="card-header">Translate</div>
            <div class="card-body">
                <form action="{{ route('translate.index') }}" method="get" target="_blank" class="form-inline">
                    <div class="form-group">
                        <input type="text" name="search" class="form-control">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </form>
            </div>
        </div>
        @if($productionLink ?? false)
            <a class="btn btn-danger mt-2" href="{{ $productionLink }}" target="_blank">Production <i
                        class="fa fa-external-link"></i></a>
        @endif
        @isset($adminMenu)
            <div class="card mt-2">
                <div class="card-header">
                    <a class="btn btn-primary" data-toggle="collapse" href="#adminMenuCollapse" role="button" aria-expanded="false" aria-controls="collapseExample">
                        Menu
                    </a>
                </div>
                <div class="card-body">
                    <div class="collapse" id="adminMenuCollapse">
                        <div class="sidebar-wrapper position-static">
                            @include('admin.menu', ['menu' => $adminMenu])
                        </div>
                    </div>
                </div>
            </div>
        @endisset
        {{--            --}}
    </div>
    <div class="admin-sidebar-overlay"></div>
</div>


@push('css-footer')
    <style type="text/css">
        .admin-sidebar-overlay,
        .admin-sidebar-container {
            position: absolute;
            left: 0;
            top: 0;
            bottom: 0;
        }

        .admin-sidebar {
            z-index: 20;
            position: fixed;
            left: -448px;
            top: 0;
            bottom: 0;
            width: 450px;
            transition: all ease .2s;
        }

        .admin-sidebar.active {
            left: 0;
        }

        .admin-sidebar-overlay {
            background: linear-gradient(to right, #83a4d4b3, #9bf4f9e0);
            border-right: 2px solid brown;
            z-index: 9;
            right: 0;
        }

        .admin-sidebar-container .button-close {
            position: absolute;
            right: 0;
            text-align: center;
            z-index: 10;
            cursor: pointer;
        }

        .admin-sidebar-container {
            width: 446px;
            margin-left: 2px;
            z-index: 10;
            overflow-y: auto;
        }

        .nav-item .nav-link p {
            display: inline;
        }

        .sidebar-wrapper .nav {
            display: block;
        }

        .sidebar-wrapper .nav-item ul {
            padding-left: 30px;
        }

        .sidebar-wrapper .drop-item {
            position: relative;
        }

        .sidebar-wrapper .collapse-btn {
            display: none;
            position: absolute;
            right: 20px;
            top: 0;
            background-color: #3c4858 !important;
            color: #fff !important;
            border-left: 1px solid #bdb7b7;
            border-radius: 0;
            padding: 12px 14px;
        }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>
@endpush
@push('js')
    <script>
        $(document).ready(function () {
            $('.admin-sidebar-overlay, .admin-sidebar-container .button-close').on('click', function () {
                $(this).parents('.admin-sidebar').toggleClass('active')
            })
        });
    </script>
@endpush
