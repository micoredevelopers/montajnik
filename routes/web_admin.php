<?php

use App\Http\Controllers\Admin\ArticleController;
use App\Http\Controllers\Admin\Category\CategoryController;
use App\Http\Controllers\Admin\Category\PriceController;
use App\Http\Controllers\Admin\Category\ProductController;
use App\Http\Controllers\Admin\User\UserController;

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
	Route::get('/login', 'Auth\MyAuthController@showLogin')->name('admin.login');
	Route::post('/login', 'Auth\MyAuthController@authenticate');
	Route::post('/logout', 'Auth\MyAuthController@logout')->name('admin.logout');
});
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['father', 'is-admin']], function () {
	Route::get('/', 'IndexController@index')->name('admin.index');
//	Route::post('upload', ['uses' => 'AdminController@upload', 'as' => 'upload']);
	/** @see UserController */
	Route::resource('users', 'User\UserController', ['as' => 'admin']);
	Route::post('/users/signsuperadmin', 'User\UserController@signSuperAdmin')->name('signsuperadmin');
	Route::group(['prefix' => 'profile', 'namespace' => 'User'], function () {
		Route::get('/', 'UserController@profile')->name('admin.profile');
		Route::post('/', 'UserController@profileUpdate')->name('admin.profile.update');
	});

	Route::resources([
		'translate' => 'TranslateController',
		'settings'  => 'SettingController',
	]);

	Route::resources([
		'roles'        => 'RoleController',
		'galleries'    => 'GalleryController',
		'sliders'      => 'SliderController',
		'menu'         => 'MenuController',
		'feedback'     => 'FeedbackController',
		'admin-menus'  => 'AdminMenuController',
		'faq'          => 'FaqController',
		'articles'     => 'ArticleController',
		'works'        => 'WorksController',
		'testimonials' => 'TestimonialController',
		'news'         => 'NewsController',
		'pages'        => 'PageController',
		'partners'     => 'PartnerController',
	], ['as' => 'admin', 'except' => ['show']]
	);

	Route::post('/sliders/item/{slider}', 'SliderController@createSliderItem')->name('admin.sliders.store-item');

	Route::group(['namespace' => 'Category'], function () {
		/** @see CategoryController */
		Route::resource('category', 'CategoryController', ['as' => 'admin', 'except' => ['show']]);
		Route::post('/category/nesting', 'CategoryController@nesting')->name('admin.category.nesting');
		Route::get('/category/search', 'CategoryController@search')->name('admin.category.search');
		/** @see ProductController */
		Route::resource('products', 'ProductController', ['as' => 'admin', 'except' => ['show']]);
		/** @see PriceController */
		Route::resource('prices', 'PriceController', ['as' => 'admin', 'except' => ['show']]);
		Route::post('prices/{price}/store-item', 'PriceController@storeItem')->name('admin.prices.store-item');
	});
	Route::group(['prefix' => 'faq'], function () {
		Route::match(['post', 'put'], '/{faq}/item', 'FaqController@storeItem')->name('faq.items.store');
	});
	Route::patch('/admin-menus/save/all', 'AdminMenuController@updateAll')->name('admin-menus.updateAll');
	Route::resource('menu-group', 'MenuGroupController', ['parameters' => ['menu-group' => 'menuGroup']]);

	Route::group(['namespace' => 'Meta'], function () {
		/** @see \App\Http\Controllers\Admin\Meta\MetaController */
		Route::resource('meta', 'MetaController', ['parameters' => ['meta' => 'meta'], 'as' => 'admin', 'except' => ['show']]);
		Route::resource('redirects', 'RedirectController', ['as' => 'admin', 'except' => ['show']]);
		Route::get('redirects/to-lower','RedirectController@toLower')->name('admin.redirects.to-lower');
	});
	Route::group(['prefix' => 'photos'], function () {
		Route::post('/edit', ['uses' => 'PhotosController@edit']);
		Route::post('/delete', ['uses' => 'PhotosController@delete']);
		Route::match(['get', 'post'], '/get-cropper', ['uses' => 'PhotosController@getPhotoCropper']);
	});
	Route::group(['as' => 'settings.', 'prefix' => 'settings',], function () {
		Route::get('{id}/delete_value', [
			'uses' => 'SettingController@delete_value',
			'as'   => 'delete_value',
		]);
	});
	Route::resource('/partner', 'PartnerController');
	// Служебные роуты, такие как elfinder
//	Route::get('glide/{path}', function ($path) {
//		$server = \League\Glide\ServerFactory::create([
//			'source' => app('filesystem')->disk('public')->getDriver(),
//			'cache'  => storage_path('glide'),
//		]);
////
//		return $server->getImageResponse($path, Input::query());
//	})->where('path', '.+');

	Route::post('/menu/nesting', 'MenuController@nesting')->name('menu.nesting');
	Route::group(['prefix' => 'ajax'], function () {
		Route::post('/sort', 'AjaxController@sort')->name('sort');
		Route::post('/delete', 'AjaxController@delete')->name('delete');
	});
	Route::get('/other', 'OtherController@index')->name('other.index');
	Route::prefix('other')->name('admin.other.')->group(function () {
		Route::get('/robots', 'RobotController@index')->name('robots.index');
		Route::put('/robots', 'RobotController@update')->name('robots.update');
		Route::resource('/sitemap', 'SitemapController', ['only' => ['index', 'store']]);
	});

	Route::group(['prefix' => 'cache'], function () {
		/** @see IndexController::clearCache() */
		Route::post('/clear/cache', 'IndexController@clearCache')->name('cache.clear');
		/** @see IndexController::clearView() */
		Route::post('/clear/view', 'IndexController@clearView')->name('cache.view');
	});

	Route::get('logs', 'Staff\\LogViewController@index');

});
