<?php /** @var $categories Collection */ ?>
<?php /** @var $category App\Models\Category\Category */ ?>
@php $categoriesChunked = $categories->chunk(3); @endphp
<footer class="footer">
    <span class="footer-logo">
        <img class="w-100" src="{{asset('img/mp/footer-logo.svg')}}" alt="" width="182" height="30">
    </span>
  <div class="container mb-lg-40">
    <div class="row">
      @foreach($categoriesChunked as $categoriesChunk)
        <div class="col-lg-3 d-flex flex-column align-items-center align-items-lg-start">
          @foreach($categoriesChunk as $category)
          <a href="{{ route('category.show', $category->getUrlAttribute()) }}" class="footer-menu-link">{{ $category->getCategoryName() }}</a>
          @endforeach
        </div>
      @endforeach
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-6 col-lg-3 mb-40 d-lg-flex flex-column">
        <a href="tel:+{{ formatTel(getSetting('contacts.phone-vodafone')) }}" class="mb-10 mb-lg-15 footer-text phone-link"><span>{{ getSetting('contacts.phone-vodafone') }}</span></a>
        <a href="tel:+{{ formatTel(getSetting('contacts.phone-kyivstar')) }}" class="mb-10 mb-lg-15 footer-text phone-link"><span>{{ getSetting('contacts.phone-kyivstar') }}</span></a>
      </div>
      <div class="col-6 col-lg-3 mb-40 text-right text-lg-left d-lg-flex flex-column">
        <a href="tel:+{{ formatTel(getSetting('contacts.phone-lifecell')) }}" class="mb-10 mb-lg-15 footer-text phone-link"><span>{{ getSetting('contacts.phone-lifecell') }}</span></a>
        <a href="tel:+{{ formatTel(getSetting('contacts.phone-intretelecom')) }}" class="mb-10 mb-lg-15 footer-text phone-link"><span>{{ getSetting('contacts.phone-intretelecom') }}</span></a>
      </div>
      <div class="offset-1 col-10 col-lg-3 offset-lg-0 mb-40 text-center text-lg-left">
        <p class="mb-10 mb-lg-15 footer-text">{{ getSetting('global.address') }}</p>
        <p class="mb-10 mb-lg-15 footer-text">{{ getTranslate('global.work-hours') }} {{ getSetting('global.work-hours') }}</p>
      </div>
      <div class="offset-2 col-8 col-lg-3 offset-lg-0">
        <div class="social-links-box justify-content-around">
          <a target="_blank" rel="nofollow" href="{{ getSetting('social.youtube') }}" class="social-links-box__item">
            <img src="{{asset('img/mp/youtube-white.svg')}}" alt="" width="30" height="22"/>
          </a>
          <a target="_blank" rel="nofollow" href="{{ getSetting('social.facebook') }}" class="social-links-box__item">
            <img src="{{ asset('img/mp/facebook-white.svg') }}" alt="" width="12" height="22"/>
          </a>
          <a target="_blank" rel="nofollow" href="{{ getSetting('social.viber') }}" class="social-links-box__item">
            <img src="{{ asset('img/mp/viber-white.svg') }}" alt="" width="19" height="20"/>
          </a>
          {{--                    <a target="_blank" rel="nofollow" href="{{ getSetting('social.gplus') }}" class="social-links-box__item">--}}
          {{--                        <img src="{{ asset('img/mp/google-white.svg') }}" alt=""/>--}}
          {{--                    </a>--}}
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <a class="footer-bottom-link d-block text-center" href="#">montajnik.od.ua © {{ (new DateTime())->format('Y') }}</a>
  </div>
</footer>

