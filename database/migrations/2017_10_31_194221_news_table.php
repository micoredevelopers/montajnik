<?php

use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsTable extends Migration
{

    use MigrationCreateFieldTypes;

    protected $disabled = false;
    protected $table = 'news';
    protected $foreign = 'news_id';
    protected $tableLang = 'news_lang';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ($this->disabled) {
            return null;
        }
        Schema::create($this->table, function (Blueprint $table) {
            $this->setTable($table);
            $table->increments('id');
            $table->char('active', 4)->default(0);
            $table->dateTime('published_at')->nullable();
            $table->string('url', 128)->unique();
            $table->string('image', 255)->nullable();
            $table->tinyInteger('sort')->default(0);
            $this->createNextPrevFields();

            $table->timestamps();
        });
        //
        Schema::create($this->tableLang, function (Blueprint $table) {
            $this->setTable($table);
            $table->integer($this->foreign)->unsigned();
            $table->string('name', 255)->nullable();
            $table->integer('language_id');
            $table->mediumText('description')->nullable();
            $table->text('except')->nullable();
            $table->text('video')->nullable();
            //
            $table->index($this->foreign);
            $table->index('language_id');
            $table->foreign($this->foreign)
                ->references('id')->on($this->table)
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ($this->disabled) {
            return null;
        }
        Schema::dropIfExists($this->tableLang);
        Schema::dropIfExists($this->table);
    }
}
