<?php

namespace App\Modules\Sitemap\LinksManager;

use App\Contracts\Models\HasSitemapLinks;
use App\Models\Work;
use Illuminate\Support\Collection;

class WorkSiteMapManager implements HasSitemapLinks
{
	public function getSiteMapLinks(): Collection
	{
		$links = collect([]);
		$news = Work::active()->get();
		$news->map(static function (Work $work) use ($links) {
			$item = [
				'loc'     => route('works.show', $work->url),
				'lastmod' => checkDateCarbon($work->updated_at),
			];
			$links->push($item);
		});
		return $links;
	}

}