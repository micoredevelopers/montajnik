<?php

use Illuminate\Database\Seeder;

class WorksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('works')->delete();
        
        \DB::table('works')->insert(array (
            0 => 
            array (
                'id' => 1,
                'active' => 1,
                'image' => NULL,
                'url' => 'asdasd',
                'sort' => 0,
                'video' => '["https:\\/\\/www.youtube.com\\/watch?v=sMlTnIoXNJA","https:\\/\\/www.youtube.com\\/watch?v=-cyfzBy4pNM"]',
                'category_id' => 223,
                'created_at' => '2019-10-03 10:58:09',
                'updated_at' => '2019-10-03 15:22:47',
            ),
        ));
        
        
    }
}