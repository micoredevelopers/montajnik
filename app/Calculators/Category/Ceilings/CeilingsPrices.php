<?php


	namespace App\Calculators\Category\Ceilings;


	class CeilingsPrices
	{
		private $baguette = 30;

		private $angles = 30;

		private $light = 80;

		private $pipe = 80;

		//Бесщелевые
		private $gapless = 490;

		private $starSky = 1500;

		//Криволинейность
		private $curvature = 80;

		public function getPriceType($type): int
		{
			$prices = [
				CeilingsTypesContract::STANDART => 160,
				CeilingsTypesContract::PREMIUM  => 180,
				CeilingsTypesContract::VIP      => 220,
			];
			return $prices[ $type ] ?? 0;
		}

		public function getPriceProfile($type): int
		{
			$prices = [
				CeilingsProfileTypesContract::PLASTIC   => 50,
				CeilingsProfileTypesContract::ALUMINIUM => 80,
			];
			return $prices[ $type ] ?? 0;
		}

		public function getPriceBaguette(): int
		{
			return $this->baguette;
		}

		public function getPriceAngles(): int
		{
			return $this->angles;
		}

		public function getPriceLight(): int
		{
			return $this->light;
		}

		public function getPricePipe(): int
		{
			return $this->pipe;
		}

		public function getPriceCurvature(): int
		{
			return $this->curvature;
		}

		/**
		 * @return int
		 */
		public function getPriceGapless(): int
		{
			return $this->gapless;
		}

		/**
		 * @return int
		 */
		public function getPriceStarSky(): int
		{
			return $this->starSky;
		}

	}