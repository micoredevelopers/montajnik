<?php


namespace App\Helpers\View;


use Illuminate\Support\Collection;

class ColumnWidthChunker
{
	/**
	 * @var ColumnWidthCounter
	 */
	private $widthCounter;

	public function __construct(ColumnWidthCounter $widthCounter)
	{
		$this->widthCounter = $widthCounter;
	}

	/**
	 * @param Collection | array $array
	 * @return array
	 */
	public function chunk($array): array
	{
		if ($array instanceof Collection) {
			$array = $array->all();
		}
		$columns = $this->widthCounter->getColumnsSize(count($array));
		if (!$columns) {
			return [[]];
		}
		if (is_int($columns)) {
			return array_chunk($array, $columns);
		}
		$chunks = [];
		foreach ($columns as $colNum) {
			$chunks[] = array_splice($array, 0, $colNum, []);
		}
		return $chunks;
	}
}