<?php


namespace App\Helpers;


use App\Models\Redirect;
use Illuminate\Support\Facades\DB;

class UrlReplacer
{

    public function getTablesWithUrls()
    {
        $raw = DB::raw("SELECT DISTINCT TABLE_NAME 
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE COLUMN_NAME IN ('url') AND TABLE_NAME != 'admin_menus' AND TABLE_NAME != 'meta'
        AND TABLE_SCHEMA='" . env('DB_DATABASE') . "';");
        $tables = collect(DB::select($raw));

        return $tables;
    }

    public function getUrlWithUpperKeys()
    {
        $urls = [];
        foreach ($this->getTablesWithUrls() as $tableName) {

            foreach (DB::table($tableName->TABLE_NAME)->get(['id', 'url'])->toArray() as $url) {

                if ($url->url !== strtolower($url->url)) {

                    $data = [
                        'id' => $url->id,
                        'table' => $tableName->TABLE_NAME,
                        'url' => '',
                        'lower_url' => '',
                        'base_url' => strtolower($url->url)
                    ];
                    $route = new ShowRedirectsUrlByTable();

                    $from = $route->getUrlByTable($tableName->TABLE_NAME, $url->url);
                    $to = $route->getUrlByTable($tableName->TABLE_NAME, strtolower($url->url));

                    $data ['url'] = getUrlWithoutHost(getNonLocaledUrl($from));
                    $data ['lower_url'] = getUrlWithoutHost(getNonLocaledUrl($to));




                    array_push($urls, $data);
                }
            }
        }
        return $urls;
    }


    private function setNewUrl(string $table, int $id, string $newUrl)
    {
        DB::table($table)->where('id', $id)->update([
            'url' => $newUrl
        ]);
    }

    public function setRedirectUpperToLower()
    {
        $urls = $this->getUrlWithUpperKeys();

        if (count($urls)) {

            foreach ($urls as $url) {
                $redirectData = [
                    'from' => $url['url'],
                    'to' => $url['lower_url'],
                    'code' => '301',
                    'active' => true
                ];
                $redirect = (new Redirect())->fillExisting($redirectData);
                $redirect->save();
                $this->setNewUrl($url['table'], $url['id'], $url['base_url']);
            }
        }
    }
}
