<?php /** @var $edit \App\Models\Slider\Slider */ ?>
@include('admin.partials.crud.default', ['name' => inputNamesManager($edit)->getNameInputByKey('comment'), 'title' => 'Комментарий'])

@include('admin.partials.crud.elements.active', ['name' => inputNamesManager($edit)->getNameInputByKey('active')])

@superadmin
    @include('admin.partials.crud.default', ['name' => inputNamesManager($edit)->getNameInputByKey('key')])
@endsuperadmin
