<?php


namespace App\Modules\Transfer;


use App\Repositories\MetaRepository;

class MetaTransfer
{

	private $metaRepository;

	public function __construct(MetaRepository $metaRepository)
	{
		$this->metaRepository = $metaRepository;
	}

	/**
	 * @param string $url
	 * @return \View
	 */
	public function getForm(string $url): \View
	{
		$meta = $this->metaRepository->findByUrl($url);
		d($meta);
		return view('admin.meta.transfer.data');
	}

	public function saveData(array $data)
	{

	}
}