<?php

use Illuminate\Database\Seeder;

class PriceItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('price_items')->delete();
        
        \DB::table('price_items')->insert(array (
            0 => 
            array (
                'id' => 1,
                'price_id' => 1,
                'name' => 'oleg',
                'price' => '15000',
                'unit' => 'еа',
            ),
            1 => 
            array (
                'id' => 2,
                'price_id' => 1,
                'name' => 'йцу',
                'price' => '342',
                'unit' => 'ыва',
            ),
            2 => 
            array (
                'id' => 3,
                'price_id' => 2,
                'name' => 'lloo',
                'price' => 'sadsa',
                'unit' => 'еа',
            ),
            3 => 
            array (
                'id' => 4,
                'price_id' => 2,
                'name' => 'sada',
                'price' => '123131',
                'unit' => '123',
            ),
        ));
        
        
    }
}