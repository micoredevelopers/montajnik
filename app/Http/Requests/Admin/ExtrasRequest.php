<?php

namespace App\Http\Requests\Admin;

use App\Models\Extra;
use Illuminate\Foundation\Http\FormRequest;

class ExtrasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		$mergeFields = [
			'active' => (int)$this->get('active'),
			'daily' => (int)$this->get('daily'),
		];

		$this->merge($mergeFields);
        return [
            'type' => ['in:' . implodeComma(array_keys(Extra::getTypes()))]
        ];
    }
}
