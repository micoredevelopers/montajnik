<?php /** @var $edit \App\Models\Category\Category */ ?>
<form action="{{ route($routeKey . '.update', $edit->id) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @method('patch')

    <div class="row">
        <div class="col-md-6">
            @include('admin.partials.preview-button', ['link' => route('category.show', $edit->url),])
        </div>
        <div class="col-md-6">
            @include('admin.partials.submit_update_buttons')
        </div>
    </div>

    <ul class="nav nav-tabs mb-5" role="tablist">
        <li role="presentation">
            <a class="active" href="#dataDefault" role="tab"
               data-toggle="tab">Главная</a>
        </li>
        <li role="presentation">
            <a href="#pages" role="tab" data-toggle="tab">Страница</a>
        </li>
        <li role="presentation">
            <a href="#info" role="tab" data-toggle="tab">Информация</a>
        </li>
            <li role="presentation">
                <a href="#slider" role="tab" data-toggle="tab">Слайдер</a>
            </li>
    </ul>
    <section class="tab-content">

        @include('admin.category.elements.main')

        @include('admin.category.elements.pages')

        @include('admin.category.elements.info')

            @include('admin.category.elements.slider')
    </section>

    @include('admin.partials.submit_update_buttons')
</form>

@section('javascript')
    <script defer>
        $(document).ready(function (e) {
            @foreach (range(1, 3) as $user)
            {!! showEditor('description['.$loop->index.']') !!}
            @endforeach
        });
    </script>
@stop

