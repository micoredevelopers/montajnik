<?php

use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryTable extends Migration
{

    use MigrationCreateFieldTypes;

	protected $table = 'categories';
	protected $foreign = 'category_id';
	protected $tableLang = 'category_lang';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create($this->table, function (Blueprint $table) {
            $this->setTable($table);
			$table->increments('id');
			$table->integer('parent_id')->nullable();
			$this->createActive();
			$this->createUrl();

			$table->string('icon', 255)->nullable()->comment('Шрифтовые изображения');
			$this->createImage();
			$this->createSort();
			$table->boolean('childrens_big_size')->default(false);
			$table->text('options')->nullable();
			$table->timestamps();

		});

		Schema::create($this->tableLang, function (Blueprint $table) {
            $this->setTable($table);
			$table->integer($this->foreign)->unsigned();

			$this->createName();
            $table->string('sub_name', 255)->nullable();
			$table->smallInteger('language_id');

            $table->text('except')->nullable();
			//
			$table->index($this->foreign);
			$table->index('language_id');

			$table->foreign($this->foreign)
				->references('id')->on($this->table)
				->onUpdate('cascade')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
