<?php

namespace App\Modules\Sitemap\LinksManager;

use App\Contracts\Models\HasSitemapLinks;
use App\Models\Category\Category;
use Illuminate\Support\Collection;

class CategorySiteMapManager implements HasSitemapLinks {

    public function getSiteMapLinks(): Collection {
        $links = collect([]);
        $news = Category::active()->get();
        $news->map(static function (Category $category) use ($links) {
            $item = [
                'loc'     => route('category.show', $category->url),
                'lastmod' => checkDateCarbon($category->updated_at),
            ];
            $links->push($item);
        });
        return $links;
    }
}