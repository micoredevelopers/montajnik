<?php

	use Carbon\Carbon;

	if (!function_exists('checkDateCarbon')) {
		function checkDateCarbon($date, $format = 'Y-m-d H:i:s'): Carbon
		{
			try {
				if (is_string($date)) {
					$date = str_replace(['.', ','], '-', $date);
				}
				$date = Carbon::parse($date);
			} catch (\Exception $e) {
				$date = Carbon::parse(now()->format($format));
			}

			return $date;
		}
	}


	if (!function_exists('dateFrom')) {
		function dateFrom($date): Carbon
		{
			$date = checkDateCarbon($date);
			return Carbon::parse($date->format('Y-m-d H:i:s'));
		}
	}
	if (!function_exists('dateTo')) {
		function dateTo($date): Carbon
		{
			$date = checkDateCarbon($date);
			return Carbon::parse($date->format('Y-m-d 23:59:59'));
		}

	}


	/**
	 * @param Carbon $dateFrom
	 * @param Carbon $dateTo
	 * @param string $format
	 * @return array
	 */
	if (!function_exists('generateDaysByDateRange')) {
		function generateDaysByDateRange(Carbon $dateFrom, Carbon $dateTo, $format = 'Y-m-d'): array
		{
			$days = [$dateFrom->format($format)];
			$dateFromCopy = clone $dateFrom;
			while ($dateFromCopy <= $dateTo) {
				$dateFromCopy = $dateFromCopy->addDay();
				$pickupString = $dateFromCopy;
				$days[] = $pickupString->format($format);
			}
			return $days;
		}
	}


