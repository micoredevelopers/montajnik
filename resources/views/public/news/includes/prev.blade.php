<?php /** @var $new \App\Models\News */ ?>

@php
    $link = route('news.show', $new->getUrlAttribute());
@endphp

<a href="{{ $link }}" class="other-news-title">{{ getTranslate('news.prev') }}</a>
<div class="news__item mb-5">
    <a href="{{ $link }}" class="news-img-box d-block">
        <img class="lazy" data-src="{{ checkImage($new->image) }}" src="{{ checkImage('') }}"
             alt="{{ translateFormat('news.image-alt', ['%name%' => $new->getNameAttribute()]) }}"/>
    </a>
    <div class="news-desc-box">
        <span class="news-date-box">{{ $new->published_at->formatLocalized('%d %b %Y') }}</span>
        <h4 class="news-title">{{ $new->getNameAttribute() }} </h4>
        <div class="mb-lg-0 text-white">{!! $new->getExcept() !!}</div>
        <a href="{{ $link }}" class="btn-custom maxWdt100 d-lg-none">{{ getTranslate('news.new-detail') }}</a>
    </div>
</div>
