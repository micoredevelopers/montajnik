<?php


namespace App\Traits\Controllers;

trait Breadcrumbs
{
    public function addBreadCrumb($breadName, $breadUrl = ''): self
	{
        \Breadcrumbs::addBreadCrumb($breadName, $breadUrl);

        return $this;
    }

    public function dropLastBreadCrumb(): void
	{
        \Breadcrumbs::dropLastBreadCrumb();
    }

    public function dropAllBreadCrumbs(): void
	{
        \Breadcrumbs::dropAllBreadCrumbs();
    }

    /**
     * @param bool $name
     * @return mixed
     */
    public function getBreadCrumbs($name = false)
    {
        return \Breadcrumbs::getBreadCrumbs($name);
    }
}