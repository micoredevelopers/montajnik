<?php

namespace App\Modules\Sitemap\LinksManager;

use App\Contracts\Models\HasSitemapLinks;
use App\Models\Category\Product;
use Illuminate\Support\Collection;

class ProductSiteMapManager implements HasSitemapLinks
{
	public function getSiteMapLinks(): Collection
	{
		$links = collect([]);
		$news = Product::active()->get();
		$news->map(static function (Product $product) use ($links) {
			$item = [
				'loc'     => route('product.show', $product->url),
				'lastmod' => checkDateCarbon($product->updated_at),
			];
			$links->push($item);
		});
		return $links;
	}
}