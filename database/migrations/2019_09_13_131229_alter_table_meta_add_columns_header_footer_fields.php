<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMetaAddColumnsHeaderFooterFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meta', function (Blueprint $table) {
			$table->text('header')->nullable()->comment('html/js etc code for header');
			$table->text('footer')->nullable()->comment('html/js etc code for footer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta', function (Blueprint $table) {
			$table->dropColumn('header');
			$table->dropColumn('footer');
        });
    }
}
