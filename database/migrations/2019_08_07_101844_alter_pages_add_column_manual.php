<?php

use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPagesAddColumnManual extends Migration
{
    use MigrationCreateFieldTypes;
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pages', function (Blueprint $table) {
            $this->setTable($table);
			$table->boolean('manual')->default(false);
			$table->unsignedInteger('parent_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pages', function (Blueprint $table) {
			$table->dropColumn('manual');
			$table->dropColumn('parent_id');
		});
	}
}
