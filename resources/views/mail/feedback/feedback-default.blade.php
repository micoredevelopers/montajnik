@extends('mail.feedback.layout')

@section('title')
	Обратная связь
@stop

@section('content')

    <?php /** @var $feedback \App\Models\Feedback */ ?>
	<table class="table table-striped">
		<tbody>
		@include('mail.feedback.partials.fields')
		</tbody>
	</table>
@stop
