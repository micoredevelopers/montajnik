<?php

namespace App\Http\Requests\Admin;

use App\Contracts\Requests\RequestParameterModelable;
use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;
use App\Models\Category\Category;
use App\Traits\Requests\Helpers\GetActionModel;

class TestimonialRequest extends AbstractRequest
{

    public function rules()
    {
        $rules = [
            'name'         => ['required', 'max:255'],
            'type_id'      => ['required', 'exists:testimonial_types,id'],
            'category_id'  => ['sometimes', 'exists:categories,id'],
        ];

        return $rules;
    }

    protected function mergeRequestValues()
    {
        $this->merge([
            'active'       => (int)$this->get('active'),
            'date' => checkDateCarbon($this->get('date')),
        ]);
    }
}
