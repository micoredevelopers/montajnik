<?php


namespace App\Helpers\View;


class ColumnWidthCounter
{

	private function isCanContain(int $count): int
	{
		if (is_int($count / 3)) {
			return 3;
		}
		if (is_int($count / 4)) {
			return 4;
		}
		return 0;
	}

	public function getColumnsSize(int $count)
	{
		if ($this->isCanContain($count)) {
			return $this->isCanContain($count);
		}

		$matches = [
			1  => [1],
			2  => [2],
			5  => [5],
			7  => [4, 3,],
			10 => [5, 5],
			11 => [4, 3, 4,],
		];
		if ($count > 12) {
			$recursive = $this->getColumnsSize($count - 8);
			$recursive = is_array($recursive) ? $recursive : (array)$recursive;
			return array_merge([4, 4], $recursive);
		}
		return $matches[$count] ?? 3;
	}
}