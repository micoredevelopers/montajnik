<?php /** @var $category \App\Models\Category\Category */ ?>
<?php /** @var $chunkedCategory \App\Models\Category\Category */ ?>
<?php /** @var $chunkedCategories \Illuminate\Support\Collection */ ?>
<?php /** @var $chunkedCategoriesWithoutChild \App\Helpers\Order\Containers\Category\ChunkCategoryContainer */ ?>


@if (isset($chunkedCategoriesWithoutChild) && ($chunkedCategoriesWithoutChild)->getChunks()->isNotEmpty())
    <section class="category-advantages">
        <div class="container">
            <div class="text-center">
                <span class="title text-center mb-20 mb-lg-5">
                    <span class="decor-bg">{{ $chunkedCategoriesWithoutChild->getCategory()->name }}</span>
                </span>
            </div>
            @foreach(($chunkedCategoriesWithoutChild)->getChunks() as $chunk)
                @php
                    $cssCol = $chunkedCategoriesWithoutChild->getColByChunk($chunk);
                @endphp
                <div class="row">
                    @foreach($chunk as $chunkedCategory)
                        @include('public.category.partials.product-or-category-item',
                        [
                            'model' => $chunkedCategory,
                            'url' => route('category.show', $chunkedCategory->url),
                        ])
                    @endforeach
                </div>
            @endforeach
        </div>
    </section>
@endif