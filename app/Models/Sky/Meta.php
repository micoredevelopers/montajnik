<?php

namespace App\Models\Sky;

use Illuminate\Support\Collection;

/**
 * App\Models\Sky\Meta
 *
 * @property int $id
 * @property string|null $url
 * @property string $active
 * @property string $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Meta newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Meta newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\AbstractSkyModel onlyMain()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Meta query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Meta whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Meta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Meta whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Meta whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Meta withLang()
 * @mixin \Eloquent
 */
class Meta extends AbstractSkyModel
{
    public $table = 'meta';
    protected $unsanitizeFields = [
        'body',
    ];

    protected $replaceAttributesKeys = [
        'excerpt' => 'text_top',
        'body'    => 'text_bottom',
    ];

    public $timestamps = false;

    public function remake()
    {
        $comments = self::WithLang()->get();
        $this->unsanitizeList($comments);
    }

    public function getList(): Collection
    {
        $meta = self::WithLang()->get();

        $this->unsanitizeList($meta);
        $this->remakeColumnsName($meta, false);
        return $meta;
    }

    public function scopeWithLang($q)
    {
        return $q->leftJoin('ru_meta', 'meta.id', 'ru_meta.meta_id');
    }

}
