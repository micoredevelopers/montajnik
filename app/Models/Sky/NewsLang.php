<?php

namespace App\Models\Sky;

use Illuminate\Support\Collection;

/**
 * App\Models\Sky\NewsLang
 *
 * @property int|null $news_id
 * @property string|null $name
 * @property string $body_m
 * @property string|null $body
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\NewsLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\NewsLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\AbstractSkyModel onlyMain()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\NewsLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\NewsLang whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\NewsLang whereBodyM($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\NewsLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\NewsLang whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class NewsLang extends AbstractSkyModel
{

    protected $primaryKey = 'news_id';

    protected $table = 'ru_news';

    protected $unsanitizeFields = ['body', 'name','body_m'];

    protected $replaceAttributesKeys = [
        'body'       => 'description',
        'body_m'     => 'except',
        'photo'     => 'image',
    ];

    public $timestamps = false;

    public function remake()
    {
        $comments = self::all();
        $this->unsanitizeList($comments);
    }


}
