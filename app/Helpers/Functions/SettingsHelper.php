<?php

if (!function_exists('getSetting')) {
	function getSetting($key, $default = null)
	{
		return \App\Models\Setting::getSetting($key) ?? $default;
	}
}
if (!function_exists('setting')) {
	function setting($key, $asValue = false)
	{
		return \App\Models\Setting::getSetting($key, $asValue);
	}
}
if (!function_exists('settingFile')) {
	function settingFile($key = null)
	{
		$path = '';
		$value = Arr::get(setting($key), 'value');
		if ($value && isJson($value)) {
			$first = Arr::get(\json_decode($value, true), 0);
			$path = Arr::get($first, 'download_link');
			if (storageFileExists($path)) {
				$path = getStorageFilePath($path);
			}
		}
		return $path;
	}
}
if (!function_exists('settingFiles')) {
	function settingFiles($key): \Illuminate\Support\Collection
	{
		$files = collect([]);
		$value = Arr::get(setting($key), 'value');
		if ($value && isJson($value)) {
			$files = collect(\json_decode($value, true));
			$files = $files->map(static function ($item) {
				$item['original'] = $item;
				$link = storageFileExists(Arr::get($item, 'download_link'))
					? getStorageFilePath(Arr::get($item, 'download_link'))
					: '';
				Arr::set($item, 'download_link', $link);
				return $item;
			});
		}
		return $files;
	}
}