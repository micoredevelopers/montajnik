@can('edit_'.$entity)
    <a href="{{ route($route.'.edit', [Str::singular($route) => $id])  }}" class="dropdown-item">@lang('form.edit')</a>
@endcan

@if(Auth::user()->id != $id)
    @can('delete_'.$entity)
        {!! Form::open( ['method' => 'delete', 'url' => route($route.'.destroy', ['user' => $id]), 'class' => 'formDeleteConfirm']) !!}
        <button type="submit" class="dropdown-item">@lang('form.delete')</button>
        {!! Form::close() !!}
    @endcan
@endif
