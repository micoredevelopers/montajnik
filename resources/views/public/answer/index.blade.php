<main id="questions" class="d-flex flex-column">
    <section class="questions">
        <div class="container">
            <div class="d-lg-flex flex-row-reverse justify-content-between align-items-center">
                @include('public.partials.breadcrumbs')

                <h1 class="title"><span class="decor-bg">{!! showMeta('Вопросы и ответы') !!}</span></h1>
            </div>
            <p class="questions-small-title">{{ getTranslate('faq.title') }}</p>
            <div class="row">
                @foreach($faq as $item)
                    <div class="col-md-6">
                        <a class="questions__item" href="{{ route('answer.show', $item->url) }}">
                            <span class="questions_num">{{$loop->iteration}}</span>
                            <p class="text_16-20 mb-0">
                                {{$item->question}}
                            </p>
                            <span class="arrow-breeze">
              				<img src="{{asset('img/questions/arrow-breeze.svg')}}" alt="arrow icon"/>
            			</span>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</main>
