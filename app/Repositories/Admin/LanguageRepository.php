<?php

namespace App\Repositories\Admin;

use App\Models\Language;
use Illuminate\Support\Collection;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;


/**
 * Class LanguageRepository.
 */
class LanguageRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Language::class;
    }

    public function getForCreateEntity(): Collection
    {
        $languages = Language::active()->get();
        return $languages;
    }
}
