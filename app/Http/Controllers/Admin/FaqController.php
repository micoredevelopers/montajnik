<?php

namespace App\Http\Controllers\Admin;

use App\Events\Admin\FaqChangedEvent;
use App\Helpers\ResponseHelper;
use App\Http\Requests\Admin\FaqRequest;
use App\Models\Faq;
use App\Models\FaqItems;
use App\Repositories\FaqRepository;
use Illuminate\Http\Request;

class FaqController extends AdminController
{
    protected $routeKey = 'admin.faq';

    protected $permissionKey = 'faq';

	protected $key = 'faq';

	private $name = 'FAQ';
	/**
	 * @var FaqRepository
	 */
	private $repository;

	public function __construct(FaqRepository $repository)
	{
		parent::__construct();
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
		$this->repository = $repository;
	}

	public function index(Faq $faq)
	{
		$this->setTitle($this->name);
		$vars['list'] = $this->repository->getListAdmin();
		$vars['table'] = $faq->getTable();
		$data['content'] = view('admin.faq.index', $vars);

		return $this->main($data);
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		$data['content'] = view('admin.faq.create');

		return $this->main($data);
	}

    /**
     * @param FaqRequest $request
     * @param Faq $faq
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
	public function store(FaqRequest $request, Faq $faq)
	{
		$input = $request->except('_token');
        $faq->fillExisting($input);
		if ($faq->save()) {
			$this->setSuccessStore();
            $this->fireEvents();
		}

		if ($request->has('createOpen')) {
			return redirect($this->resourceRoute('edit', $faq->getPrimaryValue()))->with($this->getResponseMessage());
		}

		return redirect($this->resourceRoute( 'index'))->with($this->getResponseMessage());
	}

    /**
     * @param Faq $faq
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function edit(Faq $faq)
	{
		$vars['edit'] = $faq;
		$title =  $this->titleEdit($faq);
		$this->addBreadCrumb($title)->setTitle($title);
		$data['content'] = view('admin.faq.edit', $vars);

		return $this->main($data);
	}

	public function update(FaqRequest $request, Faq $faq)
	{
		$input = $request->except('_token');

		$faq->fillExisting($input);
		if ($faq->save()) {
			if ($request->has('items')) {
				$items = $request->get('items');
				$ids = array_column($items, 'id');
				if ($ids) {
					$faqItems = FaqItems::find($ids);
					/** @var $faqItem \App\Models\FaqItems */
					foreach ($faqItems as $faqItem) {
						$itemArr = \Arr::get($items, $faqItem->getPrimaryValue());
						$faqItem->fillExisting($itemArr)->save();
					}
				}
			}
            $this->fireEvents();
			$this->setSuccessUpdate();
		}
		if ($request->has('saveClose')) {
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		return redirect()->back()->with($this->getResponseMessage());
	}


	/**
	 * @param Faq $faq
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws \Exception
	 */
	public function destroy(Faq $faq)
	{
        if ($faq->delete()) {
            $this->setSuccessDestroy();
            $this->fireEvents();
        }

        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}


	private function fireEvents(){
	    event(new FaqChangedEvent);
    }
}
