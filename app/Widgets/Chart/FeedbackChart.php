<?php


namespace App\Widgets\Chart;

use App\Models\Feedback;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;

class FeedbackChart extends BaseChart
{

	public function handle(): LaravelChart
	{
		$config = $this->mergeOptions([
				'chart_title'     => 'Feedback requests by last month',
				'report_type'     => 'group_by_date',
				'model'           => Feedback::class,
				'group_by_field'  => 'created_at',
				'group_by_period' => 'day',
				'chart_type'      => 'bar',
				'filter_field'    => 'created_at',
				'filter_days'     => 30,
			], \Arr::wrap($this->options));

		/** @var  $chart */
		$chart = new LaravelChart($config);
		return $chart;
	}

}