<?php

	namespace App\Repositories;

	use App\Models\Faq;
	use App\Scopes\SortOrderScope;
	use App\Traits\Repositories\RebuildNextAndPrev;
	use Illuminate\Support\Collection;

	class FaqRepository extends AbstractRepository
	{
		use RebuildNextAndPrev;

		/**
		 * @return string
		 *  Return the model
		 */
		public function model()
		{
			return Faq::class;
		}

		private function initScopesPublic()
		{
			Faq::initScopesPublic();
		}

		public function getListAdmin()
		{
			return $this->model->newQuery()->get();
		}

		public function findForEditById($id)
		{
			$this->initScopesPublic();
			return $this->newQuery()->with('lang')->findOrFail($id);
		}

		public function findByUrl(string $url)
		{
			$this->initScopesPublic();
			return $this->newQuery()->with(['prevFaq', 'nextFaq'])->whereUrl($url)->firstOrFail();
		}

		public function getAllForNextPrev(): Collection
		{
			$this->initScopesPublic();
			return $this->model->newQuery()->get();
		}

	}
