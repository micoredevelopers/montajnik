<?php /** @var $allImages \Illuminate\Support\Collection */ ?>
<main id="review">
  <section class="review">
    <div class="container">
      <div class="d-lg-flex flex-row-reverse justify-content-between align-items-center mb-25 mb-lg-35">
        @include('public.partials.breadcrumbs')
        <h1 class="title ">
          <span class="decor-bg">{!! showMeta(getTranslate('testimonials.testimonials')) !!}</span>
        </h1>
      </div>

      <div class="row mb-15">
        @foreach($testimonials as $testimonial)
          <div class="col-lg-6">
            <div class="review__item">
              <div class="d-flex justify-content-between align-items-center mb-25">
                <img src="{{asset('img/reviewIcon.svg')}}" alt="" width="36" height="32"/>
                <div class="d-flex flex-column align-items-end">
                  <span class="review-name">{{$testimonial->name}}</span>
                  @if ($category = $testimonial->category)
                    <a href="{{ route('category.show', $category->url) }}"
                       class="review-name-desc">{{ $category->getCategoryName()}}</a>
                  @else
                    <span class="review-name-desc">{{ getTranslate('testimonials.no-category') }}</span>
                  @endif
                </div>
              </div>
              <div class="mb-30 review-text-box">
                <p class="review-text mb-0">{{ $testimonial->message }}</p>
              </div>
                <?php /** @var $testimonial \App\Models\Testimonial */ ?>
              @php
                $allImages = $testimonial->images;

                $date = \Carbon\Carbon::parse($testimonial->getPublishedAt())->formatLocalized('%d %B %Y');
                if($allImages->isNotEmpty()){
                    $firstImagePath = checkImageWebp($allImages->first()->image);
                }
              @endphp
              <div class="d-flex {!! $allImages->isNotEmpty() ? 'justify-content-between' : 'justify-content-end' !!} align-items-center">
                @if($allImages->isNotEmpty())
                  <a href="{{imgPathOriginal($firstImagePath)}}" class="review-link" data-fancybox="review-link-1">
               				 		<span class="review-link-img">
                  						<img class="lazy" data-src="{{ $firstImagePath }}"
                                   src="{{ checkImage('') }}"
                                   alt="{{ translateFormat('testimonials.image-alt', ['%name%' => $testimonial->name]) }}"/>
                					</span>
                    <span class="position-relative">Открыть фото</span>
                  </a>
                  <div class="d-none">
                    @foreach($allImages as $image)
                      @php
                        if ($loop->first){
                            continue;
                        }
                      @endphp
                      <a href="{{checkImage(imgPathOriginal($image->image))}}" class="review-link"
                         data-fancybox="review-link-1"></a>
                    @endforeach
                  </div>
                @endif
                <span class="review-date">{{$date}}</span>
              </div>
            </div>
          </div>
        @endforeach
      </div>

      <div class="row">
        <div class="col-12 d-flex justify-content-center">
          {{ $testimonials->links() }}
        </div>
        <div class="col-12 d-flex justify-content-center">
          <button style="width:240px" class="btn-custom btn-custom_pagin mr-lg-0 ml-0"
                  data-toggle="modal" data-target="#feedBackModal">
            Оставить отзыв
          </button>
        </div>
      </div>
    </div>
  </section>
</main>