<?php

namespace App\Http\Requests\Admin;

use App\Contracts\Requests\RequestParameterModelable;
use App\Http\Requests\AbstractRequest;
use App\Models\Gallery;
use App\Traits\Requests\Helpers\GetActionModel;

class GalleryRequest extends AbstractRequest implements RequestParameterModelable
{
    use GetActionModel;

    protected $toBooleans = ['active'];

    protected $requestKey = 'gallery';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'max:255',
            'url'  => ['required', 'unique' => 'unique:galleries,url'],
        ];
        if ($this->isActionUpdate() AND $gallery = $this->getActionModel()) {
            /** @var  $gallery Gallery */
            $rules['url']['unique'] = 'unique:galleries,url,' . $gallery->id;
        }
        return $rules;
    }

    protected function mergeRequestValues()
    {
        $this->mergeUrlFromName();

    }
}
