
<form action="{{ route('admin.'.$key . '.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
	@method('post')
	@csrf

	@include('admin.testimonials.partials.form')

	@include('admin.partials.submit_create_buttons')
</form>
