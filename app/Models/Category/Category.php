<?php

namespace App\Models\Category;

use App\Contracts\HasLocalized;
use App\Models\Model;
use App\Models\Partner;
use App\Models\Slider\Slider;
use App\Models\Testimonial;
use App\Models\TestimonialType;
use App\Models\Work;
use App\Repositories\CategoryRepository;
use App\Scopes\SortOrderScope;
use App\Scopes\WhereActiveScope;
use App\Traits\Models\ImageAttributeTrait;
use App\Traits\Models\SubNameAttributeTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

/**
 * App\Models\Category\Category
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int $active
 * @property string|null $url
 * @property string|null $icon Шрифтовые изображения
 * @property string|null $image
 * @property int|null $sort
 * @property int $childrens_big_size
 * @property string|null $options
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category\CategoryPage[] $categoryPages
 * @property-read int|null $category_pages_count
 * @property-read mixed $except
 * @property-read mixed $lower_name
 * @property-read mixed $name
 * @property-read mixed $sub_name
 * @property-read \App\Models\Category\Category|null $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Partner[] $partners
 * @property-read int|null $partners_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category\Price[] $prices
 * @property-read int|null $prices_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category\Product[] $products
 * @property-read int|null $products_count
 * @property-read \App\Models\Slider\Slider $slider
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Testimonial[] $testimonials
 * @property-read int|null $testimonials_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Testimonial[] $testimonialsImage
 * @property-read int|null $testimonials_image_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Testimonial[] $testimonialsVideo
 * @property-read int|null $testimonials_video_count
 * @property-read \App\Models\Work $work
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Category whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Category whereChildrensBigSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Category whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Category whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Category whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Category whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\Category whereUrl($value)
 * @mixin \Eloquent
 */
class Category extends Model implements HasLocalized
{

	public const CEILING_ID = 1;

	use SubNameAttributeTrait;

	use ImageAttributeTrait;

	protected $hasOneLangArguments = [CategoryLang::class];

	protected $guarded = ['id'];

	protected $casts = [
		'childrens_size' => 'boolean',
	];

	public function testimonials()
	{
		return $this->hasMany(Testimonial::class);
	}

	public function work()
	{
		return $this->hasOne(Work::class);
	}

	public function testimonialsImage()
	{
		return $this->hasMany(Testimonial::class)->where('type_id', TestimonialType::TYPE_IMAGE_ID);
	}

	public function testimonialsVideo()
	{
		return $this->hasMany(Testimonial::class)->where('type_id', TestimonialType::TYPE_VIDEO_ID);
	}

	public function products()
	{
		return $this->hasMany(Product::class);
	}

	public function getActiveSubcategories()
	{
		return $this->categories->filter(function (self $category) {
			return $category->active;
		});
	}

	public function categories()
	{
		return $this->hasMany(self::class, 'parent_id', 'id');
	}

	public function partners()
	{
		return $this->hasMany(Partner::class);
	}

	public function categoryPages(): HasMany
	{
		return $this->hasMany(CategoryPage::class, 'category_id', 'id');
	}

	public function allCategories()
	{
		return $this->categories()->with(['lang', 'allCategories']);
	}

	public function prices()
	{
		return $this->hasMany(Price::class);
	}

	public function slider()
	{
		return $this->morphOne(Slider::class, 'sliderable');
	}

	/**
	 * @return mixed
	 */
	public function getCategoryName()
	{
		return $this->lang->name;
	}

	public function getNameAttribute()
	{
		return $this->getCategoryName();
	}

	public function getExceptAttribute()
	{
		return $this->lang->getAttribute('except');
	}

	public function getUrlAttribute()
	{
		return $this->attributes['url'];
	}

	public function getLowerNameAttribute()
	{
		return Str::lower($this->getCategoryName());
	}

	public static function findForNestable($id)
	{
		static $categoriesNestable;
		if ($categoriesNestable === null) {
			$categoriesNestable = self::all()->keyBy('id');
		}
		return \Arr::get($categoriesNestable, $id, []);
	}

	public static function nestable(array $categories, $parent_id = 0)
	{
		/** @var $findCategory Category */
		foreach ($categories as $num => $category) {
			if ($findCategory = self::findForNestable(\Arr::get($category, 'id'))) {
				$data = [
					'sort'      => $num,
					'parent_id' => (int)$parent_id,
				];
				$findCategory->fillExisting($data)->save();
				if (isset($category['children'])) {
					self::nestable($category['children'], $category['id']);
				}
			}
		}
	}

	public function isChildrensDisplayBigSize()
	{
		return $this->childrens_big_size;
	}

	public function getDescriptionByIndex(int $index): string
	{
		if ($this->categoryPages->isNotEmpty() && $categoryPage = $this->categoryPages->get($index)) {
			return $categoryPage->lang->description ?? '';
		}
		return '';
	}

	public function isCategoryWithoutChild()
	{
		return !$this->categories->count();
	}

	public function isMainCategory()
	{
		return !((int)$this->parent_id);
	}

	public function parent()
	{
		return $this->belongsTo(__CLASS__, 'parent_id', 'id')->with('lang');
	}

	public function isOrBelongsToCeilingsCategory(CategoryRepository $categoryRepository)
	{
		/** @var  $mainCategory Category */
		$mainCategory = null;
		if ($this->isMainCategory()) {
			$mainCategory = $this;
		} else {
			$parents = $categoryRepository->getCategoryParents($this);
			$mainCategory = $parents->first();
		}

		return $mainCategory ? $mainCategory->getPrimaryValue() === self::CEILING_ID : false;
	}

	public static function boot()
	{
		parent::boot();

		self::bootGlobalScopes();
	}

	public static function bootGlobalScopes()
	{
		self::addGlobalScope(new SortOrderScope());
	}

	public static function dropGlobalScopes($scope, \Closure $implementation = null)
	{
		self::removeGlobalScope($scope, $implementation);
	}

	public static function initScopesPublic()
	{
		if (!self::hasGlobalScope(new WhereActiveScope())) {
			self::addGlobalScope(new WhereActiveScope());
		}
	}

	public function __clone()
	{
		$new = $this->replicate();

		$this->relations = [];

		foreach ($this->relations as $relationName => $values) {
			if ($this->relationLoaded($relationName)) {
				$new->{$relationName}()->sync($values);
			}
		}
	}

}
