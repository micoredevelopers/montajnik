<?php

namespace App\Http\Controllers\Category;

use App\Helpers\View\ColumnWidthChunker;
use App\Http\Controllers\SiteController;
use App\Models\Category\Category;
use App\Models\Slider\Slider;
use App\Models\TestimonialType;
use App\Repositories\CategoryRepository;
use App\Repositories\SliderRepository;
use App\Repositories\TestimonialRepository;
use App\Traits\Controllers\Category\BreadcrumbsTree;
use Illuminate\View\View;

class CategoryController extends SiteController
{
	use BreadcrumbsTree;

	private $categoryRepository;
	/**
	 * @var SliderRepository
	 */
	private $sliderRepository;


	public function __construct(CategoryRepository $categoryRepository, SliderRepository $sliderRepository)
	{
		parent::__construct();
		$this->categoryRepository = $categoryRepository;
		$this->sliderRepository = $sliderRepository;
	}

	public function show($url, TestimonialRepository $testimonialRepository, ColumnWidthChunker $chunker)
	{
		$category = $this->categoryRepository->findByUrl($url);
		$this->categoryRepository->loadForCategoryShow($category);

		$subCatsWithChilds = $category->categories->filter(static function (Category $category) {
			return $category->categories->isNotEmpty();
		});
		$subCatsWithoutChilds = $category->categories->filter(static function (Category $category) {
			return $category->categories->isEmpty();
		});

		$chunkedCategories = $this->categoryRepository->getChunkedSubCategories($subCatsWithChilds, $chunker);
		$chunkedCategoriesWithoutChild = $this->categoryRepository->getCategoriesChunkedContainer($category, $subCatsWithoutChilds, $chunker);
		$chunkedProducts = $category->isCategoryWithoutChild() ? collect($chunker->chunk($category->products)) : collect([]);

		$this->addBreadCrumbsTree($category, $this->categoryRepository);
		$slider = $this->sliderRepository->findByCategoryForDisplay($category);
		$includeCeilingsCalculator = $showCeilingsFrame = ($this->categoryRepository->isOrBelongsToCeilingsCategory($category));
		$testimonialsVideo = $testimonialRepository->getTestimonialsForMainPage(TestimonialType::TYPE_VIDEO_ID);
		$testimonialsImage = $category->testimonialsImage;

		$this->setTitle($category->name);

		view()->composer('includes.modal', static function (View $view) use ($category) {
			$view->with('category', $category);
		});

		$data['content'] = view('public.category.category', compact(
			'category'
			, 'testimonialsVideo'
			, 'slider'
			, 'showCeilingsFrame'
			, 'includeCeilingsCalculator'
			, 'chunkedCategories'
			, 'chunkedProducts'
			, 'chunkedCategoriesWithoutChild'
			, 'testimonialsImage'
		));
		return $this->main($data);
	}

}
