<?php


	namespace App\Containers\Admin\Category;


	class SearchDataContainer
	{
		private $search = '';

		/**
		 * @return string
		 */
		public function getSearch(): string
		{
			return $this->search;
		}

		/**
		 * @param string $search
		 */
		public function setSearch(string $search): self
		{
			$this->search = $search;
			return $this;
		}

	}