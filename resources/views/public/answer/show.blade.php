<?php /** @var $faq \App\Models\Faq */ ?>
<?php /** @var $prevFaq \App\Models\Faq */ ?>
<?php /** @var $nextFaq \App\Models\Faq */ ?>
<main id="answer">
    <section class="news-open">
        <div class="container bg-white pt-3 pt-lg-5">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="d-lg-flex flex-column justify-content-between align-items-center mb-30 ">

                        @include('public.partials.breadcrumbs')

                        <h1 class="title mb-3 mb-lg-0 ">{!! showMeta($faq->getQuestionAttribute()) !!}</h1>
                    </div>
                    <div class="news-open-text mb-30">{!! $faq->getAnswerAttribute() !!}</div>
                </div>
            </div>
        </div>
    </section>
    <section class="other-questions">
        <div class="container">
            <div class="row">
                @if ($prevFaq = $faq->prevFaq)
                    <div class="col-md-6 pl-lg-0">
                        <a class="questions__item" href="{{ route('answer.show', $prevFaq->getUrlAttribute()) }}">
                            <span class="questions_num">{{ $prevFaq->getPrimaryValue() }}</span>
                            <p class="text_16-20 mb-0">{{ $prevFaq->getQuestionAttribute() }}</p>
                            <span class="arrow-breeze">
				 			 <img src="{{ asset('img/questions/arrow-breeze.svg') }}" alt=""/>
						</span>
                        </a>
                    </div>
                @endif
                @if ($nextFaq = $faq->nextFaq)
                    <div class="col-md-6 pr-lg-0">
                        <a class="questions__item" href="{{ route('answer.show', $nextFaq->getUrlAttribute()) }}">
                            <span class="questions_num">{{ $nextFaq->getPrimaryValue() }}</span>
                            <p class="text_16-20 mb-0">{{ $nextFaq->getQuestionAttribute() }}</p>
                            <span class="arrow-breeze">
              				<img src="{{ asset('img/questions/arrow-breeze.svg') }}" alt=""/>
            			</span>
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </section>
</main>