@extends('errors.illustrated-layout')

@section('title', getTranslate('global.404'))
@section('code', '404')
@section('message', getTranslate('global.404-description'))
