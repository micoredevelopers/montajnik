<?php

namespace App\Modules\Sitemap\LinksManager;

use App\Contracts\Models\HasSitemapLinks;
use App\Models\Faq;
use Illuminate\Support\Collection;

class FaqSiteMapManager implements HasSitemapLinks
{
	public function getSiteMapLinks(): Collection
	{
		$links = collect([]);
		$faqs = Faq::active()->get();
		$faqs->map(static function (Faq $faq) use ($links) {
			$item = [
				'loc'     => route('answer.show', $faq->url),
				'lastmod' => checkDateCarbon($faq->updated_at),
			];
			$links->push($item);
		});
		return $links;
	}
}