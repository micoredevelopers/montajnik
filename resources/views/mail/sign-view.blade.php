<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Новая заявка на просмотр</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<?php /** @var $feedback \App\Models\Feedback */ ?>
<div class="container">
    <table class="table">
        <tbody>
        <tr>
            <td>Имя:</td>
            <td>{{ $feedback->name }}</td>
        </tr>
        @if ($feedback->email)
            <tr>
                <td>Email:</td>
                <td>{{ $feedback->email }}</td>
            </tr>
        @endif
        @if ($feedback->text)
            <tr>
                <td>Сообщение:</td>
                <td>{{ $feedback->text }}</td>
            </tr>
        @endif
        @if ($feedback->phone)
            <tr>
                <td>Телефон:</td>
                <td>{{ $feedback->phone }}</td>
            </tr>
        @endif
        @if ($feedback->flat_id AND $feedback->flat)
            <tr>
                <td>Квартира:</td>
                <td>
                    <a href="{{ route('flat.edit', $feedback->flat_id ) }}">{{ $feedback->flat->number }}</a>
                </td>
            </tr>
        @endif
        @if ($feedback->language_id AND $feedback->getLanguage)
            <tr>
                <td>Язык:</td>
                <td>
                    {{ $feedback->getLanguage->name }}
                </td>
            </tr>
        @endif
        @if ($feedback->referer)
            <tr>
                <td colspan="2" class="text-center">
                    <a href="{{ $feedback->referer }}" target="_blank">Страница заявки</a>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
</div>

</body>
</html>