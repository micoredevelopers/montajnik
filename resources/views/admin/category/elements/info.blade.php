<?php /** @var $edit \App\Models\Category\Category */ ?>
<?php /** @var $product \App\Models\Category\Product */ ?>
<div role="tabpanel" class="tab-pane" id="info">
    <div class="panel-body">
        <div class="card">
            <div class="card-header">Товары</div>
            <div class="card-body">
                <div class="row">
                    @forelse($edit->products as $product)
                        <div class="col-md-4">
                            <img src="{{ checkImage($product->getImageAttribute()) }}" alt=""
                                 class="img-fluid img-thumbnail-30">
                            <a href="{{ route(routeKey('products', 'edit'), $product->id) }}"
                               title="{{ __('modules._.do-edit') }}">{{ $product->getProductName() }}</a>
                        </div>
                    @empty
                        Товаров пока нет, <a href="{{ route(routeKey('products', 'create')) }}">Создать</a>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">Доп работы</div>
            <div class="card-body">
                @if ($prices = $edit->prices)
                    @foreach($prices as $price)
                        <a href="{{ route(routeKey('prices', 'edit'), $price->id) }}">{{ $price->name }}</a>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="card">
            <div class="card-header">Партнеры</div>
            <div class="card-body">
                @if ($partners = $edit->partners)
                    @foreach($partners as $partner)
                        <img src="{{ checkImage($partner->image) }}" alt=""
                             class="img-fluid img-thumbnail-30">
                        <a href="{{ route(routeKey('partners', 'edit'), $partner->id) }}">{{ $partner->name }}</a>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="card">
            <div class="card-header">Наши работы</div>
            <div class="card-body">
                @if ($work = $edit->work)
                        <a href="{{ route(routeKey('works', 'edit'), $work->id) }}">{{ __('modules._.do-edit') }}</a>
                @endif
            </div>
        </div>


    </div>
</div>