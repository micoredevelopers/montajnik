<?php


namespace App\Traits\Controllers;

use Artesaos\SEOTools\Facades\SEOMeta as SEOMetaBase;

trait Assets
{
    protected $styles = [];

    protected $scripts = [];

    protected $scriptsDefer = [];

    //  CSS  //
    public function checkForStylesArr($destination)
    {
        if (!isset($this->styles[$destination])) $this->styles[$destination] = [];

        return $this->styles[$destination];
    }

    public function addCss($style, $destination = 'header')
    {
        $this->checkForStylesArr($destination);
        if (is_array($style)) {
            foreach ($style as $css) {
                $this->addCss($css, $destination);
            }
        } else {
            if (!isStringUrl($style)) {
                if (assetFileExists($style)) {
                    $filemtime = assetFilemtime($style) ?? false;
                    if ($filemtime) $style .= '?' . $filemtime;
                }
                $style = asset($style, env('HTTPS'));
            }
            $this->styles[ $destination ][] = $style;
        }
    }

    public function getCss($destination)
    {
        return $this->checkForStylesArr($destination);
    }

    public function getStylesString($destination = 'header')
    {
        $return = '';
        if ($styles = $this->getCss($destination)) {
            foreach ($styles as $style) {
                $return .= ' <link rel="stylesheet" href="' . $style . '">' . PHP_EOL;
            }
        }

        return $return;
    }

    //  JS  //
    public function checkForScriptsArr($defer = false)
    {
        $var = ($defer) ? 'scriptsDefer' : 'scripts';
        if (!isset($this->$var)) $this->$var = [];

        return $this->$var;
    }

    public function dropListScripts($defer = false)
    {
        $var = ($defer) ? 'scriptsDefer' : 'scripts';
        $this->$var = [];

        return $this->$var;
    }

    public function addScripts($scripts)
    {
        $this->checkForScriptsArr();
        if (is_array($scripts)) {
            foreach ($scripts as $script) {
                $this->addScripts($script);
            }
        } else {
            if (!isStringUrl($scripts)) {
                if (assetFileExists($scripts)) {
                    $filemtime = assetFilemtime($scripts) ?? false;
                    if ($filemtime) $scripts .= '?v=' . $filemtime;
                }
                $scripts = asset($scripts, env('HTTPS'));
            }
            $this->scripts[] = $scripts;
        }
    }

    public function getScripts($defer = false)
    {
        return $this->checkForScriptsArr($defer);
    }

    public function getScriptsString($scripts, $attributes = '')
    {
        $return = '';
        if ($scripts) {
            foreach ($scripts as $script) {
                $return .= ' <script type="text/javascript" ' . $attributes . ' src="' . $script . '"></script>' . PHP_EOL;
            }
        }

        return $return;
    }

    // JS DEFER //
    public function addScriptsDefer($scripts)
    {
        if (!$scripts) return;
        $this->checkForScriptsArr(true);
        if (is_array($scripts)) {
            foreach ($scripts as $script) {
                $this->addScriptsDefer($script);
            }
        } else {
            if (!isStringUrl($scripts)) {

                if (assetFileExists($scripts)) {
                    $filemtime = assetFilemtime($scripts) ?? now();
                    if ($filemtime) $scripts .= '?' . $filemtime;
                }
                $scripts = asset($scripts, env('HTTPS'));
            }
            $this->scriptsDefer[] = $scripts;
        }
    }
}