<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\GalleryRequest;
use App\Models\GalleriesLang;
use App\Models\Gallery;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;

class GalleryController extends AdminController
{
    use SaveImageTrait;
    use Authorizable;

    private $name = 'Галлерея';

    protected $key = 'galleries';

    protected $permissionKey = 'galleries';

    protected $routeKey = 'admin.galleries';

    public function __construct()
    {
        parent::__construct();
        $this->addBreadCrumb($this->name, $this->resourceRoute('index'));
        $this->shareViewModuleData();
    }

    public function index()
    {
        $this->setTitle($this->name);
        $vars['list'] = Gallery::latest()->GetLang($this->langAdmin)->paginate();
        $data['content'] = view('admin.galleries.index', $vars);
        return $this->main($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['content'] = view('admin.galleries.create');
        return $this->main($data);
    }

    public function store(GalleryRequest $request, Gallery $gallery)
    {
        $input = $request->except('_token', 'image');

        if ($gallery->fillExisting($input)->save()) {
            $this->setSuccessStore();
            foreach ($this->languagesList as $language) {
                $galleriesLang = new GalleriesLang();
                /** @var $language \App\Models\Language */
                $galleriesLang->gallery()->associate($gallery);
                $galleriesLang->associateWithLanguage($language);
                $galleriesLang->fillExisting($input)->save();
            }
            $this->saveImage($request, $gallery);
        }

        if ($request->has('createOpen')) {
            return redirect($this->resourceRoute('edit', $gallery->id))->with($this->getResponseMessage());
        }

        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
    }

    /**
     * @param Gallery $gallery
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Gallery $gallery)
    {
        $vars['edit'] = $gallery = Gallery::getLang($this->langAdmin)->findOrFail($gallery->id);
        $vars['photosList'] = $gallery->images;
        $title = $this->titleEdit($gallery);
        $this->addBreadCrumb($title)->setTitle($title);

        $data['content'] = view('admin.galleries.edit', $vars);
        return $this->main($data);
    }

    public function update(GalleryRequest $request, Gallery $gallery)
    {
        $input = $request->except('_token', 'image');
        //
        $gallery->fillExisting($input);
        if ($gallery->save()) {
            $this->saveImage($request, $gallery);
            $gallery->lang($this->langAdmin)->first()->fillExisting($input)->save();
            $this->setSuccessUpdate();
        }
        if ($request->hasFile('images')) {
            $this->saveAdditionalImages($gallery, $request);
        }

        if ($request->has('saveClose')) {
            return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
        }

        return redirect()->back()->with($this->getResponseMessage());
    }

    public function destroy(Gallery $gallery)
    {
        if ($gallery->delete()) {
            $this->setSuccessDestroy();
            $this->photoModel->deleteImageStorage($gallery->getImageAttribute());
            foreach ($gallery->images as $image) {
                $this->photoModel->deleteImageStorage($image->image);
            }
        }

        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
    }
}
