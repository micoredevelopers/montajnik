<?php

namespace App\Http\Requests\Admin;

use App\Contracts\Requests\RequestParameterModelable;
use App\Http\Requests\AbstractRequest;
use App\Models\Category\Category;
use App\Traits\Requests\Helpers\GetActionModel;

class WorksRequest extends AbstractRequest implements RequestParameterModelable
{
    use GetActionModel;

    protected $requestKey = 'work';

    public function rules()
    {
        $rules = [
            'url'         => ['required', 'unique:works,url'],
            'category_id' => [
                'required',
                'exists:categories,id',
                'not_exists' => 'not_exists:works,category_id',
            ],
        ];

        if ($this->isActionUpdate() AND $work = $this->getActionModel()) {
            /** @var  $category Category */
            $rules['url'] = ['required', 'unique:works,url,' . $work->id];
            $rules['category_id']['not_exists'] .= ',id,'. $work->id;
        }

        return $rules;
    }

    protected function mergeRequestValues()
    {
        if (!$this->get('url')) {
            $this->merge(['url' => $this->get('name'),]);
        }
        $this->merge([
            'url'    => \Str::slug($this->get('url')),
            'active' => (int)$this->get('active'),
            'video'  => (array)$this->get('video', []),
        ]);
    }

}
