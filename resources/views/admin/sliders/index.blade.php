<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th>ID</th>
            <th class="th-description">Комментарий</th>
            <th class="text-right">
                @if(isSuperAdmin())
                    <a href="{{ route($routeKey . '.create') }}" class="btn btn-primary">@lang('form.create')</a>
                @endif
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>
                    {{ $item->comment }}
                </td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<div class="text-center">
    {{ $list->links()}}
</div>
