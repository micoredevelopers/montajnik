<?php /** @var $category \App\Models\Category\Category */ ?>
<?php /** @var $loop object */ ?>
<main id="main-page">
    @include('public._includes.slider')

    @include('public.index.includes.services')

    @include('public.category.includes.why-choose-us')

    @include('public.index.includes.advantages')

    @include('public.index.includes.our-works')

    @include('public.index.includes.made-booking')

    <section class="feedback d-lg-flex flex-row-reverse">
        <div class="feedback-img-box">
            <img class="w-100 lazy" data-src="{{ assetWebp('img/feedback.png') }}" alt="" width="374" height="228"/>
        </div>
        <div class="feedback-desc-box text-center text-lg-left d-lg-flex justify-content-end align-items-center">
            <div class="feedback-desc">
                <h2 class="feedback-title mb-lg-20">Модуль тестов</h2>
                <p class="text_white mb-20 mb-lg-30 maxWdt350 feedback-text">
                    Идейные соображения высшего порядка, а также начало повседневной работы
                    по формированию начала повседневной
                </p>
                <a href="" class="btn-custom mx-auto mx-lg-0 mr-lg-auto">Пройти</a>
            </div>
        </div>
    </section>
    @include('public.index.includes.news')
    @include('public.index.includes.reviews')
    @include('public.index.includes.video-reviews')

</main>
<script src="//cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
<script src="https://threejs.org/examples/js/libs/stats.min.js"></script>
