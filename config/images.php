<?php
return [
	'default'    => [
		'width'  => 500,
		'height' => 500,
	],
	'categories' => [
		'thumbnail' => [],
	],
	'watermark'  => [
		'path'     => public_path('/images/staff/watermark.png'),
		'position' => 'center',
	],
];