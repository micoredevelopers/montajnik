<?php

namespace App\Observers;

use App\Models\Gallery;

class GalleryObserver
{
    public function creating(Gallery $gallery)
    {
        if (!$gallery->url){
            $gallery->url =  \Str::slug( \Str::random(4));
        }
        if ($new = Gallery::where('url', $gallery->url)->first()){
            $gallery->url .= \Str::random(2);
        }
    }
    /**
     * Handle the news "created" event.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return void
     */
    public function created(Gallery $gallery)
    {
        //
    }


    public function updating(Gallery $gallery)
    {
        if (!$gallery->url){
            $gallery->url =  \Str::slug( \Str::random(4));
        }

        if ($gallery->url !== $gallery->getOriginal('url') AND Gallery::where('url', $gallery->url)->first()){
            $gallery->url .= \Str::random(2);
        }
    }

    /**
     * Handle the news "updated" event.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return void
     */
    public function updated(Gallery $gallery)
    {

    }

    /**
     * Handle the news "deleted" event.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return void
     */
    public function deleted(Gallery $gallery)
    {
        //
    }

    /**
     * Handle the news "restored" event.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return void
     */
    public function restored(Gallery $gallery)
    {
        //
    }

    /**
     * Handle the news "force deleted" event.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return void
     */
    public function forceDeleted(Gallery $gallery)
    {
        //
    }
}
