<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends SiteController
{

	public function __construct()
	{
		parent::__construct();
		$this->addBreadCrumb('О нас',route('about.index'));
	}

	public function index()
	{
		$this->setTitle(getSetting('global.sitename'));
		$data['content'] = view('public.about-us.index');
		return $this->main($data);
	}
}
