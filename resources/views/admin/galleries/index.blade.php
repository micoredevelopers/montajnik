<div class="table-responsive">
	<table class="table table-shopping">
		<thead>
		<tr>
			<th>ID</th>
			<th class="">@lang('form.image.image')</th>
			<th class="th-description">@lang('form.title')</th>
			<th class="text-right">
				<a href="{{ route($routeKey . '.create') }}" class="btn btn-primary">@lang('form.create')</a>
			</th>
		</tr>
		</thead>
		<tbody>
		@foreach($list as $item)
			<tr>
				<td>{{ $item->id }}</td>
				<td>
					<div class="img-container">
						<a href="{{ imgPathOriginal(checkImage($item->image)) }}" class="fancy"
						   data-fancybox="galleries-image">
							<img src="{{ checkImage($item->image) }}" alt=""/>
						</a>
					</div>
				</td>
				<td>
					{{ $item->name }}
				</td>
				<td class="text-primary text-right">
					@include('admin.partials.action.index_actions')
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>

<div class="text-center">
	{{ $list->links()}}
</div>
