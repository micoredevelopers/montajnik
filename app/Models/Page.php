<?php

namespace App\Models;


use App\Contracts\HasImagesContract;
use App\Contracts\HasLocalized;
use App\Scopes\WhereActiveScope;
use App\Traits\HasContent;
use App\Traits\Models\HasImages;
use App\Traits\Models\ImageAttributeTrait;
use App\Traits\Singleton;
use Illuminate\Support\Arr;

/**
 * App\Models\Page
 *
 * @property int $id
 * @property int $active
 * @property int|null $sort
 * @property mixed $url
 * @property string|null $image
 * @property string|null $page_type
 * @property string|null $options
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property bool $manual
 * @property int|null $parent_id
 * @property-read \App\Models\Content $content
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Content[] $contents
 * @property-read int|null $contents_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PageLang[] $langs
 * @property-read int|null $langs_count
 * @property-read \App\Models\Menu $menu
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereManual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page wherePageType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereUrl($value)
 * @mixin \Eloquent
 */
class Page extends Model implements HasImagesContract, HasLocalized
{
    use Singleton;
    use ImageAttributeTrait;
    use HasImages;
    use HasContent;

    protected $hasOneLangArguments = [PageLang::class];

    public static $publicMenusCacheKey = 'public.menus';

    private static $pagesByType;

    protected $table = 'pages';

    protected $casts = [
        'manual' => 'boolean',
    ];
    protected $guarded = [
        'id',
    ];

    protected static $pageTypes = [
    ];

    public function menu()
    {
        return $this->hasOne(Menu::class);
    }

    public function parent($withLang = false)
    {
        $query = $this->belongsTo(Page::class, 'parent_id', 'id');
        if ($withLang) {
            $query->GetLang();
        }
        return $query;
    }

    public static function getPageTypes()
    {
        return self::$pageTypes;
    }

    public function pageTypeExists($pageType)
    {
        return Arr::exists(self::$pageTypes, $pageType);
    }

    public static function getPageByType($type): ?Page
    {
        if (self::getInstance()->pageTypeExists($type)) {
            if (!Arr::has(self::$pagesByType, $type)) {
                $page = self::where('page_type', $type)->first();
                Arr::set(self::$pagesByType, $type, $page);
                return $page;
            }
        }
        return null;
    }

    public function setUrlAttribute($url): self
    {
        if (!$this->getAttribute('manual')) {
            $url = \Str::slug($url);
        } else {
            $url = '/' . ltrim($url, '/');
        }
        $column = 'url';
        $this->attributes[$column] = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrlAttribute()
    {
        $column = 'url';
        $name = \Arr::get($this->attributes, $column);
        return $name;
    }

    public function getRouteUrl()
    {
        if ($this->isManualLink()) {
            return url($this->getUrlAttribute());
        }
        return route('page.show', $this->getUrlAttribute());
    }

    public function isManualLink()
    {
        return $this->getAttribute('manual');
    }

    public function langs()
    {
        return $this->hasMany(PageLang::class);
    }

    public function setPageTypeAttribute($type)
    {
        if (!$this->pageTypeExists($type)) {
            $type = null;
        }
        $this->attributes['page_type'] = $type;
    }

    public static function getManualPagesUrlRoutes(): array
    {
        try {
            $pages = self::where('manual', true)->get('url');
        } catch (\Exception $exception) {
            $pages = [];
        }
        $urls = [];
        foreach ($pages as $page) {
            $urls[] = \Route::get($page->url, 'PageController@manualUrl');
        }
        return $urls;
    }

    public static function initScopesPublic()
    {
        if (!self::hasGlobalScope(new WhereActiveScope())) {
            self::addGlobalScope(new WhereActiveScope());
        }
    }

}
