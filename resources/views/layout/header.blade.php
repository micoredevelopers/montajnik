<div class="preloader-holder" id="page-preloader">
  <div class="preloader">
    <div></div>
    <div></div>
  </div>
</div>
<script type="text/javascript" defer>
  // window.on.ready(function (e) {
  //     // $('#app').removeClass('blurred-app');
  //     $('#page-preloader').fadeOut(300);
  // });
  document.addEventListener("DOMContentLoaded", function (event) {
    document.getElementById('page-preloader').style = "display:none"
  });
</script>
<header>
  <div class="header-desctop d-none d-xl-block">
    <div class="container-fluid header-desctop-top">
      <div class="row ">
        <div class="col-3">
          <a class="header-logo" href="{{ route('home') }}">
            <img class="w-100" src="{{ asset('img/mp/logo.svg') }}" alt="" width="322" height="53"/>
          </a>
        </div>
        <div class="col-6">
          <div class="phones-box">
            @if (getSetting('contacts.phone-vodafone'))
              <a href="tel:+{{ formatTel(getSetting('contacts.phone-vodafone')) }}"
                 class="phones-box__item vod">
                <img src="{{ asset('img/mp/Vodafone_Ukraine.png') }}" alt=""/>
                <span class="phone">{{  getSetting('contacts.phone-vodafone') }}</span>
              </a>
            @endif
            @if (getSetting('contacts.phone-kyivstar'))
              <a href="tel:+{{ formatTel(getSetting('contacts.phone-kyivstar')) }}"
                 class="phones-box__item ks">
                <img src="{{ asset('img/mp/kievstar.png') }}" alt=""/>
                <span class="phone">{{  getSetting('contacts.phone-kyivstar') }}</span>
              </a>
            @endif
            @if (getSetting('contacts.phone-lifecell'))
              <a href="tel:+{{ formatTel(getSetting('contacts.phone-lifecell')) }}"
                 class="phones-box__item lc">
                <img src="{{ asset('img/mp/Lifecell_logo.png') }}" alt=""/>
                <span class="phone">{{ getSetting('contacts.phone-lifecell') }}</span>
              </a>
            @endif
            @if (getSetting('contacts.phone-intretelecom'))
              <a href="tel:+{{ formatTel(getSetting('contacts.phone-intretelecom')) }}"
                 class="phones-box__item cph">
                @include('layout.elements.svg.phone')
                <span class="phone">{{ getSetting('contacts.phone-intretelecom') }}</span>
              </a>
            @endif
          </div>
        </div>
        <div class="col-3">
          <div class="social-links-box justify-content-center">
            <a target="_blank" rel="nofollow" href="{{ getSetting('social.youtube') }}"
               class="social-links-box__item social-links-box_mr40">
              @include('layout.elements.svg.youtube')
            </a>
            <a target="_blank" rel="nofollow" href="{{ getSetting('social.facebook') }}"
               class="social-links-box__item social-links-box_mr40">
              @include('layout.elements.svg.facebook')
            </a>
            <a target="_blank" rel="nofollow" href="{{ getSetting('social.viber') }}"
               class="social-links-box__item social-links-box_mr40">
              @include('layout.elements.svg.viber')
            </a>
            {{--                        <a target="_blank" rel="nofollow" href="{{ getSetting('social.gplus') }}"--}}
            {{--                           class="social-links-box__item ">--}}
            {{--                            @include('layout.elements.svg.google-plus')--}}
            {{--                        </a>--}}
          </div>
        </div>
      </div>
    </div>
    <div class="header-desctop-bottom">
      <div class="menu-dropdown-wrap">
        <span class="menu-dropdown-title">Все услуги</span>
      </div>
      <nav class="menu">
        @foreach ($menus as $menu)
          <a href="{{ langUrl($menu->url) }}" class="menu__link">{{ $menu->lang->name}}</a>
        @endforeach
        <label class="m-0 menu-input-wrap p-0" for="search-input">
          <input class="menu__input"
                 type="text"
                 data-url="{{ route('search') }}"
                 name="search" placeholder="Поиск"
                 autocomplete="off"
          />
          <span>Ничего не найдено</span>
        </label>
      </nav>
    </div>
  </div>
  <div class="header-mobile d-xl-none">
    <div class="container-fluid d-flex justify-content-between align-items-center">
      <div class="burger">
        <span></span> <span></span> <span></span>
      </div>
      <a class="logo-xs d-block" href="{{ route('home') }}">
        <img class="w-100 " src="{{ asset('img/mp/logo.svg') }}" alt="" width="322" height="53"/>
      </a>
      <a class="phone-link-xs d-block"
         href="#"
         data-toggle="modal"
         data-target="#phoneModal">
        <img class="w-100 " src="{{ asset('img/mp/phone-xs.svg') }}" alt="" width="24" height="24"/>
      </a>
    </div>
    <label class="m-0 menu-input-wrap w-100 menu-input-wrap-mobile d-lg-none" for="search-input">
      <input id="search-input" class="menu__input w-100" data-url="{{ route('search') }}" type="text" name="search" placeholder="Поиск" autocomplete="off"/>
      <span> Ничего не найдено</span>
    </label>
  </div>
</header>
<div class="overlay"></div>

<ul class="menu-dropdown menu-dropdown_position mb-0 d-none d-lg-block">
  @foreach($categories as $category)
    <li class="menu-dropdown__item sub__item">
			<span class="menu-dropdown-link"><a href="{{route('category.show', $category->url) }}">{{ $category->getCategoryName() }}</a>
				@if($category->allCategories->count())
          <span class="menu-arrow"></span>
        @endif
			</span>
      @include('layout.general-menu-loop',['categories' => $category->allCategories, 'nesting' => 0,'nestingArrData' => ['55','80','105','130','155']])
    </li>
  @endforeach
</ul>

<div class="menu-dropdown menu-dropdown_position mb-0 d-lg-none d-flex flex-column justify-content-between">
  <div>
    <div class="menu-dropdown__item sub__item">
      <div class="menu-dropdown-link" href="#">
        <a>Все услуги</a><span class="menu-arrow"></span>
      </div>
      <ul class="menu-dropdown sub mb-0 ">
        @foreach($categories as $category)
          <li class="menu-dropdown__item sub__item">
						<span class="menu-dropdown-link pl-80">
              <a href="{{route('category.show', $category->url)}}">{{ $category->getCategoryName() }}</a>
							@if(count($category->allCategories))
                <span class="menu-arrow"></span>
              @endif
						</span>
            @include('layout.general-menu-loop', ['categories'=>$category->allCategories,'nesting'=> 0,'nestingArrData' => ['105','130','155','180','205']])
          </li>
        @endforeach
      </ul>
    </div>
    <div class="menu-dropdown__item sub__item">
      <ul class="pl-0">
        @foreach ($menus as $menu)
          <a href="{{ langUrl($menu->url) }}" class="menu-dropdown-link">{{ $menu->lang->name}}</a>
        @endforeach
      </ul>
    </div>
  </div>
  <nav class="social-links-box social-links-box_absolute justify-content-center py-4">
    <a target="_blank" rel="nofollow" href="{{ getSetting('social.youtube') }}" class="social-links-box__item mx-4">
      <img class="svg" src="{{ asset('img/mp/youtube-white.svg') }}" alt="" width="30" height="22"/>
    </a>
    <a target="_blank" rel="nofollow" href="{{ getSetting('social.facebook') }}" class="social-links-box__item mx-4">
      <img class="svg" src="{{ asset('img/mp/facebook-white.svg') }}" alt="" width="12" height="22"/>
    </a>
    <a target="_blank" rel="nofollow" href="{{ getSetting('social.viber') }}" class="social-links-box__item mx-4">
      <img class="svg" src="{{ asset('img/mp/viber-white.svg') }}" alt="" width="19" height="20"/>
    </a>
    {{--        <a target="_blank" rel="nofollow" href="{{ getSetting('social.gplus') }}" class="social-links-box__item mx-4">--}}
    {{--            <img class="svg" src="{{ asset('img/mp/google-white.svg') }}" alt=""/>--}}
    {{--        </a>--}}
  </nav>
</div>
