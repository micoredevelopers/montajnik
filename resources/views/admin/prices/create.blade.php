<form action="{{ route($routeKey . '.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
    @method('post')
    @csrf

    @include('admin.prices.partials.form')

    @include('admin.partials.submit_create_buttons')
</form>

@section('javascript')
    <script defer>
        $(document).ready(function () {
            $('.select2').select2({
                tags: true,
                width: 'resolve'
            });
        });
    </script>
@stop