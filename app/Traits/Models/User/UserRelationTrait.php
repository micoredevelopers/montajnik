<?php

namespace App\Traits\Models\User;


use App\Models\User\UserDiscount;
use App\Models\User\UserField;
use App\Models\User\UserGroup;
use Illuminate\Database\Eloquent\Relations\HasOne;

trait UserRelationTrait
{
	protected $defaults = [
		'value' => 0,
		'bonus' => 0,
	];
}
