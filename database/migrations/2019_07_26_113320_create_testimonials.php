<?php

use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonials extends Migration
{

    use MigrationCreateFieldTypes;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimonials', function (Blueprint $table) {
            $this->setTable($table);
			$table->bigIncrements('id');
			$table->string('name', 255)->nullable();
			$table->char('phone', 255)->nullable();
			$table->char('email', 255)->nullable();
			$table->text('message')->nullable();
			$table->tinyInteger('active')->default(0);
			$table->dateTime('date')->nullable();
			$table->smallInteger('sort')->nullable()->default(0);
			$table->string('referer', 255)->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonials');
    }
}
