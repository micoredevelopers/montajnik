<?php

namespace App\Repositories;

use App\Models\News;
use App\Models\Partner;
use App\Traits\Repositories\RebuildNextAndPrev;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class NewsRepository.
 */
class PartnersRepository extends AbstractRepository
{
    use RebuildNextAndPrev;

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Partner::class;
    }

    public function getListForAdmin(): LengthAwarePaginator
    {
        $list = $this->newQuery()->paginate();
        return $list;
    }

    /**
     * @param $id
     * @return Partner|null
     */
    public function findForEdit($id): ?Partner
    {
        $edit = Partner::with('category')->findOrFail($id);
        return $edit;
    }

}
