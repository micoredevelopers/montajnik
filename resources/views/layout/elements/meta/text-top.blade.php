@if ($textTop = showMeta('', 'text_top'))
    <div class="container pb-4 offset-header-height">
        @if (isAdmin() && $meta = getMeta())
            <a href="{{ route('admin.meta.edit', $meta->id) }}#text_top" target="_blank" class="badge badge-danger">Редактировать мета текст</a>
        @endif
        <div class="decodeWrapper">
            {!! $textTop !!}
        </div>
    </div>
@endif
