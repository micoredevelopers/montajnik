<?php

namespace App\Traits\Models\User;


use App\Traits\Models\NameAttributeTrait;

trait UserMutatorTrait
{
	use NameAttributeTrait;

	public function getGroupAttribute()
	{
		return \Arr::get($this->attributes, 'group');
	}

	/**
	 * @param $group
	 * @return UserMutatorTrait
	 */
	public function setGroupAttribute($group) :self
	{
		if ($this->groupExists($group)){
			$this->attributes['group'] = $group;
		}
		return $this;
	}

}
