
<section class="advantages">
    <div class="container">
        <div class="text-center">
            <h2 class="title text-center mb-20">
                <span class="decor-bg">Преимущества</span>
            </h2>
        </div>
        <div class="row">
            <div class="col-sm-6 col-lg-3 text-center">
                <div class="advantages__item mb-20">
                    <img class="mb-20" src="{{ asset('img') }}/advantages-icon/Montajnik-Icon_1kachestvo.svg" alt="Icon" width="45" height="45"/>
                    <h3 class="advantages-item-title mb-25">Качество</h3>
                    <p class="text_light mb-0">
                        Выполняем все виды строительных и ремонтных работ с гарантией качества
                    </p>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3 text-center">
                <div class="advantages__item mb-20">
                    <img class="mb-20 " src="{{ asset('img') }}/advantages-icon/Montajnik-Icon_2nadejnost.svg" alt="Icon" width="45" height="45"/>
                    <h3 class="advantages-item-title mb-25">Надёжность</h3>
                    <p class="text_light mb-0">
                        Не просим авансов и предоплат. Работаем сертифицированными материалами. Оплата материала производятся после предоставления чека
                    </p>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3 text-center">
                <div class="advantages__item mb-20">
                    <img class="mb-20 " src="{{ asset('img') }}/advantages-icon/Montajnik-Icon_3oplata.svg" alt="Icon" width="45" height="45"/>
                    <h3 class="advantages-item-title mb-25">Поэтапная оплата</h3>
                    <p class="text_light mb-0">
                        Вы оплачиваете каждый этап работы, а не весь объем сразу
                    </p>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3 text-center">
                <div class="advantages__item mb-20">
                    <img class="mb-20 " src="{{ asset('img') }}/advantages-icon/Montajnik-Icon_4smeta.svg" alt="Icon" width="45" height="45"/>
                    <h3 class="advantages-item-title mb-25">Понятная и прозрачная смета</h3>
                    <p class="text_light mb-0">
                        Производим предварительный расчет - подробно расписываем
                        стоимость материалов, работу персонала, поставщиков, транспортные
                        расходы
                    </p>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3 text-center">
                <div class="advantages__item mb-20">
                    <img class="mb-20 " src="{{ asset('img') }}/advantages-icon/Montajnik-Icon_5dogovor.svg" alt="Icon" width="45" height="45"/>
                    <h3 class="advantages-item-title mb-25">Официальный договор</h3>
                    <p class="text_light mb-0">
                        Заключаем договор на предоставление услуг, в котором четко
                        прописываем обязанности сторон
                    </p>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3 text-center">
                <div class="advantages__item mb-20">
                    <img class="mb-20 " src="{{ asset('img') }}/advantages-icon/Montajnik-Icon_6sertificat.svg" alt="Icon" width="45" height="45"/>
                    <h3 class="advantages-item-title mb-25">Гарантийный сертификат</h3>
                    <p class="text_light mb-0">
                        Работаем только сертифицированными материалами. Закупаем
                        материалы проверенных украинских и импортных производителей
                    </p>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3 text-center">
                <div class="advantages__item mb-20">
                    <img class="mb-20 " src="{{ asset('img') }}/advantages-icon/Montajnik-Icon_7opit.svg" alt="Icon" width="45" height="45"/>
                    <h3 class="advantages-item-title mb-25">15 лет опыта</h3>
                    <p class="text_light mb-0">
                        Опыт работы – это наша гордость и ваши положительные отзывы.
                        Повышаем квалификацию сотрудников, идем в ногу со временем
                    </p>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3 text-center">
                <div class="advantages__item mb-20">
                    <img class="mb-20 " src="{{ asset('img') }}/advantages-icon/Montajnik-Icon_8nalogi.svg" alt="Icon" width="45" height="45"/>
                    <h3 class="advantages-item-title mb-25">Оперативность</h3>
                    <p class="text_light mb-0">
                        Выполняем работу согласно оговоренным срокам
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>