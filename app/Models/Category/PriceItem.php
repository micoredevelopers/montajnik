<?php

namespace App\Models\Category;


use App\Models\Model;

/**
 * App\Models\Category\PriceItem
 *
 * @property int $id
 * @property int $price_id
 * @property string|null $name
 * @property \App\Models\Category\Price $price
 * @property string|null $unit
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\PriceItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\PriceItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\PriceItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\PriceItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\PriceItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\PriceItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\PriceItem wherePriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\PriceItem whereUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class PriceItem extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;

    public function price()
    {
        return $this->belongsTo(Price::class);
    }
}
