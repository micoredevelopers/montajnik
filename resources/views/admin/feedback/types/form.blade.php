<div class="row">
	<div class="col-6">
		@include('admin.partials.crud.default', ['name'=>'name','title'=>'Имя'])
	</div>
	<div class="col-6">
		@include('admin.partials.crud.elements.active')
	</div>
	<div class="col-6">
		@include('admin.partials.crud.date.date',['title'=>'Дата публикации','name'=>'published_at'])
{{--		@include('admin.partials.crud.default',['name'=>'url','title'=>'Ссылка'])--}}
	</div>

</div>
<div class="row">

	<div class="col-12">
		@include('admin.partials.crud.textarea',['name'=>'message','title'=>'Текст'])
	</div>
</div>

