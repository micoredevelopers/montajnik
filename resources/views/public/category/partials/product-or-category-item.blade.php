<?php /** @var $model \App\Models\Model */ ?>

@isset($model)
  @php
    $altKey = ($isProduct ?? false) ? 'product.product-alt' : 'category.category-alt';
    $alt = translateFormat($altKey, ['%название%' => $model->getLowerNameAttribute()]);
    $col = $cssCol ?? 'col';
    $url  = $url ?? '';
  @endphp
  <div class="{{ $col }}">
    <div class="category-advantages__item">
      <div>
        <a href="{{ $url }}" class="d-block">
          <img class="w-100 lazy" src="{{ checkImageWebp('') }}" data-src="{{ checkImageWebp($model->getImageAttribute()) }}" alt="{{ $alt }}" width="500" height="500">
        </a>
      </div>
      <div class="category-advantages-item__desc">
        <a href="{{ $url }}" class="text-center ">{{ $model->getNameAttribute() }}</a>
        <br/>
        <span class="category-advantages-item__price"
        >{{ $model->getSubNameAttribute() }}</span>
      </div>
    </div>
  </div>
@endisset
