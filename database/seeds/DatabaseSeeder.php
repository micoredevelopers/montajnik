<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		$this->call(LanguageTableSeeder::class);
		$this->call(UsersTableSeeder::class);
		$this->call(RolesAndPermissionsSeeder::class);
        $this->call(AdminMenuSeeder::class);
        $this->call(SliderTableSeeder::class);
		$this->call(CategorySeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(TranslateTableSeeder::class);
        $this->call(SliderCategoryTableSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(MenuGroupSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(FaqSeeder::class);
        $this->call(TestimonialTypeSeed::class);
        $this->call(TestimonialsTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(NewsLangTableSeeder::class);
        $this->call(MetaTableSeeder::class);
        $this->call(MetaLangTableSeeder::class);
        $this->call(PartnersTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ProductLangTableSeeder::class);
        $this->call(PricesTableSeeder::class);
        $this->call(PriceItemsTableSeeder::class);
        $this->call(WorksTableSeeder::class);
        $this->call(RedirectsTableSeeder::class);
    }
}
