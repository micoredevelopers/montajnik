<?php /** @var $category \App\Models\Category\Category */ ?>

<main id="category" class="category">
    @if (storageFileExists(imgPathOriginal($category->image)))
        <section class="main-page-top">
            @include('public._includes.slider', ['defaultImage' => imgPathOriginal($category->image)])
        </section>
    @endif

    <div class="category-advantages">
        <div class="container">
            <div class="breadcrumbs-category">
                @include('public.partials.breadcrumbs')
            </div>
        </div>
    </div>
    @isset($firstCategory)
        @include('public.category.includes.subcategories', ['chunkedCategories' => $firstCategory])
    @endisset

    @include('public.index.includes.advantages')


    @isset($secondCategories)
        @include('public.category.includes.subcategories', ['chunkedCategories' => $secondCategories])
    @endisset


    <section class="category-desc position-relative">
        <div id="particles-js"></div>
        <div class="container pt-4 py-lg-5 bg-white position-relative">
            <h1 class="text-center">{{ showMeta($category->getCategoryName()) }}</h1>

            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="decodeWrapper">
                        {!! $category->getDescriptionByIndex(0) !!}
                    </div>
                </div>
                <div class="col-lg-8 offset-lg-2">
                    @include('public.category.includes.prices')
                </div>
            </div>
        </div>
    </section>

    @include('public.category.includes.why-choose-us')

    @php
        $descriptionByIndex = $category->getDescriptionByIndex(1);
    @endphp
    @if($descriptionByIndex !== '')
        <section class="calc-block">
            <div class="container bg-white pt-4 py-lg-5">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2 ">
                        <div class="decodeWrapper">
                            {!! $descriptionByIndex !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @include('public.category.includes.calculators')

    @include('public.category.includes.work')

    {{--        --}}

    <section class="materials pb-3 pb-lg-5">
        <div class="container bg-white py-4">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 ">
                    @include('public.category.includes.partners')
                    <div class="decodeWrapper">
                        {!! $category->getDescriptionByIndex(2) !!}
                    </div>
                </div>
            </div>
            @include('public.category.includes.video-reviews')
            @include('public.category.includes.image-reviews')

        </div>
    </section>
</main>

@section('javascript')
    <script src="//cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
@stop

{!! editButtonAdmin(route(routeKey('category', 'edit'), $category->getPrimaryValue())) !!}
