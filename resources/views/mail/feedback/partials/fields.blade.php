
<?php /** @var $feedback \App\Models\Feedback */ ?>
@if ($feedback->getAttribute('name'))
	<tr>
		<td>{{ getTranslate('forms.name') }}:</td>
		<td>{{ $feedback->getAttribute('name') }}</td>
	</tr>
@endif
@if ($feedback->getAttribute('email'))
	<tr>
		<td>{{ getTranslate('forms.email') }}:</td>
		<td>{{ $feedback->email }}</td>
	</tr>
@endif
@if ($feedback->phone)
	<tr>
		<td>{{ getTranslate('forms.phone') }}:</td>
		<td>{{ $feedback->phone }}</td>
	</tr>
@endif
@if ($feedback->getAttribute('address'))
	<tr>
		<td>{{ getTranslate('forms.address') }}:</td>
		<td>{{ $feedback->getAttribute('address') }}</td>
	</tr>
@endif
@if ($feedback->getAttribute('message'))
	<tr>
		<td>{{ getTranslate('forms.message') }}:</td>
		<td>{{ $feedback->getAttribute('message') }}</td>
	</tr>
@endif
@if ($fields = $feedback->getAttribute('fields') AND is_array($fields))
	@foreach($fields as $name => $field)
		<tr>
			<td>{{ $name }}:</td>
			<td>{{ $field }}</td>
		</tr>
	@endforeach
@endif
