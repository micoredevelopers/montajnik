<?php


	namespace App\Calculators\Category\Ceilings;


	class CeilingsTypes
	{
		public static function getTypes(): array
		{
			return [
				CeilingsTypesContract::STANDART => 'Стандарт',
				CeilingsTypesContract::PREMIUM  => 'Премиум',
				CeilingsTypesContract::VIP      => 'VIP',
			];
		}

		public static function getTypesKeys(): array
		{
			return array_keys(self::getTypes());
		}

	}