<?php

namespace App\Models;

use App\Traits\EloquentMultipleForeignKeyUpdate;
use App\Traits\Models\BelongsToLanguage;

/**
 * App\Models\NewsLang
 *
 * @property int $news_id
 * @property string|null $name
 * @property int $language_id
 * @property string|null $description
 * @property string|null $except
 * @property array|null $video
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\News $news
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereExcept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereVideo($value)
 * @mixin \Eloquent
 */
class NewsLang extends Model
{
    use BelongsToLanguage;
	use EloquentMultipleForeignKeyUpdate;

	protected $guarded = [];

	protected $casts = [
		'video' => 'array'
	];

    protected $table = 'news_lang';

	public $incrementing = false;

	public $timestamps = false;

	protected $primaryKey = ['news_id', 'language_id'];

    public function news()
    {
        return $this->belongsTo(News::class);
	}

}
