<?php

use App\Traits\Migrations\CategoryForeignKey;
use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryPageTable extends Migration
{
    use CategoryForeignKey;
    use MigrationCreateFieldTypes;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_page', function (Blueprint $table) {
            $this->setTable($table);
            $table->bigIncrements('id');
            $this->setTable($table);
            $this->addForeignCategory();
        });

        Schema::create('category_page_lang', function (Blueprint $table) {
            $this->setTable($table);
            $table->bigIncrements('id');

            $table->unsignedBigInteger('category_page_id');
            $table->foreign('category_page_id')->references('id')->on('category_page')->onDelete('cascade');

            $table->text('description')->nullable();

           $this->createLanguageKey();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_page');
        Schema::dropIfExists('category_page_lang');

    }
}
