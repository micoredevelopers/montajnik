<?php

namespace App\Repositories;

use App\Models\Category\Category;
use App\Models\Category\Price;
use App\Models\Category\PriceItem;
use Illuminate\Support\Collection;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;

/**
 * Class PriceRepository.
 */
class PriceRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Price::class;
    }

    public function getDistinctUnits()
    {
        return PriceItem::distinct('unit')->get('unit');
    }

    public function getPriceByCategory(Category $category)
    {
        $priceWithItems = $category->load('price.items');
        return $priceWithItems;
    }

    public function all(array $columns = ['*'])
    {
        $this->with = ['items','category.lang'];
        $this->newQuery()->eagerLoad();

        $models = $this->query->get($columns);

        $this->unsetClauses();

        return $models;
    }

    public function getNotAssociatedCategories()
    {
        $categoryRepository = resolve(CategoryRepository::class);
//        $associatedCategories = Price::select('category_id')->pluck('category_id');
        $associatedCategories = [];
        $categories = $categoryRepository->with('lang')->all()->whereNotIn('id', $associatedCategories);
        return $categories;
    }

    public function findForEdit($id)
    {
        return Price::with('items')->find($id);
    }
}
