<main id="about">
    <section class="about">
        <div class="container">
            <div class="d-lg-flex flex-row-reverse justify-content-between align-items-center mb-35 mb-lg-50">
                @include('public.partials.breadcrumbs')
                <h1 class="title"><span class="decor-bg">{!! showMeta('О нас') !!}</span></h1>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <h2 class="about-title mb-3">
                        Оказываем широкий спектр услуг – от проектирования до
                        строительства, всех видов ремонта и дизайна интерьера
                    </h2>
                    <p class="text_opacity03 mb-40 about-text">
                        Компания «Монтажник» 15 лет на рынке. Производим ремонтные и
                        строительные работы в квартирах, частных домах, офисах, монтаж
                        натяжных потолков, остекление и утепление балконов по Одессе и за
                        пределами города
                    </p>
                </div>
                <div class="col-lg-5 offset-lg-2">
                    <div class="about-advantages mx-auto">
                        <h3 class="about-small-title">Прозрачная смета</h3>
                        <p class="mb-40 about-small-text">
                            Составляем подробную смету – описываем все виды работ и
                            количество материала
                        </p>
                        <h3 class="about-small-title">Заключение договора</h3>
                        <p class=" mb-40 about-small-text">
                            Фиксируем обязательства сторон и стоимость работ, которая не
                            меняется до сдачи объекта
                        </p>
                        <h3 class="about-small-title ">Гарантия качества</h3>
                        <p class=" mb-40 about-small-text">
                            Даем официальную гарантию на материалы и выполненную работу
                        </p>
                    </div>
                </div>
                <div class="col-lg-11 offset-lg-1 position-relative">
                    <div class="decor-box-1"></div>
                    <div class="decor-box-2"></div>
                    <img class="w-100 lazy about-advantages-img " data-src="./img/photo_.jpg" src="" alt=""/>
                </div>
            </div>
        </div>
    </section>
    <section class="about-desc">
        <div class="container-fluid">
            <h4 class="about-desc-sub-title text-center">Компания «Монтажник»</h4>
            <h2 class="about-desc-title text-center">Индивидуальный подход к вашему заказу</h2>
            <p class="about-text-decs about-desc-text text-center">
                Не работаем шаблонно. Учитываем пожелания заказчика и важные
                технические моменты, предлагаем несколько вариантов. Совместно с
                клиентом воплощаем в жизнь уникальные и продуманные проекты.
            </p>
        </div>
    </section>
    <section class="about-desc-second">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="about-desc-second-img-box mb-40">
                        <img class="w-100 lazy" data-src="./img/tools.png" alt=""/>
                    </div>
                </div>
                <div
                        class="offset-lg-1 col-lg-5 d-flex flex-column justify-content-center"
                >
                    <h2 class="about-title-medium mb-3">Командная работа</h2>
                    <p class="about-text-decs">
                        Над каждым проектом работает штат специалистов – инженер-
                        проектировщик, дизайнер, строители. Они согласовывают все этапы
                        работ. Наша задача – осуществить строительство или ремонт объекта с
                        учетом технических особенностей и нюансов.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="docs">
        <div class="container">
            <div class="text-center">
                <h2 class="title mb-2 mb-lg-4 text-center">
                    <span class="decor-bg">Документы</span>
                </h2>
            </div>
            <p class="docs-desc mb-3 mb-lg-4">
                {{getTranslate('about.documents-title')}}
            </p>
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="docs-slider ">
                        @foreach(settingFiles('about.certificates') as $file)
                            <div class="docs-slider__item">
                                <div class="docs-img-wrap">
                                    <a href="{{ $file['download_link'] }}" class="fancybox" data-fancybox="about-certificates">
                                        <img src="{{ $file['download_link'] }}" class="img-fluid"
                                             alt="Сертификат, {{ $file['original_name'] }}"/>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
