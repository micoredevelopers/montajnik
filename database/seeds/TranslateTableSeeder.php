<?php

use Illuminate\Database\Seeder;
use \App\Models\Translate;
use \App\Models\TranslateLang;

class TranslateTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $groups = [
            'global' => [
                ['key' => 'show-more', 'value' => 'Показать еще'],
                ['key' => 'back', 'value' => 'назад'],
                ['key' => 'yes', 'value' => 'Да'],
                ['key' => 'no', 'value' => 'Нет'],
                ['key' => 'download', 'value' => 'Скачать'],
                ['key' => '404', 'value' => 'Страница не найдена'],
                ['key' => '404-description', 'value' => 'Страница не найдена, попробуйте другой адресс', 'type' => 'textarea'],
                'view-details' => 'Подробнее',
                'see-more'     => 'Показать еще',
                'work-hours'   => 'График работы:',
            ],

            'forms' => [
                'name'    => 'Имя',
                'phone'   => 'Телефон',
                'email'   => 'E-mail',
                'message' => 'Текст соообщения',
                'send'    => 'Отправить заявку',
            ],
            'pages' => [
            ],

            'about' => [
                'documents-title' => 'Автошкола Мартин - это школа нового формата',
            ],

            'product'  => [
                'product-alt' => 'Товар %название%, фото',
            ],
            'category' => [
                'categories'       => 'Услуги',
                'price-head'       => 'Цены на %категория% в Одессе',
                'our-partner'      => 'Наш партнер %партнер%',
                'our-partner-alt'  => 'Наш партнер %партнер%, фото',
                'category-alt'     => 'Категория %название%, фото',
                'slide-alt'        => 'Слайд %название%',
                'partners-title'   => 'Мы используем только сертифицированные материалы ведущих фирм:',
                'adwantages-title' => 'Преимущества',
            ],

            'feedback' => [
                'throlle-message' => 'Отправлять заявку можно не чаще 1 раза в минуту.',
                'send-success'    => 'Заявка успешно отправлена',
                'send-failed'     => 'Заявка не была отправлена, произошла ошибка на сервере, пожалуйста попробуйте позже, или позвоните нам по одному из номеров телефона',
            ],

            'faq'  => [
                'faq'   => 'Вопросы и ответы',
                'title' => 'Ответы специалистов фирмы Монтажник на типичные вопросы клиентов.',
            ],
            'news' => [
                'news'                   => 'Новости',
                'news-all'               => 'Все новости',
                'new-single'             => 'Новость %name%',
                'new-detail'             => 'Подробнее',
                'image-alt'              => 'Новость %name%, фото',
                'prev'                   => 'Предыдущая новость',
                'next'                   => 'Следующая новость',
                'meta_title'             => 'Новости',
                'meta_title_paged'       => 'Страница %page% - Новости',
                'meta_description'       => 'Ремонт квартир Одесса - Новости【Монтажник】► Выезд мастера бесплатный ◀ Звоните ✆ 38 (093) 223-12-37',
                'meta_description_paged' => 'Страница %page% - Ремонт квартир Одесса - Новости【Монтажник】► Выезд мастера бесплатный ◀ Звоните ✆ 38 (093) 223-12-37',
            ],

            'testimonials' => [
                'testimonials'           => 'Отзывы',
                'send-failed'            => 'Заявка не была отправлена, пожалуйста попробуйте позже',
                'send-success'           => 'Ваш отзыв успешно отправлен, и будет добавлен после проверки',
                'no-category'            => 'Категория не указана',
                'image-alt'              => 'Отзыв от %name%, фото',
                'video-from-name'        => 'Видео отзыв от %name%',
                'category-empty'         => 'Отзывы для данной услуги в данный момент отсутствуют.',
                'meta_title'             => 'Отзывы о строительной компании Монтажник',
                'meta_title_paged'       => 'Страница %page% - Честные отзывы про строительную компанию Монтажник',
                'meta_description'       => 'Честные отзывы про строительную компанию Монтажник. Открыты к критике, клиенты превыше всего. Ждем справедливые отзывы - montajnik.od.ua',
                'meta_description_paged' => 'Страница %page% - Честные отзывы про строительную компанию Монтажник. Открыты к критике, клиенты превыше всего. Ждем справедливые отзывы - montajnik.od.ua',
            ],
            'work'         => [
                'works'            => 'Наши работы',
                'work'             => 'Наши работы - %category%, фото %number%',
                'all-works'        => 'Все работы',
                'meta_title'       => 'Наши работы: %name% - строительство, ремонт домов и квартир «под ключ» в Одессе',
                'meta_description' => 'Ремонт квартир Одесса — Наши работы %name%【Монтажник】► Выезд мастера бесплатный ◀ Звоните ✆ 38 (093) 223-12-37',
                ['key' => 'all-description', 'value' => 'Тут общее описание для всех работ', 'type' => 'textarea',],
            ],
        ];
        $languages = \App\Models\Language::all();
        foreach ($groups as $groupName => $group) {
            foreach ($group as $key => $item) {
                if (!is_array($item)) {
                    $item = ['key' => $key, 'value' => $item];
                }
                $item['group'] = $groupName;
                $item['key'] = implode('.', [$groupName, $item['key']]);
                if ($translate = Translate::where('key', $item['key'])->first()) {
                    continue;
                }
                ($translate = new Translate())->fillExisting($item)->save();

                foreach ($languages as $lang) {
                    ($translateLang = new TranslateLang())->fillExisting($item)
                        ->translate()->associate($translate)
                        ->language()->associate($lang)
                        ->save();
                }
            }
        }
    }

    private function getBuilder(?string $key = null) {
        $builder = new class implements \ArrayAccess {
            private $key = '';

            private $value = '';

            private $type = 'text';

            private $displayName = '';

            /** @var array */
            private $variables = [];

            public function setKey(string $key): self {
                $this->key = $key;
                return $this;
            }

            public function setTypeText(): self {
                $this->type = Translate::TYPE_TEXT;
                return $this;
            }

            public function setTypeTextarea(): self {
                $this->type = Translate::TYPE_TEXTAREA;
                return $this;
            }

            public function setTypeEditor(): self {
                $this->type = Translate::TYPE_EDITOR;
                return $this;
            }

            public function setValue(?string $value = null): self {
                $this->value = $value;
                return $this;
            }

            public function setDisplayName(string $displayName): self {
                $this->displayName = $displayName;
                return $this;
            }

            /**
             * @param array $variables
             */
            public function setVariables(array $variables): self {
                $this->variables = $variables;
                return $this;
            }

            public function build() {
                return [
                    'key'          => $this->key,
                    'value'        => $this->value,
                    'type'         => $this->type,
                    'display_name' => $this->displayName,
                    'variables'    => $this->variables,
                ];
            }

            public function offsetGet($offset) {
                return $this->offsetExists($offset) ? $this->{$offset} : null;
            }

            public function offsetExists($offset) {
                return property_exists($this, $offset);
            }

            public function offsetSet($offset, $value) {
                $method = Str::camel('set' . ucfirst($offset));
                if (method_exists($this, $method)) {
                    $this->$method($value);
                }
            }

            public function offsetUnset($offset) {
                if ($this->offsetExists($offset)) {
                    $this->{$offset} = null;
                }
            }

        };
        if ($key) {
            $builder->setKey($key);
        }
        return $builder;
    }
}
