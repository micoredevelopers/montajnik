<?php

namespace App\Repositories;

use App\Models\Testimonial;
use Illuminate\Support\Collection;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;

/**
 * Class TestimonialRepository.
 */
class TestimonialRepository extends BaseRepository
{
    protected $with = ['category.lang', 'images'];

    private $scopesInit = false;

    private function initScopes()
    {
        if (!$this->scopesInit) {
            Testimonial::initScopesPublic();
            $this->scopesInit = 1;
        }
    }

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Testimonial::class;
    }

    public function getTestimonialsForMainPage(int $type): Collection
    {
        $this->initScopes();
        $testimonials = Testimonial::with($this->with)->where('type_id', $type)->orderByDesc('date')->limit(20)->get();
        return $testimonials;
    }
}
