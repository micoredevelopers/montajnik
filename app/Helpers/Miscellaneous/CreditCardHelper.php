<?php


namespace App\Helpers\Miscellaneous;


class CreditCardHelper
{
	public static function getMonths()
	{
		$months = array_map(function ($item) {
			if ($item < 10)
				$item = 0 . $item;
			return (string)$item;
		}, range(1, 12));
		return $months;
	}

	public static function getYearsExpire($max = 13, $from = null)
	{
		$curYearSmall = $from ?? now()->format('y');
		$expiresYears = range($curYearSmall, $curYearSmall + 13);
		return $expiresYears;
	}

}
