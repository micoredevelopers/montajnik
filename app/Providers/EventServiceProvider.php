<?php

namespace App\Providers;

use App\Events\Admin\ArticlesChangedEvent;
use App\Events\Admin\CategoriesChanged;
use App\Events\Admin\FaqChangedEvent;
use App\Events\Admin\Image\PathReplacedEvent;
use App\Events\Admin\ImageUploaded;
use App\Events\Admin\MenusChanged;
use App\Events\Admin\MultipleImageUploaded;
use App\Events\Admin\NewsChangedEvent;
use App\Events\FeedbackSubmittedEvent;
use App\Events\TestimonialSubmittedEvent;
use App\Listeners\Admin\Article\ArticlesRebuildNextPrevListener;
use App\Listeners\Admin\Article\FaqRebuildNextPrevListener;
use App\Listeners\Admin\Category\DropCategoryCache;
use App\Listeners\Admin\Image\PathReplacedListener;
use App\Listeners\Admin\ImageUploadedListener;
use App\Listeners\Admin\MultipleImageUploadedListener;
use App\Listeners\Admin\News\NewsRebuildNextPrevListener;
use App\Listeners\Admin\Menu\DropMenuCache;
use App\Listeners\Admin\User\IpChecker;
use App\Listeners\FeedbackSendEmailListener;
use App\Listeners\TestimonialSendEmailListener;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
	/**
	 * The event listener mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		Registered::class                => [
			SendEmailVerificationNotification::class,
		],
		Login::class                     => [
			IpChecker::class,
		],
		MenusChanged::class              => [
			DropMenuCache::class,
		],
		CategoriesChanged::class         => [
			DropCategoryCache::class,
		],
		NewsChangedEvent::class          => [
			NewsRebuildNextPrevListener::class,
		],
		ArticlesChangedEvent::class      => [
			ArticlesRebuildNextPrevListener::class,
		],
		FaqChangedEvent::class           => [
			FaqRebuildNextPrevListener::class,
		],
		TestimonialSubmittedEvent::class => [
			TestimonialSendEmailListener::class,
		],
		FeedbackSubmittedEvent::class    => [
			FeedbackSendEmailListener::class,
		],
		ImageUploaded::class             => [
			ImageUploadedListener::class,
		],
		MultipleImageUploaded::class     => [
			MultipleImageUploadedListener::class,
		],
		PathReplacedEvent::class         => [
			PathReplacedListener::class,
		],
	];

	/**
	 * Register any events for your application.
	 *
	 * @return void
	 */
	public function boot()
	{
		parent::boot();

		//
	}
}
