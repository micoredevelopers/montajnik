<?php

namespace App\Http\Controllers\User;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\SiteController;
use App\Http\Requests\User\UserCredentialsRequest;
use App\Http\Requests\User\UserProfileRequest;
use App\Models\User\UserField;
use App\User;


class UserController extends SiteController
{
	public function __construct()
	{
		parent::__construct();

		$this->addBreadCrumb(getTranslate('users.cabinet'), route('user.index'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$vars = [];
		$data['content'] = view('public.user.index')->with($vars);
		return $this->main($data);
	}

	public function profile()
	{
		$this->addBreadCrumb(getTranslate('users.profile'), route('user.profile'));
		$user = \Auth::user();
		$data['content'] = view('public.user.profile')->with(compact('user'));
		return $this->main($data);
	}

	public function credentials()
	{
		$this->addBreadCrumb(getTranslate('users.profile'), route('user.profile'));
		$this->addBreadCrumb(getTranslate('users.changing-credentials'));
		$data['content'] = view('public.user.credentials')->with(['user' => \Auth::user()]);
		return $this->main($data);
	}

	public function modify()
	{
		$data['content'] = view('public.user.modify')->with([]);
		return $this->main($data);
	}

	/**
	 * @param UserCredentialsRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function updateCredentials(UserCredentialsRequest $request)
	{
		/** @var $user User */
		$user = $request->getUser();
		$this->setMessage(getTranslate('users.current-password-invalid'));
		$passwordsMatches = \Hash::check($request->get('password'), $user->password);
		if ($passwordsMatches) {
			if ($request->isEmailWasChanged()) {
				$user->setAttribute('email', $request->get('email'));
			}
			if ($request->isOnlyNewPasswordsWasSend()){
				$user->setAttribute('password', \Hash::make($request->get('password_new')));
			}
			if ($user->save()) {
				if ($request->isEmailWasChanged() OR $request->isPasswordsWasSend()) {
					//re login user if credentials changed
					\Auth::guard()->login($user);
				}
				$this->setStatus(true);
				$this->setMessage(getTranslate('users.credentials-updated'));
			}
		}

		if ($request->expectsJson()	){
			return response()->json($this->getResponseMessageForJson());
		}

		return redirect()->back()->with($this->getResponseMessage());
	}

	public function updateProfile(UserProfileRequest $request)
	{
		/** @var $user  User */
		$user = $request->getUser();
		$fields = $user->isCompany() ? UserField::getFieldsCompany() : UserField::getFieldsPersonal();
		$input = $request->only($fields);
		if ($user->fields->fillExisting($input)->save()) {
			$this->setStatus(true);
			$this->setMessage(getTranslate('users.save-profile-success'));
		}
		return redirect()->back()->with($this->getResponseMessage());
	}


}
