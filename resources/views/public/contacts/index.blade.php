<main id="contacts" >
	<section class="contacts">
		<div class="container">
			<div class="d-lg-flex flex-row-reverse justify-content-between align-items-center mb-30 mb-lg-50">
				@include('public.partials.breadcrumbs')
				<h1 class="title"><span class="decor-bg">{!! showMeta('Контакты') !!}</span></h1>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<h3 class="contacts-title">Адрес:</h3>
					<p class="contacts-text mb-35">
						{{ getSetting('global.address') }}
						<br />
						2 павильон слева от главного входа
					</p>
					<h3 class="contacts-title">Время работы:</h3>
					<p class="contacts-text mb-35">
						{{ getSetting('global.work-hours') }}
						<br />
						выходной - воскресенье
					</p>
					<h3 class="contacts-title">Телефоны:</h3>
					<div class="d-flex flex-column mb-35">
						<a class="contacts-phone" href="tel:+{{ formatTel( getSetting('contacts.phone-intretelecom') ) }}">{{ getSetting('contacts.phone-intretelecom') }}</a>
						<a class="contacts-phone" href="tel:+{{ formatTel( getSetting('contacts.phone-lifecell') ) }}">{{ getSetting('contacts.phone-lifecell') }}</a>
						<a class="contacts-phone" href="tel:+{{ formatTel( getSetting('contacts.phone-vodafone') ) }}">{{ getSetting('contacts.phone-vodafone') }}</a>
						<a class="contacts-phone " href="tel:+{{ formatTel( getSetting('contacts.phone-kyivstar') ) }}">{{ getSetting('contacts.phone-kyivstar') }}</a>
					</div>
					<h3 class="contacts-title">E-mail:</h3>
					<a class="contacts-phone " href="mailto:{{ getSetting('contacts.email') }}">{{ getSetting('contacts.email') }}</a>
				</div>
				<div class="col-lg-8 p-0">
					{!! getSetting('pages.contact_map') !!}
				</div>
			</div>
		</div>
	</section>
</main>
{{--					<iframe--}}
{{--						src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3295.224488905731!2d30.722573493710875!3d46.39314505958635!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c634a8868c0bcf%3A0xf37f668f5868f00!2zNDNBLCDRg9C7LiDQkNC60LDQtNC10LzQuNC60LAg0JrQvtGA0L7Qu9GR0LLQsCwgNDPQkCwg0J7QtNC10YHRgdCwLCDQntC00LXRgdGB0LrQsNGPINC-0LHQu9Cw0YHRgtGMLCA2NTAwMA!5e0!3m2!1sru!2sua!4v1564387598673!5m2!1sru!2sua"--}}
{{--						class="contacts-map"--}}
{{--						frameborder="0"--}}
{{--						style="border:0"--}}
{{--						allowfullscreen--}}
{{--					></iframe>--}}