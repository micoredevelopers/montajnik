<?php

namespace App\Models\Category;


use App\Contracts\HasLocalized;
use App\Models\Model;
use App\Traits\Models\Relations\Category\BelongsToCategory;

/**
 * App\Models\Category\CategoryPage
 *
 * @property int $id
 * @property int|null $category_id
 * @property-read \App\Models\Category\Category|null $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\CategoryPage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\CategoryPage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\CategoryPage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\CategoryPage whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\CategoryPage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class CategoryPage extends Model implements HasLocalized
{
    use BelongsToCategory;

    protected $hasOneLangArguments = [CategoryPageLang::class, 'category_page_id', 'id'];

    protected $table = 'category_page';

    public $timestamps = false;

}
