@php
/**
 * @var $sliderItem     App\Models\Slider\SliderItem
 * @var $sliderItemLang App\Models\Slider\SliderItemLang
 * @var $slider         App\Models\Slider\Slider
 */
@endphp
@php
  $defaultImage = checkImageWebp($defaultImage ?? '', 'img/slider-img.png');
@endphp

<div class="top-slider-container">
  <div class="top-slider">
    @if ($slider && $slider->getValidItems()->isNotEmpty())
      @foreach($slider->items as $sliderItem)
        @php
          $src = $loop->first ? checkImageWebp($sliderItem->src) : checkImageWebp('');
        @endphp
        @if (storageFileExists($sliderItem->src))
          <div class="top-slider__item_wrap">
            <div class="top-slider__item">
              @if ($sliderItemLang = $sliderItem->lang)
                <img class="top-slider-bg"
                     src="{{ $src }}"
                     @if (!$loop->first) data-lazy="{{ checkImageWebp($sliderItem->src) }}"
                     @endif
                     alt="{{ translateFormat('category.slide-alt', ['%название%' => $sliderItemLang->name,]) }}"/>
                <div class="container">
                  <div class="row">
                    <div class="col-lg-5 d-flex flex-column justify-content-center">
                      <h2 class="top-slider-title">{{ $sliderItemLang->getNameAttribute() }}</h2>
                      <div class="slide-desc">{!! $sliderItemLang->description !!}</div>
                      @if ($sliderItem->link)
                        <a href="{{ $sliderItem->link }}" class="btn-custom">Подробнее</a>
                      @endif
                    </div>
                  </div>
                </div>
              @endif
            </div>
          </div>
        @endif
      @endforeach
    @else
      <div class="top-slider__item_wrap">
        <div class="top-slider__item">
          <img class="top-slider-bg"
               src="{{ $defaultImage }}"/>
          <div class="container">
            <div class="row">
              <div class="col-lg-5 d-flex flex-column justify-content-center"></div>
            </div>
          </div>
        </div>
      </div>
    @endif
  </div>
  <div class="top-slider-navigation-box">
    <div class="container d-flex justify-content-center justify-content-lg-start align-items-center">
      <div class="top-slider-arrow top-slider-arrow_prev">
        <img class="svg d-block" src="{{ asset('img/mp/arrow-white.svg') }}" alt="" width="7" height="12">
      </div>
      <div class="top-slider-dots__wrap"></div>
      <div class="top-slider-arrow top-slider-arrow_next">
        <img class="svg d-block" src="{{ asset('img/mp/arrow-white.svg') }}" alt="" width="7" height="12">
      </div>
    </div>
  </div>
</div>
