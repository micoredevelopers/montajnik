<?php /** @var $testimonialsImage \Illuminate\Support\Collection */ ?>
<?php use App\Models\Testimonial;

/** @var $testimonial Testimonial */ ?>
@if ($testimonialsImage->isNotEmpty())
  <section class="reviews">
    <div class="container">
      <div class="text-center">
        <h2 class="title text-center mb-25">
          <span class="decor-bg">Отзывы</span>
        </h2>
      </div>
      <div class="reviews-slider">
        @foreach($testimonialsImage as $testimonial)
          @php
            $image = ($testimonial->images->first()->image ?? '');
            $category = $testimonial->category ? $testimonial->category->getCategoryName() : getTranslate('testimonials.no-category');
          @endphp
          <div class="reviews-slider__item">
            <div class="reviews-slider__item_shadow">
              <div class="reviews-slider__img ">
                <img class="w-100 lazy" src="{{ checkImageWebp('') }}" data-lazy="{{ checkImageWebp($image) }}"
                     alt="{{ translateFormat('testimonials.image-alt', ['%name%' => $testimonial->name]) }}"/>
              </div>
              <div class="reviews-slider__desc">
                <div>
                  <h4 class="reviews-slider__name mb-25">{{ $testimonial->name }}</h4>
                  <p class="mb-20">{{ $testimonial->message }}</p>
                </div>
                <div class="d-flex justify-content-between">
                  <span class="text_light text_opacity03">{{ $category }}</span>
                  <span class="text_light text_opacity03">{{ $testimonial->date->format('d.m.y') }}</span>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
      <a href="" class="btn-custom mx-auto" data-toggle="modal" data-target="#feedBackModal">Оставить отзыв</a>
    </div>
  </section>
@endif