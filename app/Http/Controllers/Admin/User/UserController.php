<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests\UserProfileRequest;
use App\Permission;
use App\Role;
use App\Traits\Authorizable;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use \Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class UserController extends AdminController
{
    use Authorizable;

    protected $routeKey = 'admin.users';

    protected $permissionKey = 'users';

    protected $key = 'users';

    protected $name;

    public function __construct()
    {
        parent::__construct();
        $this->name = __('modules.users.title');
        $this->addBreadCrumb(__('modules.users.title'), $this->resourceRoute('index'));
        $this->shareViewModuleData();
    }

    public function index()
    {
        $title = $this->name;
        $this->setTitle($title);
        $query = User::getUsersQuery()->with('roles')->latest();
        $search = '';
        if (Input::has('search')) {
            $search = Input::get('search');
            $query->where(function (Builder $builder) use ($search) {
                $builder->where('name', 'like', '%' . $search . '%')
                    ->orWhere('email', 'like', '%' . $search . '%');
            });
        }
        $result = $query->paginate(30);
        $data['content'] = view('admin.user.index', compact('result', 'search'));

        return $this->main($data);
    }

    public function create(User $user)
    {
        $title = __('form.create');
        $this->setTitle($title)->addBreadCrumb($title);
        $roles = Role::pluck('name', 'id');
        $data['content'] = view('admin.user.create', compact('roles'));

        return $this->main($data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'     => 'bail|required|min:2',
            'email'    => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);
        $this->setSuccessStore();
        $request->merge(['password' => bcrypt($request->get('password'))]);
        // Create the user
        $input = $request->except('roles', 'permissions');
        $input['active'] = (int)$request->get('active');
        $input['percent'] = (int)$request->get('percent');

        if (($user = new User)->fillExisting($input)->save()) {
            $this->syncPermissions($request, $user);
        }
        if ($request->has('createOpen')) {
            return redirect($this->resourceRoute('edit', $user->id))->with($this->getResponseMessage());
        }

        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
    }

    public function edit($id)
    {
        $edit = User::findUserOrFail($id);
        $title = $this->titleEdit($edit);
        $this->addBreadCrumb($title)->setTitle($title);
        $roles = Role::pluck('name', 'id');
        $permissions = Permission::getList();
        $data['content'] = view('admin.user.edit', compact('edit', 'roles', 'permissions'));

        return $this->main($data);
    }

    public function update(Request $request, $id)
    {
        $this->setMessage(__('generic.successfully_updated'));
        $this->validate($request, [
            'name'  => 'bail|required|min:2',
            'email' => 'required|email|unique:users,email,' . $id,
        ]);
        // Get the user
        $user = User::findUserOrFail($id);
        // Update user
        $input = $request->except('roles', 'permissions', 'password');
        $input['active'] = (int)$request->get('active');
        $input['percent'] = (int)$request->get('percent');
        $user->fillExisting($input);
        if ($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }
        $this->syncPermissions($request, $user);
        if ($user->save()) {
            $this->setSuccessUpdate();
        }

        if ($request->has('saveClose')) {
            return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
        }

        return redirect()->back()->with($this->getResponseMessage());
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        if (Auth::user()->id == $id) {
            $this->setFailMessage('Deletion of currently logged in user is not allowed :(');

            return redirect()->back();
        }
        if (User::findUserOrFail($id)->delete()) {
            $this->setSuccessMessage('User has been deleted');
        } else {
            $this->setFailMessage('User not deleted');
        }

        return redirect()->back()->with($this->getResponseMessage());
    }

    private function syncPermissions(Request $request, $user)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);
        // Get the roles
        $roles = Role::find($roles);
        // check for current role changes
        if (!$user->hasAllRoles($roles)) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }
        $user->syncRoles($roles);

        return $user;
    }

    protected function dropCache()
    {
        \Artisan::call('permission:cache-reset');
    }

    public function profile()
    {
        $locales = \LaravelLocalization::getSupportedLocales();
        $this->dropLastBreadCrumb();
        $title = __('modules.users.profile.title');
        $this->addBreadCrumb($title)->setTitle($title);
        $data['content'] = view('admin.user.profile', [
            'user'    => \Illuminate\Support\Facades\Auth::user(),
            'locales' => $locales,
        ]);
        return $this->main($data);
    }

    /**
     * @param UserProfileRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function profileUpdate(UserProfileRequest $request)
    {
        /** @var $user User */
        $user = \Auth::user();
        if ($request->isPasswordsWasSend()) {
            $passwordsMatches = Hash::check($request->get('password'), $user->password);
            if ($passwordsMatches) {
                $user->password = Hash::make($request->get('password_new'));
            } else {
                $this->setMessage(__('user.invalid-current-password'));
                return back()->with($this->getResponseMessage())->withInput($request->input());
            }
        }
        $this->setSuccessUpdate();
        $data = $request->only([
            'locale',
            'name',
        ]);
        $user->fillExisting($data)->save();

        return redirect()->back()->with($this->getResponseMessage());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function signSuperAdmin(Request $request)
    {
        $this->authorize('superadmin');
        if ($userId = (int)$request->get('user_id')) {
            if ($user = User::find($userId)) {
                session()->flash('superadmin-login');
                \Auth::guard()->login($user);
            }
        }
        return redirect()->back();
    }
}
