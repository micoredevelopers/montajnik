<?php

use App\Traits\Migrations\CategoryForeignKey;
use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateWorksTable extends Migration
{
    use MigrationCreateFieldTypes;
    use CategoryForeignKey;
    protected $table = 'works';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->setTable($table);
            $table->increments('id');

            $this->createActive();
            $this->createImage();
            $this->createUniqueUrl();
            $this->createSort();
            $this->createVideo();

            $this->addForeignCategory();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
