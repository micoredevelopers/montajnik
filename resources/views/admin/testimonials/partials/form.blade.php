<div class="row">
	<div class="col-6">
		@include('admin.partials.crud.default', ['name'=>'name','title'=>'Имя'])
	</div>
	<div class="col-6">
		@include('admin.partials.crud.elements.phone')
	</div>
	<div class="col-6">
		@include('admin.partials.crud.elements.email')
	</div>
</div>
<div class="row">
	<div class="col-6">
		@include('admin.partials.crud.elements.active')
	</div>
	<div class="col-6">
		@include('admin.partials.crud.date.date')
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<label for="type">Тип отзыва</label>
		<select name="type_id" id="type" class="form-control selectpicker" title="Тип отзыва" required autocomplete="off">
			@php
				$typeId = $edit->type_id ?? null
			@endphp
			@foreach($types as $type)
				<option {!! selectedIfTrue($type->id == $typeId) !!} value="{{$type->id}}">{{$type->name}}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-6">
		<label for="category">Категория</label>
		<select name="category_id" id="category" data-live-search="true" class="form-control selectpicker" title="Категория" autocomplete="off"	>
			@php
				$editCategoryId = (int)($edit->category_id ?? null);
			@endphp
			@foreach($categories as $category)
				<option {!! selectedIfTrue((int)$category->id === $editCategoryId) !!} value="{{$category->id}}">{{$category->getCategoryName()}}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="row">

	<div class="col-12">
		@include('admin.partials.crud.textarea',['name'=>'message','title'=>'Текст'])
	</div>
</div>

