<?php

namespace App\Http\Controllers\Admin;

use App\Traits\Authorizable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OtherController extends AdminController
{
	use Authorizable;
	protected $tb;
	private $name = 'Прочие настройки';
	private $controller = 'other';

	protected $key = 'other';

	protected $permissionKey = 'other';

	protected $routeKey = 'admin.other';

	public function __construct()
	{
		parent::__construct();
		$this->tb = 'other';
		$this->addBreadCrumb($this->name, route($this->tb . '.index'));
	}

	public function index()
	{
		$vars['list'] = [
			[
				'name' => 'Карта сайта',
				'url'  => route(routeKey('sitemap')),
			],
			[
				'name' => 'Robots.txt',
				'url'  => route(routeKey('robots')),
			],

		];
		$vars['controller'] = $this->controller;
		$data['content'] = view('admin.' . $this->tb . '.index', $vars);
		return $this->main($data);
	}
}
