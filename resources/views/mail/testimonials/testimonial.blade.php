@extends('mail.feedback.layout')

@section('title')
    Новый отзыв
@stop

@section('content')

<?php /** @var $testimonial \App\Models\Testimonial */ ?>
<table class="table table-striped">
    <tbody>
    <tr class="small-12 columns">
        <td>{{ getTranslate('forms.name') }}:</td>
        <td>{{ $testimonial->getAttribute('name') }}</td>
    </tr>
    @if ($testimonial->getAttribute('email'))
        <tr class="small-12 large-12 columns first last">
            <td>{{ getTranslate('forms.email') }}:</td>
            <td>{{ $testimonial->getAttribute('email') }}</td>
        </tr>
    @endif
    @if ($testimonial->getAttribute('phone'))
        <tr class="small-12 large-12 columns first last">
            <td>{{ getTranslate('forms.phone') }}:</td>
            <td>{{ $testimonial->getAttribute('phone') }}</td>
        </tr>
    @endif
    @if ($testimonial->getAttribute('comment'))
        <tr class="small-12 large-12 columns first last">
            <td>{{ getTranslate('forms.comment') }}:</td>
            <td>{{ $testimonial->getAttribute('comment') }}</td>
        </tr>
    @endif
    </tbody>
</table>
    <h5 style="text-align: center"><a href="{{ route('admin.testimonials.index') }}">Отзывы</a></h5>
@stop
