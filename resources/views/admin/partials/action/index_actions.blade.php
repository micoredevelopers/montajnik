@php
    $routeKey = $routeKey ?? $key ?? '';
	$permissionKey = $permissionKey ?? $key ?? '';
	$canEdit = $canEdit ?? Gate::allows('edit_'. $permissionKey);
	$canDelete = $canDelete ?? Gate::allows('delete_'. $permissionKey);
@endphp
<div class="dropdown menu_drop">
	<button
		class="btn btn-secondary dropdown-toggle" type="button"
		id="dropdownMenuButton_{{ $item->id }}" data-toggle="dropdown"
		aria-haspopup="true" aria-expanded="false">
		<i class="material-icons">menu</i>
	</button>
	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton_{{ $item->id }}">
		{{--<form class="dropdown-item p-0" href="#">--}}
		{{--<button class="drop_menu_button text-left"--}}
		{{--type="submit">@lang('form.disable-disable')</button>--}}
		{{--</form>--}}
		@if($canEdit)
			<a href="{{ route($routeKey.'.edit',  $item->id)  }}"
			   class="dropdown-item">{{ __('form.edit') }}</a>
		@endif
		@if($canDelete)
			{!! Form::open( [
			'method' => 'delete',
			 'url' => route($routeKey.'.destroy', $item->id),
			  'onSubmit' => 'return confirm("'. __('form.delete-confirm') .'")'
			  ]) !!}
			<button type="submit" class="dropdown-item">@lang('form.delete')</button>
			{!! Form::close() !!}
		@endif
	</div>
</div>

{{-- todo remake route $key to $routeKey--}}