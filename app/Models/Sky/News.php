<?php

namespace App\Models\Sky;

use Illuminate\Support\Collection;

/**
 * App\Models\Sky\News
 *
 * @property int $id
 * @property int|null $sort
 * @property string|null $active
 * @property string|null $url
 * @property string $photo
 * @property string|null $date_add
 * @property int $catalog_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\News newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\News newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\AbstractSkyModel onlyMain()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\News query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\News whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\News whereCatalogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\News whereDateAdd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\News wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\News whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\News whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\News withLang()
 * @mixin \Eloquent
 */
class News extends AbstractSkyModel
{
    protected $unsanitizeFields = [
        'body'
    ];

    protected $replaceAttributesKeys = [
        'body'     => 'description',
        'body_m'   => 'except',
        'photo'    => 'image',
        'date_add' => 'published_at',
    ];

    public $timestamps = false;

    public function remake()
    {
        $comments = self::WithLang()->get();
        $this->unsanitizeList($comments);
    }

    public function getList(): Collection
    {
        $news = self::WithLang()->get();

        $this->unsanitizeList($news);
        $this->remakeColumnsName($news, false);
        return $news;
    }

    public function scopeWithLang($q)
    {
        return $q->leftJoin('ru_news', 'news.id', 'ru_news.news_id');
    }

}
