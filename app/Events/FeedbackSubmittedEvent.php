<?php

namespace App\Events;

use App\Models\Feedback;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class FeedbackSubmittedEvent
{
	use Dispatchable, SerializesModels;
	/** @var Feedback */
	private $feedback;

	/**
	 * Create a new event instance.
	 *
	 * @param Feedback $feedback
	 */
	public function __construct(Feedback $feedback)
	{
		$this->feedback = $feedback;
	}

	public function getFeedback(): Feedback
	{
		return $this->feedback;
	}

}
