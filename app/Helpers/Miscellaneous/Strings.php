<?php

namespace App\Helpers\Miscellaneous;

class Strings
{
    static function deleteDuplicatedChars($string, $char_to_delete)
    {
        $word = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $string[$i] == $char_to_delete && $string[$i + 1] == $char_to_delete ? '' : $word .= $string[$i];
        }
        $word[strlen($word) - 1] == $char_to_delete ? $word = substr($word, 0, -1) : '';
        return $word;
    }

    public static function unSanitize(string $var)
    {
        return self::sanitize($var, true);
    }

    static function sanitize(string $var, $reverse = false)
    {
        $sanMethod = [
            ['&amp;', '&'],
            ['&', '&#038;'],
            ['"', '&#034;'],
            ['"', '&quot;'],
            ["'", '&#039;'],
            ['%', '&#037;'],
            ['(', '&#040;'],
            [')', '&#041;'],
            ['+', '&#043;'],
            ['<', '&lt;'],
            ['>', '&gt;'],
            ['\'', '&apos;'],
            ['&', '&amp;'],
            ['«', '&laquo;'],
            ['»', '&raquo;'],
            ['»', '&ndash;'],
            ['“', '&ldquo;'],
            ['”', '&rdquo;'],
            ['—', '&mdash;'],
            ['±', '&plusmn;'],
            ['°', '&deg;'],
            [' ', '&nbsp;'],
        ];
        if (!is_array($var)) {
            $charsCount = count($sanMethod);
            if ($reverse)
                for ($j = $charsCount; $j > 0; $j--) {
                    if (isset($sanMethod[$j][1]))
                        $var = str_replace($sanMethod[$j][1], $sanMethod[$j][0], $var);
                }
            else {
                for ($j = 0; $j < $charsCount; $j++) {
                    if (isset($sanMethod[$j][0]))
                        $var = str_replace($sanMethod[$j][0], $sanMethod[$j][1], $var);
                }
            }
            return $var;
        }
        $varCount = count($var);
        $keys = array_keys($var);
        $i = 0;
        while ($i < $varCount) {
            if (is_array($var[$keys[$i]]))
                return self::sanitize($var[$keys[$i]]);
            else {
                $charsCount = count($sanMethod);
                if ($reverse)
                    for ($j = $charsCount; $j > 0; $j--)
                        $var = str_replace($sanMethod[$j][1], $sanMethod[$j][0], $var);
                else for ($j = 0; $j < $charsCount; $j++)
                    $var[$keys[$i]] = str_replace($sanMethod[$j][0], $sanMethod[$j][1], $var[$keys[$i]]);
            }
            $i++;
        }
        return $var;
    }
    
}
