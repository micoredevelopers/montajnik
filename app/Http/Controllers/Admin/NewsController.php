<?php

namespace App\Http\Controllers\Admin;

use App\Events\Admin\NewsChangedEvent;
use App\Http\Requests\Admin\NewsRequest;
use App\Models\News;
use App\Models\NewsLang;
use App\Repositories\NewsRepository;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class NewsController extends AdminController
{
	use Authorizable;
	use SaveImageTrait;

	private $name = 'Новости';

	protected $key = 'news';

	protected $routeKey = 'admin.news';

	protected $permissionKey = 'news';

	public function __construct()
	{
		parent::__construct();
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
	}

	public function index(NewsRepository $newsRepository)
	{
		$this->setTitle($this->name);
		$vars['list'] = $newsRepository->getListForAdmin();
		$data['content'] = view('admin.news.index', $vars);
		return $this->main($data);
	}

	/**
	 * @return Factory|View
	 */
	public function create()
	{
		$data['content'] = view('admin.news.create');
		return $this->main($data);
	}

	public function store(NewsRequest $request, News $news)
	{
		$input = $request->except('image');
		if ($news->fillExisting($input)->save()) {
			$this->setSuccessStore();
			/** @var $language \App\Models\Language */
			foreach ($this->languagesList as $language) {
				$newsLang = new NewsLang();
				$newsLang->news()->associate($news);
				$newsLang->associateWithLanguage($language);
				$newsLang->fillExisting($input)->save();
			}
			$this->saveImage($request, $news);
			$this->fireEvents();
		}
		if ($request->has('createOpen')) {
			return redirect($this->resourceRoute('edit', $news->id))->with($this->getResponseMessage());
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}


	public function edit($id, NewsRepository $newsRepository)
	{
		$edit = $newsRepository->findForEdit($id);
		if (!$edit) {
			return $this->redirectWhenNotFound();
		}
		$photosList = $edit->images;
		$title = $this->titleEdit($edit);
		$this->addBreadCrumb($title)->setTitle($title);

		$data['content'] = view('admin.news.edit', compact(
			'edit',
			'photosList'
		));
		return $this->main($data);
	}

	public function update(NewsRequest $request, News $news)
	{
		$input = $request->except('image');
		//
		$this->saveImage($request, $news);
		$news->fillExisting($input);
		if ($news->save()) {
			$news->lang($this->langAdmin)->first()->fillExisting($input)->save();
			$this->setSuccessUpdate();
			$this->fireEvents();
		}

		if ($request->hasFile('images')) {
			$this->saveAdditionalImages($news, $request);
		}

		if ($request->has('saveClose')) {
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		return redirect()->back()->with($this->getResponseMessage());
	}

	public function destroy(News $news)
	{
		try {
			if ($news->delete()) {
				$this->photoModel->deleteImageStorage($news->image);
				foreach ($news->images as $image) {
					$this->photoModel->deleteImageStorage($image->image);
				}
				$this->setSuccessDestroy();
				$this->fireEvents();
			}
		} catch (\Exception $e) {
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

	private function fireEvents(): void
	{
		event(new NewsChangedEvent);
	}
}
