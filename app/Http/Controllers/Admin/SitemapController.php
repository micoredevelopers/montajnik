<?php

namespace App\Http\Controllers\Admin;


class SitemapController extends AdminController
{

	protected $routeKey = 'admin.other.sitemap';
	protected $key = 'sitemap';

	public function __construct()
	{
		parent::__construct();
		$this->shareViewModuleData();

		$this->addBreadCrumb('Sitemap', $this->resourceRoute('index'));
		$this->setTitle('Sitemap');
	}


	public function index()
	{
		$data['content'] = view('admin.other.sitemap.index')->with([]);
		return $this->main($data);
	}

	public function store()
	{
		\Artisan::call('sitemap:generate');
		$this->setSuccessStore();
		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}
}
