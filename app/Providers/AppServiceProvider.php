<?php

namespace App\Providers;

use App\Contracts\Admin\AdminMenuRepositoryContract;
use App\Models\Category\Category;
use App\Models\Category\CategoryPage;
use App\Models\FaqItems;
use App\Models\Menu;
use App\Models\News;
use App\Models\Setting;
use App\Models\Slider\SliderItem;
use App\Models\Testimonial;
use App\Models\Translate;
use App\Observers\Admin\CategoryObserver;
use App\Observers\Admin\CategoryPageObserver;
use App\Observers\Admin\MenuObserver;
use App\Observers\FaqItemsObserver;
use App\Observers\NewsObserver;
use App\Observers\SettingObserver;
use App\Observers\Slider\SliderItemObserver;
use App\Observers\TestimonialObserver;
use App\Observers\TranslateObserver;
use App\Observers\UserObserver;
use App\Repositories\Admin\AdminMenuRepository;
use App\User;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		Carbon::setLocale(config('app.locale'));
		Schema::defaultStringLength(191);
		$this->registerDev();
		$this->registerBind();
	}

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->bootObservers();
		$this->bootBlade();
		$this->bootCollection();
		$this->bootRules();
	}

	private function registerDev()
	{
		if ($this->app->environment() !== 'production') {
			$this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
		}
	}

	private function registerBind()
	{
		$this->registerBindAdmin();

	}

	private function registerBindAdmin()
	{
		$this->app->singleton(AdminMenuRepositoryContract::class, AdminMenuRepository::class);
	}

	private function bootObservers()
	{
		Translate::observe(TranslateObserver::class);
		Setting::observe(SettingObserver::class);
		News::observe(NewsObserver::class);
		FaqItems::observe(FaqItemsObserver::class);
		User::observe(UserObserver::class);
		Menu::observe(MenuObserver::class);
		Category::observe(CategoryObserver::class);
		CategoryPage::observe(CategoryPageObserver::class);
		SliderItem::observe(SliderItemObserver::class);
		Testimonial::observe(TestimonialObserver::class);
	}

	private function bootBlade()
	{
		Blade::if('superadmin', function () {
			return isSuperAdmin();
		});
		Blade::if('dev', function () {
			return env('APP_ENV') === 'local';
		});
	}

	private function bootCollection()
	{
		Collection::macro('whereActive', function ($active = 1) {
			return $this->filter(function ($item) use ($active) {
				return (int)\Arr::get($item, 'active') === $active;
			});
		});
		Collection::macro('columns', function ($columns = null) {
			$columns = is_array($columns) ? $columns : func_get_args();
			return $this->map(function ($item) use ($columns) {
				return $item->only($columns);
			});
		});
		Collection::macro('pluckDistinct', function (string $column) {
			return array_unique($this->pluck($column)->toArray());
		});
	}

	private function bootRules()
	{
		\Validator::extend('not_exists', function ($attribute, $value, $parameters) {
			\DB::enableQueryLog();
			$column = \Arr::get($parameters, 1);
			$table = \Arr::get($parameters, 0);
			$columnRecord = \Arr::get($parameters, 2, 0);
			$recordId = \Arr::get($parameters, 3, 0);

			$query = \DB::table($table)
				->where($column, $value);
			if ($columnRecord AND $recordId) {
				$query->where($columnRecord, '<>', $recordId);
			}
			$count = $query->count();
			return $count < 1;
		});
	}
}
