<?php


	namespace App\Calculators\Category\Ceilings;


	interface CeilingsProfileTypesContract
	{
		public const PLASTIC = 'PLASTIC';
		public const ALUMINIUM = 'ALUMINIUM';
	}