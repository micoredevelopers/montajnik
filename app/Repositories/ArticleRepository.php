<?php

namespace App\Repositories;

use App\Models\Article;
use App\Models\Model;
use App\Traits\Repositories\RebuildNextAndPrev;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;

/**
 * Class ArticleRepository.
 */
class ArticleRepository extends AbstractRepository
{
    use RebuildNextAndPrev;
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Article::class;
    }

    public function findForEditById($id)
    {
        return Article::GetLang()->findOrFail($id);
    }


}
