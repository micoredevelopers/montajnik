<div class="menu">
    <div class="menu_container">
        <div class="menu_container_header">
            <form class="menu_container_header_search">
                <button type="submit"><img src="{{ asset('img/search.svg') }}" alt=""></button>
                <input type="text" placeholder="Search">
            </form>
            <div class="menu_container_header_login">
                <img src="{{ asset('img/acc.svg') }}" alt="">
                <a href="">Login</a>
                <span>/</span>
                <a href="">Register</a>
            </div>
        </div>
        <div class="menu_container_item">
            <div class="box">
                <p class="menu_container_item_text">Rent</p>
                <img src="{{ asset('img/arrow_down.svg') }}" alt="" data-menu="#drop1">
            </div>
            <div class="menu_container_item_drodpown" id="drop1">
                <div class="flex_block">
                    <a href="" class="menu_container_item_drodpown_item">About us</a>
                    <a href="" class="menu_container_item_drodpown_item">Personal rentals</a>
                    <a href="" class="menu_container_item_drodpown_item">Personal rentals</a>
                    <a href="" class="menu_container_item_drodpown_item">Buy a car</a>
                </div>
            </div>
        </div>
        <div class="menu_container_item">
            <div class="box">
                <p class="menu_container_item_text">Rent</p>
                <img src="{{ asset('img/arrow_down.svg') }}" alt="" data-menu="#drop2">
            </div>
            <div class="menu_container_item_drodpown" id="drop2">
                <div class="flex_block">
                    <a href="" class="menu_container_item_drodpown_item">About us</a>
                    <a href="" class="menu_container_item_drodpown_item">Personal rentals</a>
                    <a href="" class="menu_container_item_drodpown_item">Personal rentals</a>
                    <a href="" class="menu_container_item_drodpown_item">Buy a car</a>
                </div>
            </div>
        </div>
        <div class="menu_container_item">
            <div class="box">
                <p class="menu_container_item_text">Rent</p>
                <img src="{{ asset('img/arrow_down.svg') }}" alt="" data-menu="#drop3">
            </div>
            <div class="menu_container_item_drodpown" id="drop3">
                <div class="flex_block">
                    <a href="" class="menu_container_item_drodpown_item">About us</a>
                    <a href="" class="menu_container_item_drodpown_item">Personal rentals</a>
                    <a href="" class="menu_container_item_drodpown_item">Personal rentals</a>
                    <a href="" class="menu_container_item_drodpown_item">Buy a car</a>
                </div>
            </div>
        </div>
        <div class="menu_container_item">
            <div class="box">
                <p class="menu_container_item_text">Rent</p>
                <img src="{{ asset('img/arrow_down.svg') }}" alt="" data-menu="#drop4">
            </div>
            <div class="menu_container_item_drodpown" id="drop4">
                <div class="flex_block">
                    <a href="" class="menu_container_item_drodpown_item">About us</a>
                    <a href="" class="menu_container_item_drodpown_item">Personal rentals</a>
                    <a href="" class="menu_container_item_drodpown_item">Personal rentals</a>
                    <a href="" class="menu_container_item_drodpown_item">Buy a car</a>
                </div>
            </div>
        </div>
    </div>
</div>