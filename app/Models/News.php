<?php

namespace App\Models;


use App\Contracts\HasImagesContract;
use App\Contracts\HasLocalized;
use App\Contracts\Models\HasNextPrevAttributes;
use App\Scopes\PublishedAtOrderScope;
use App\Scopes\SortOrderScope;
use App\Scopes\WhereActiveScope;
use App\Scopes\WhereIsPublishedScope;
use App\Traits\Models\HasImages;
use App\Traits\Models\MainOrLangNameAttributeTrait;
use App\Traits\Models\PrevNextAttributes;
use App\Traits\Models\UrlAttributeTrait;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\News
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $published_at
 * @property string $active
 * @property string $url
 * @property string|null $photo
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereDatePub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereUrl($value)
 * @mixin \Eloquent
 * @property string|null $image
 * @property int|null $prev_id Previous news id
 * @property int|null $next_id Next news id
 * @property bool $on_main Display on main page
 * @property-read mixed|string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \App\Models\News $nextNew
 * @property-read \App\Models\News $prevNew
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereNextId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereOnMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News wherePrevId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News wherePublishedAt($value)
 */
class News extends Model implements HasImagesContract, HasNextPrevAttributes, HasLocalized
{
	use HasImages;
	use PrevNextAttributes;
	use UrlAttributeTrait;
    use MainOrLangNameAttributeTrait;

    protected $hasOneLangArguments = [NewsLang::class, 'news_id'];

    protected $table = 'news';

    protected $casts = [
        'published_at' => 'datetime',
        'video'        => 'array',
        'on_main'      => 'boolean',
    ];

    protected $guarded = ['id'];

    public function prevNew(): HasOne
    {
        return $this->hasOne(News::class, 'prev_id')->with('lang');
    }

    public function nextNew(): HasOne
    {
        return $this->hasOne(News::class, 'next_id')->with('lang');
    }

    public function getExcept()
    {
        return $this->lang->except ?? '';
    }

    public function getDescription()
    {
        return $this->lang->description ?? '';
    }

    public function displayOnMain()
    {
        return $this->getAttribute('on_main');
    }

    public static function initScopesPublic()
    {
        if (!self::hasGlobalScope(WhereActiveScope::class)) {
            self::addGlobalScope(new WhereActiveScope());
        }
        if (!self::hasGlobalScope(WhereIsPublishedScope::class)) {
            self::addGlobalScope(new WhereIsPublishedScope());
        }
        if (!self::hasGlobalScope(PublishedAtOrderScope::class)) {
            self::addGlobalScope(new PublishedAtOrderScope());
        }
    }

    public static function boot()
    {
        parent::boot();

    }
}
