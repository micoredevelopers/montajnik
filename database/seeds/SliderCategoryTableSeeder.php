<?php

use App\Repositories\CategoryRepository;
use Illuminate\Database\Seeder;

class SliderCategoryTableSeeder extends Seeder
{
	/** @var \App\Repositories\SliderRepository */
	private $sliderRepository;
	/** @var CategoryRepository */
	private $categoryRepository;

	public function __construct(\App\Repositories\SliderRepository $sliderRepository, CategoryRepository $categoryRepository)
	{
		$this->sliderRepository = $sliderRepository;
		$this->categoryRepository = $categoryRepository;
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$request = new \Illuminate\Http\Request();
		foreach ($this->categoryRepository->getMainCategories() as $category) {
			$slider = new \App\Models\Slider\Slider();
			$this->sliderRepository->save($request, $slider, $category);
			$this->sliderRepository->storeSlideItem($request, $slider);
			$this->sliderRepository->storeSlideItem($request, $slider);
		}
	}
}
