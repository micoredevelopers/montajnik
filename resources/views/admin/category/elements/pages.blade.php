<?php /** @var $edit \App\Models\Category\Category */ ?>
<div role="tabpanel" class="tab-pane" id="pages">
    <div class="panel-body">
        <?php /** @var $categoryPages \Collection */ ?>
        @if(isset($edit) AND ($categoryPages = $edit->categoryPages)->isNotEmpty())
            @foreach ($categoryPages as $page)
                @include('admin.partials.crud.textarea',['name'=>'description['.$loop->index.']','title'=>'Описание ' . $loop->iteration,'value' => $page->lang->description])
            @endforeach
        @else
            @foreach (range(1, 3) as $user)
                @include('admin.partials.crud.textarea',['name'=>'description['.$loop->index.']','title'=>'Описание'])
            @endforeach
        @endif
    </div>
</div>