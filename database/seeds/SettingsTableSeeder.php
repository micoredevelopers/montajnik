<?php

	use App\Builders\Settings\SettingBuilder;
	use Illuminate\Database\Seeder;

	class SettingsTableSeeder extends Seeder
	{
		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
			$groups = [
				'global'   => [
					['key' => 'sitename', 'value' => 'Строительство, ремонт домов и квартир «под ключ» в Одессе', 'display_name' => 'Site name', 'type' => 'text',],
					['key' => 'home-bread-name', 'value' => 'Ремонт квартир Одесса', 'display_name' => 'Название первой хлебной крошки', 'type' => 'text',],
					['key' => 'address', 'value' => 'Одесса, ак. Королёва, 43а', 'display_name' => 'Адрес офиса', 'type' => 'text',],
					['key' => 'work-hours', 'value' => 'пн-сб с 9:00 до 18:00', 'display_name' => 'График работы', 'type' => 'text',],
					['key' => 'currency', 'value' => 'грн.', 'display_name' => 'Валюта', 'type' => 'text',],
				],
				'contacts' => [
					['key' => 'phone-vodafone', 'value' => '(095) 095 76 68', 'display_name' => 'Vodafone', 'type' => 'text',],
					['key' => 'phone-kyivstar', 'value' => '(068) 468 63 86', 'display_name' => 'Kyivstar', 'type' => 'text',],
					['key' => 'phone-intretelecom', 'value' => '(048) 703 74 23', 'display_name' => 'Интертелеком', 'type' => 'text',],
					['key' => 'phone-lifecell', 'value' => '(093) 223 12 37', 'display_name' => 'Lifecell', 'type' => 'text',],
					['key' => 'email', 'value' => 'mmontajnik2017@gmail.com', 'display_name' => 'Email для связи', 'type' => 'text',],
				],
				'about'    => [
					['key' => 'certificates', 'value' => '', 'display_name' => 'Изображения сертификатов на странице "о нас"', 'type' => 'file_multiple',],
				],
				'email'    => [
					['key' => 'email', 'value' => env('DEBUG_EMAIL') ?: 'mmontajnik2017@gmail.com,vladq7@gmail.com', 'display_name' => 'e-mail Администратора', 'type' => 'text',],
					['key' => 'email-from', 'value' => 'info@montajnik.od.ua', 'display_name' => 'E-mail отправителя в письмах', 'type' => 'text',],
					['key' => 'name-from', 'value' => 'info@montajnik.od.ua', 'display_name' => 'Имя отправителя в письме', 'type' => 'text',],
				],
				'social'   => [
					['key' => 'youtube', 'value' => 'https://www.youtube.com/channel/UC9XVXJDkNyaFfJarfc45Eiw', 'display_name' => 'Account youtube', 'type' => 'text',],
					['key' => 'gplus', 'value' => 'https://plus.google.com/communities/109881739014998470887', 'display_name' => 'Google plus', 'type' => 'text',],
					['key' => 'facebook', 'value' => 'https://www.facebook.com/montajnikodua/', 'display_name' => 'Account facebook', 'type' => 'text',],
					['key' => 'viber', 'value' => '', 'display_name' => 'Account Viber', 'type' => 'text',],
				],
				'pages'    => [
					['key' => 'contact_map', 'display_name' => 'Гугл карты для контактов', 'type' => 'text_area', 'value' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2751.789638749078!2d30.722914615841884!3d46.393397178980884!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c634a8868c0bcf%3A0xf37f668f5868f00!2zNDNBLCDQstGD0LvQuNGG0Y8g0JDQutCw0LTQtdC80ZbQutCwINCa0L7RgNC-0LvRjNC-0LLQsCwgNDPQkCwg0J7QtNC10YHQsCwg0J7QtNC10YHRjNC60LAg0L7QsdC70LDRgdGC0YwsIDY1MDAw!5e0!3m2!1suk!2sua!4v1540558095086" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>'],
				],
				'main'     => [
					['key' => 'fallback-news-last', 'value' => '1', 'display_name' => 'Если не выбрана новость для отображения на главной, выводить последнюю свежую новость?', 'type' => 'checkbox',],
					$this->getBuilder()->setKey('cache-enabled')->setType('checkbox')->setValue('1')->setDisplayName('Включить кэш главной страницы?')->build(),
				],
				'seo'      => [
					['key' => 'google-webmaster-id', 'value' => 'JWlkx0dObZ0T4bqU3RKamZO7X7079yu7_lU2pViO4AI', 'display_name' => 'Google webmaster id', 'type' => 'text',],
					['key' => 'google-analytics-head', 'value' => '', 'display_name' => 'Google analytics head', 'type' => 'text_area',],
					['key' => 'google-analytics-body', 'value' => '', 'display_name' => 'Google analytics body', 'type' => 'text_area',],
					['key' => 'head-global-codes', 'value' => $this->getBigText('seo.head-global-codes'), 'display_name' => 'Seo данные в разделе <head> каждой странице ', 'type' => 'text_area'],
					['key' => 'footer-global-codes', 'value' => $this->getBigText('seo.footer-global-codes'), 'display_name' => 'Seo данные в разделе <body> ( footer) каждой странице ', 'type' => 'text_area'],
				],
				'_'        => [
					['key' => 'main-page-slider-id', 'value' => '1', 'display_name' => 'Id слайдера на главной странице', 'type' => 'text',],
				],
				'category' => [
					['key' => 'image.width', 'value' => '500', 'display_name' => 'Ширина маленького изображения', 'type' => 'text',],
					['key' => 'image.height', 'value' => '500', 'display_name' => 'Высота маленького изображения', 'type' => 'text',],
				],
				'news'     => [
					['key' => 'image.width', 'value' => '500', 'display_name' => 'Ширина маленького изображения', 'type' => 'text',],
					['key' => 'image.height', 'value' => '500', 'display_name' => 'Высота маленького изображения', 'type' => 'text',],
				],
			];
			$groups = array_reverse($groups);

			foreach ($groups as $groupName => $settings) {
				foreach ($settings as $setting) {
					$setting['group'] = $groupName;
					$setting['key'] = implode('.', [$groupName, $setting['key']]);
					if (\App\Models\Setting::withTrashed()->where('key', $setting['key'])->first()) {
						continue;
					}
					debugInfo($setting['key']);
					(new \App\Models\Setting())->fillExisting($setting)->save();
				}
			}
		}

		private function getBigText(string $id)
		{
			$arr = [
				'seo.head-global-codes' => '<script type=\'application/ld+json\'>
    {
        "@context": "http://www.schema.org",
        "@type": "Organization",
        "name": "Строительство и ремонт в Одессе - montajnik.od.ua",
        "url": "https://montajnik.od.ua",
        "logo": "https://montajnik.od.ua/files/storage/logo.png",
        "description": "Строительство и ремонт квартир, домов в Одессе под ключ",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "г.Одесса, Академика Королёва, 43а",
            "addressCountry": "Украина"
        },
        "openingHours": "Mo, Tu, We, Th, Fr, Sa 09:00-18:00",
        "contactPoint": [{
            "@type": "ContactPoint",
            "contactType": "customer support",
            "name": "City",
            "telephone": "+38 (048) 703 74 23"
        }, {
            "@type": "ContactPoint",
            "contactType": "customer support",
            "name": "Lifecell",
            "telephone": "+38 (093) 223 12 37"
        }, {
            "@type": "ContactPoint",
            "contactType": "customer support",
            "name": "MTS",
            "telephone": "+38 (095) 095 76 68"
        }, {
            "@type": "ContactPoint",
            "contactType": "customer support",
            "name": "Kyivstar",
            "telephone": "+38 (068) 468 63 86"
        }]
    }
</script>',
			];
			return \Arr::get($arr, $id);
		}

		private function getBuilder(): SettingBuilder
		{
			return new SettingBuilder();
		}
	}
