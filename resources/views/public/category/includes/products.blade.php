<?php /** @var $product \App\Models\Category\Product */ ?>
<?php /** @var $chunkedProducts \Illuminate\Support\Collection */ ?>

@if (($chunkedProducts ?? false) && $chunkedProducts->isNotEmpty())
    <section class="category-advantages">
        <div class="container">
            <div class="text-center">
                <h2 class="title text-center mb-20 mb-lg-5">
                    <span class="decor-bg">{{ getTranslate('category.adwantages-title') }}</span>
                </h2>
            </div>
            <div class="row">
                @foreach($chunkedProducts as $products)
                    @foreach($products as $product)
                        @include('public.category.partials.product-or-category-item',
                         [
                         'model'     => $product,
                         'url'       => route('product.show', $product->url),
                         'isProduct' => true
                         ])
                    @endforeach
                @endforeach
            </div>
        </div>
    </section>
@endif