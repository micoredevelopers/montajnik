<?php

namespace App\Models\Sky;

use App\Helpers\Miscellaneous\Strings;
use App\Models\Model;
use App\Scopes\SortOrderScope;
use Illuminate\Support\Collection;

/**
 * App\Models\Sky\Category
 *
 * @property int $id
 * @property int $old_id
 * @property int|null $sub
 * @property int|null $sort
 * @property string|null $active
 * @property string|null $url
 * @property string $photo
 * @property string $position
 * @property float $rating
 * @property int $votes
 * @property float $min_price
 * @property float $max_price
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Sky\Category[] $categories
 * @property-read int|null $categories_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\AbstractSkyModel onlyMain()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category whereMaxPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category whereMinPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category whereOldId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category whereSub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category whereVotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sky\Category withLang()
 * @mixin \Eloquent
 */
class Category extends AbstractSkyModel
{
    protected $unsanitizeFields = ['description', 'name'];

	protected $table = 'catalog';

	public function __construct()
	{
		parent::__construct();
	}

    public function remake()
    {
        $list = self::all();
        $this->unsanitizeList($list);
    }


    public static function getTree($asArray = true)
	{
		$res = Category::with('allCategories')->WithLang()->OnlyMain()->get();
		$res = self::remakeColumnsNameCategories($res, $asArray);
		return $res;
	}

	private static function remakeColumnsNameCategories(Collection $categories, bool $asArray)
	{
		$categories->map(function (Category $category) use ($asArray) {
			$category->image = $category->photo;
			$category->parent_id = $category->sub;
			$category->description = htmlspecialchars_decode(Strings::unSanitize($category->body));
			$category->category_id = $category->id;


			if ($category->allCategories) {
				self::remakeColumnsNameCategories($category->allCategories, $asArray);
				$category->categories = $category->allCategories->toArray();
			}
			\Arr::forget($category, ['photo', 'sub', 'body', 'allCategories', 'old_id']);
			return $category;
		});
		if ($asArray) {
			$categories = $categories->toArray();
		}
		return $categories;
	}



	public function scopeWithLang($query)
	{
		return $query->leftJoin('ru_catalog', 'catalog.id', 'ru_catalog.catalog_id');
	}

	public function allCategories()
	{
		return $this->categories()->with('allCategories');
	}

	public function categories()
	{
		return $this->hasMany(Category::class, 'sub', 'id')->WithLang();
	}

	public static function boot()
	{
		parent::boot();
		static::addGlobalScope(new SortOrderScope());
	}
}
