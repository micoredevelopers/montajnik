<?php

namespace App\Models;


use App\Contracts\HasLocalized;
use App\Scopes\SortOrderScope;
use App\Scopes\WhereActiveScope;
use App\Traits\Models\ImageAttributeTrait;

/**
 * App\Models\Menu
 *
 * @property int $id
 * @property int $menu_group_id
 * @property int|null $parent_id
 * @property int $active
 * @property string|null $url
 * @property string|null $icon Шрифтовые изображения
 * @property string|null $image обычная картинка, впринципе любого типа
 * @property int $sort
 * @property string|null $options
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $page_id
 * @property-read \App\Models\MenuGroup $menuGroup
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Menu[] $menus
 * @property-read int|null $menus_count
 * @property-read \App\Models\Page|null $page
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereMenuGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereUrl($value)
 * @mixin \Eloquent
 */
class Menu extends Model implements HasLocalized
{
	use ImageAttributeTrait;

    protected $hasOneLangArguments = [MenuLang::class, 'menu_id'];

	protected $table = 'menu';
	protected $guarded = ['id'];

	public function page()
	{
		return $this->belongsTo(Page::class);
	}

	public function menus()
	{
		return $this->hasMany(self::class, 'parent_id', 'id')->GetLang();
	}

	public function allMenus()
	{
		return $this->menus()->with('allMenus');
	}

	public function getMenuName()
	{
		$name = '';
		if ($this->lang AND $this->lang->name) {
			$name = $this->lang->name;
		} else if ($this->page AND $this->page->lang) {
			$name = $this->page->lang->title;
		}
		return $name;
	}

	public function getMenuUrl()
	{
		if ($this->page) {
			return $this->page->getRouteUrl();
		}
		return url($this->url);
	}


	public function menuGroup()
	{
		return $this->belongsTo(MenuGroup::class, 'menu_group_id', 'id');
	}

	public function getUrlAttribute()
	{
		$url = $this->attributes['url'];
		return $url;
	}

	public function getFullUrl()
	{
		$url = $this->getUrlAttribute();
		if (!isStringUrl($url))
			$url = langUrl($url);

		return $url;
	}

	public function isExternal()
	{
		return isStringUrl($this->getUrlAttribute());
	}

	public function getRel()
	{
		return $this->isExternal() ? 'rel="nofollow"' : '';
	}

	public function getTarget()
	{
		return $this->isExternal() ? 'targer="_blank"' : '';
	}

	public static function getForDisplay($onlyActive = true)
	{
		$query = self::GetLang();
		if ($onlyActive) {
			$query->Active();
		}

		return $query->get();
	}

	public static function getForDisplayEdit()
	{
		return self::getForDisplay(false);
	}

	public static function nestable(array $menus, $parent_id = 0)
	{
		/** @var $findMenu Menu */
		foreach ($menus as $num => $menu) {
			if ($findMenu = static::find(\Arr::get($menu, 'id'))) {
				$data = [
					'sort'      => $num,
					'parent_id' => (int)$parent_id,
				];
				$findMenu->fillExisting($data)->save();
				if (isset($menu['children'])) {
					static::nestable($menu['children'], $menu['id']);
				}
			}
		}
	}

	public static function boot()
	{
		parent::boot();

		self::addGlobalScope(new SortOrderScope());
	}

	public static function initScopesPublic()
	{
		if (!self::hasGlobalScope(new WhereActiveScope())) {
			self::addGlobalScope(new WhereActiveScope());
		}
	}
}
