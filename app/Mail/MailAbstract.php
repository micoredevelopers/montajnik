<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailAbstract extends Mailable
{
	use Queueable, SerializesModels;

	public function __construct()
	{
	}

	protected function getEmailFrom(): string
	{
		$default = 'info@' . $this->getHost();
		$emailFrom = getSetting('email.email-from', $default);
		return (string)$emailFrom;
	}

	protected function getNameFrom(): string
	{
		$nameFrom = getSetting('email.name-from', $this->getHost());
		return (string)$nameFrom;
	}

	protected function getEmailTo(): array
	{
		$emails = trim(getSetting('email.email'), ' ');
		$emailTo = explode(',', $emails);
		/** @var  $emailsTo array */
		$emailsTo = array_map('trim', $emailTo);
		return $emailsTo;
	}

	protected function getNameTo(): string
	{
		$nameTo = '';
		return (string)$nameTo;
	}


	protected function getHost()
	{
		return parse_url(env('APP_URL'), PHP_URL_HOST);
	}

}
