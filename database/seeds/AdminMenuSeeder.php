<?php

use App\Contracts\Admin\AdminMenuRepositoryContract;
use App\Events\Admin\MenusChanged;
use App\Models\Admin\Admin_menu;
use Illuminate\Database\Seeder;

class AdminMenuSeeder extends Seeder
{
	private $adminMenuRepository;

	public function __construct(AdminMenuRepositoryContract $adminMenuRepository)
	{
		$this->adminMenuRepository = $adminMenuRepository;
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$menus = [
			[
				'name'      => 'Прочее',
				'url'       => '/admin/other',
				'gate_rule' => 'view_other',
				'icon_font' => '<i class="material-icons">rounded_corner</i>',
			],
			[
				'name'      => 'Настройки',
				'url'       => '/admin/settings',
				'gate_rule' => 'view_settings',
				'icon_font' => '<i class="material-icons">settings</i>',
			],
			[
				'name'      => 'Локализация',
				'url'       => '/admin/translate',
				'gate_rule' => 'view_translate',
				'icon_font' => '<i class="fa fa-language" aria-hidden="true"></i>',
			],
			[
				'name'      => 'Новости',
				'url'       => '/admin/news',
				'gate_rule' => 'view_news',
				'icon_font' => '<i class="fa fa-newspaper-o" aria-hidden="true"></i>',
			],
			[
				'active'    => 0,
				'name'      => 'Статьи',
				'url'       => '/admin/articles',
				'gate_rule' => 'view_articles',
				'icon_font' => '<i class="fa fa-newspaper-o" aria-hidden="true"></i>',
			],
			[
				'name'      => 'Слайдеры',
				'url'       => '/admin/sliders',
				'gate_rule' => 'view_sliders',
				'icon_font' => '<i class="fa fa-area-chart" aria-hidden="true"></i>',
			],
			[
				'name'      => 'Страницы',
				'url'       => '/admin/pages',
				'gate_rule' => 'view_pages',
				'icon_font' => '<i class="fa fa-clipboard" aria-hidden="true"></i>',
			],
			[
				'active'    => 1,
				'name'      => 'Отзывы',
				'url'       => '/admin/testimonials',
				'gate_rule' => 'view_testimonials',
				'icon_font' => '<i class="fa fa-comment-o" aria-hidden="true"></i>',
			],
			[
				'name'      => 'SEO',
				'url'       => '/admin/meta',
				'gate_rule' => 'view_meta',
				'icon_font' => '<i class="fa fa-google-plus" aria-hidden="true"></i>',
				'childrens' => [
					[
						'name'      => 'Перенаправления',
						'url'       => '/admin/redirects',
						'gate_rule' => 'view_redirects',
						'icon_font' => '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
					],
				],
			],
			[
				'name'      => 'Пользователи',
				'url'       => '/admin/users',
				'gate_rule' => 'view_users',
				'icon_font' => '<i class="fa fa-user" aria-hidden="true"></i>',
			],
			[
				'active'    => 0,
				'name'      => 'Роли',
				'url'       => '/admin/roles',
				'gate_rule' => 'view_roles',
				'icon_font' => '<i class="fa fa-users" aria-hidden="true"></i>',
			],
			[
				'active'    => 0,
				'name'      => 'Галерея',
				'url'       => '/admin/galleries',
				'gate_rule' => 'view_galleries',
				'icon_font' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>',
			],
			[
				'name'      => 'Admin Menu',
				'url'       => '/admin/admin-menus',
				'gate_rule' => 'view_admin-menus',
				'icon_font' => '<i class="fa fa-bars" aria-hidden="true"></i>',
			],
			[
				'name'      => 'Меню',
				'url'       => '/admin/menu',
				'gate_rule' => 'view_menu',
				'icon_font' => '<i class="fa fa-bars" aria-hidden="true"></i>',
			],
			[
				'name'      => 'Категории',
				'url'       => '/admin/category',
				'gate_rule' => 'view_categories',
				'icon_font' => '<i class="fa fa-bars" aria-hidden="true"></i>',
				'childrens' => [
					[
						'name'      => 'Доп работы (категория)',
						'url'       => '/admin/prices',
						'gate_rule' => 'view_prices',
						'',
					],
					[
						'name'      => 'Товары',
						'url'       => '/admin/products',
						'gate_rule' => 'view_products',
						'',
					],
					[
						'name'      => 'Партнеры',
						'url'       => '/admin/partners',
						'gate_rule' => 'view_partners',
						'icon_font' => '<i class="fa fa-handshake-o" aria-hidden="true"></i>',
					],
					[
						'name'      => 'Наши работы',
						'url'       => '/admin/works',
						'gate_rule' => 'view_works',
						'icon_font' => '<i class="material-icons">work_outline</i>',
					],
				],
			], [
				'name'      => 'Обратная связь',
				'url'       => '/admin/feedback',
				'gate_rule' => 'view_feedback',
				'icon_font' => '<i class="fa fa-commenting" aria-hidden="true"></i>',
			], [
				'name'      => 'F.A.Q.',
				'url'       => '/admin/faq',
				'gate_rule' => 'view_faq',
				'icon_font' => '<i class="material-icons">forum</i>',
			], [
				'name'      => 'Logs',
				'url'       => '/admin/logs',
				'sort'       => '20',
				'gate_rule' => 'view_logs',
				'icon_font' => '<i class="fa fa-history" aria-hidden="true"></i>',
			],
		];
		$this->loop($menus);
		$this->adminMenuRepository->dropMenuCache();
	}

	/**
	 * @param array $menus
	 * @param Admin_menu|null $parentMenu
	 */
	private function loop(array $menus, Admin_menu $parentMenu = null)
	{
		foreach ($menus as $menu) {
			$pageModel = $this->createMenu($menu, $parentMenu);
			if ($pageModel AND Arr::has($menu, 'childrens')) {
				$this->loop(Arr::get($menu, 'childrens'), $pageModel);
			}
		}
	}

	private function createMenu(array $menu, Admin_menu $parentMenu = null): ?Admin_menu
	{
		if (Admin_menu::findByUrl(Arr::get($menu, 'url'))) {
			return null;
		}
		$menu = array_merge(['active' => 1], $menu);
		$menuModel = app()->make(Admin_menu::class);
		$menuModel->fillExisting($menu);
		if ($parentMenu) {
			$menuModel->setAttribute('parent_id', $parentMenu->getPrimaryValue());
		}
		$menuModel->save();
		return $menuModel;
	}
}
