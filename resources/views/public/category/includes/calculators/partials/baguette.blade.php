<div class="col-12">
    <div class="row">
        <div class="col-lg-6">
            <span class="category-dropdown-title">{{ __('Вставочная резиночка ( багет )') }}</span>
        </div>
        <div class="col-lg-6  mb-30">
            <select class="selectpicker" name="baguette" title="{{ __('Вставочная резиночка ( багет )') }}"
                    data-style="btn-group category-dropdown" data-width="300px" required>
                <option value="0" data-style="dropdown-item" selected>{{ getTranslate('global.no') }}</option>
                <option value="1" data-style="dropdown-item">{{ getTranslate('global.yes') }}</option>
            </select>
        </div>
    </div>
</div>