<?php

use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaq extends Migration
{
    use MigrationCreateFieldTypes;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faqs', function (Blueprint $table) {
            $this->setTable($table);
			$table->increments('id');
			//$table->unsignedInteger('faq_id')->index();
			$table->string('question', 1000)->nullable();
			$table->text('answer')->nullable();
			$table->smallInteger('sort')->default(0);
			$table->tinyInteger('active')->default(1);
			$table->string('url', 255)->nullable();

            $this->createNextPrevFields();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faqs');
    }
}
