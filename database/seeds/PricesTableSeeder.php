<?php

use Illuminate\Database\Seeder;

class PricesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('prices')->delete();
        
        \DB::table('prices')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_id' => 223,
                'created_at' => '2019-10-03 11:00:59',
                'updated_at' => '2019-10-03 11:05:42',
                'name' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'category_id' => 223,
                'created_at' => '2019-10-03 12:25:23',
                'updated_at' => '2019-10-03 12:51:04',
                'name' => 'asd',
            ),
        ));
        
        
    }
}