<?php


namespace App\Traits\Requests\Helpers;


use App\Models\Model;

trait IsAction
{

	public function isActionUpdate()
	{
		$method = $this->getMethod();
		$result = in_array($method, [
			'PUT', 'PATCH',
		]);
		return $result;
	}

	public function isActionStore()
	{
		$method = $this->getMethod();
		$result = in_array($method, [
			'POST',
		]);
		return $result;
	}

	public function isActionDelete()
	{
		$method = $this->getMethod();
		$result = in_array($method, [
			'DELETE',
		]);
		return $result;
	}
}
