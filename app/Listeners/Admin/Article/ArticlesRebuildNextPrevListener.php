<?php

namespace App\Listeners\Admin\Article;

use App\Repositories\ArticleRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ArticlesRebuildNextPrevListener
{

    private $articleRepository;

    /**
     * ArticlesRebuildNextPrevListener constructor.
     * @param ArticleRepository $articleRepository
     */
    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $this->articleRepository->rebuildNextPrev();
    }
}
