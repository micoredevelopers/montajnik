<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTestimonialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('testimonials',function (Blueprint $table){
        	$table->integer('type_id')->unsigned()->index();//todo make default type
        	$table->foreign('type_id')->references('id')->on('testimonial_types')->onDelete('no action');
        	$table->string('video',255)->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
