<?php

function generateGetSetAttributes(array $array)
{
	return (new \App\Helpers\Dev\AttributesSettersGettersCreatorFromArray())->generate($array);
}


function getTestField($field, int $length = 10)
{
	if (!isLocalEnv()) {
		return '';
	}

	static $faker;
	if (null === $faker) {
		$faker = Faker\Factory::create();
	}

	switch ($field) {
		case 'phone':
			return $faker->e164PhoneNumber;
			break;
		case 'name':
			return $faker->firstName();
			break;
		case 'email':
			return $faker->email;
			break;
		case 'message':
		case 'comment':
			return $faker->realText();
			break;
		case 'table_number':
		case 'person':
			return $faker->randomNumber($length);
			break;
	}

	return \Illuminate\Support\Str::random($length);
}
