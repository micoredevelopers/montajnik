<?php

namespace App\Models\Category;

use App\Models\Model;
use App\Traits\Models\BelongsToLanguage;

/**
 * App\Models\Category\CategoryPageLang
 *
 * @property int $id
 * @property int $category_page_id
 * @property string|null $description
 * @property int $language_id
 * @property-read \App\Models\Category\CategoryPage $categoryPage
 * @property-read \App\Models\Language $language
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\CategoryPageLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\CategoryPageLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\CategoryPageLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\CategoryPageLang whereCategoryPageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\CategoryPageLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\CategoryPageLang whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category\CategoryPageLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class CategoryPageLang extends Model
{
    use BelongsToLanguage;

    protected $table = 'category_page_lang';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function categoryPage()
    {
        return $this->belongsTo(CategoryPage::class);
    }
}
