<?php


namespace App\Helpers\Dev\Database;


class DatabaseHelper
{
	private $database;

	public function __construct()
	{
		$this->setDatabase(env('DB_DATABASE'));
	}

	/**
	 * @return string
	 */
	private function getDatabaseName(): string
	{
		return $this->database;
	}

	/**
	 * @param string $database
	 * @return $this
	 */
	public function setDatabase(string $database): self
	{
		$this->database = $database;
		return $this;
	}

	/**
	 * @param $columns
	 * @return array
	 */
	public function getTablesWhichHasColumns($columns): array
	{
		$columnsName = implode(',', \Arr::wrap($columns));
		$sql = "SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME IN (%s) AND TABLE_SCHEMA='%s'";
		$raw = \DB::raw(sprintf($sql, $columnsName, $this->getDatabaseName()));
		$tables = collect(\DB::select($raw));
		$tables = $tables->pluck('TABLE_NAME');
		return $tables;
	}

	//todo добавить метод поиска подстроки по всей базе (опционально можно передать нужные колонки) - возвращает многомерку - ['table' => [ 'id' => $primaryKey, 'column' => $findedColumnName ]]

	public function getPrimaryKeysOfTable($table = null)
	{
		$tables = $this->wrapValues(is_array($table) ? $table : func_get_args());
		$sqlWhereTable = $table ? ' AND tab.table_name ' . $this->getSqlIn($tables) : '';
		$sql = "select tab.table_schema as database_schema,
    sta.index_name as pk_name,
    sta.seq_in_index as column_id,
    sta.column_name,
    tab.table_name
from information_schema.tables as tab
inner join information_schema.statistics as sta
        on sta.table_schema = tab.table_schema
        and sta.table_name = tab.table_name
        and sta.index_name = 'primary'
where tab.table_schema = '%s'
    and tab.table_type = 'BASE TABLE'
" . $sqlWhereTable . '
order by tab.table_name, column_id';

		$sqlFormatted = sprintf($sql, $this->getDatabaseName());

		return \DB::select($sqlFormatted);
	}

	

	public function getSqlIn($values): string
	{
		$values = implode(',', \Arr::wrap($values));
		$sql = ' IN(%s) ';
		return sprintf($sql, $values);
	}

	public function getSqlWhereIn(string $table, $values): string
	{
		$string = $this->getSqlIn($values);
		$sql = sprintf(' WHERE %s', $table);
		return $sql . $string;
	}

	public function getSqlAndWhereIn(string $table, $values): string
	{
		$string = $this->getSqlWhereIn($table, $values);
		return ' AND ' . $string;
	}

	private function wrapValues($values, $char = "'")
	{
		$values = \Arr::wrap($values);
		foreach ($values as &$value) {
			$value = $char . $value . $char;
		}
		return $values;
	}
}
