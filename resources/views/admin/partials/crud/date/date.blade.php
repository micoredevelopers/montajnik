@include('admin.partials.crud.default',
[
    'title' => $title ?? __('form.date.date'),
    'type'  => $type ?? 'datetime-local',
    'name'  => $name ?? 'date',
    'props' => $props ?? 'data-flatpickr-type="datetime_local"'
])
