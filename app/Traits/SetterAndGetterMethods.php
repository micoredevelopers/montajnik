<?php


namespace App\Traits;


trait SetterAndGetterMethods
{
	protected function hasSetter(string $key)
	{
		return method_exists($this, $this->getNameGetter($key));
	}

	protected function hasGetter(string $key)
	{
		return method_exists($this, $this->getNameGetter($key));
	}

	protected function getNameGetter(string $key, $prefix = 'Attribute')
	{
		return 'get' . \Str::studly($key) . $prefix;
	}

	protected function getNameSetter(string $key, $prefix = 'Attribute')
	{
		return 'set' . \Str::studly($key) . $prefix;
	}
}