<?php

namespace App\Http\Controllers\Admin\User	;

use App\Http\Controllers\Admin\AdminController;
use App\Models\User\UserGroup;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class UserGroupController extends AdminController
{
	private $tb;
	private $name;

	public function __construct()
	{
		parent::__construct();
		$this->name = __('modules.user-group.title');
		$this->tb = 'users_groups';

		$this->addBreadCrumb(__('modules.users.title'), route('users.index'));
		$this->addBreadCrumb($this->name, route('admin.user.groups.index'));
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 * @throws \Illuminate\Auth\Access\AuthorizationException
	 */
	public function index()
	{
		$this->authorize('view_user-groups');
		$data['cardTitle'] = $title = $this->name;
		$this->setTitle($title);
		$groups = UserGroup::getAll();
		$data['content'] = view('admin.user.group.index', compact( 'groups'));

		return $this->main($data);
	}

	/**
	 * @param UserGroup $group
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 * @throws \Illuminate\Auth\Access\AuthorizationException
	 */
	public function edit(UserGroup $group)
	{
		$this->authorize('edit_user-groups');
		$edit = $group;
		$data['cardTitle'] = $title = $this->titleEdit($group);
		$this->addBreadCrumb($title)->setTitle($title);
		$data['content'] = view('admin.user.group.edit', compact( 'edit'));
		return $this->main($data);
	}

	/**
	 * @param Request $request
	 * @param UserGroup $group
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws \Illuminate\Auth\Access\AuthorizationException
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function update(Request $request, UserGroup $group)
	{
		$this->authorize('edit_user-groups');
		$this->validate($request, [
			'name'  => 'bail|required|min:2',
		]);
		$this->setMessage(__('generic.successfully_updated'));
		$input = $request->except('_token');
		$input['percent'] = (int)$request->get('percent');
		$group->fillExisting($input)->save();

		if ($request->has('saveClose')) {
			return redirect(route('admin.user.groups.index'))->with($this->getResponseMessage());
		}

		return redirect()->back()->with($this->getResponseMessage());
	}


}
