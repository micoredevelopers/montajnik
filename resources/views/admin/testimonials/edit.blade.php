<?php /** @var $edit \App\Models\Testimonial */ ?>


<form action="{{ route($routeKey . '.update', $edit->id) }}" method="post" class="form-horizontal"
	  enctype="multipart/form-data">
	@csrf
	@method('patch')
	@include('admin.partials.submit_update_buttons')
	<ul class="nav nav-tabs mb-5" role="tablist">
		<li role="presentation">
			<a class="active" href="#dataDefault" aria-controls="home" role="tab" data-toggle="tab">@lang('modules._.tabs.main')</a>
		</li>

		@if($edit->type->type == 'image')
			<li role="presentation"><a href="#photosList" aria-controls="profile" role="tab"
								   data-toggle="tab">@lang('modules._.tabs.photos')</a></li>
		@else
			<li role="presentation"><a href="#video" aria-controls="profile" role="tab"
								   data-toggle="tab">@lang('modules._.tabs.video-youtube')</a></li>
		@endif
	</ul>
	<section class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="dataDefault">
			<div class="panel-body">

				@include('admin.testimonials.partials.form')

			</div>
		</div>
		@if($edit->type->type == 'image')
		<div role="tabpanel" class="tab-pane" id="photosList">
			<div class="panel-body">
				@include('admin.additionalPhotos')
			</div>
		</div>
		@else
		<div role="tabpanel" class="tab-pane" id="video">
			<div class="mb-5">

				@if(!empty($edit->video) )
						<div class="row mt-3 clearfix">
							<div class="col-8 vertical-align-bottom">
								<input class="form-control" type="text" name="video" value="{{ $edit->video }}" placeholder="@lang('modules._.tabs.video-youtube-link')">
							</div>
							<div class="col-2 vertical-align-bottom">
								<a href="{{$edit->video}}" class="fancy" data-fancybox="cats_video">
									<img src="{{ getPreviewYoutubeFromLink($edit->video) }}" alt="" class="img-fluid" width="85">
								</a>
							</div>
						</div>
				@else
					<div class="form-group row">
						<div class="col-10">
							<input class="form-control" type="text" name="video" placeholder="@lang('modules._.tabs.video-youtube-link')">
						</div>
					</div>
				@endif
			</div>
		</div>
		@endif


	</section>
	@include('admin.partials.submit_update_buttons')
</form>
