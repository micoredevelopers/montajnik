<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
	private $actions = [
		'view',
		'add',
		'edit',
		'delete',
	];

	public function run()
	{
// Reset cached roles and permissions
		app(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();

// create permissions

		$permissions = $this->getExistsPermissions();
		foreach ($permissions as $permission) {
			(new Permission(['name' => $permission]))->save();
		}

// create roles and assign created permissions
		$roleWriter = Role::create(['name' => 'writer']);
		$roleWriter->givePermissionTo('edit_articles', 'view_articles');

// or may be done by chaining
		$roleModerator = Role::create(['name' => 'moderator'])->givePermissionTo($this->getRolesModerator());
		if (\App\User::MODERATOR_USER_ID AND $moderator = \App\User::find(\App\User::MODERATOR_USER_ID)) {
			$moderator->assignRole($roleModerator);
		}

		$role = Role::create(['name' => 'admin'])->givePermissionTo(Permission::all());
		if (\App\User::SUPER_ADMIN_ID) {
			\App\User::find(\App\User::SUPER_ADMIN_ID)->assignRole($role);
		}
	}

	/**
	 * @return array
	 */
	private function getRolesModerator()
	{
		$permissions = [];
		$permissions = array_merge($permissions, $this->_getPermissionModify('other'));
		$permissions = array_merge($permissions, $this->_getPermissionModify('settings'));
		$permissions = array_merge($permissions, $this->_getPermissionModify('translate'));
		//
		$permissions = array_merge($permissions, $this->_getPermissionCrud('users', 'delete'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('menu', 'delete'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('testimonials', ['add']));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('feedback', ['add', 'edit']));
		//
//		$permissions = array_merge($permissions, $this->_getPermissionCrud('articles'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('comments'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('categories'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('faq'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('meta'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('partners'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('products'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('prices'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('redirect'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('sliders'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('news'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('works'));
		//
		$permissions = array_merge($permissions, $this->_getPermission('index', 'view'));

		return $permissions;
	}

	private function getExistsPermissions()
	{
		$permissions = [];
		$permissions[] = 'view_index';
		$create = [
			'comments',
			'articles',
			'users',
			'roles',
			'settings',
			'sliders',
			'news',
			'menu',
			'galleries',
			'meta',
			'infoblocks',
			'translate',
			'faq',
			'pages',
			'testimonials',
			'feedback',
			'prices',
			'products',
			'partners',
			'redirect',
			'categories',
			'works',
			'other',
		];

		foreach ($create as $entity) {
			foreach ($this->_getPermissionCrud($entity) as $perm) {
				$permissions[] = $perm;
			}
		}

		return $permissions;
	}

	private function _getPermission($entity, $permissions)
	{
		$res = [];
		foreach ((array)$permissions as $permission) {
			$res[] = $permission . '_' . $entity;
		}
		return $res;
	}

	private function _getPermissionCrud($entity, $except = [])
	{

		$perms = $this->actions;
		if ($except = (array)$except) {
			$perms = array_diff($perms, $except);
		}
		return $this->_getPermission($entity, $perms);
	}

	private function _getPermissionModify($entity)
	{
		$perms = [
			'view',
			'edit',
		];
		return $this->_getPermission($entity, $perms);
	}
}
