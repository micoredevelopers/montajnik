<?php

	namespace App\Http\Requests\Admin;

	use App\Contracts\Requests\RequestParameterModelable;
	use App\Helpers\Validation\ValidationMaxLengthHelper;
	use App\Http\Requests\AbstractRequest;
	use App\Models\News;
	use App\Traits\Requests\Helpers\GetActionModel;

	class NewsRequest extends AbstractRequest implements RequestParameterModelable
	{
		use GetActionModel;

		protected $toBooleans = ['active', 'on_main'];

		protected $requestKey = 'news';

		public function rules()
		{
			$rules = [
				'name'         => ['required', 'max:255'],
				'url'          => ['required', 'unique' => 'unique:news,url'],
				'published_at' => ['required', 'date'],
				'except'       => ['max:' . ValidationMaxLengthHelper::TEXT],
				'description'  => ['max:' . ValidationMaxLengthHelper::TEXT],
			];

			if ($this->isActionUpdate() AND $news = $this->getActionModel()) {
				/** @var  $news News */
				$rules['url']['unique'] = 'unique:news,url,' . $news->id;
			}

			return $rules;
		}


		protected function mergeRequestValues()
		{
			$this->mergeUrlFromName();
			$this->merge([
				'published_at' => checkDateCarbon($this->get('published_at')),
				'video'        => $this->get('video'),
			]);
		}
	}
