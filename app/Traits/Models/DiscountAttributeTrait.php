<?php

namespace App\Traits\Models;

trait DiscountAttributeTrait
{
	public function setDiscountAttribute($discount)
	{
		$column = 'discount';
		$discount = (float)$discount;
		$discount = (int)($discount * 100);
		$this->attributes[$column] = $discount;

		return $this;
	}

	public function getDiscountAttribute()
	{
		$column = 'discount';
		$price = (float)(\Arr::get($this->attributes, $column) / 100);

		return $price;
	}
}