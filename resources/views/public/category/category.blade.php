<?php /** @var $category \App\Models\Category\Category */ ?>

@extends('public.category.category-layout')

@section('beforeSubcategories')
    @include('public.category.partials.beforeSubcategories')
@stop

@section('subcategories')
    @if ($category->isCategoryWithoutChild())
        @include('public.category.includes.products')
    @else
        @include('public.category.includes.subcategories')
        @include('public.category.includes.subcategories-without-child')
    @endif
@stop
