<?php

namespace App\Http\Controllers;

use App\Models\Category\Product;
use App\Repositories\CategoryRepository;
use App\Traits\Controllers\Category\BreadcrumbsTree;

class ProductController extends SiteController
{
    use BreadcrumbsTree;
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        parent::__construct();
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $this->dropLastBreadCrumb();
        $data = [];
        $this->setTitle(getSetting('global.sitename'));

        $data['content'] = view('public.contacts.index');
        return $this->main($data);
    }

    public function show($url)
    {
        /** @var  $product Product*/
        $product = Product::with('category.lang', 'lang')->whereUrl($url)->first();
        if ($product->category) {
            $product->category = $this->categoryRepository->findByUrl($product->category->url);
            $this->addBreadCrumbsTree($product->category, $this->categoryRepository);
        }
        $category = $product->category;
        $this->addBreadCrumb($product->getNameAttribute());
        $data['content'] = view('public.product.product')->with(compact('product', 'category'));
        return $this->main($data);
    }
}
