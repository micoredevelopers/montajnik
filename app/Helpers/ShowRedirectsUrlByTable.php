<?php


namespace App\Helpers;


use Illuminate\Support\Arr;

class ShowRedirectsUrlByTable
{
    private $tableArray = [
        'categories'=>'category.show',
        'news' =>'news.show',
        'articles'=>'',
        'faqs'=>'',
        'galleries'=>'',
        'menu'=>'',
        'meta'=>'',
        'pages'=>'page.show',
        'products'=>'products.show',
        'works'=>'works.show',
    ];

    public function getRoute(string $table)
    {
        return Arr::get($this->tableArray,$table);
    }

    public function getUrlByTable(string $table, string $url):string
    {
        try
        {
            return route($this->getRoute($table),$url);
        }
        catch (\Exception $e)
        {
            return '';
        }
    }
}
