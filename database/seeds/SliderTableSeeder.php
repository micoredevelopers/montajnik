<?php

use App\Repositories\CategoryRepository;
use Illuminate\Database\Seeder;

class SliderTableSeeder extends Seeder
{
	/** @var \App\Repositories\SliderRepository */
	private $sliderRepository;

	public function __construct(\App\Repositories\SliderRepository $sliderRepository)
	{
		$this->sliderRepository = $sliderRepository;
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$slider = new \App\Models\Slider\Slider();
		(new \App\Models\Slider\Slider())->fillExisting(['key' => 'main-page', 'comment' => 'Слайдер на главной странице'])->save();
		$request = new \Illuminate\Http\Request();
		$this->sliderRepository->save($request, $slider);
	}
}
