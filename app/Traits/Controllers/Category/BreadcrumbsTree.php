<?php


namespace App\Traits\Controllers\Category;


use App\Models\Category\Category;
use App\Repositories\CategoryRepository;

trait BreadcrumbsTree
{
    private function addBreadCrumbsTree(Category $category, CategoryRepository $categoryRepository)
    {
        $categories = $categoryRepository->getCategoryParents($category);
        /** @var  $categoryParent Category*/
        foreach ($categories as $categoryParent) {
            $this->addBreadCrumb($categoryParent->getNameAttribute(), route('category.show', $categoryParent->getUrlAttribute()));
        }
        $this->addBreadCrumb($category->getNameAttribute(), route('category.show', $category->getUrlAttribute()));
    }

}