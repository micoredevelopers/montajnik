<?php

namespace App\Models;

use App\Contracts\HasLocalized;
use App\Traits\Singleton;
use Illuminate\Support\Arr;


/**
 * App\Models\Meta
 *
 * @property int $id
 * @property string $url
 * @property int $type
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $header html/js etc code for header
 * @property string|null $footer html/js etc code for footer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereFooter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Meta whereUrl($value)
 * @mixin \Eloquent
 */
class Meta extends Model implements HasLocalized
{
    use Singleton;

    protected $hasOneLangArguments = [MetaLang::class];

    protected static $results = [];

    protected $table = 'meta';

    protected $guarded = ['id'];

    public static function getMetaData($url = null, $fromCache = true)
    {
        if ($url === null) {
            $url = getUrlWithoutHost(getNonLocaledUrl());
        }
        if (!Arr::has(static::$results, $url) OR !$fromCache) {
            $meta = static::WhereUrl($url)->Active(1)->GetLang(getLang())->first();
            Arr::set(static::$results, $url, $meta);
        }

        return Arr::get(static::$results, $url);
    }

    public function isDefault()
    {
        return $this->url === '*';
    }

	public static function makeUrlClear(string $url)
	{
		return getUrlWithoutHost(getNonLocaledUrl($url));
	}
}
