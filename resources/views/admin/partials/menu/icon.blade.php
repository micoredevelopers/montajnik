<?php /** @var $item \App\Models\Admin\Admin_menu */ ?>
@if (Arr::get($item, 'icon_font'))
    <span class="icon-left">{!! Arr::get($item, 'icon_font') !!}</span>
@elseif ($image = Arr::get($item, 'image') AND storageFileExists($image))
    <img src="{{ checkImage($image) }}" class="img-fluid pull-left mr-3" width="30" alt="">
@endif