<?php

use App\Traits\Migrations\CategoryForeignKey;
use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTestimonialsTable extends Migration
{
    use CategoryForeignKey;
    use MigrationCreateFieldTypes;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('testimonials',function (Blueprint $table){
            $this->setTable($table);
            $this->addForeignCategory();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('testimonials',function (Blueprint $table){
			$table->dropColumn('category_id');
		});
    }
}
