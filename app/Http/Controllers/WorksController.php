<?php

namespace App\Http\Controllers;

use App\Models\Category\Category;
use App\Models\Work;
use App\Repositories\CategoryRepository;
use App\Repositories\WorkRepository;
use Illuminate\Http\Request;

class WorksController extends SiteController {
    private $workRepository;

    public function __construct(WorkRepository $workRepository) {
        parent::__construct();
        $this->workRepository = $workRepository;
    }

    public function index() {
        return $this->show('');
    }


    public function show($url) {
        /** @var  $work Work */
        $work = $this->workRepository->findByUrl($url);
        $works = $this->workRepository->getListForDisplayWithoutCurrent($work);
        if (!$work) {
            $work = $works->first();
            $redirect = $work ? route('works.show', $work->url) : route('home');
            return redirect($redirect)->with($this->getResponseMessage());
        }
        $vars = [
            'work'  => $work,
            'works' => $works,
        ];
        $vars['currentCategory'] = $currentCategory = $work->getCategory();

        $name = $currentCategory->getCategoryName();
        $title = str_replace('%name%', $name, getTranslate('work.meta_title'));
        $description = str_replace('%name%', $name, getTranslate('work.meta_description'));

        $this->setTitle($title);
        $this->setDescription($description);
        $this->addBreadCrumb(getTranslate('work.works'));

        $vars['categories'] = $this->categories->where('id', '!=', ($currentCategory->id ?? 0));
        $data['content'] = view('public.works.index', $vars);
        return $this->main($data);
    }
}
