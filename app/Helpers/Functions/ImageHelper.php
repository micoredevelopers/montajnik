<?php

use Illuminate\Support\Facades\Storage;
use WebPConvert\WebPConvert;

function GetPathToPhoto(string $pathToImage, string $defaultPath, $storage = 'public') {
    return Storage::disk($storage)->exists($pathToImage) ? Storage::disk($storage)->url($pathToImage) : $defaultPath;
}

function FileExists(string $pathToImage, string $storage = 'public') {
    return Storage::disk($storage)->exists($pathToImage);
}

function imgPathOriginal($pathToImage) {
    return \Str::replaceLast('_s.', '.', (string)$pathToImage);
}

function imgPathThumbnail($pathToImage) {
    return \Str::replaceLast('.', '_s.', (string)$pathToImage);
}

function GetImageAdmin($pathToImage, string $defaultPath, $storage = 'public') {
    return "<img width=150 src='" . GetPathToPhoto((string)$pathToImage, $defaultPath, $storage) . "' alt=''>";
}

if (!function_exists('checkImage')) {
    function checkImage($src, $default = 'images/default.svg', $disk = null) {
        if (storageFileExists($src, $disk)) {
            return getStorageFilePath($src, $disk);
        }
        return asset($default);
        //return storageFileExists($src, $disk) ? getStorageFilePath($src, $disk) : asset($default);
    }
}

if (!function_exists('assetWebp')) {
    function assetWebp($path, $secure = null) {
        $pathInfo = pathinfo($path);

        if (!isset($_SERVER['HTTP_ACCEPT']) || strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') == false || strtolower($pathInfo['extension']) == 'webp') {
            return app('url')->asset($path, $secure);
        }

        $cachedPath = sprintf('cache/%s/%s.webp',
            $pathInfo['dirname'],
            $pathInfo['filename']
        );

        $originalFile = public_path($path);
        $cachedFile = public_path($cachedPath);

        if (!file_exists($cachedFile)) {
            WebPConvert::convert($originalFile, $cachedFile, [
                'png'  => [
                    'encoding'      => 'auto',
                    'near-lossless' => 60,
                    'quality'       => 85,
                    'sharp-yuv'     => true,
                ],
                'jpeg' => [
                    'encoding'   => 'auto',
                    'quality'    => 75,
                    'auto-limit' => true,
                    'sharp-yuv'  => true,
                ],
            ]);
        }

        return app('url')->asset($cachedPath, $secure);;
    }
}

if (!function_exists('checkImageWebp')) {
    function checkImageWebp($src, $default = 'images/default.svg', $disk = null) {
        if (!storageFileExists($src, $disk)) {
            return asset($default);
        }

        $pathInfo = pathinfo($src);

        if (!isset($_SERVER['HTTP_ACCEPT']) || strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') == false || strtolower($pathInfo['extension']) == 'webp') {
            return checkImage($src, $default, $disk);
        }

        $cachedPath = sprintf('cache/%s/%s.webp',
            $pathInfo['dirname'],
            $pathInfo['filename']
        );

        $originalFile = Storage::disk($disk)->path($src);
        $cachedFile = Storage::disk($disk)->path($cachedPath);

        if (!file_exists($cachedFile)) {
            WebPConvert::convert($originalFile, $cachedFile, [
                'png'  => [
                    'encoding'      => 'auto',
                    'near-lossless' => 60,
                    'quality'       => 85,
                    'sharp-yuv'     => true,
                ],
                'jpeg' => [
                    'encoding'   => 'auto',
                    'quality'    => 75,
                    'auto-limit' => true,
                    'sharp-yuv'  => true,
                ],
            ]);
        }

        return Storage::disk($disk)->url($cachedPath);
    }
}