<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class RemovePublicFromUrl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (true !== ($check = $this->checkPublicInUrl($request))) {
			return $check;
		}

        return $next($request);
    }


	private function checkPublicInUrl(Request $request)
	{
		$host = $request->getHost();
		$needle = $host . '/public';
		if (!Str::contains($request->getUri(), $needle)) {
			return true;
		}
		$redirectTo = env('APP_URL') . getUrlWithoutHost($request->getUri());
		return redirect($redirectTo, Response::HTTP_MOVED_PERMANENTLY);
	}
}
