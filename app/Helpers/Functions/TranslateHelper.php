<?php

if (!function_exists('getTranslate')) {
	function getTranslate($key, $defaultText = '', $asObject = false)
	{
		return \App\Models\Translate::getTranslate($key, $asObject) ?? $defaultText ?: $key;
	}
}
if (!function_exists('translateFormat')) {
	function translateFormat($key, array $values)
	{
		/** @var $translate \App\Models\Translate */
		$translate = getTranslate($key, false, true);
		if (is_object($translate)) {
			return $translate->format($values);
		}
		return '';
	}
}
if (!function_exists('translateYesNo')) {
	function translateYesNo($condition)
	{
		return $condition ? getTranslate('global.yes') : getTranslate('global.no');
	}
}