<?php

namespace App\Models\Admin;


use App\Models\Model;
use App\Scopes\SortOrderScope;
use App\Traits\Models\ImageAttributeTrait;
use App\Traits\Models\NameAttributeTrait;

/**
 * App\Models\Admin\Admin_menu
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int $active
 * @property string|null $name
 * @property string|null $url
 * @property string|null $gate_rule
 * @property string|null $route
 * @property string|null $icon
 * @property string|null $icon_font
 * @property string|null $content_provider
 * @property string|null $option
 * @property string|null $description
 * @property int|null $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereContentProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereGateRule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereIconFont($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereUrl($value)
 * @mixin \Eloquent
 * @property string|null $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Admin\Admin_menu[] $childrens
 * @property-read int|null $childrens_count
 * @property-read \App\Models\Admin\Admin_menu|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin_menu whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 */
class Admin_menu extends Model
{
	use NameAttributeTrait;
	use ImageAttributeTrait;

	protected $guarded = [
		'id',
	];

	public function parent(): \Illuminate\Database\Eloquent\Relations\BelongsTo
	{
		return $this->belongsTo(__CLASS__, 'parent_id', 'id');
	}

	public function childrens(): \Illuminate\Database\Eloquent\Relations\HasMany
	{
		return $this->hasMany(__CLASS__, 'parent_id', 'id');
	}

	public static function boot(): void
	{
		parent::boot();

		self::addGlobalScope(new SortOrderScope());
	}
}
