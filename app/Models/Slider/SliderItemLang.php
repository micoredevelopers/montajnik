<?php


namespace App\Models\Slider;


use App\Contracts\HasLocalized;
use App\Models\Model;
use App\Traits\EloquentMultipleForeignKeyUpdate;
use App\Traits\Models\BelongsToLanguage;
use App\Traits\Models\NameAttributeTrait;

/**
 * App\Models\Slider\SliderItemLang
 *
 * @property int $slider_item_id
 * @property int $language_id
 * @property mixed $name
 * @property string|null $description
 * @property string|null $except
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Slider\SliderItem $sliderItem
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItemLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItemLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItemLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItemLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItemLang whereExcept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItemLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItemLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItemLang whereSliderItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class SliderItemLang extends Model implements HasLocalized
{
	use BelongsToLanguage;

	use NameAttributeTrait;

	use EloquentMultipleForeignKeyUpdate;

	public $table = 'slider_item_lang';

	protected $guarded = [];

	protected $casts = [
	];

	public $incrementing = false;

	public $timestamps = false;

	protected $primaryKey = ['slider_item_id', 'language_id'];

	public function sliderItem()
	{
		return $this->belongsTo(SliderItem::class);
	}


}