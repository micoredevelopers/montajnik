<?php


namespace App\Modules\Transfer;

use App\Models\Category\Category;
use App\Models\Slider\Slider;
use App\Repositories\SliderRepository;
use Illuminate\Http\Request;

class SliderTransfer
{

	private $sliderRepository;

	private $slideEmpty;

	public function __construct(
		SliderRepository $sliderRepository,
		Slider $slider
	)
	{
		$this->sliderRepository = $sliderRepository;
		$this->slideEmpty = $slider;
	}

	/**
	 * @param Category $category
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
	 */
	public function getForm(Category $category)
	{
		$slider = $this->sliderRepository->findByCategory($category);
		if (!$slider) {
			return '';
		}
		return view('admin.sliders.transfer.data')->with(['edit' => $slider]);
	}

	public function saveData(Request $request, Category $category)
	{
		$slider = $this->sliderRepository->findByCategory($category);
		if (!$slider){
			$slider = new Slider();
		}
		$this->sliderRepository->save($request, $slider, $category);
	}
}