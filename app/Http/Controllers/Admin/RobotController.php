<?php

namespace App\Http\Controllers\Admin;

use App\ContentTypes\File;
use App\Traits\Authorizable;
use Illuminate\Http\Request;


class RobotController extends OtherController
{
	use Authorizable;
	protected $tb;
	private $name = 'robots.txt';
	private $controller = 'robots';

	protected $key = 'robots';
	protected $routeKey = 'admin.other.robots';
	private $robotsPath;

	public function __construct()
	{
		parent::__construct();
		$this->tb = 'other.robots';
		$this->robotsPath = public_path('robots.txt');
		$this->breadCrumbIndex();
		$this->shareViewModuleData();
	}

	protected function breadCrumbIndex(): void
	{
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
	}

	public function index()
	{
		$vars = [];
		if (!file_exists($this->robotsPath)) {
			$this->writeRobots('User-agent: *' . PHP_EOL . 'Disallow: /');
		}

		$vars['data'] = (string)file_get_contents($this->robotsPath);

		$this->setTitle($this->name);
		$data['content'] = view('admin.other.robots.index', $vars);
		return $this->main($data);
	}

	public function update(Request $request)
	{
		$robots = (string)$request->get('robots');

		$this->writeRobots($robots);

		$this->setMessage('robots.txt успешно изменен!')->setStatus(true);

		if ($request->has('saveClose')) {
			return redirect(route('other.index'))->with($this->getResponseMessage());
		}

		return redirect()->back()->with($this->getResponseMessage());
	}

	private function writeRobots(string $string)
	{
		file_put_contents($this->robotsPath, $string);
	}
}
