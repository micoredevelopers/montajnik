<form action="" method="get">
    <div class="form-group">
        <div class="row">
            <div class="col-6">
                <input type="text" class="form-control mt-3" name="search" placeholder="Текст поиска (название, описание)" value="{{ request('search') }}" autocomplete="off">
            </div>
            <div class="col-2">
                <button class="btn btn-danger" type="submit"><i class="fa fa-search" aria-hidden="true"></i>
                    @lang('form.search')
                </button>
            </div>
        </div>
    </div>
</form>

<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th>ID</th>
            <th class="th-description">@lang('form.title')</th>
            <th class="text-right">
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>
                    {{ $item->name }}
                </td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>