<?php

namespace App\Traits\Models;


trait IsDailyTrait
{
	public function isDaily()
	{
		$column = 'daily';
		$attribute = \Arr::get($this->attributes, $column);
		return $attribute;
	}


}
