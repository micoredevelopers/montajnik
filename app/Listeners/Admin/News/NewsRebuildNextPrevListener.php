<?php

namespace App\Listeners\Admin\News;

use App\Repositories\NewsRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewsRebuildNextPrevListener
{
    private $newsRepository;

    /**
     * NewsRebuildNextPrevListener constructor.
     * @param NewsRepository $newsRepository
     */
    public function __construct(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $this->newsRepository->rebuildNextPrev();
    }
}
